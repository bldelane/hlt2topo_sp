import sys
sys.path.append("/home/nschulte/.local/lib/python3.11/site-packages")

import numpy as np
import uproot
import pandas as pd
import matplotlib.pyplot as plt
import mplhep as hep
import pathlib 
import matplotlib.pyplot as plt
import numpy as np
import os

hep.style.use("LHCb2")
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")

file_c1 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c1_selected.root"
file_c2 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c2_selected.root"
file_c3 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c3_selected.root"
file_c4 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c4_selected.root"

topo_lines = ["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"]

def get_df(file):
    """
    Parameters:
    file: str
        Path to the ROOT file containing the data
    get_lines: array
        Array containing the list of HLT2 lines used for TIS and TOS efficiencies 
        TODO: make this more general for more decays
    """
    with uproot.open(file) as root_file:
        tree = root_file["JPsiK/DecayTree"]
        arrays = tree.arrays(library="np")
        # Convert to DataFrame in one go
        df = pd.DataFrame(arrays)

    return df

def plot_topo_decision(data, variable, topo_lines, line_label, xlabel, outputpath, decay_descriptor, bins=50, alpha=0.5, ran=(0, 30_000)):
    """
    Plot the distribution of a variable with overlays showing Topo decision for multiple lines.
    
    Parameters:
    data : pd.DataFrame
        The data containing the event information
    variable : str
        The variable to plot
    topo_lines : list
        A list of the Topo line column names to use for filtering
    xlabel : str
        The label for the x-axis
    outputpath : str
        The directory to save the plot
    decay_descriptor : str
        The decay description used for labeling
    bins : int, optional
        Number of bins for the histogram, default is 50
    alpha : float, optional
        Transparency level of the overlays, default is 0.5
    """
    # Filter out NaN/Inf values for the variable
    valid_data = data[variable][~data[variable].isin([np.nan, np.inf, -np.inf])]

    # Create the plot
    plt.figure(figsize=(12, 10))

    # Calculate histogram data for overall distribution
    counts, bin_edges = np.histogram(valid_data, bins=bins, range=ran)
    bin_widths = np.diff(bin_edges)  # width of each bin

    # Normalize overall distribution to maximum of 1
    overall_max = np.max(counts)
    overall_normalized = counts / overall_max  # Normalize so max value is 1

    # Plot the overall distribution using bar plot (normalized to max of 1)
    plt.bar(bin_edges[:-1], overall_normalized, width=bin_widths, alpha=0.6, color='black', label=f"Overall")

    # Initialize lists for accepted and rejected data (all lines combined)
    accepted_data_combined = []
    rejected_data_combined = []

    # Loop through each Topo line and filter the data
    for line in topo_lines:
        # Topo decision positive (accepted)
        topo_pos_data = data[data[f"BP_{line}Decision_TOS"] == True]
        valid_pos_data = topo_pos_data[variable][~topo_pos_data[variable].isin([np.nan, np.inf, -np.inf])]
        accepted_data_combined.append(valid_pos_data)

        # Topo decision negative (rejected)
        topo_neg_data = data[data[f"BP_{line}Decision_TOS"] == False]
        valid_neg_data = topo_neg_data[variable][~topo_neg_data[variable].isin([np.nan, np.inf, -np.inf])]
        rejected_data_combined.append(valid_neg_data)

    # Combine all accepted and rejected data from all lines
    accepted_data_combined = np.concatenate(accepted_data_combined)
    rejected_data_combined = np.concatenate(rejected_data_combined)

    # Plot accepted distribution
    accepted_counts, _ = np.histogram(accepted_data_combined, bins=bin_edges, range=ran)
    accepted_max = np.max(accepted_counts)
    accepted_normalized = accepted_counts / accepted_max  # Normalize to max value
    plt.bar(bin_edges[:-1], accepted_normalized, width=bin_widths, alpha=alpha, color='dodgerblue', label=f"Accepted {line_label}")

    # Plot rejected distribution
    rejected_counts, _ = np.histogram(rejected_data_combined, bins=bin_edges, range=ran)
    rejected_max = np.max(rejected_counts)
    rejected_normalized = rejected_counts / rejected_max  # Normalize to max value
    plt.bar(bin_edges[:-1], rejected_normalized, width=bin_widths, alpha=alpha, color='deeppink', linestyle='--', label=f"Rejected {line_label}")

    # Set labels and limits
    plt.xlabel(xlabel)
    plt.ylabel('Normalized Events')
    plt.xlim(ran)
    plt.ylim(0, 1)  # Fix the y-axis to range from 0 to 1

    # Label and save plot
    hep.lhcb.label(loc=0, rlabel=f"{decay_descriptor}", data=True, label="Preliminary")
    plt.legend(loc='best', prop={'size': 19})

    # Save the plot to the output directory
    outpath = f"{outputpath}/{variable}/"
    os.makedirs(outpath, exist_ok=True)
    filename = f"TopoDecision_{line_label.replace(' ', '_')}_{variable}.pdf"
    plt.savefig(os.path.join(outpath, filename))
    print(f"Plot saved to {os.path.join(outpath, filename)}")

# Define configurations
configurations = [
    (["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"], "Any Topological", "k"),
    (["Hlt2Topo3Body"], "3Body Beauty Topo", "#377eb8"),
    (["Hlt2TopoMu3Body"], "3Body MuonTopo", "#e41a1c"),
    (["Hlt2Topo2Body"], "2body Beauty Topo", "#984ea3"),
    (["Hlt2TopoMu2Body"], "2body Muon Topo", "#f781bf"),
]

# Assuming you have your data in pandas DataFrames (df_c1, df_c2, etc.)
outputpath = "plots/no_sidebandsubs_topo_rejected"
decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$"
topo_lines = ["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"]
line_label = "Any Topological"

# Call the function to plot
plot_topo_decision(get_df(file_c4), 'BP_DTFPV_PT', topo_lines, line_label, r"$p_T\left(B^+\right)$ / $\mathrm{MeV}c^{-1}$", outputpath, decay_descriptor, bins=100, alpha=0.5)
