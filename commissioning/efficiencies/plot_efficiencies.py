import sys
sys.path.append("/home/nschulte/.local/lib/python3.11/site-packages")

import numpy as np
import uproot
import pandas as pd
import matplotlib.pyplot as plt
import mplhep as hep
import triggercalib
from triggercalib import HltEff
import ROOT as R
import pathlib 

hep.style.use("LHCb2")
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from src.efficiencies_triggercalib import get_df_and_hlt_eff, TOSEfficiencyPlotter
from efficiency_variable_conf import hlt_eff_config

file_c1_blockY = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c1_blockY_selected.root"
file_c1_blockX = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c1_blockX_selected.root"
file_c2_block1 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c2_block1_selected.root"
file_c2_block2 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c2_block2_selected.root"
file_c2_block3 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c2_block3_selected.root"
file_c2_block4 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c2_block4_selected.root"
file_c3_block5 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c3_block5_selected.root"
file_c3_block6 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c3_block6_selected.root"
file_c4_block7 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c4_block7_selected.root"
file_c4_block8 = f"/ceph/users/nschulte/TopoCommissioning/selected/s24c4_block8_selected.root"

file_c1 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c1_selected.root"
file_c4 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c4_selected.root"



# Define blocks and lines
blocks = [
    (file_c1_blockY, "blockY s24c1", [5200, 5320],[[5140, 5180], [5340, 5430]]),
    (file_c1_blockX, "blockX s24c1", [5200, 5320],[[5140, 5180], [5340, 5430]]),
    # (file_c2_block1, "block1 s24c2"),
    # (file_c2_block2, "block2 s24c2"),
    # (file_c2_block3, "block3 s24c2"),
    # (file_c2_block4, "block4 s24c2"),
    # (file_c3_block5, "block5 s24c3"),
    # (file_c3_block6, "block6 s24c3"),
    (file_c4_block7, "block7 s24c4", [5230 , 5320],[[5150 , 5200], [5340, 5430]]),
    (file_c4_block8, "block8 s24c4", [5230 , 5320],[[5150 , 5200], [5340, 5430]])
]

# Define configurations
configurations = [
    (["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"], "Any Topological", "k"),
    (["Hlt2Topo3Body"], "3BodyB", "#377eb8"),
    (["Hlt2TopoMu3Body"], "3BodyMu", "#e41a1c"),
    (["Hlt2Topo2Body"], "2bodyB", "#984ea3"),
    (["Hlt2TopoMu2Body"], "2bodyMu", "#f781bf"),
]

global_tis = ["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"]
    
##########################################################################################
# Plot the mass and the sidebands
##########################################################################################
# #c1
# plotter = TOSEfficiencyPlotter(sprucing_campaign=f"s24c4", outputpath="plots/sideband_substraction", decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$")

# df2, hlt_eff_object = get_df_and_hlt_eff(
#     file=file_c4,
#     tos_lines=global_tis,
#     tis_lines=global_tis,
#     particle="BP",
#     variable="BP_DTFPV_PT",
#     binning=[n*1e3 for n in (
#                         0.5,1,1.5, 2,2.5, 3,3.5, 4,4.5, 5,5.5, 6,6.5, 7,7.5, 8,8.5, 9,9.5, 10,10.5, 11,11.5, 12,12.5, 13,13.5,
#                         14, 15, 16, 17, 18, 19, 20, 21, 22, 25
#                     )],
#     signal_band= [5230 , 5320],
#     sidebands= [[5150 , 5200], [5340, 5430]]
# )

# # Add multiple distributions for comparison
# plotter.add_distribution(hlt_eff_object, variable="BP_DTFPV_M", color='k', alpha=0.5, label=r"$B^+_\text{mass}$ / $\mathrm{MeV}c^{-1}$", hlt_eff_variable="sel_all", legend_label="Overall distribution")
# plotter.add_distribution(hlt_eff_object, variable="BP_DTFPV_M", color='magenta', alpha=0.5, label=r"$B^+_\text{mass}$ / $\mathrm{MeV}c^{-1}$", hlt_eff_variable="sel_sideband", legend_label= "Sideband distributions")
# plotter.add_distribution(hlt_eff_object, variable="BP_DTFPV_M", color='dodgerblue', alpha=0.5, label=r"$B^+_\text{mass}$ / $\mathrm{MeV}c^{-1}$", hlt_eff_variable="sel", legend_label="Signal distribution")
# # Plot
# plotter.plot_sideband_variable()

# #c4
# plotter = TOSEfficiencyPlotter(sprucing_campaign=f"s24c1", outputpath="plots/sideband_substraction", decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$")
# df2, hlt_eff_object = get_df_and_hlt_eff(
#     file=file_c1,
#     tos_lines=global_tis,
#     tis_lines=global_tis,
#     particle="BP",
#     variable="BP_DTFPV_PT",
#     binning=[n*1e3 for n in (
#                         0.5,1,1.5, 2,2.5, 3,3.5, 4,4.5, 5,5.5, 6,6.5, 7,7.5, 8,8.5, 9,9.5, 10,10.5, 11,11.5, 12,12.5, 13,13.5,
#                         14, 15, 16, 17, 18, 19, 20, 21, 22, 25
#                     )],
#     signal_band= [5200, 5320],
#     sidebands= [[5140, 5180], [5340, 5430]]
# )

# # Add multiple distributions for comparison
# plotter.add_distribution(hlt_eff_object, variable="BP_DTFPV_M", color='k', alpha=0.5, label=r"$B^+_\text{mass}$ / $\mathrm{MeV}c^{-1}$", hlt_eff_variable="sel_all")
# plotter.add_distribution(hlt_eff_object, variable="BP_DTFPV_M", color='magenta', alpha=0.5, label=r"$B^+_\text{mass}$ / $\mathrm{MeV}c^{-1}$", hlt_eff_variable="sel_sideband")
# plotter.add_distribution(hlt_eff_object, variable="BP_DTFPV_M", color='dodgerblue', alpha=0.5, label=r"$B^+_\text{mass}$ / $\mathrm{MeV}c^{-1}$", hlt_eff_variable="sel")
# # Plot
# plotter.plot_sideband_variable()

##########################################################################################
# Compare with and without UT
##########################################################################################

# for variable, config in hlt_eff_config.items():
#     binning = config["bins"]
#     xlabel = config["xlabel"]

#     for lines_config, line_label, color in configurations:
#         try:
#             plotter = TOSEfficiencyPlotter(sprucing_campaign="s1 vs. s4", outputpath="plots/with_without_UT_s1_s4", decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$") # TODO: make sprucing campaign dynamic
#             df1, hlt_eff_object1 = get_df_and_hlt_eff(
#                 file=file_c1,
#                 tos_lines=lines_config,
#                 tis_lines=global_tis,
#                 particle="BP",
#                 variable=variable,
#                 binning=binning,
#                 signal_band= [5200, 5320],
#                 sidebands= [[5140, 5180], [5340, 5430]]
#             )

#             df2, hlt_eff_object2 = get_df_and_hlt_eff(
#                 file=file_c4,
#                 tos_lines=lines_config,
#                 tis_lines=global_tis,
#                 particle="BP",
#                 variable=variable,
#                 binning=binning,
#                 signal_band= [5230 , 5320],
#                 sidebands= [[5150 , 5200], [5340, 5430]]
#             )

                
#             # # Add efficiencies
#             plotter.add_efficiency(hlt_eff_object1, variable=variable, xlabel=xlabel, line=line_label, color='deeppink', UT_label="without UT")
#             plotter.add_efficiency(hlt_eff_object2, variable=variable, xlabel=xlabel, line=line_label, color='k', UT_label="with UT")
#             # Add multiple distributions for comparison
#             plotter.add_distribution(hlt_eff_object1, variable=variable, color='deeppink', alpha=0.5, label="s24c1", hlt_eff_variable="sel_signal_count")
#             plotter.add_distribution(hlt_eff_object2, variable=variable, color='k', alpha=0.5, label="s24c4", hlt_eff_variable="sel_signal_count")
#             # Plot
#             plotter.plot()
#         except Exception as e:
#             print(f"Failed to process {variable} with lines {lines_config} and label {line_label}: {e}")

# ########################################################################################
# # Compare all lines per block
# ########################################################################################

for variable, config in hlt_eff_config.items():
    binning = config["bins"]
    xlabel = config["xlabel"]

    for file_var, efficiency_name, signal_band, sidebands in blocks:
        plotter = TOSEfficiencyPlotter(
            sprucing_campaign=efficiency_name,
            outputpath="plots/all_efficiencies",
            decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$"
        )

        for lines_config, line_label, color in configurations:
            try:
                dataframe, hlt_eff_object = get_df_and_hlt_eff(
                    file=file_var,
                    tos_lines=lines_config,
                    tis_lines=global_tis,
                    particle="BP",
                    variable=variable,
                    binning=binning,
                    signal_band=signal_band,
                    sidebands=sidebands
                )
                plotter.add_efficiency(
                    hlt_eff_object=hlt_eff_object,
                    variable=variable,
                    xlabel=xlabel,
                    line=line_label,
                    color=color,
                    UT_label=None
                )
                plotter.add_distribution(hlt_eff_object= hlt_eff_object, variable=variable, color='grey', alpha=0.3, label=variable, hlt_eff_variable="sel_signal_count")

            except Exception as e:
                print(f"Failed to process {efficiency_name} with lines {lines_config} and label {line_label}: {e}")

        # Plot after adding efficiencies
        plotter.plot()

# ########################################################################################
# # Understand Importance of the TIS denominator
# # ########################################################################################

# for variable, config in hlt_eff_config.items():
#     binning = config["bins"]
#     xlabel = config["xlabel"]

#     for file_var, efficiency_name, signal_band, sidebands in blocks:
#         plotter = TOSEfficiencyPlotter(
#             sprucing_campaign=efficiency_name,
#             outputpath="plots/TIS_study/Hlt2Topo2Body",
#             decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$"
#         )

#         for lines_config, line_label, color in configurations:
#             try:
#                 dataframe, hlt_eff_object = get_df_and_hlt_eff(
#                     file=file_var,
#                     tos_lines="Hlt2Topo2Body",
#                     tis_lines=lines_config,
#                     particle="BP",
#                     variable=variable,
#                     binning=binning,
#                     signal_band=signal_band,
#                     sidebands=sidebands
#                 )
#                 plotter.add_efficiency(
#                     hlt_eff_object=hlt_eff_object,
#                     variable=variable,
#                     xlabel=xlabel,
#                     line=line_label,
#                     color=color,
#                     UT_label=None
#                 )
#                 plotter.add_distribution(dataframe=dataframe, variable=variable, color='grey', alpha=0.3, label=variable, hlt_eff_variable="sel_signal_count")

#             except Exception as e:
#                 print(f"Failed to process {efficiency_name} with lines {lines_config} and label {line_label}: {e}")

#         # Plot after adding efficiencies
#         plotter.plot()

# ########################################################################################
# # Single efficiencies
# # ########################################################################################

for variable, config in hlt_eff_config.items():
    binning = config["bins"]
    xlabel = config["xlabel"]

    for file_var, efficiency_name, signal_bands, sidebands in blocks:
        for lines_config, line_label, color in configurations:
            try:
                plotter = TOSEfficiencyPlotter(
                    sprucing_campaign=efficiency_name,
                    outputpath="plots/single_efficiencies",
                    decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$"
                )
                dataframe, hlt_eff_object = get_df_and_hlt_eff(
                    file=file_var,
                    tos_lines=lines_config,
                    tis_lines=global_tis,
                    particle="BP",
                    variable=variable,
                    binning=binning,
                    sidebands=sidebands,
                    signal_band=signal_bands
                )
                plotter.add_efficiency(
                    hlt_eff_object=hlt_eff_object,
                    variable=variable,
                    xlabel=xlabel,
                    line=line_label,
                    color=color,
                    UT_label=None
                )
                plotter.add_distribution(variable=variable, color='grey', alpha=0.3, label=variable, hlt_eff_variable="sel_signal_count")
                # Plot after adding efficiencies
                plotter.plot()

            except Exception as e:
                print(f"Failed to process {efficiency_name} with lines {lines_config} and label {line_label}: {e}")

        