""" Collection of all functions needed to evaluate efficiencies with trigger calib """

import sys
sys.path.append("/home/nschulte/.local/lib/python3.11/site-packages")
import uproot 
import numpy as np
import matplotlib.pyplot as plt
import mplhep as hep
import os
import triggercalib
from triggercalib import HltEff
import pandas as pd

# Change a tgraph object to a numpy array -> code from @jgooding 
def tgraph_to_np(th1, x_scale=1):
    histogram = uproot.from_pyroot(th1)
    #print("Values from histogram:", histogram.values())
    xvals, yvals = histogram.values()
    xlow_errs, ylow_errs = histogram.errors("low")
    xhigh_errs, yhigh_errs = histogram.errors("high")

    return xvals, yvals, (xlow_errs, xhigh_errs), (ylow_errs, yhigh_errs)

def th_to_np(th):
    histogram = uproot.from_pyroot(th)
    yvals, edges = histogram.to_numpy()
    xerrs = np.diff(edges)
    xvals = edges[:-1] + xerrs
    yerrs = histogram.errors()

    print("xvals", xvals)

    return xvals, yvals, xerrs, yerrs


# Function to optain the HLTEff object from triggercalib
def get_df_and_hlt_eff(file, tos_lines, tis_lines, particle, variable, binning, signal_band, sidebands):
    """
    Parameters:
    file: str
        Path to the ROOT file containing the data
    get_lines: array
        Array containing the list of HLT2 lines used for TIS and TOS efficiencies 
        TODO: make this more general for more decays
    """
    with uproot.open(file) as root_file:
        tree = root_file["JPsiK/DecayTree"]
        arrays = tree.arrays(library="np")
        # Convert to DataFrame in one go
        df = pd.DataFrame(arrays)

    hlt_eff = HltEff(
        "bu2jpsik",
        f"{file}:JPsiK/DecayTree",
        tos=tos_lines,
        tis=tis_lines,
        particle=particle,
        binning={
            variable : {
                "bins" : binning
            }
        },
        # TODO: Check sideband region, Should be also stored in the HLTEffObject
        sideband={
            "BP_DTFPV_M": {
                    "signal": signal_band,
                    "sidebands": sidebands,
            }
        }
    )
    return df, hlt_eff

class TOSEfficiencyPlotter:
    def __init__(self, sprucing_campaign, outputpath, decay_descriptor):
        self.sprucing_campaign = sprucing_campaign
        self.outputpath = outputpath
        self.decay_descriptor = decay_descriptor
        self.efficiencies = []
        self.distributions = []

    def add_efficiency(self, hlt_eff_object, variable, xlabel, line,UT_label, color='r', marker='.'):
        self.efficiencies.append({
            'hlt_eff_object': hlt_eff_object,
            'variable': variable,
            'xlabel': xlabel,
            'line': line,
            'color': color,
            'marker': marker,
            'UT_label': UT_label
        })

    def add_distribution(self, hlt_eff_object, hlt_eff_variable, variable, label, color='grey', alpha=0.5, legend_label=None):
        self.distributions.append({
            'hlt_eff_object': hlt_eff_object,
            'variable': variable,
            'color': color,
            'alpha': alpha,
            'label': label,
            'hlt_eff_variable': hlt_eff_variable,
            'legend_label': legend_label
        })
    
    def plot(self):
        plt.figure(figsize=(12, 10))
        plt.plot((0, 1), (1, 1), color='k', ls='dashed', lw=2)

        global_xmin, global_xmax = float('inf'), float('-inf')
        global_ymax = float('-inf')

        # Plot efficiencies
        for eff_data in self.efficiencies:
            hlt_eff_object = eff_data['hlt_eff_object']
            variable = eff_data['variable']
            xlabel = eff_data['xlabel']
            line = eff_data['line']
            color = eff_data['color']
            marker = eff_data['marker']
            UT_label = eff_data['UT_label']

            midpoints, values, xerrors, yerrors = tgraph_to_np(hlt_eff_object["efficiencies"][f"tos_efficiency_{variable}"])
            _, eff_per_bin, _, _ = tgraph_to_np(hlt_eff_object["efficiencies"][f"tos_total_efficiency_{variable}"])

            total_counts = np.sum(values)
            total_eff = np.sum(eff_per_bin * (values / total_counts)) * 100

            yerr_fixed = np.maximum(yerrors, 0)

            bin_edges = [midpoints[0] - (midpoints[1] - midpoints[0]) / 2]
            for i in range(len(midpoints) - 1):
                bin_edges.append(midpoints[i] + (midpoints[i + 1] - midpoints[i]) / 2)
            bin_edges.append(midpoints[-1] + (midpoints[-1] - midpoints[-2]) / 2)

            global_xmin = min(global_xmin, bin_edges[0])
            global_xmax = max(global_xmax, bin_edges[-1])
            global_ymax = max(global_ymax, values.max())

            if len(self.efficiencies) > 2:  # More than 2 efficiencies, use line label
                legend_label = f"{line}, $\\epsilon_\\text{{TOS}}$: {total_eff:.2f}%"
            elif len(self.efficiencies) == 2:  # Only two or less efficiencies, use with and without UT label
                legend_label =f"{UT_label}, $\\epsilon_\\text{{TOS}}$: {total_eff:.2f}%"
            elif len(self.efficiencies) == 1:
                legend_label = f"{line}, $\\epsilon_\\text{{TOS}}$: {total_eff:.2f}%"
            plt.errorbar(
                x=midpoints, y=values, xerr=[xerrors[0], xerrors[1]], yerr=yerr_fixed,
                color=color, elinewidth=2, ls="none", marker=marker, markersize=8, label=legend_label # Efficiency
            )

        # Loop over distributions and plot each
        for dist_data in self.distributions:
            hlt_eff_object = dist_data['hlt_eff_object']
            variable = dist_data['variable']
            color = dist_data['color']
            alpha = dist_data['alpha']
            label = dist_data['label']
            hlt_eff_variable = dist_data['hlt_eff_variable']
            legend_label = dist_data['legend_label']

            # Extract bin centers and contents
            hist = hlt_eff_object["counts"][f"{hlt_eff_variable}_{variable}"]

            # Extract bin centers (x-values), contents (y-values), and bin widths
            midpoints_dist = np.array([hist.GetBinCenter(i) for i in range(1, hist.GetNbinsX() + 1)])
            values_dist = np.array([hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)])
            bin_widths = np.array([hist.GetBinWidth(i) for i in range(1, hist.GetNbinsX() + 1)])

            # Normalize by bin width to account for variable bin sizes
            values_dist_scaled = values_dist / bin_widths

            # Scale to match the maximum efficiency value
            efficiency_max = values.max()
            values_dist_scaled *= efficiency_max / values_dist_scaled.max()

            plt.bar(midpoints_dist, values_dist_scaled, width=bin_widths, alpha=alpha, color=color, label=legend_label)

        plt.plot((global_xmin, global_xmax), (1, 1), color='k', ls='dashed', lw=2)
        plt.xlim(global_xmin, global_xmax)
        
        # Adjust ylim to add space above 1 for the legend
        if global_ymax <= 1:
            plt.ylim(0, 1.3)  # Set a fixed padding above 1
        else:
            plt.ylim(0, global_ymax * 1.3)


        plt.xlabel(xlabel)
        plt.ylabel(r"TOS efficiency, $\varepsilon_\mathrm{TOS}$")

        # Dynamic labeling for the 'hep' label based on context
        if len(self.efficiencies) > 2:  # More than 2 efficiencies, use sprucing campaign label
            hep_label = self.sprucing_campaign
        else:  # Only two or less efficiencies, use the sprucing campaign
            #line_labels = [eff_data['line'] for eff_data in self.efficiencies]
            hep_label = f"{eff_data['line']}"

        hep.lhcb.label(loc=0, rlabel=f"{self.decay_descriptor}"+f"{hep_label}", data=True, label="Preliminary")

        ncol = 1  # Default to one column
        if len(self.efficiencies) > 3:
            ncol = 2
        plt.legend(loc='upper left', prop={'size': 22}, ncol=ncol, bbox_to_anchor=(0.0, 1))

        outpath = f"{self.outputpath}/{variable}/"
        os.makedirs(outpath, exist_ok=True)

        # Filename generation logic:
        if len(self.efficiencies) > 2:  # More than two efficiencies, use the sprucing campaign label
            filename = f"TOS_efficiency_{self.sprucing_campaign.replace(' ', '_')}_{variable}.pdf"
        else:  # Single or comparison efficiency plot
            filename = f"TOS_efficiency_{self.sprucing_campaign.replace(' ', '_')}_{line.replace(' ', '_')}_{variable}.pdf"
        plt.savefig(os.path.join(outpath, filename))
        print(f"Plot saved to {os.path.join(outpath, filename)}")
        plt.close()
    
    def plot_sideband_variable(self):
        # Loop over distributions and plot each
        for dist_data in self.distributions:
            hlt_eff_object = dist_data['hlt_eff_object']
            variable = dist_data['variable']
            color = dist_data['color']
            alpha = dist_data['alpha']
            label = dist_data['label']
            hlt_eff_variable = dist_data['hlt_eff_variable']
            legend_label = dist_data['legend_label']

            # Extract bin centers and contents
            hist = hlt_eff_object["counts"][f"{hlt_eff_variable}_{variable}"]

            # Extract bin centers (x-values), contents (y-values), and bin widths
            midpoints_dist = np.array([hist.GetBinCenter(i) for i in range(1, hist.GetNbinsX() + 1)])
            values_dist = np.array([hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)])
            bin_widths = np.array([hist.GetBinWidth(i) for i in range(1, hist.GetNbinsX() + 1)])

            # Normalize by bin width to account for variable bin sizes
            values_dist_scaled = values_dist / bin_widths
            plt.bar(midpoints_dist, values_dist_scaled, width=bin_widths, alpha=alpha, color=color, label=legend_label)


        plt.xlabel(label)
        plt.ylabel(r"A.U.")

        hep.lhcb.label(loc=0, rlabel=f"{self.decay_descriptor}", data=True, label="Preliminary")
        plt.legend(loc='best', prop={'size': 19})

        outpath = f"{self.outputpath}/{variable}/"
        os.makedirs(outpath, exist_ok=True)

        # Filename generation logic:
 
        filename = f"Sidebandsubtraction_{self.sprucing_campaign.replace(' ', '_')}_{variable}_unnormed.pdf"

        plt.savefig(os.path.join(outpath, filename))
        print(f"Plot saved to {os.path.join(outpath, filename)}")
        plt.close()

    def plot_sideband_variable_normalised(self):
        # Loop over distributions and plot each
        for dist_data in self.distributions:
            hlt_eff_object = dist_data['hlt_eff_object']
            variable = dist_data['variable']
            color = dist_data['color']
            alpha = dist_data['alpha']
            label = dist_data['label']
            hlt_eff_variable = dist_data['hlt_eff_variable']
            legend_label = dist_data['legend_label']
    
            # Extract bin centers and contents
            hist = hlt_eff_object["counts"][f"{hlt_eff_variable}_{variable}"]

            # Extract bin centers (x-values), contents (y-values), and bin widths
            midpoints_dist = np.array([hist.GetBinCenter(i) for i in range(1, hist.GetNbinsX() + 1)])
            values_dist = np.array([hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)])
            bin_widths = np.array([hist.GetBinWidth(i) for i in range(1, hist.GetNbinsX() + 1)])

            # Normalize by bin width to account for variable bin sizes
            values_dist_scaled = values_dist / bin_widths

            # Scale to 1
            values_dist_scaled *= 1 / values_dist_scaled.max()

            plt.bar(midpoints_dist, values_dist_scaled, width=bin_widths, alpha=alpha, color=color, label=legend_label)


        plt.xlabel(label)
        plt.ylabel(r"A.U.")

        hep.lhcb.label(loc=0, rlabel=f"{self.decay_descriptor}", data=True, label="Preliminary")
        plt.legend(loc='best', prop={'size': 19})

        outpath = f"{self.outputpath}/{variable}/"
        os.makedirs(outpath, exist_ok=True)

        # Filename generation logic:
        filename = f"Sidebandsubtraction_{self.sprucing_campaign.replace(' ', '_')}_{variable}_normed.pdf"

        plt.savefig(os.path.join(outpath, filename))
        print(f"Plot saved to {os.path.join(outpath, filename)}")
        plt.close()
