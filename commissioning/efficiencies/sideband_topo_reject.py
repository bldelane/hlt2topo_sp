import sys
sys.path.append("/home/nschulte/.local/lib/python3.11/site-packages")

import numpy as np
import uproot
import pandas as pd
import matplotlib.pyplot as plt
import mplhep as hep
import triggercalib
from triggercalib import HltEff
import ROOT as R
import pathlib 

hep.style.use("LHCb2")
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from src.efficiencies_triggercalib import get_df_and_hlt_eff, TOSEfficiencyPlotter
from shape_compare import hlt_eff_config

file_c1 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c1_selected.root"
file_c2 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c2_selected.root"
file_c3 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c3_selected.root"
file_c4 =  f"/ceph/users/nschulte/TopoCommissioning/selected/s24c4_selected.root"

# Define configurations
configurations = [
    (["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"], "Any Topological", "k"),
    (["Hlt2Topo3Body"], "3Body Beauty Topo", "#377eb8"),
    (["Hlt2TopoMu3Body"], "3Body MuonTopo", "#e41a1c"),
    (["Hlt2Topo2Body"], "2body Beauty Topo", "#984ea3"),
    (["Hlt2TopoMu2Body"], "2body Muon Topo", "#f781bf"),
]

global_tis = ["Hlt2TopoMu3Body", "Hlt2TopoMu2Body", "Hlt2Topo3Body", "Hlt2Topo2Body"]

##########################################################################################
# Plot the sideband substracted distributions 
##########################################################################################

def filter_data(file, lines, output_file, persist_TOS=True):
    # Open the ROOT file and extract data into a DataFrame
    with uproot.open(file) as root_file:
        tree = root_file["JPsiK/DecayTree"]
        df = pd.DataFrame(tree.arrays(library="np"))

    print("Length of original DataFrame:", len(df))
    if persist_TOS:
        # Retain rows where at least one line is TOS == True
        mask = np.any([df[f"BP_{line}Decision_TOS"] for line in lines], axis=0)
    else:
        # Retain rows where all lines are TOS == False
        mask = np.all([~df[f"BP_{line}Decision_TOS"] for line in lines], axis=0)

    # Filter the DataFrame
    filtered_data = df[mask]
    print("Length of filtered DataFrame:", len(filtered_data))
    # Write the filtered data to a new ROOT file
    with uproot.recreate(output_file) as root_output:
        root_output["JPsiK/DecayTree"] = filtered_data.to_records(index=False)

    return None

# filter_data(file_c1, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/persist_signal/s24c1_signal_region.root", persist_TOS=True)
# filter_data(file_c1, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/topo_rejected/s24c1_signal_region_topo_rejected.root", persist_TOS=False)

# filter_data(file_c2, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/persist_signal/s24c2_signal_region.root", persist_TOS=True)
# filter_data(file_c2, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/topo_rejected/s24c2_signal_region_topo_rejected.root", persist_TOS=False)

# filter_data(file_c3, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/persist_signal/s24c3_signal_region.root", persist_TOS=True)
# filter_data(file_c3, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/topo_rejected/s24c3_signal_region_topo_rejected.root", persist_TOS=False)

# filter_data(file_c4, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/persist_signal/s24c4_signal_region.root", persist_TOS=True)
# filter_data(file_c4, global_tis, f"/ceph/users/nschulte/TopoCommissioning/JPsiK/topo_rejected/s24c4_signal_region_topo_rejected.root", persist_TOS=False)

##########################################################################################

# Define the list of campaigns and corresponding file paths
campaigns = {
    's24c1': {
        "files": [
            "/ceph/users/nschulte/TopoCommissioning/JPsiK/persist_signal/s24c1_signal_region.root",
            "/ceph/users/nschulte/TopoCommissioning/JPsiK/topo_rejected/s24c1_signal_region_topo_rejected.root",
            file_c1
        ],
        "legend_labels": [
            "Topo selected signal",
            "Topo rejected signal",
            "Total persisted signal"
        ],
        "signal_band": [5200, 5320],
        "sidebands": [[5140, 5180], [5340, 5430]]
    },
    's24c4': {
        "files":  [
            "/ceph/users/nschulte/TopoCommissioning/JPsiK/persist_signal/s24c4_signal_region.root",
            "/ceph/users/nschulte/TopoCommissioning/JPsiK/topo_rejected/s24c4_signal_region_topo_rejected.root",
            file_c4
        ],
        "legend_labels": [
            "Topo selected signal",
            "Topo rejected signal",
            "Total persisted signal"
        ],
        "signal_band": [5230 , 5320],
        "sidebands": [[5150 , 5200], [5340, 5430]]
    }
}

# Define the colors and labels for each plot
colors = ['dodgerblue', 'deeppink', 'k']
labels = [r"$B^+_\text{PT}$ / $\mathrm{MeV}c^{-1}$", r"$B^+_\text{M}$ / $\mathrm{MeV}c^{-1}$"]

# Loop over the campaigns (s24c1 and s24c4)
for campaign, campaign_data in campaigns.items():
    # Extract files, signal_band, and sidebands for the current campaign
    files = campaign_data["files"]
    legend_labels = campaign_data["legend_labels"]

    signal_band = campaign_data["signal_band"]
    sidebands = campaign_data["sidebands"]
    
    print(f"Processing campaign: {campaign}")
    print(f"Signal band: {signal_band}")
    print(f"Sidebands: {sidebands}")
    
    # Initialize the plotter for each campaign for PT and M separately
    plotter_m = TOSEfficiencyPlotter(sprucing_campaign=campaign, outputpath=f"plots/signal_region_extracted_topo_rejected", decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$")
    
    # Loop over the files (signal and topo_rejected)
    for idx, (file, color, legend_label) in enumerate(zip(files, colors, legend_labels)):
        print(f"  File {idx + 1}: {file}")
        print(f"  Color: {color}")
        print(f"  Legend Label: {legend_label}")
        
        # Get the hlt_eff_object for M variable, passing signal_band and sidebands
        _, hlt_eff_object_m = get_df_and_hlt_eff(
            file=file,
            tos_lines=global_tis,
            tis_lines=global_tis,
            particle="BP",
            variable="BP_DTFPV_M",
            binning=[n*1e3 for n in (
            0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75, 3,3.25,3.5, 3.75,4,4.25,4.5, 4.75, 5, 5.25, 5.5, 5.75, 6,6.25,6.5, 6.75, 7,7.5, 8,8.5, 9,9.5, 10,10.5, 11,11.5, 12,12.5, 13,13.5,
            14, 15, 16, 17, 18, 19, 20, 21, 22, 25)],
            signal_band=signal_band,
            sidebands=sidebands
        )
        # Add distribution for M to the second plotter
        plotter_m.add_distribution(hlt_eff_object_m, variable="BP_DTFPV_M", color=color, alpha=0.5, label=labels[1], hlt_eff_variable="sel_signal", legend_label=legend_label)

    # Plot all M distributions in a separate canvas
    print(f"Plotting M distributions for campaign: {campaign}")
    plotter_m.plot_sideband_variable_normalised()
    plotter_m.plot_sideband_variable()



# Define the colors and labels for each plot
colors = ['dodgerblue', 'deeppink', 'k']

# Loop over the campaigns (s24c1 and s24c4)
for campaign, campaign_data in campaigns.items():
    # Extract files, signal_band, sidebands, and legend_labels for the current campaign
    files = campaign_data["files"]
    legend_labels = campaign_data["legend_labels"]
    signal_band = campaign_data["signal_band"]
    sidebands = campaign_data["sidebands"]

    print(f"Processing campaign: {campaign}")
    print(f"Signal band: {signal_band}")
    print(f"Sidebands: {sidebands}")

    # Loop over the variables in the hlt_eff_config dictionary
    for variable, config in hlt_eff_config.items():
        bins = config["bins"]
        xlabel = config["xlabel"]

        print(f"  Plotting variable: {variable}")

        # Initialize a plotter for this variable
        plotter = TOSEfficiencyPlotter(
            sprucing_campaign=campaign,
            outputpath=f"plots/signal_region_extracted_topo_rejected",
            decay_descriptor=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$"
        )

        # Loop over the files and corresponding legend_labels
        for idx, (file, color, legend_label) in enumerate(zip(files, colors, legend_labels)):
            print(f"    File {idx + 1}: {file}")
            print(f"    Color: {color}")
            print(f"    Legend Label: {legend_label}")

            # Get the hlt_eff_object for the current variable
            _, hlt_eff_object = get_df_and_hlt_eff(
                file=file,
                tos_lines=global_tis,
                tis_lines=global_tis,
                particle="BP",
                variable=variable,
                binning=bins,
                signal_band=signal_band,
                sidebands=sidebands
            )

            # Add distribution for this variable to the plotter
            plotter.add_distribution(
                hlt_eff_object,
                variable=variable,
                color=color,
                alpha=0.5,
                label=xlabel,
                hlt_eff_variable="sel_count",
                legend_label=legend_label  # Pass the legend label here
            )

        # Plot all distributions for the current variable
        print(f"  Plotting distributions for variable: {variable}")
        plotter.plot_sideband_variable_normalised()
        plotter.plot_sideband_variable()
