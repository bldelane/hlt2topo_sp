import yaml
import uproot
import pandas as pd
import sys
from pathlib import Path
import re
import numpy as np


# Inputs and parameters
campaign = snakemake.wildcards.campaign
block = snakemake.wildcards.block

# Load configurations
with open(snakemake.input.yaml_in, "r") as f:
    input_files = yaml.safe_load(f)

with open(snakemake.input.branches_in, "r") as f:
    branches_config = yaml.safe_load(f)

with open(snakemake.input.block_in, "r") as f:
    block_data = yaml.safe_load(f)

with open(snakemake.input.selection, "r") as f:
    selection_criteria = yaml.safe_load(f)

# Get block info
block_info = block_data[block]

# Branches to read
branches_to_read = branches_config["branches"]

# Selection criteria
simple_criteria = selection_criteria["complete"]
selection_query = " & ".join(simple_criteria)

# ROOT files for the campaign
files = input_files[f"{campaign.lower()}_{block_info['polarity'].lower()}"]

# Process files for the block
filtered_data = []
for file in files:
    print("Processing file", file)
    with uproot.open(file) as root_file:
        tree = root_file["JPsiK/DecayTree"]
        arrays = tree.arrays(branches_to_read, library="np")
        # Convert to DataFrame in one go
        df = pd.DataFrame(arrays)
        tos_mask = (
            (df["BP_Hlt1TwoTrackMVADecision_TOS"] == True) | 
            (df["BP_Hlt1TrackMVADecision_TOS"] == True) 
        )
        block_mask = ((df["FillNumber"] >= block_info["fill_nr_start"]) &
            (df["FillNumber"] <= block_info["fill_nr_end"]) &
            (df["RUNNUMBER"] >= block_info["run_nr_start"]) &
            (df["RUNNUMBER"] <= block_info["run_nr_end"]))
        
        df = df[tos_mask & block_mask]
        print(f"length of data after TOS and block selection: {len(df)}")
        filtered_data.append(df)


run_ranges = []
with open(snakemake.input.DQ_flags, "r") as f:
    for line in f:
        line = line.strip()
        # Skip lines without actual run data
        if "runs" not in line or "Only" in line or "Total" in line:
            continue
        
        # Handle lines with ranges or single runs
        # Match ranges like "292306:292310 : 5 runs"
        match_range = re.match(r"(\d+):(\d+)\s*:\s*\d+\s*runs", line)
        # Match single runs like "292312 : 1 runs"
        match_single = re.match(r"(\d+)\s*:\s*1\s*runs", line)

        if match_range:
            start, end = map(int, match_range.groups())
            run_ranges.append((start, end))
        elif match_single:
            run = int(match_single.group(1))
            run_ranges.append((run, run))  # Treat single run as a range

# Combine and save
if filtered_data:
    combined_df = pd.concat(filtered_data)
    print(f"length of data after deviding into blocks: {len(combined_df)}")

    # Filter ROOT file data based on run ranges
    df_filtered = combined_df[combined_df["RUNNUMBER"].apply(lambda x: any(start <= x <= end for start, end in run_ranges))]
    print(f"length of data after filtering based on DQ flags: {len(df_filtered)}")
    
    output_file_unselected = snakemake.output.root_file_unselected
    with uproot.recreate(output_file_unselected) as root_output:
        root_output["JPsiK/DecayTree"] = df_filtered.to_records(index=False)
    # # # Apply the selection criteria
    df_selected = df_filtered.query(selection_query, local_dict={"min": min})
    print(f"length of data after applying selection criteria: {len(df_selected)}")

    # Save the data
    output_file_selected = snakemake.output.root_file_selected
    with uproot.recreate(output_file_selected) as root_output:
        root_output["JPsiK/DecayTree"] = df_selected.to_records(index=False)
