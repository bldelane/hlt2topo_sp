""" Collection of all functions needed to evaluate efficiencies with trigger calib """

import sys
import uproot 
import numpy as np
import matplotlib.pyplot as plt
import mplhep as hep
import os
import pandas as pd

# Function to optain the HLTEff object from triggercalib
def get_df(file):
    """
    Parameters:
    file: str
        Path to the ROOT file containing the data
    get_lines: array
        Array containing the list of HLT2 lines used for TIS and TOS efficiencies 
        TODO: make this more general for more decays
    """
    with uproot.open(file) as root_file:
        tree = root_file["JPsiK/DecayTree"]
        arrays = tree.arrays(library="np")
        # Convert to DataFrame in one go
        df = pd.DataFrame(arrays)

    return df

def plot_variables_single(dataframe, variable, bins, xlabel, outputpath, block, cut=None, xlim=None):
    plt.figure(figsize=(12, 10))
    plt.plot((0, 1), (1, 1), color='k', ls='dashed', lw=2)
    
    hist_values, bin_edges = np.histogram(dataframe[variable], bins=bins, range=xlim)
    plt.bar(bin_edges[:-1], hist_values, width=np.diff(bin_edges), align='edge', color='grey', alpha=0.5)
    
    plt.xlabel(xlabel)
    plt.ylabel(r"A.U.")
    hep.lhcb.label(loc=0, rlabel=r"$B^+\to J/\psi\left(\mu\mu\right)K^+$"+f"{block}", data=True, label="Preliminary")
    
    if cut:
        plt.axvline(cut, color='r', linestyle='dashed', label=f"Cut at {cut}")
    
    if xlim:
        plt.xlim(xlim)
        # ylim = plt.ylim()
        
        #TODO: Fix this, it looks shit
        # # Add proper break marks (tilted lines) on the x-axis at the cut-off
        # break_size = 0.02 * (ylim[1] - ylim[0])  # Size of the break
        # x_break = xlim[1]
        
        # # First tilted line
        # plt.plot([x_break - break_size, x_break + break_size],
        #          [ylim[0] - break_size, ylim[0] + break_size],
        #          color='k', lw=1.5, clip_on=False)
        
        # # Second tilted line
        # plt.plot([x_break - break_size, x_break + break_size],
        #          [ylim[0] + 1.5 * break_size, ylim[0] + 2.5 * break_size],
        #          color='k', lw=1.5, clip_on=False)

    plt.legend()
    
    outpath = f"{outputpath}/{block.replace(' ', '_')}/{variable}"
    os.makedirs(outpath, exist_ok=True)
    filename = f"{variable}_distribution_cut{cut}.pdf"
    plt.savefig(os.path.join(outpath, filename))
    plt.close()
    print(f"Plot saved to {os.path.join(outpath, filename)}")


file_c1_blockY = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c1_blockY.root"
file_c1_blockX = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c1_blockX.root"
file_c2_block1 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c2_block1.root"
file_c2_block2 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c2_block2.root"
file_c2_block3 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c2_block3.root"
file_c2_block4 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c2_block4.root"
file_c3_block5 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c3_block5.root"
file_c3_block6 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c3_block6.root"
file_c4_block7 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c4_block7.root"
file_c4_block8 = f"/ceph/users/nschulte/TopoCommissioning/unselected/s24c4_block8.root"

  #- np.min([mup_BPVIPCHI2, mum_BPVIPCHI2]) > 9 # Plotting still todo
# TODO: Cut is not displaying and saving properly, fix this
variable_dict = {
    "nFTClusters": {
        "bins": 100,
        "xlabel": "Number of FT clusters",
        "range": (0, 20000),
        "cut": None
    },
    "nPVs": {
        "bins": 18,
        "xlabel": "Number of PVs",
        "range": (0, 18),
        "cut": None
    },
    "nLongTracks": {
        "bins": 100,
        "xlabel": "Number of long tracks",
        "range": (0, 1000),
        "cut": None
    },
    "nVPClusters": {
        "bins": 100,
        "xlabel": "Number of VP Clusters",
        "range": (0, 20000),
        "cut": None
    },
    "BP_DTFPV_M": {
        "bins": 100,
        "xlabel": "B DTF mass [MeV]",
        "range": (5100, 5600),
        "cut": None
    },
    "BP_DTFPV_PT": {
        "bins": 100,
        "xlabel": "B DTF pT [MeV]",
        "range": (0, 25000),
        "cut": None
    },
    "BP_DTFPV_BPVLTIME": {
        "bins": 100,
        "xlabel": "B DTF decay time [ns]",
        "range": (0, 0.001),
        "cut": None
    },
    "mum_ISMUON": {
        "bins": 2,
        "xlabel": "ISMUON",
        "range": (0, 2),
        "cut": 1
    },
    "mup_ISMUON": {
        "bins": 2,
        "xlabel": "ISMUON",
        "range": (0, 2),
        "cut": 1
    },  
    "KP_PID": { 
        "bins": 650,
        "xlabel": "Kaon PID",
        "range": (-325, 325),
        "cut": 2
    },
    "Jpsi_BPVIPCHI2": {
        "bins": 100,
        "xlabel": "Jpsi IP chi2",
        "range": (0,100),
        "cut": 0
    },
    "BP_BPVIPCHI2": {
        "bins": 100,
        "xlabel": "B IP chi2",
        "range": (0,100),
        "cut": 25
    },
    "BP_BPVDIRA": {
        "bins": 100,
        "xlabel": "B DIRA",
        "range": (0.9,1),
        "cut": 0.9995
    },
    "mum_BPVIPCHI2": {
        "bins": 100,
        "xlabel": "Muon- IP chi2",
        "range": (0,100),
        "cut": 4
    },
    "mup_BPVIPCHI2": {
        "bins": 100,
        "xlabel": "Muon+ IP chi2",
        "range": (0,100),
        "cut": 4
    },
    "mum_PID": {
        "bins": 30,
        "xlabel": "Muon- PID",
        "range": (-15, 15),
        "cut": 2
    },
    "mup_PID":{
        "bins": 30,
        "xlabel": "Muon+ PID",
        "range": (-15, 15),
        "cut": 2
    },
    "KP_BPVIPCHI2": {
        "bins": 100,
        "xlabel": "Kaon IP chi2",
        "range": (0,500),
        "cut": 9
    },
    "BP_FDCHI2_OWNPV":{
        "bins": 100,
        "xlabel": "B FD chi2",
        "range": (0, 7000),
        "cut": 100
    },
    "Jpsi_MAXSDOCACHI2": {
        "bins": 100,
        "xlabel": "Jpsi SDOCA chi2",
        "range": (0,25),
        "cut": 30
    },
    "Jpsi_PT": {
        "bins": 100,
        "xlabel": "Jpsi pT",
        "range": (0, 25000),
        "cut": 300
    },
    "Jpsi_DTFPV_PT": {
        "bins": 100,
        "xlabel": "Jpsi DTF pT",
        "range": (0, 25000),
        "cut": 300
    },
    "BP_DTFPV_PT": {
        "bins": 100,
        "xlabel": "B DTF pT",
        "range": (0, 25000),
        "cut": None
    },
    "Jpsi_ENDVERTEX_CHI2DOF": {
        "bins": 100,
        "xlabel": "Jpsi vertex chi2",
        "range": (0, 25),
        "cut": 9
    },
    "BP_ENDVERTEX_CHI2DOF": {
        "bins": 100,
        "xlabel": "B vertex chi2",
        "range": (0, 20),
        "cut": 9
    }
}

# Define blocks and lines
blocks = [
    (file_c1_blockY, "blockY s24c1"),
    (file_c1_blockX, "blockX s24c1"),
    # (file_c2_block1, "block1 s24c2"),
    # (file_c2_block2, "block2 s24c2"),
    # (file_c2_block3, "block3 s24c2"),
    # (file_c2_block4, "block4 s24c2"),
    # (file_c3_block5, "block5 s24c3"),
    # (file_c3_block6, "block6 s24c3"),
    (file_c4_block7, "block7 s24c4"),
    (file_c4_block8, "block8 s24c4")
]

for variable, config in variable_dict.items():
    bins = config["bins"]
    xlabel = config["xlabel"]
    xlim = config["range"]
    cut = config["cut"]
    
    for file_var, block in blocks:
        # Initialize plotter for each block
        dataframe = get_df(file_var)
        plot_variables_single(dataframe, variable, bins, xlabel, "plots/unselected_distributions", block=block, xlim=xlim, cut=cut)

# TODO: Make overlay of selected and unselected distributions with the cut as a line and the efficiency number in a box
# TODO: Compare data taking periods
# TODO: Compare sweighted data with training data
        
# def plot_variable_comparison(dataframes, labels, variable, bins, xlabel, outputpath, blocks,
#                               cut=None, xlim=None, compare=False, overlay=False):
#     """
#     Plots single distributions, overlay of selected/unselected with efficiency, or comparisons across blocks.

#     Parameters:
#     - dataframes: list of DataFrames (single df or [unselected, selected] for overlay)
#     - labels: list of labels for the dataframes (e.g., ["Unselected", "Selected"])
#     - variable: str, variable name to plot
#     - bins: int, number of bins
#     - xlabel: str, label for the x-axis
#     - outputpath: str, directory to save plots
#     - blocks: str or list, block names for labeling
#     - cut: float, optional cut value for vertical line
#     - xlim: tuple, x-axis limits
#     - compare: bool, if True, compare across different blocks
#     - overlay: bool, if True, overlay selected and unselected distributions
#     """
#     plt.figure(figsize=(12, 10))

#     colors = ['grey', 'blue', 'green', 'orange', 'purple']  # Different colors for comparison
    
#     if overlay:
#         # Overlay selected and unselected distributions
#         for df, label, color in zip(dataframes, labels, colors):
#             hist_values, bin_edges = np.histogram(df[variable], bins=bins, range=xlim)
#             plt.step(bin_edges[:-1], hist_values, where='mid', label=label, linewidth=2, color=color)
#             plt.fill_between(bin_edges[:-1], hist_values, step='mid', alpha=0.3, color=color)
        
#         # Efficiency calculation
#         total = len(dataframes[0])
#         passed = len(dataframes[1])
#         efficiency = passed / total if total > 0 else 0
#         plt.text(0.6, 0.8, f"Efficiency: {efficiency:.3f}", transform=plt.gca().transAxes,
#                  bbox=dict(boxstyle="round", facecolor='white', alpha=0.8))
        
#     elif compare:
#         # Compare distributions across blocks
#         for df, label, color in zip(dataframes, labels, colors):
#             hist_values, bin_edges = np.histogram(df[variable], bins=bins, range=xlim)
#             plt.step(bin_edges[:-1], hist_values, where='mid', label=label, linewidth=2, color=color)
#     else:
#         # Single distribution
#         hist_values, bin_edges = np.histogram(dataframes[0][variable], bins=bins, range=xlim)
#         plt.bar(bin_edges[:-1], hist_values, width=np.diff(bin_edges), align='edge', color='grey', alpha=0.5)
    
#     plt.xlabel(xlabel)
#     plt.ylabel("A.U.")
#     hep.lhcb.label(loc=0, rlabel=f"$B^+ \to J/\psi(\mu\mu)K^+$ {blocks}", data=True, label="Preliminary")
    
#     if cut:
#         plt.axvline(cut, color='r', linestyle='dashed', label=f"Cut at {cut}")
    
#     if xlim:
#         plt.xlim(xlim)
#         ylim = plt.ylim()
#         break_size = 0.02 * (ylim[1] - ylim[0])
#         x_break = xlim[1]
#         plt.plot([x_break - break_size, x_break + break_size],
#                  [ylim[0] - break_size, ylim[0] + break_size],
#                  color='k', lw=1.5, clip_on=False)
#         plt.plot([x_break - break_size, x_break + break_size],
#                  [ylim[0] + 1.5 * break_size, ylim[0] + 2.5 * break_size],
#                  color='k', lw=1.5, clip_on=False)

#     plt.legend()
#     outpath = f"{outputpath}/{blocks.replace(' ', '_')}/{variable}"
#     os.makedirs(outpath, exist_ok=True)
#     filename = f"{variable}_distribution.pdf"
#     plt.savefig(os.path.join(outpath, filename))
#     plt.close()
#     print(f"Plot saved to {os.path.join(outpath, filename)}")

# plot_variable_comparison(
#     [get_df(file_c1_blockY)], 
#     ["BlockY s24c1"], 
#     "BP_DTFPV_M", 
#     100, 
#     "B DTF mass [MeV]", 
#     "plots/single_distributions", 
#     "blockY s24c1", 
#     xlim=(5100, 5600)
# )
# unselected_df = get_df(file_c1_blockY)
# selected_df = get_df(file_c1_blockY_selected)  # Example cut

# plot_variable_comparison(
#     [unselected_df, selected_df], 
#     ["Unselected", "Selected"], 
#     "BP_DTFPV_PT", 
#     100, 
#     "B DTF pT [MeV]", 
#     "plots/overlay_distributions", 
#     "blockY s24c1", 
#     cut=5000, 
#     xlim=(0, 25000), 
#     overlay=True
# )

# dfs = [get_df(file_c1_blockY), get_df(file_c1_blockX)]
# labels = ["blockY s24c1", "blockX s24c1"]

# plot_variable_comparison(
#     dfs, 
#     labels, 
#     "BP_DTFPV_BPVLTIME", 
#     100, 
#     "B DTF decay time [ns]", 
#     "plots/block_comparison", 
#     "s24c1 comparison", 
#     xlim=(0, 0.001), 
#     compare=True
# )
