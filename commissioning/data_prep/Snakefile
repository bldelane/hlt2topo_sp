import yaml

# Load YAML configurations
with open("branches.yaml", "r") as f:
    branches_config = yaml.safe_load(f)

with open("input_files.yaml", "r") as f:
    input_files = yaml.safe_load(f)

with open("block_data.yaml", "r") as f:
    block_data = yaml.safe_load(f)

# Branches to read
branches_to_read = branches_config["branches"]

outpath = "/ceph/users/nschulte/TopoCommissioning"

# Group blocks by campaign and precompute output filenames
output_files = []
for block, data in block_data.items():
    campaign = data["campaign"]
    output_files.append(f"{outpath}/selected/{campaign}_{block}_selected.root")
    output_files.append(f"{outpath}/unselected/{campaign}_{block}.root")


# Rule to aggregate all blocks
rule all:
    input:
        output_files

print(output_files)

# inspired from JGooding: https://gitlab.cern.ch/jagoodin/tistos-efficiencies-in-run-3/-/blob/jagoodin/process-charm/src/processing/workflow/rules/processing.smk
# Rule to get OK runs; Make sure you have your proxy generated
# rule get_okay_runs:
#     input:
#         block_data="block_data.yaml"
#     params:
#         run_start=lambda wildcards: block_data[wildcards.block]["run_nr_start"],
#         run_end=lambda wildcards: block_data[wildcards.block]["run_nr_end"]
#     output:
#         okay_DQ_flags="dq_flagged/{block}_okay_runs.txt"
#     shell:
#         """
#         lb-dirac dirac-bookkeeping-get-run-ranges --Runs={params.run_start}:{params.run_end} --Fast --Activity=Collision24 --DQFlag=OK --RunGap=1 > {output.okay_DQ_flags}
#         """


# Rule to process one block
rule process_block:
    input:
        DQ_flags = "dq_flagged/{block}_okay_runs.txt",
        yaml_in="input_files.yaml",
        branches_in="branches.yaml",
        block_in="block_data.yaml",
        selection="selection.yaml"
    output:
        root_file_unselected = outpath + "/unselected/{campaign}_{block}.root",
        root_file_selected = outpath + "/selected/{campaign}_{block}_selected.root"
    log:
        logfile = "logs/{campaign}_{block}.log"  # Correct log file reference
    threads: 20
    resources:
        MaxRunHours = 2,
        request_memory = 35000
    params:
        campaign = lambda wildcards: block_data[wildcards.block]["campaign"],
        fill_start = lambda wildcards: block_data[wildcards.block]["fill_nr_start"],
        fill_end = lambda wildcards: block_data[wildcards.block]["fill_nr_end"],
        run_start = lambda wildcards: block_data[wildcards.block]["run_nr_start"],
        run_end = lambda wildcards: block_data[wildcards.block]["run_nr_end"]
    script:
        "prepare_data.py"
