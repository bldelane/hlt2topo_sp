import yaml
import uproot4 as uproot
import pandas as pd
import awkward as ak
import polars as pl
import hist
from hist import Hist
import mplhep
import matplotlib.pyplot as plt
import scienceplots
import numpy as np
from scipy.stats import crystalball, norm
import time
from hlt2topo_sp.commissioning.RD_AP.analytical_models import dcbwg,dcbwg_cdf
from iminuit import Minuit
from iminuit.cost import UnbinnedNLL, ExtendedUnbinnedNLL, BinnedNLL, ExtendedBinnedNLL
from hlt2topo_sp.commissioning.RD_AP.plotting import plot_fit
from scipy.stats import expon


# load in the yaml file with all the branches 
with open("JPsiK_branches.yaml", "r") as f:
    branches = yaml.safe_load(f)

# this will all be made automatic, but it is to get a first fit going
# read in the merged data
ceph_path = "/ceph/RD/2024_tuples/Hlt2RD_BuToKpJpsi_JpsiToMuMu.root"

# Since there is now way to to pass the polars library to uproot directly, write a conversion
# function to convert the awkward array to numpy. Some variables have substructures that 
# break this conversion, so we will skip those for now. Flattening does not work
def awkward_to_polars(ak_array):
    data_dict = {}
    for key in ak_array.fields:
        try:
            # Try converting to NumPy (works for regular arrays)
            data_dict[key] = ak.to_numpy(ak_array[key])
        except ValueError:
            # Handle variable-length arrays (skip)
            # Skip variable-length arrays for this example
            print(f"Skipping field {key} due to variable-length subarrays")
    return pl.DataFrame(data_dict)

# Also time everything to identify bottlenecks. One bottleneck was to convert awkward to dictionaries and 
# then dictonaries to polars. This is a huge slowdown
start = time.time()

file = uproot.open(f"{ceph_path}:DecayTree")
opening = time.time()
print("Opening: ", opening - start)

ak_array = file.arrays(branches,library="ak")
conversion_awkward = time.time()
print("Conversion to awkward: ", conversion_awkward - opening)

pl_dataset = awkward_to_polars(ak_array)
polars_operations = time.time()
print("Conversion to polars: ", polars_operations - conversion_awkward)

complete = time.time()
print("Complete: ", complete - start)

def build_simple_data_cdf(
    sig_pars,
    mrange,
    comps=["sig", "comb"],
):
    """Closure to pdf to mass to minuit"""

    # build the structure for the data cdf
    def nominal_data_cdf(
        x,
        n_sig,
        n_comb,
        mug,
        sgg,
        sgl,
        sgr,
        lb,
    ):
        """Build the data composite cdf inheriting some fixed parameters from the MC fits"""

        # book the pdf for th expected components
        # ---------------------------------------
        # signal: DCB + gaussian, all share mu (freely floating)
        sig = dcbwg_cdf(
            x,
            sig_pars["f1"],
            sig_pars["f2"],
            mug,
            mug,
            mug,
            sgg,
            sgl,
            sgr,
            sig_pars["al"],
            sig_pars["ar"],
            sig_pars["nl"],
            sig_pars["nr"],
            mrange,
        )

        # combinatorial: exponential
        exp = expon(mrange[0], lb)
        comb = exp.cdf(x) / np.diff(exp.cdf(mrange))

        return n_sig * sig + n_comb * comb

    return nominal_data_cdf


def build_simple_data_pdf(
    sig_pars,
    mrange,
    comps,
):
    """Closure to pdf to mass to minuit"""

    # build the structure for the data cdf
    def data_pdf(
        x,
        n_sig,
        n_comb,
        mug,
        sgg,
        sgl,
        sgr,
        lb,
    ):
        """Build the data composite pdf inheriting some fixed parameters from the MC fits"""

        # book the pdf for th expected components
        # ---------------------------------------
        # signal: DCB + gaussian, all share mu (freely floating)
        sig = dcbwg(
            x,
            sig_pars["f1"],
            sig_pars["f2"],
            mug,
            mug,
            mug,
            sgg,
            sgl,
            sgr,
            sig_pars["al"],
            sig_pars["ar"],
            sig_pars["nl"],
            sig_pars["nr"],
            mrange,
        )
        
        # combinatorial: exponential
        exp = expon(mrange[0], lb)
        comb = exp.pdf(x) / np.diff(exp.cdf(mrange))

        # build the composite pdf
        tot = 0
        for comp in comps:
            if comp == "sig":
                tot += n_sig * sig
            elif comp == "comb":
                tot += n_comb * comb
            else:
                raise ValueError(f"Component {comp} not allowed in this simplified model")

        return tot

    return data_pdf