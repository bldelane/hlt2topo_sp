import yaml
import uproot4 as uproot
import pandas as pd
import awkward as ak
import polars as pl
import hist
from hist import Hist
import mplhep
import matplotlib.pyplot as plt
import scienceplots
import numpy as np
from scipy.stats import crystalball, norm
import time
from hlt2topo_sp.commissioning.RD_AP.analytical_models import dcbwg,dcbwg_cdf
from iminuit import Minuit
from iminuit.cost import UnbinnedNLL, ExtendedUnbinnedNLL, BinnedNLL, ExtendedBinnedNLL

plt.style.use(
    [
        "science",
        "no-latex",
    ]
)

def plot_fit(data, pdf, minute_model, bins_pdf, bins_data, range, name, path):
    fig, ax = plt.subplots()

    counts, edges = np.histogram(data, bins=bins_data, range=range)

    # plot data with hist
    histogram = Hist(
        hist.axis.Regular(bins_data, range[0], range[1]), # bins, start, stop, name
    ).fill(data)

    mplhep.histplot(histogram, label="2024_expected_conditions", color="black", ax=ax)

    # plot fit
    x = np.linspace(*range, bins_pdf)
    integr_factor = np.sum(counts) * (range[1] - range[0]) / bins_data
    ax.plot(
        x,
        pdf(x, *minute_model.values) * integr_factor,  # pdf x integral
        color="magenta",
        label="Fit",
    )
    ax.set_ylabel(
        r"Candidates / ({:2g} MeV$/c^2$)".format((range[1] - range[0]) / bins_data)
    )
    ax.legend()
    [plt.savefig(path / f"{name}_massfit.{ext}") for ext in ["png", "pdf"]]

    return None