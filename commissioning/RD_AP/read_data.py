import yaml
import uproot
import pandas as pd
import awkward as ak
import polars as pl
import hist
from hist import Hist
import mplhep
import matplotlib.pyplot as plt
import scienceplots

plt.style.use(
    [
        "science",
        "no-latex",
    ]
)

# Load in the data paths from the yaml file
with open("rd_2024_data_turbo.yaml", "r") as f:
    data_paths = yaml.safe_load(f)
with open("JPsiK_branches.yaml", "r") as f:
    branches = yaml.safe_load(f)

file = uproot.open(f"{data_paths[0]}:Hlt2RD_BuToKpJpsi_JpsiToMuMu/DecayTree")
ak_array = file.arrays(library="ak", expressions=branches)
data_dict = {key: ak_array[key].to_list() for key in ak_array.fields}
dataset = pl.DataFrame(data_dict)

# now make a histogram using hist and plot B_DTF_PV_B_PT as well as B_PT from the dataset
# This is a 1D histogram
histogram_1 = Hist(
    hist.axis.Regular(70, 0, 30_000, label="B_PT"), # bins, start, stop, name
).fill(dataset["B_PT"])
histogram_2 = Hist(
    hist.axis.Regular(70, 0, 30_000, label="B_DTF_PV_B_PT"), # bins, start, stop, name
).fill(dataset["B_DTF_PV_B_PT"])

fig = plt.figure()
mplhep.histplot(histogram_1, label="B_PT")
mplhep.histplot(histogram_2, label="B_DTF_PV_B_PT")
plt.ylabel("A.U.")
plt.xlabel(r"$B_{PT}$ [MeV/c]")
plt.legend()
plt.show()


histogram_1 = Hist(
    hist.axis.Regular(40, 5_000, 6_000, label="B_M"), # bins, start, stop, name
).fill(dataset["B_M"])
histogram_2 = Hist(
    hist.axis.Regular(50, 5_278, 5_287, label="B_DTF_PV_B_M"), # bins, start, stop, name
).fill(dataset["B_DTF_PV_B_M"])

fig = plt.figure()
#mplhep.histplot(histogram_1, label="B_M")
mplhep.histplot(histogram_2, label="B_DTF_PV_B_M")
plt.ylabel("A.U.")
plt.xlabel(r"$B_{M}$ [MeV/c]")
plt.legend()
plt.show()