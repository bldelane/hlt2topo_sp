import yaml
import uproot4 as uproot
import pandas as pd
import awkward as ak
import polars as pl
import hist
from hist import Hist
import mplhep
import matplotlib.pyplot as plt
import scienceplots
import numpy as np
from scipy.stats import crystalball, norm
import time
from hlt2topo_sp.commissioning.RD_AP.analytical_models import dcbwg,dcbwg_cdf
from iminuit import Minuit
from iminuit.cost import UnbinnedNLL, ExtendedUnbinnedNLL, BinnedNLL, ExtendedBinnedNLL
from hlt2topo_sp.commissioning.RD_AP.plotting import plot_fit


# load in the yaml file with all the branches 
with open("JPsiK_oldMC_columns.yaml", "r") as f:
    branches = yaml.safe_load(f)

# this will all be made automatic, but it is to get a first fit going
# read in the MC files for both polarities
ceph_path = "/ceph/users/nschulte/RD_JPsiK_BR/MC/Bu2JpsiK/2024/"

# Since there is now way to to pass the polars library to uproot directly, write a conversion
# function to convert the awkward array to numpy. Some variables have substructures that 
# break this conversion, so we will skip those for now. Flattening does not work
def awkward_to_polars(ak_array):
    data_dict = {}
    for key in ak_array.fields:
        try:
            # Try converting to NumPy (works for regular arrays)
            data_dict[key] = ak.to_numpy(ak_array[key])
        except ValueError:
            # Handle variable-length arrays (skip)
            # Skip variable-length arrays for this example
            print(f"Skipping field {key} due to variable-length subarrays")
    return pl.DataFrame(data_dict)

dataset = []
# Also time everything to identify bottlenecks. One bottleneck was to convert awkward to dictionaries and 
# then dictonaries to polars. This is a huge slowdown
start = time.time()
for polarity in ["MagUp", "MagDown"]:
    file = uproot.open(f"{ceph_path}{polarity}/data.root:Bu2JpsiK_Tuple/DecayTree")
    opening = time.time()
    print("Opening: ", opening - start)

    ak_array = file.arrays(branches,library="ak")
    conversion_awkward = time.time()
    print("Conversion to awkward: ", conversion_awkward - opening)

    pl_dataset = awkward_to_polars(ak_array)
    dataset.append(pl_dataset)

    polars_operations = time.time()
    print("Conversion to polars: ", polars_operations - conversion_awkward)

dataset = pl.concat(dataset)
complete = time.time()

print("Complete: ", complete - start)

# now define the fit functions. We chose double sided chrystal ball funtion with gaussian for the signal
def run_MC_fit(
    data,
    mrange: tuple = (5_000, 5_300),
    verbose: bool = True,
    fit_obs: str = "B_M",
    f1: float = 0.106, #fraction of the Gaussian component
    f2: float = 0.878, #fraction of the left-sided Crystal Ball component
    mug: float = 5_230,  # NOTE: shared in dcbwg; mean of the Gaussian component
    sgl: float = 18.13, # standard deviation of the left-sided Crystal Ball component.
    sgr: float = 43.5, #standard deviation of the right-sided Crystal Ball component.
    sgg: float = 28.3, # standard deviation of the Gaussian component
    al: float = 0.388, # transition point from Gaussian to the power-law tail on the left side.
    ar: float = 0.4 , # The transition point from Gaussian to the power-law tail on the right side
    nl: float = 2.244,  # slope of the power-law tail on the left side
    nr: float = 6,  # slope of the power-law tail on the right side
    f1_limits: tuple = (0, 0.3),
    f2_limits: tuple = (0.7, 1),
    mug_limits: tuple = (5_230 - 10, 5_230 + 10),
    al_limits: tuple = (0, 1),
    ar_limits: tuple = (0, 0.3),
    nl_limits: tuple = (0, 7),
    nr_limits: tuple = (5, 10),
    sgl_limits: tuple = (15, 25),
    sgr_limits: tuple = (40, 45),
    sgg_limits: tuple = (25, 30),
):
    """Execut the ML fit to extract the DCB tail parameters from a fit to signal MC

    Returns
    -------
    FitObj(data, model, result): namedtuple
        container of the fit results
    """
    sig_pdf = lambda x, f1, f2, mug, sgg, sgl, sgr, al, ar, nl, nr: dcbwg(
        x, f1, f2, mug, mug, mug, sgg, sgl, sgr, al, ar, nl, nr, mrange
    )
    sig_cdf = lambda x, f1, f2, mug, sgg, sgl, sgr, al, ar, nl, nr: dcbwg_cdf(
        x, f1, f2, mug, mug, mug, sgg, sgl, sgr, al, ar, nl, nr, mrange
    )

    nh, xe = np.histogram(data, range=mrange, bins=400)

    # negative unbinned log-likelood
    cost = BinnedNLL(
        nh, xe, sig_cdf
    )  # note: we go binned owing to the high number of pars

    # book the minimizer
    m = Minuit(
        cost,
        # init parameters
        f1=f1,
        f2=f2,
        mug=mug,
        al=al,
        ar=ar,
        nl=nl,
        nr=nr,
        sgg=sgg,
        sgl=sgl,
        sgr=sgr,
    )

    # limits, all floating
    m.limits["f1"] = f1_limits
    m.limits["f2"] = f2_limits
    m.limits["mug"] = mug_limits
    m.limits["al"] = al_limits
    m.limits["ar"] = ar_limits
    m.limits["nl"] = nl_limits
    m.limits["nr"] = nr_limits
    m.limits["sgl"] = sgl_limits
    m.limits["sgr"] = sgr_limits
    m.limits["sgg"] = sgg_limits

    # minimise
    m.migrad()

    # error computation - https://scikit-hep.org/iminuit/notebooks/hesse_and_minos.html
    m.hesse()

    # print the fitted parameters and fit status
    if verbose:
        print(m)

    return m, data, sig_pdf

m, data, sig_pdf = run_MC_fit(dataset["B_M"])

plot_fit(data, sig_pdf, m, 400, 50, (5_000, 5_300), "2024_expected_conditions_MC", "plots")

print("The minuit params are:", m.values)