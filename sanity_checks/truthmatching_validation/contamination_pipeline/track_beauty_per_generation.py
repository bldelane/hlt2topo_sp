import numpy as np
import pandas as pd
import awkward as ak
import os
import sys
import argparse
import time
import uproot
import pathlib
from pathlib import Path

from warnings import simplefilter
simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

from helpers.truthmatch_branches import twobody_truthmatch_branches, threebody_truthmatch_branches
from functionalities.process_data import process_file

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[3]}")
# Now you can import from the topo module
from topo import yml_to_dict, GhostFinder

if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--channel", help="Decay channel", required=True)
    parser.add_argument("-i", "--input", help="Inputf file path", required=True)
    parser.add_argument(
        "-o",
        "--output",
        help="Full path to output file (including filename)",
        required=True,
    )
    parser.add_argument("--from_eos", action="store_true", help="Read from EOS")
    opts = parser.parse_args()

    config = yml_to_dict("config.yaml")

    # match the appropriate branches to the key
    match config["key"]:
        case "TwoBody":
            branches = twobody_truthmatch_branches
            particles = ["Track1", "Track2"]
        case "ThreeBody":
            branches = threebody_truthmatch_branches
            particles = ["Track1", "Track2", "TrackB"]
        case _:
            raise ValueError(f"Unknown key {config['key']}")

    max_entries = config["max_entries"]

    # Time the execution time
    start = time.time()

    print("starting")
    # read in the data
    # ----------------
    data = uproot.open(f"{opts.input}:{config['key']}/DecayTree", timeout=10_000)

    print("Data opened")
    dataset = data.arrays(
        expressions=branches,
        cut=None,
        library="ak",
        entry_stop=max_entries,
    )
    print(f"Success: loaded {len(dataset)} candidates")

    if opts.from_eos:
        ghost_finder = GhostFinder()
        dataset["GhostTag"] = ghost_finder.check_ghosts(particles=particles, sample=dataset) 
        dataset = dataset[~dataset["GhostTag"]]

    # Check that there are no Ghosts in there anymore
    assert (
        ak.sum(dataset["GhostTag"]) == 0
    ), "Ghost removal failed; non-zero ghost tag detected"

    # Call the truthmatching per generation function and store the results
    # --------------------------------------------------------------------

    results = process_file(df=dataset, key=config["key"], source_type=config["source_type"], fs_tracks=particles)
    
    # Define the output directory
    output_dir = pathlib.Path(opts.output)
    # Create the directory structure if it doesn't exist
    output_dir.mkdir(parents=True, exist_ok=True)

    # Print the results
    print("The results for this file are:", results)
    print("Writing results into file")

    # Open a text file for writing
    output_file = output_dir / "result_per_generation.txt"
    with open(output_file, 'w') as file:
        # Iterate over the array and write each element to the file
        for element in results:
            file.write(str(element) + '\n')  # Write each element to a new line
