import numpy as np
import pandas as pd
import awkward as ak
from abc import ABC, abstractmethod
from particle.pdgid import has_bottom, has_charm

from warnings import simplefilter
simplefilter(action="ignore", category=pd.errors.PerformanceWarning)


class BaseMatching(ABC):
    """Abstract Base Class for truth-matching a (part-reco) B decay"""

    def __init__(self, source_type, sample, fs_tracks: list, num_generations: int = 14):
        # the source type here corresponds to the kind of matching we want to perform. Do we want to match to a B hadron or investigate charm contributions?
        if source_type not in ["fromB", "fromD"]:
            raise ValueError("Invalid source type provided.")

        self.source_type = source_type
        self._sample = sample
        self._pids = self.pdgids()[self.source_type]
        self.fs_tracks = fs_tracks
        self.num_generations = num_generations


    @classmethod
    def pdgids(cls):
        return {
            "fromB": [
                511,
                521,
                531,
                541,
                5122,
                5132,
                5232,
                5212,
            ],  # B0, B+, Bs_0, Bc+, Lambdab0, Xib-, Xib0, Sigb0
            "fromD": [411, 421, 431, 4122, 4132],  # D+, D0, Ds+, Lambdac+, Xic0
        }

    @property
    def source_pdgids(self):
        return self.pdgids()[self.source_type]

    @property
    def pids(self):
        return self._pids

    @property
    def sample(self):
        # take in the dataset. If pandas DataFrame, convert to akward array object
        if isinstance(self._sample, pd.DataFrame):
            return ak.Array(self._sample.to_dict(orient="list"))
        else:
            return self._sample
        
    def generate_suffixes(self):
        suffixes = [f"MC_GD_MOTHER_{i}_ID" for i in range(2, self.num_generations + 1)]
        if self.num_generations > 1:
            return ("MC_MOTHER",) + tuple(suffixes)
        else:
            return ("MC_MOTHER",)

    @abstractmethod
    def heavy_flavour_match(self, *args):
        """Establish whether a beauty/charm ancestor is common to all tracks"""
        pass

    @abstractmethod
    def exact_heavy_flavour_match(self, *args):
        pass

    def is_from_source_pdgids(self, val):
        return abs(val) in self.source_pdgids

    def __str__(self) -> str:
        return f"Source type: {self.source_type}; "
    
class CompositeMatching(BaseMatching):
    """Truth-matcher accounting for a (part-reco) B -> 2-/3-track decay"""

    def __init__(self, source_type, sample, fs_tracks: list, num_generations: int = 14):
        super().__init__(source_type, sample, fs_tracks)
        self.num_generations = num_generations

    def check1d_shape(self) -> bool:
        """The ID variables should be 1D, not nested. This allows for numpy functionality"""
        suffixes = ["ID", "KEY"]
        are_all_1d = all(
            self.sample[f"{particle}_{ancestor}_{suffix}"].ndim == 1
            for particle in self.fs_tracks
            for ancestor in [
                "MC_MOTHER"] + [f"MC_GD_MOTHER_{i}" for i in range(2, self.num_generations + 1)]
            for suffix in suffixes
        )
        if not are_all_1d:
            raise ValueError("Not all arrays are 1D")



    def heavy_flavour_match(self) -> bool:
        """Execute beauty matching: one common heavy-flavour ancestor among the two tracks"""

        # check that the ID arrays are 1D
        self.check1d_shape()

        # Convert the PIDs to a NumPy array to unlock np.isin functionality
        numpy_pids = np.array(self.pids)

        # Now use NumPy's isin function to store an n-particle array where each encodes if at leats one per-particle ancestor is beauty-ful
        per_track_beauty_ancestor = [
            np.isin(
                np.abs(
                    np.array(
                        [
                            self.sample[f"{particle}_MC_MOTHER_ID"],
                            *[self.sample[f"{particle}_MC_GD_MOTHER_{i}_ID"] for i in range(2, self.num_generations + 1)],                                            
                        ]
                    )
                ),
                numpy_pids,
            ).any(
                axis=0
            )  # Check if any condition per particle is True across all MCID conditions
            for particle in self.fs_tracks
        ]  # this establishes if a beauty ancestor is present in the ancestry of each track, separately
        # Now check if the beauty ancestor is common to all final-state tracks
        return ak.all(ak.Array(per_track_beauty_ancestor), axis=0)


    def exact_heavy_flavour_match(self) -> bool:
        """Execute beauty matching: one common heavy-flavour ancestor among the tracks
        NOTE: this requires that the exact same candidate - tagged by its key - is present in the ancestry of all tracks
        """
        # Check that the ID arrays are 1D
        self.check1d_shape()

        # Convert the PIDs to a NumPy array to unlock np.isin functionality
        numpy_pids = np.array(self.pids)

        per_track_keys = {}

        for particle in self.fs_tracks:
            # Simply collect per track the ancestor IDs and keys
            ancestry = []
            keys = []
            suffixes = [f"MC_MOTHER"] if self.num_generations == 1 else [f"MC_GD_MOTHER_{i}" for i in range(2, self.num_generations + 1)]
            for suffix in suffixes:
                # Append the ID
                ancestry.append(np.array(self.sample[f"{particle}_{suffix}_ID"]))
                # Append the candidate key
                keys.append(np.array(self.sample[f"{particle}_{suffix}_KEY"]))

            ancestry = np.abs(np.array(ancestry))  # Absolute value of the IDs
            per_track_ancestry = ancestry.T  # Transpose to get per-track ancestry

            # Book particle x keys
            per_track_ancestral_keys = np.array(
                keys
            ).T  # Transpose to get per-track keys

            # Now check if the ancestors are beauty; true if per-particle at least one ancestor is beauty
            beauty_ancestral_mask = np.isin(per_track_ancestry, numpy_pids)

            # Now mask the keys: if it is beauty, keep it, otherwise set to the unique FS track identifier
            per_track_keys[particle] = np.where(
                beauty_ancestral_mask, per_track_ancestral_keys, particle
            )

        # Per candidate, concatenate the keys
        flattened_keys = np.concatenate(list(per_track_keys.values()), axis=1)

        # FIXME: It would be great to parallelize this or use numba to accelerate it
        bool_same_keys = []
        for flattened in flattened_keys:
            unique_values, counts = np.unique(
                flattened[~np.isin(flattened, self.fs_tracks)], return_counts=True
            )  # Count if there is any repeated numerical key value
            bool_same_keys.append(
                np.any(counts == len(self.fs_tracks))
            )  # One common key per track

        return bool_same_keys
    
    def detect_heavy_flavour(self) -> bool:
        """One heavy flavour ancestor is sufficient to return True
        NOTE: designed such at least one heavy flavour causes the candidate to be vetod in the minbias
        """
        # check that the ID arrays are 1D
        self.check1d_shape()

        # Convert the PIDs to a NumPy array to unlock np.isin functionality
        numpy_pids = np.array(self.pids)

        # Now use NumPy's isin function to store an n-particle array where each encodes if at leats one per-particle ancestor is beauty-ful
        per_track_beauty_ancestor = [
            np.isin(
                np.abs(
                    np.array(
                        [
                            self.sample[f"{particle}_MC_MOTHER_ID"],
                            *[self.sample[f"{particle}_MC_GD_MOTHER_{i}_ID"] for i in range(2, self.num_generations + 1)],    
                        ]
                    )
                ),
                numpy_pids,
            ).any(
                axis=0
            )  # Check if any condition per particle is True across all MCID conditions
            for particle in self.fs_tracks
        ]  # this establishes if a beauty ancestor is present in the ancestry of each track, separately

        # if there is a heavy flavour ancestor at all, return True
        return ak.any(ak.Array(per_track_beauty_ancestor), axis=0)
    
class TwoBodyMatching(CompositeMatching):
    def __init__(self, source_type, sample, fs_tracks: list):
        super().__init__(source_type, sample, fs_tracks)
        assert (
            len(self.fs_tracks) == 2
        ), "TwoBodyMatching only supports two-body decays"  # like to access final-state track names as class attributes; a good place for sanity test: only two allowed

    def __str__(self) -> str:
        return f"TwoBody truth-matcher class (source type: {self.source_type});\nallowed pids: {self.pids};\nfinal-state tracks: {self.fs_tracks}\n"


class ThreeBodyMatching(CompositeMatching):
    def __init__(self, source_type, sample, fs_tracks: list):
        super().__init__(source_type, sample, fs_tracks)
        assert (
            len(self.fs_tracks) == 3
        ), "ThreeBodyMatching only supports two-body decays"  # like to access final-state track names as class attributes; a good place for sanity test: only two allowed

    def __str__(self) -> str:
        return f"ThreeBody truth-matcher class (source type: {self.source_type});\nallowed pids: {self.pids};\nfinal-state tracks: {self.fs_tracks}\n"


# Factory to call the right matching class
def get_truth_matcher(key: str, source_type, sample: ak.Array, fs_tracks: list):
    """Fetch the correct truth matcher class based on the trigger type (key)

    Parameters
    ----------
    key: str
        The trigger key ['TwoBody', 'ThreeBody']
    sample: awkward array
        The input sample
    source_type:
        The source type, ie what is the parent particle type ['fromB', 'fromD']

    Returns
    -------
    matcher: BaseMatching
        The correct truth matcher class
    """
    match key:
        case "TwoBody":
            return TwoBodyMatching(
                sample=sample, source_type=source_type, fs_tracks=fs_tracks
            )
        case "ThreeBody":
            return ThreeBodyMatching(
                sample=sample, source_type=source_type, fs_tracks=fs_tracks
            )
        case _:
            raise ValueError("Invalid matcher type")
