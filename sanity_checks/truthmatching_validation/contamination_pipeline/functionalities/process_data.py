import numpy as np
import pandas as pd
import awkward as ak
import os
import sys
from abc import ABC, abstractmethod
from particle.pdgid import has_bottom, has_charm
import os
import uproot

from warnings import simplefilter
simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

from functionalities.truthmatch_per_gen import get_truth_matcher

def process_file(df, key, source_type, fs_tracks):

    """
    Function to process the data for each root file. Here, we will loop over each generation in the data and perform the truthmatching. This way, we can track how beauty statistics develops over increasing generation in truthmatching
    """

    #df = load_truthmatched_data_from_folder(root_file_path, line)
    results = []

    #Since there are 15 generations, loop from 1 to 15
    for num_generations in range(1, 15):
        print("Looking at generation up to generation: ", num_generations)
        
        #Get the correct truthmatcher object
        matcher = get_truth_matcher(key=key, source_type=source_type, sample=df, fs_tracks=fs_tracks)
        
        #Set the number of generations to look at
        matcher.num_generations = num_generations

        #Get the amount of beauty matches and exact beauty matches
        heavy_flavour_match_result = matcher.heavy_flavour_match()
        exact_heavy_flavour_match_result = matcher.exact_heavy_flavour_match()

        #Since the match is a boolean, get the sum to get the total number of True matches
        num_heavy_flavour_matches = sum(heavy_flavour_match_result)
        num_exact_heavy_flavour_matches = sum(exact_heavy_flavour_match_result)

        #Print a report about the statistics
        print(f"Heavy flavor matches in generation {num_generations}: ", num_heavy_flavour_matches)
        print(f"Exact heavy flavor matches in generation {num_generations}: ", num_exact_heavy_flavour_matches)
        print(f"Total number of candidates in dataframe: ", len(df))

        #how many generations, how many beauty matches per track, how many exact beauty matches, how long is the dataframe in general
        results.append((num_generations, num_heavy_flavour_matches, num_exact_heavy_flavour_matches, len(df))) 

    return results

