"""Implement the scalers used to preprocess the training and testing datasets"""

from typing import Union, List, Any
from abc import ABC, abstractmethod
import pandas as pd
import awkward as ak
from numpy.typing import ArrayLike
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from .plot import twoclass_plot

# plt.style.use(["science", "high-vis"])

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


class Transformer(ABC):
    """Abstract base class for preprocessor objects.
    Define the plotting method inherited by the child classes
    """

    def __init__(
        self,
        dataset: Union[ak.Array, ArrayLike],
        features: List[str],
        key: str,
    ) -> None:
        self._dataset = dataset
        self._features = features
        self._key = key

    @property
    def dataset(self) -> Union[ak.Array, ArrayLike]:
        return self._dataset

    @property
    def features(self) -> List[str]:
        return self._features

    @property
    def key(self) -> str:
        return self._key

    @abstractmethod
    def transform(self, *args: Any, **kwds: Any) -> Union[ak.Array, ArrayLike]:
        """Preprocess the dataset"""
        pass
