"""Tranform observables to produce features for the classifier."""

from abc import ABC, abstractmethod
from numpy.typing import ArrayLike
import numpy as np

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


# scalers
# -------
class Scaler(ABC):
    """An abstract base class for all scalers."""

    @abstractmethod
    def fit(self, feature: ArrayLike) -> ArrayLike:
        """Fit the scaler to the data."""
        pass


class LogScaler(Scaler):
    """A scaler that applies a log transform to the data."""

    def fit(self, feature: ArrayLike) -> ArrayLike:
        """Fit the scaler to the data."""

        # mask away the 0- values
        feature[feature <= 0] = 1e-5

        return np.log(feature)


class GeVScaler(Scaler):
    """A scaler that recasts in units of GeV by dividing by 1_000."""

    def fit(self, feature: ArrayLike) -> ArrayLike:
        """Fit the scaler to the data."""
        return feature / 1_000.0


# interface
# ---------
class ScalerFactory:
    """
    Factory providing the desidered per-variable scaling method.
    The factory doesn't maintain the instances of the objects it creates [source: ArjanCodes]
    """

    def get_scaler(self, format: str) -> Scaler:
        """Return the scaler object."""
        if format == "Log":
            return LogScaler()
        elif format == "GeV":
            return GeVScaler()
        else:
            raise ValueError(f"Scaler {format} not recognised.")
