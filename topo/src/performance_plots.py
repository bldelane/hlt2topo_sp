import numpy as np
import hist
import matplotlib.pyplot as plt
import scienceplots
from typing import Union, Tuple, Dict
from numpy.typing import ArrayLike
import pandas as pd
import mplhep as hep
import warnings
from functools import partial
import pathlib

# plt.style.use(["science"])

# response working points
cosmetics = {
    "0.85": {
        "color": "#0571b0",
        "fmt": ".",
        "label": r"$\hat{y} > 0.75$",
        "markersize": 4,
    },
    "0.90": {
        "color": "#92c5de",
        "fmt": ".",
        "label": r"$ \hat{y} > 0.90$",
        "markersize": 4,
    },
    "0.95": {
        "color": "#f4a582",
        "fmt": ".",
        "label": r"$\hat{y} > 0.95$",
        "markersize": 4,
    },
    "0.99": {
        "color": "#ca0020",
        "fmt": ".",
        "label": r"$\hat{y} > 0.99$",
        "markersize": 4,
    },
}


def event_count(
    data: Union[ArrayLike, pd.DataFrame],
    event_branch: str = "unique_event",  # identifier accounting for multiple beauty candidates, sharing the same event number, but only within the same run number
) -> int:
    """Count how many _events_ in the sample

    NOTE: multiple topo candidates are expected per event with
    nonzero beauty hadronisation within LHCb acceptance
    """
    assert data.class_label.all() == 0, "Sanity check: event count must be on minbias"
    if isinstance(data, pd.DataFrame):
        return len(data[event_branch].unique())
    else:
        return len(np.unique(data[event_branch]))


def rate_optimised_resp_cut(
    minbias_data: Union[ArrayLike, pd.DataFrame],
    prediction_branch: str = "preds_per_cand",
    input_rate: int = 1e6,  # Hz
    output_rate: int = 1e4,  # Hz
    tolerance: float = 1e-2,
) -> float:
    """Find the optimal response cut to achieve the nominal HLT2 throughput"""
    assert (
        prediction_branch in minbias_data.columns
    ), f"KeyError: data must have {prediction_branch} column"

    # how many events can be let through
    suppression_factor = output_rate / input_rate

    # denominator
    den = event_count(minbias_data)

    # identify the lowest cut satisfying the condition
    nominal_resp_cut = 0.0  # placeholder
    for t in np.arange(0.95, 1.00, 0.0001):
        num = event_count(minbias_data[minbias_data[prediction_branch] > t])
        print(
            f"""        
        ------------------
        cut: {t},
        num: {num},
        den: {den},
        num/den (:= I/O rate ratio): {num/den:.4f},
        suppression_factor (:= target rate ratio) :  {suppression_factor:.4f},
        tolerance: {tolerance},
        num/den - suppression_factor: {np.abs(num/den - suppression_factor)}
        ------------------\n"""
        )
        if np.abs(num / den - suppression_factor) <= tolerance:
            print("success")
            nominal_resp_cut = t
            print("Success: performance delivered at response cut: ", nominal_resp_cut)
            break

    return nominal_resp_cut


def to_hist(
    data: ArrayLike,
    edges: ArrayLike,
    weights: Union[ArrayLike, None] = None,
    maxNorm: bool = False,  # normalise by max bin
    sumNorm: bool = False,  # normalise by sum(bins)
) -> Tuple[hist.Hist, ArrayLike, ArrayLike, ArrayLike, ArrayLike]:
    """Returns hist, edges, centers, bin height, bin err"""

    # fill non-nromalised hist
    if weights is None:
        h = hist.Hist(hist.axis.Variable(edges, name="obs"))
        h.fill(data)
        nh = h.view()
        err = h.view() ** 0.5  # poisson error

    if weights is not None:
        h = hist.Hist(
            hist.axis.Variable(edges, name="obs"), storage=hist.storage.Weight()
        )
        h.fill(data, weight=weights)
        nh = h.view().value
        err = h.view().variance ** 0.5  # weighted errro

    # same centers, either case
    xe = h.axes[0].edges
    cx = h.axes[0].centers
    frac_err = err / nh

    if maxNorm:
        integral = np.max(nh)
        nh = nh / integral
        err = err / integral
        scaled_frac_err = err / nh
        # assert np.max(nh) == 1 # HACK: removed for now
        # assert frac_err.all() == scaled_frac_err.all() # HACK: removed for now

    if sumNorm:
        integral = np.sum(nh)
        nh = nh / integral
        err = err / integral
        scaled_frac_err = err / nh
        # assert frac_err.all() == scaled_frac_err.all() # HACK: removed for now

    return h, xe, cx, nh, err


def std_eff(_pass: float, _tot: float) -> float:
    """Standard efficiency error calculation"""
    # from Paterno, sec 2.2 - https://lss.fnal.gov/archive/test-tm/2000/fermilab-tm-2286-cd.pdf
    return (1 / _tot) * np.sqrt(_pass * (1 - _pass / _tot))


def plot_cut_impact(
    df: pd.DataFrame,
    cut_dict: Dict,  # {cut: {color, fmt, label, markersize}}
    branch: str,
    _edges: ArrayLike,
    _xlabel: str,
    _lambda: float,
    cosmetics_dict: Union[Dict, None] = None,
    class_label: int = 1,
    outputpath: Union[str, None] = None,
    show_rate_info: bool = False,
    channel_label: Union[str, None] = None,
) -> None:
    """Plot efficiency of a branch in a dataframe"""
    # get the signal
    sig = df[df.class_label == class_label]

    # upper extremum of edges
    EDGES = _edges  # + [df[branch].max()]

    # book the plot and canvas
    fig, ax = plt.subplots()
    H, xe, cx, nh, err = to_hist(
        data=sig[branch].to_numpy(), edges=EDGES, maxNorm=True
    )  # -> variable edges & normalise to maximal bin for the efficiency plot overlay

    ax2 = ax.twinx()
    ax2.set_ylabel("Arbitrary Units", color="grey")
    ax2.tick_params(axis="y", colors="grey", which="both")
    hep.histplot(nh, bins=xe, ax=ax2, color="lightgrey", alpha=0.75, histtype="fill")

    for nncut in list(cut_dict.keys()):
        h_sel, _, _, _, _ = to_hist(
            data=sig[sig.preds_per_cand > float(nncut)][branch].to_numpy(), edges=EDGES
        )

        _cosmetics = cosmetics_dict if cosmetics_dict is not None else cosmetics

        ax.errorbar(
            x=H.axes[0].centers,
            # NaN mapped to 0
            y=np.nan_to_num(
                h_sel.project("obs").view() / H.project("obs").view() * 100.0
            ),
            xerr=H.axes[0].widths / 2.0,
            yerr=std_eff(
                _pass=h_sel.project("obs").view(), _tot=H.project("obs").view()
            )
            * 100.0,
            elinewidth=1,
            # capsize=3,
            # markeredgewidth=1.5,
            **_cosmetics[nncut],
        )

    # default legend configuration
    leg_alignment = {
        "loc": "center",
        "bbox_to_anchor": (0.5, -0.4),
    }
    order = [0, 1, 2]

    # custom text to show the input and output rates
    if show_rate_info is True:
        ax.plot([], [], "", color="white", label=r"Input rate: 1 MHz")
        ax.plot([], [], "", color="white", label=r"Output rate: 30 kHz")
        ax.plot(
            [], [], "", color="white", label=r"Negligible preprocessing time assumed"
        )
        order = [0, 1, 2, 5, 4, 3]

    ax.set_zorder(ax2.get_zorder() + 1)
    ax.patch.set_visible(False)
    ax.axhline(100, color="black", ls=":", lw=1)
    ax.set_xlabel(_xlabel)
    ax.set_ylabel(r"Selection efficiency, $\varepsilon$ $[\%]$")
    ax.set_title(r"LHCb Simulation", loc="left")
    ax2.set_title(r"$\lambda = %s$" % (_lambda), loc="right")

    # legend, with custom order
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(
        [handles[idx] for idx in order],
        [labels[idx] for idx in order],
        ncol=2,
        **leg_alignment,
    )

    ax.set_ylim(bottom=0, top=105)
    ax2.set_ylim(bottom=0, top=1.05)

    # overwrite the default y-axis range and add channel label
    if channel_label is not None:
        ax2.set_ylim(bottom=0, top=1.175)
        ax.set_ylim(bottom=0, top=117.5)

        # custom placement of tex label
        _decay_x = 0.4
        if channel_label == "D0Ds":
            _decay_x = 0.2
        if channel_label == "LcMuNu":
            _decay_x = 0.4
        ax.text(
            _decay_x,
            0.93,
            channel_label,
            horizontalalignment="center",
            verticalalignment="center",
            transform=ax.transAxes,
            fontsize=11.5,
        )

    pathlib.Path(outputpath).parent.absolute().mkdir(parents=True, exist_ok=True)
    [plt.savefig(f"{outputpath}_{branch}.{ext}") for ext in ("pdf", "png")]


# proxy comparison plots
plot_obs_eff = partial(plot_cut_impact, cut_dict=cosmetics)


def extract_minbias(data_path: str, class_label: int = 0) -> pd.DataFrame:
    """Read in a test sample and extract the minbias events"""
    # load the data
    data = pd.read_pickle(data_path)  # NOTE: assumes pkl in pipeline

    # extract the minbias
    return data[data.class_label == class_label]


def plot_rate_aware_eff(
    probe_data: pd.DataFrame,
    minbias_path: str,  # NOTE: load the minbias data from a file to determine the rate-aware cut
    _hlt2_output_rate: float = 2.5e4,
    **kwargs,
) -> None:
    """Closure to extract the rate-aware response cut"""

    # compute WP on minbias rate reduction: 1MHz -> 10kHz
    # ----------------------------------------------------
    # set the minbias HLT1-filtered
    wp_on_minbias = partial(
        rate_optimised_resp_cut, minbias_data=extract_minbias(minbias_path)
    )

    # extract the working points (WPs)
    nominal = wp_on_minbias(output_rate=_hlt2_output_rate)
    twice_rate = wp_on_minbias(output_rate=_hlt2_output_rate * 2)
    half_rate = wp_on_minbias(output_rate=_hlt2_output_rate / 2)

    # plot the efiiciency on rate-determined WPs on probe beauty decay channel
    rate_aware_cut_dict = {
        f"{twice_rate:.2f}": {
            "label": "Twice nominal",
            "color": "#a50026",
            "fmt": "^",
            "markersize": 3,
        },
        f"{half_rate:.2f}": {
            "label": "Half nominal",
            "color": "#f46d43",
            "fmt": "x",
            "markersize": 4,
        },
        str(nominal): {
            "label": "Nominal rate",
            "color": "#4575b4",
            "fmt": ".",
            "markersize": 4,
        },
    }

    return plot_cut_impact(
        df=probe_data,
        cut_dict=rate_aware_cut_dict,
        cosmetics_dict=rate_aware_cut_dict,
        **kwargs,
    )
