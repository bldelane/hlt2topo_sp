"""Export the pytorch model to JSON for the stack"""

from typing import Dict, Any
import numpy as np
import torch
import json
from monotonenorm import get_normed_weights
from .train import get_device, get_model, model_arch_optim
from .utils import make_chunks

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


def assign_constrains(constrain_path: str, state_dict: Dict) -> Dict:
    """Pass the constrains from the M-scaling to the state_dict

    Parameters
    ----------
    path : str
        path to the location of the model
    nbody : str
        according to the key specified in the config file: Twobody/ThreeBody
    lip: float
        lipschitz constant corresponding to the used model
    state_dict
        state_dict of the pytorch model that needs to be modified
    Returns
    -------
    Dict
        modified state_dict model
    """
    variables_constraints = []

    with open(f"{constrain_path}", "r") as file:
        next(file)  # skip the first line as it is an information statement
        for line in file:
            if ":" not in line:  # get the lines with min: value and max: value
                continue
            values = line.replace("\n", "").split(":")[
                -1
            ]  # make stringsplit for values
            values = float(values)  # convert values from string to floats
            variables_constraints.append(values)

    variables_constraints = list(
        make_chunks(variables_constraints, 2)
    )  # chunk it into lists with size 2: (min, max)

    print(variables_constraints)
    state_dict.update({"variables_constraints": variables_constraints})
    state_dict.move_to_end(
        "variables_constraints", last=False
    )  # Need to be at the beginning of the file

    return state_dict


def assign_weights(state_dict: Dict, lip: float) -> Dict:
    """Norm the weights of the state_dict

    Parameters
    ----------
    state_dict
        state_dict of the pytorch model that needs to be modified
    Returns
    -------
    Dict
        modified state_dict model
    """
    weight_keys = [x for x in state_dict if "weight" in x]

    depth = len(weight_keys)
    print(depth)
    assert depth > 0

    #NOTE: Do not assign weights again (and norm then) as it is already done in model_arch_optim when loading the model
    for k in state_dict:
        if k in weight_keys:
            state_dict[k] = get_normed_weights(
                state_dict[k],
                always_norm=False,
                kind="one",  # NOTE:We should move this to the config everywhere
                max_norm=lip ** (1.0 / depth),  
                vectorwise=True,
            ).tolist()
        else:
            state_dict[k] = state_dict[k].tolist()

    for weight_matrix in weight_keys:
        sum_of_weights = [sum(column) for column in state_dict[weight_matrix]]
        # assuming that everything worked correctly, the absolute sum of a column in the weights
        # matrix should never superseed one
        for column_sum in sum_of_weights:
            print(f"{weight_matrix} sum: {abs(column_sum)} \n")
            # assert abs(column_sum) <= 1

    return state_dict


def assign_sigmanet_label(state_dict: Dict) -> Dict:
    """Rename keys according to stack requirements, beware of OrderedDict

    Parameters
    ----------
    state_dict
        state_dict of the pytorch model that needs to be modified
    Returns
    -------
    Dict
        modified state_dict model
    """

    dict_keys = list(state_dict.keys())
    for key in dict_keys:
        state_dict["sigmanet." + key] = state_dict[key]
        del state_dict[key]

    return state_dict

# requires load_model to still
def load_model(
    modelpath: str, nn_config: Any, params_dict: dict | None = None, export: bool = True
) -> Dict:
    """Load the pytorch model and extract the state_dict for modifications

    Parameters
    ----------
    path : str
        path to the location of the model
    nbody : str
        according to the key specified in the config file: Twobody/ThreeBody
    nn_config : Any
        Config according to the pytorch model/ Robustness/Monotonicity etc. Should be retrieved from config.yaml

    Returns
    -------
    Dict
        state_dict of the pytorch model
    """
    device = get_device()

    if params_dict is not None:
        # enable flexibility of the NN architectue
        with open(params_dict, "r") as f:
            model_spec = json.load(f)

        MODEL = model_arch_optim(
            params_dict=model_spec,
            robust=nn_config["robust"],
            monotonic=nn_config["monotonic"],
            _nbody=nn_config["_nbody"],
            _features=nn_config["_features"],
            LIP=nn_config["LIP"],
            config_path=nn_config["config_path"],
        ).to(device)
    else:
        MODEL = get_model(**nn_config).to(device)

    location = f"{modelpath}"
    if device == torch.device("cpu"):
        MODEL.load_state_dict(
            torch.load(location, map_location=torch.device("cpu"))
        )  # Map Location needed if running on cpu
    else:
        MODEL.load_state_dict(torch.load(location))
    state_dict = MODEL.state_dict()

    if export:
        state_dict = assign_weights(state_dict=state_dict, lip=nn_config["LIP"])
    return state_dict
