"""Create all functions for the training"""

import pandas as pd
from typing import Tuple, Any, Union, Dict, List, Any
import numpy as np
from monotonenorm import SigmaNet, GroupSort, direct_norm
import torch

# must make the topo module visible
import sys
import pathlib


sys.path.insert(1, "/home/nschulte/purity_development/hlt2topo_sp")
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo

_config_path = "/home/nschulte/hlt2topo_sp/pipeline/config.yaml"
config = topo.yml_to_dict(_config_path)


__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


def model_arch_optim(
    params_dict: dict,  # Add this line to accept the Python dictionary
    robust: bool,
    monotonic: bool,
    _nbody: str,
    _features: List[str],
    LIP: float,
    config_path: str,
) -> torch.nn.Module:
    """Parse the architecture complexity supplied by Optuna to deliver an optimized model"""

    # Extract relevant information from the params_dict
    n_layers = params_dict.get("n_layers", 1)
    n_units_layers = [params_dict.get(f"global_n_units_layer") for i in range(n_layers)]

    def lipschitz_norm(module, is_norm=False, _kind="one"):
        if not robust:
            print("Running with unconstrained NN")
            return module
        else:
            print("Booked Lip NN")
            return direct_norm(
                module,
                always_norm=is_norm,
                kind=_kind,
                max_norm=LIP ** (1 / (n_layers + 1)),
            )

    model_layers = []
    in_features = len(_features)

    # hidden layers
    for n_units in n_units_layers:
        model_layers.append(lipschitz_norm(torch.nn.Linear(in_features, n_units)))
        model_layers.append(GroupSort(n_units // 2))
        in_features = n_units

    # Final layer
    model_layers.append(lipschitz_norm(torch.nn.Linear(in_features, 1)))

    model = torch.nn.Sequential(*model_layers)

    if robust:
        print("NOTE: running with monotonicity/robustness in place")
        if monotonic:
            _monotone_constraints = topo.load_monotone_constrs(
                path=config_path, key=_nbody
            )
        else:
            _monotone_constraints = list(np.zeros(len(_features)))

        # report features and monotonicity constraints
        for i in range(len(_features)):
            print(_features[i], _monotone_constraints[i])
        assert len(_monotone_constraints) == len(_features)

        # include the robustness and monotonicity constraints
        model = SigmaNet(model, sigma=LIP, monotone_constraints=_monotone_constraints)

    print(model)
    return model

def get_model(
    robust: bool,
    monotonic: bool,
    _nbody: str,
    _features: List[str],
    LIP: float,
) -> "torch.model":
    def lipschitz_norm(module, is_norm=False, _kind="one"):
        if not robust:
            print("Running with uncostrained NN")
            return module  # condition to call this only if robust
        else:
            print("Booked Lip NN")
            print(is_norm, _kind)
            return direct_norm(
                module,  # the layer to constrain
                always_norm=is_norm,
                kind=_kind,  # |W|_1 constraint type
                max_norm=float(LIP) ** (1 / 4),  # norm of the layer (LIP ** (1/nlayers))
            )

    model = torch.nn.Sequential(
        lipschitz_norm(torch.nn.Linear(len(_features), 32)), 
        GroupSort(32 // 2),
        lipschitz_norm(torch.nn.Linear(32, 64)), 
        GroupSort(64 // 2),
        lipschitz_norm(torch.nn.Linear(64, 32)), 
        GroupSort(32 // 2),
        lipschitz_norm(torch.nn.Linear(32, 1)), 
    )

    if robust:
        print("NOTE: running with monotonicity/robustness in place")
        print(f"Lambda: {LIP}")
        if monotonic:
            _monotone_constraints = topo.load_monotone_constrs(
                path=_config_path, key=_nbody
            )
        else:
            _monotone_constraints = list(np.zeros(len(_features)))
            for i in range(len(_features)):
                print(_features[i], _monotone_constraints[i])
        if not monotonic:
            _monotone_constraints = np.zeros(len(_features))
        assert len(_monotone_constraints) == len(_features)
        model = SigmaNet(model, sigma=LIP, monotone_constraints=_monotone_constraints)
    print(model)
    return model



def get_device() -> Any:
    if torch.cuda.is_available():
        device = torch.device("cuda")
        print("Running on cuda")
    else:
        device = torch.device("cpu")
        print("Running on cpu")

    return device
