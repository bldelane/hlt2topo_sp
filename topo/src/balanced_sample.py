"""Create the balances train and test samples"""

import pandas as pd
from typing import Tuple, Any, Union, Dict, List
import numpy as np
import math

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


def determine_overall_population(df: pd.DataFrame) -> Dict:
    dictionary = {}
    """Get a dictionary of the population of each sample

    Parameters
    ----------
    df : pd.DataFrame
        dataframe containing all samples

    Returns
    -------
    Dict
        Dictionary with information on the population
    """

    for channel in df[df.channel != "minbias"].channel.unique():
        print(f"Sample {channel} has: {len(df[(df.channel == channel)])} before drop")
        total_events = df[df.channel == channel].unique_event.nunique()
        signal_candidates = df[(df.channel == channel) & (df.class_label == 1)]
        print(
            f"Number of events in sample {channel}: {total_events} \nNumber of signal composites in sample {channel}: {len(signal_candidates)}"
        )

        dictionary[channel] = len(signal_candidates)

    return dictionary


def determine_minimal_population(
    population_dict: Dict, lower_bound: int, puffer: int
) -> Tuple[Dict, Dict]:
    """Determine the sample with the least population as well as the cut off, when a sample if to little
    populated to be used for training

    Parameters
    ----------
    population_dict: Dict
        Dictionary containing signal candidate information

    Returns
    -------
    Dict:
        dictionary with list of samples to be removed from the training
    Dict:
        dictionary with parameters for the balanced sample: How many events from each sample for training/testing
    """

    # Some samples simply do not provide enough candidates. Figure out the least amount you need for training and add this here
    not_enough_population = {
        channel: sample_size
        for (channel, sample_size) in population_dict.items()
        if sample_size < lower_bound
    }
    remaining_samples = {
        channel: sample_size
        for (channel, sample_size) in population_dict.items()
        if sample_size >= lower_bound
    }

    print(f"Not sufficiently populated samples: {not_enough_population}")

    smallest_sample = min(remaining_samples, key=remaining_samples.get)
    minimal_value = population_dict[smallest_sample]

    print(
        f"Minimal populated sample used for the base of the balanced sample is {smallest_sample} with {minimal_value} signal candidates"
    )

    # -puffer because after determining the training sample, one has to clean up the remaining sample before
    # assembling the testing sample because you do not want events from the training in the testing data
    # so you need to create a puffer
    training_sample_size = np.round((minimal_value * 0.8) - puffer).astype(int)
    test_sample_size = np.round((minimal_value * 0.2)).astype(int)

    minbias_training_size = training_sample_size * len(remaining_samples)
    minbias_testing_size = test_sample_size * len(remaining_samples)

    assert minbias_training_size >= training_sample_size
    balanced_dict = {
        "training_sample_size": training_sample_size,
        "test_sample_size": test_sample_size,
        "minbias_training_size": minbias_training_size,
        "minbias_testing_size": minbias_testing_size,
    }
    print(
        f"Parameters for the balanced sample {balanced_dict}, with {len(remaining_samples)} sample out of {len(population_dict)} samples eligable for training"
    )

    print(population_dict)
    print(balanced_dict)
    print(remaining_samples)
    # NOTE: Dump dict into txt file or something
    return not_enough_population, balanced_dict


# HACK: use what Nicole wrote but remove the balancing entirely
def determine_minimal_population_unbalanced(population_dict: Dict) -> Dict:
    """Determine the sample with the least population as well as the cut off, when a sample is too little
    populated to be used for training

    Parameters
    ----------
    population_dict: Dict
        Dictionary containing signal candidate information

    Returns
    -------
    Dict:
        dictionary with parameters for the balanced sample: How many events from each sample for training/testing
    """

    # Calculate the total number of instances in all samples
    total_population = sum(population_dict.values())

    # Determine the size of the training and testing samples based on the total population
    training_sample_size = np.round(total_population * 0.8).astype(int)
    test_sample_size = np.round(total_population * 0.2).astype(int)

    # There's no need to calculate minbias sizes as we are using all available data
    balanced_dict = {
        "training_sample_size": training_sample_size,
        "test_sample_size": test_sample_size,
        "minbias_training_size": training_sample_size,
        "minbias_testing_size": test_sample_size,
    }
    print(
        f"Parameters for the balanced sample {balanced_dict}, out of {len(population_dict)} available samples"
    )

    print(population_dict)
    print(balanced_dict)
    return balanced_dict


def drop_less_populated(drop_list: Dict, df: pd.DataFrame) -> pd.DataFrame:
    """Clean DataFrame from samples that show no significant population

    Parameters
    ----------
    drop_list: Dict
        Dictionary containing samples to be dropped from the dataframe

    Returns
    -------
    pd.DataFrame:
        Cleaned up dataframe, ready for balancing
    """
    # drop samples from dataframe that do not provide enough signal candidates

    # Reindexing VERY important because otherwise indices between samples are repeated and candidates are dropped, that should not be dropped
    df = df.reset_index(drop=True)
    for id_, channel in drop_list.items():
        print(f"{id_} dropped from dataframe")
        unwanted_samples = df[df.channel == id_].index
        df = df[~df.index.isin(unwanted_samples)]

    print(df)
    return df


def create_balanced_train_sample(df: pd.DataFrame, parameters: Dict, key: str):
    """Create a balanced train sample

    Parameters
    ----------
    df: pd.DataFrame
        Complete dataframe containing all samples remaining after clean-up
    parameters: Dict
        Dictionary containing information on how many candidates are needed
    key : str
        Key to the n-body tree [`TwoBody` or `ThreeBody`]

    Returns
    -------
    pd.DataFrame:
        Balanced train sample
    """
    training_dataframe = []
    # HACK: if the nominal way does not guarantee stats, then use the test minbias sample // samples available
    assert "minbias" in df.channel.unique(), "No minbias sample available"
    if parameters["training_sample_size"] * (len(df.channel.unique()) - 1) > len(
        df[(df.channel == "minbias") & (df.class_label == 0)][
            : parameters["minbias_training_size"]
        ]  # nsig - minbias
    ):
        print(
            f"Minbias size is now:{df[(df.channel == 'minbias') & (df.class_label == 0)]}"
        )
        print(f"class label 0 {len(df[df.class_label == 0])}")
        print(f"minbias length {df[(df.channel == 'minbias')]}")
        print(f"Minbias train size before: {parameters['minbias_training_size']}")
        print(
            f"HACK: overwriting training_sample_size: {parameters['training_sample_size']}"
        )
        parameters["training_sample_size"] = math.ceil(
            (
                len(
                    df[(df.channel == "minbias") & (df.class_label == 0)][
                        : parameters["minbias_training_size"]
                    ]
                )
                * 0.8
            )  # 80% for training
            / (len(df.channel.unique()) - 1)
        )  # ceil to round up to max stats
        parameters["minbias_training_size"] = parameters["training_sample_size"] * (
            len(df.channel.unique()) - 1
        )  # FIXME: ugly but removes the rounding error
        print(
            f"new training_sample_size: {parameters['training_sample_size']}"
        )  # the - 1 removes the minbias from channels.unique
        print(
            f"new minbias size: {parameters['minbias_training_size']}"
        )  # the - 1 removes the minbias from channels.unique

    for channel in df.channel.unique():
        if channel != "minbias":
            sig = df[(df.channel == channel) & (df.class_label == 1)][
                : parameters["training_sample_size"]
            ]
            training_dataframe.append(sig)
            print(f"Mode {channel} provided  {len(sig)} composites for the training")
        else:
            bkg = df[(df.channel == "minbias") & (df.class_label == 0)][
                : parameters["minbias_training_size"]
            ]
            print(f"Mode {channel} provided  {len(bkg)} composites for the training")
            training_dataframe.append(bkg)
    training_dataframe = pd.concat(training_dataframe).sample(frac=1)

    print(len(training_dataframe[training_dataframe.class_label == 1]))
    print(len(training_dataframe[training_dataframe.class_label == 0]))

    # ---------------------------------------------------------
    # NOTE: this breaks the pipeline if running without minbias
    assert len(training_dataframe[training_dataframe.class_label == 1]) == len(
        training_dataframe[training_dataframe.class_label == 0]
    )
    # ---------------------------------------------------------

    print(training_dataframe)
    training_dataframe.to_pickle(
        f"scratch/{key}/balanced/train.pkl"
    )  # FIXME: hardcoded path

    for channel in training_dataframe[
        training_dataframe.channel != "minbias"
    ].channel.unique():
        total_events = training_dataframe[
            training_dataframe.channel == channel
        ].unique_event.nunique()
        signal_candidates = training_dataframe[
            (training_dataframe.channel == channel)
            & (training_dataframe.class_label == 1)
        ]
        print(
            f"Number of events in sample {channel}: {total_events} \nNumber of signal composites in sample {channel}: {len(signal_candidates)}"
        )

    return training_dataframe


def clean_from_training_events(
    complete_df: pd.DataFrame, train_df: pd.DataFrame
) -> pd.DataFrame:
    """Clean the events used in the training from the rest

    Note! Candidates and events are not the same. To avoid having candidates from the same
    event in training and testing, you need to see the events used in the training and
    clean them from the rest before assembling the testing to avoid biases

    Parameters
    ----------
    complete_df: pd.DataFrame
        Complete dataframe containing all samples remaining after clean-up
    train_df: pd.DataFrame
        Balanced train dataframe

    Returns
    -------
    pd.DataFrame:
        cleaned up rest dataframe
    """

    events_in_training = train_df.unique_event.unique()
    clean_df = complete_df[~complete_df["unique_event"].isin(events_in_training)]

    for channel in clean_df[clean_df.channel != "minbias"].channel.unique():
        total_events = clean_df[clean_df.channel == channel].unique_event.nunique()
        signal_candidates = clean_df[
            (clean_df.channel == channel) & (clean_df.class_label == 1)
        ]
        print(
            f"Number of events in sample {channel}: {total_events} \nNumber of signal composites in sample {channel}: {len(signal_candidates)}"
        )

    return clean_df


def create_balanced_test_sample(
    df: pd.DataFrame, parameters: Dict, key: str
) -> pd.DataFrame:
    """Create a balanced test sample

    Parameters
    ----------
    df: pd.DataFrame
        Complete dataframe containing all samples remaining after clean-up
    parameters: Dict
        Dictionary containing information on how many candidates are needed
    key : str
        Key to the n-body tree [`TwoBody` or `ThreeBody`]

    Returns
    -------
    pd.DataFrame:
        Balanced train sample
    """
    print(df.channel.unique())
    testing_dataframe = []
    # assemble test dataset

    # HACK: if the nominal way does not guarantee stats, then use the test minbias sample // samples available
    assert "minbias" in df.channel.unique(), "minbias not in channels.unique()"
    if parameters["test_sample_size"] * (len(df.channel.unique()) - 1) > len(
        df[(df.channel == "minbias") & (df.class_label == 0)][
            : parameters["minbias_testing_size"]
        ]  # nsig - minbias
    ):
        print(f"HACK: overwriting test_sample_size: {parameters['test_sample_size']}")
        parameters["test_sample_size"] = math.ceil(
            (
                len(
                    df[(df.channel == "minbias") & (df.class_label == 0)][
                        : parameters["minbias_testing_size"]
                    ]
                )
                * 0.2
            )
            / (len(df.channel.unique()) - 1)
        )  # ceil to round up to max stats
        parameters["minbias_testing_size"] = parameters["test_sample_size"] * (
            len(df.channel.unique()) - 1
        )  # FIXME: ugly but removes the rounding error
        print(
            f"new test_sample_size: {parameters['test_sample_size']}"
        )  # the - 1 removes the minbias from channels.unique()

    for channel in df.channel.unique():
        print(channel)
        if channel != "minbias":
            print(f"Sample {channel} has: {len(df[(df.channel == channel)])}")
            rest_signal = df[(df.channel == channel) & (df.class_label == 1)][
                : parameters["test_sample_size"]
            ]
            print(
                f"Mode {channel} provided  {len(rest_signal)} composites for the testing"
            )
            testing_dataframe.append(rest_signal)
        else:
            test_bkg = df[(df.channel == "minbias") & (df.class_label == 0)][
                : parameters["minbias_testing_size"]
            ]
            testing_dataframe.append(test_bkg)
            print(
                f"Mode {channel} provided  {len(test_bkg)} composites for the testing"
            )
    testing_dataframe = pd.concat(testing_dataframe).sample(frac=1)

    print(len(testing_dataframe[testing_dataframe.class_label == 1]))
    print(len(testing_dataframe[testing_dataframe.class_label == 0]))

    # ---------------------------------------------------------
    # NOTE: this breaks the pipeline if running without minbias
    assert len(testing_dataframe[testing_dataframe.class_label == 1]) == len(
        testing_dataframe[testing_dataframe.class_label == 0]
    )
    # ---------------------------------------------------------

    testing_dataframe.to_pickle(
        f"scratch/{key}/balanced/test.pkl"
    )  # FIXME: hardcoded path

    return testing_dataframe
