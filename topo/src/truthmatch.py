"""Container of all functionalities related to FromB and FromD matching"""

from abc import ABC, abstractmethod
from particle.pdgid import has_bottom, has_charm
import pandas as pd
import awkward as ak

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


class NoGhosts:
    def check_ghosts(self, particles, sample):
        ghost_array = []

        for particle in particles:
            particle_ghosts = (sample[f"{particle}_TRUEID"] == 0) & (
                sample[f"{particle}_MC_MOTHER_ID"] == 0
            )
            ghost_array.append(particle_ghosts)

        # now you have an array with 2 entries, one for each particle. If one is true, then the ghosttag is true
        # if both are false, the ghost tag is false
        ghost_tag = ak.any(ghost_array, axis=0)
        return ghost_tag


class BaseMatching(ABC):
    @classmethod
    def pdgids(cls):
        # TODO: Confirm that no IDs are missing
        return {
            "fromB": [
                511,
                521,
                531,
                541,
                5122,
                5132,
                5232,
                5212,
            ],  # B0, B+, Bs_0, Bc+, Lambdab0, Xib-, Xib0, Sigb0
            "fromD": [411, 421, 431, 4122, 4132],  # D+, D0, Ds+, Lambdac+, Xic0
        }

    def __init__(self, source_type, sample):
        # the source type here corresponds to the kind of matching we want to perform. Do we want to match to a B hadron or investigate charm contributions?
        if source_type not in ["fromB", "fromD"]:
            raise ValueError("Invalid source type provided.")

        self.source_type = source_type
        self._sample = sample

    @property
    def source_pdgids(self):
        return self.pdgids()[self.source_type]

    @property
    def sample(self):
        # take in the dataset. If pandas DataFrame, convert to akward array object
        if isinstance(self.sample, pd.DataFrame):
            sample = ak.from_pandas(self._sample)
        else:
            sample = self._sample

        return sample

    @abstractmethod
    def match(self, *args):
        pass

    @abstractmethod
    def retrieve_Bkey(self, *args):
        pass

    @abstractmethod
    def has_bottom_parent(self, *args):
        pass

    def has_bottom_track(self, *args):
        pass

    def has_charm_parent(self, *args):
        pass

    def has_charm_track(self, *args):
        pass


class TwoBodyMatching(BaseMatching):
    def match(
        self,
        track1_mc_mother_id,
        track1_mc_gd_mother_id,
        track1_mc_gd_gd_mother_id,
        track2_mc_mother_id,
        track2_mc_gd_mother_id,
        track2_mc_gd_gd_mother_id,
    ):
        # make sure that the particles are not fromB or fromD
        if (
            abs(track1_mc_mother_id) in self.source_pdgids
            or abs(track1_mc_gd_mother_id) in self.source_pdgids
            or abs(track1_mc_gd_gd_mother_id) in self.source_pdgids
        ) and (
            abs(track2_mc_mother_id) in self.source_pdgids
            or abs(track2_mc_gd_mother_id) in self.source_pdgids
            or abs(track2_mc_gd_gd_mother_id) in self.source_pdgids
        ):
            return True
        else:
            return False

    def match_background(
        self,
        track1_mc_mother_id,
        track1_mc_gd_mother_id,
        track1_mc_gd_gd_mother_id,
        track2_mc_mother_id,
        track2_mc_gd_mother_id,
        track2_mc_gd_gd_mother_id,
    ):
        # make sure that the particles are not fromB or fromD
        # This differs from the match function in that it only requires a particle to originate from B or D, not both
        if (
            abs(track1_mc_mother_id) in self.source_pdgids
            or abs(track1_mc_gd_mother_id) in self.source_pdgids
            or abs(track1_mc_gd_gd_mother_id) in self.source_pdgids
        ) or (
            abs(track2_mc_mother_id) in self.source_pdgids
            or abs(track2_mc_gd_mother_id) in self.source_pdgids
            or abs(track2_mc_gd_gd_mother_id) in self.source_pdgids
        ):
            return True
        else:
            return False

    def retrieve_Bkey(self, particles, parent, sample):
        # if the fromB flag is false, don't bother checking the keys
        if sample[f"{parent}_FromB"] == False:
            return False

        key1 = []
        key2 = []
        for particle in particles:
            key_columns = [
                f"{particle}_MC_MOTHER_KEY",
                f"{particle}_MC_GD_MOTHER_KEY",
                f"{particle}_MC_GD_GD_MOTHER_KEY",
            ]

            # if there is a B hadron in the ancestry chain, put the key into the list, so we can check if the keys are the same
            for particle_ancestor in key_columns:
                if (
                    abs(sample[particle_ancestor.replace("KEY", "ID")])
                    in self.source_pdgids
                ):
                    if particle == particles[0]:
                        key1.append(sample[particle_ancestor])
                    elif particle == particles[1]:
                        key2.append(sample[particle_ancestor])
                    elif particle == particles[2]:
                        print("Error: TwoBody Class called but more particles received")
                        break

        # now check if the keys are the same
        common_elements = set(key1).intersection(set(key2))

        if common_elements:
            return True
        else:
            return False

    def has_bottom_parent(self, track1_mc_mother_id, track2_mc_mother_id):
        # if a parent particle has a bottom quark, return True
        # now check for bottom quark content in the parents
        return has_bottom(track1_mc_mother_id) or has_bottom(track2_mc_mother_id)

    def has_bottom_track(self, track1_trueid, track2_trueid):
        # if a parent particle has a bottom quark, return True
        # now check for bottom quark content in the parents
        return has_bottom(track1_trueid) or has_bottom(track2_trueid)

    def has_charm_parent(self, track1_mc_mother_id, track2_mc_mother_id):
        # if a parent particle has a charm quark, return True
        # now check for charm quark content in the parents
        return has_charm(track1_mc_mother_id) or has_charm(track2_mc_mother_id)

    def has_charm_track(self, track1_trueid, track2_trueid):
        # if a parent particle has a charm quark, return True
        # now check for charm quark content in the parents
        return has_charm(track1_trueid) or has_charm(track2_trueid)


class ThreeBodyMatching(BaseMatching):
    def match(
        self,
        track1_mc_mother_id,
        track1_mc_gd_mother_id,
        track1_mc_gd_gd_mother_id,
        track2_mc_mother_id,
        track2_mc_gd_mother_id,
        track2_mc_gd_gd_mother_id,
        trackb_mc_mother_id,
        trackb_mc_gd_mother_id,
        trackb_mc_gd_gd_mother_id,
    ):
        # make sure that the particles are not fromB or fromD
        if (
            (
                abs(track1_mc_mother_id) in self.source_pdgids
                or abs(track1_mc_gd_mother_id) in self.source_pdgids
                or abs(track1_mc_gd_gd_mother_id) in self.source_pdgids
            )
            and (
                abs(track2_mc_mother_id) in self.source_pdgids
                or abs(track2_mc_gd_mother_id) in self.source_pdgids
                or abs(track2_mc_gd_gd_mother_id) in self.source_pdgids
            )
            and (
                abs(trackb_mc_mother_id) in self.source_pdgids
                or abs(trackb_mc_gd_mother_id) in self.source_pdgids
                or abs(trackb_mc_gd_gd_mother_id) in self.source_pdgids
            )
        ):
            return True
        else:
            return False

    def match_background(
        self,
        track1_mc_mother_id,
        track1_mc_gd_mother_id,
        track1_mc_gd_gd_mother_id,
        track2_mc_mother_id,
        track2_mc_gd_mother_id,
        track2_mc_gd_gd_mother_id,
        trackb_mc_mother_id,
        trackb_mc_gd_mother_id,
        trackb_mc_gd_gd_mother_id,
    ):
        # make sure that the particles are not fromB or fromD
        # This differs from the match function in that it only requires a particle to originate from B or D, not all three
        if (
            (
                abs(track1_mc_mother_id) in self.source_pdgids
                or abs(track1_mc_gd_mother_id) in self.source_pdgids
                or abs(track1_mc_gd_gd_mother_id) in self.source_pdgids
            )
            or (
                abs(track2_mc_mother_id) in self.source_pdgids
                or abs(track2_mc_gd_mother_id) in self.source_pdgids
                or abs(track2_mc_gd_gd_mother_id) in self.source_pdgids
            )
            or (
                abs(trackb_mc_mother_id) in self.source_pdgids
                or abs(trackb_mc_gd_mother_id) in self.source_pdgids
                or abs(trackb_mc_gd_gd_mother_id) in self.source_pdgids
            )
        ):
            return True
        else:
            return False

    def retrieve_Bkey(self, particles, parent, sample):
        # if the fromB flag is false, don't bother checking the keys
        if sample[f"{parent}_FromB"] == False:
            return False

        for particle in particles:
            key1 = []
            key2 = []
            key3 = []

            key_columns = [
                f"{particle}_MC_MOTHER_KEY",
                f"{particle}_MC_GD_MOTHER_KEY",
                f"{particle}_MC_GD_GD_MOTHER_KEY",
            ]

            for particle_ancestor in key_columns:
                if (
                    abs(sample[particle_ancestor.replace("KEY", "ID")])
                    in self.source_pdgids
                ):
                    if particle == particles[0]:
                        key1.append(sample[particle_ancestor])
                    elif particle == particles[1]:
                        key2.append(sample[particle_ancestor])
                    elif particle == particles[2]:
                        key3.append(sample[particle_ancestor])
                    elif particle == particles[3]:
                        print(
                            "Error: ThreeBody Class called but more particles received"
                        )
                        break

            # now check if the keys are the same
            common_elements = set(key1).intersection(set(key2), set(key3))

            if common_elements:
                return True
            else:
                return False

    def has_bottom_parent(
        self,
        twobody_mc_mother_id,
        track1_mc_mother_id,
        track2_mc_mother_id,
        trackb_mc_mother_id,
    ):
        # if a parent particle has a bottom quark, return True
        # now check for bottom quark content in the parents
        return (
            has_bottom(twobody_mc_mother_id)
            or has_bottom(track1_mc_mother_id)
            or has_bottom(track2_mc_mother_id)
            or has_bottom(trackb_mc_mother_id)
        )

    def has_bottom_track(self, track1_trueid, track2_trueid, trackb_trueid):
        # if a parent particle has a bottom quark, return True
        # now check for bottom quark content in the parents
        return (
            has_bottom(track1_trueid)
            or has_bottom(track2_trueid)
            or has_bottom(trackb_trueid)
        )

    def has_charm_parent(
        self,
        twobody_mc_mother_id,
        track1_mc_mother_id,
        track2_mc_mother_id,
        trackb_mc_mother_id,
    ):
        # if a parent particle has a charm quark, return True
        # now check for charm quark content in the parents
        return (
            has_charm(twobody_mc_mother_id)
            or has_charm(track1_mc_mother_id)
            or has_charm(track2_mc_mother_id)
            or has_charm(trackb_mc_mother_id)
        )

    def has_charm_track(self, track1_trueid, track2_trueid, trackb_trueid):
        # if a parent particle has a charm quark, return True
        # now check for charm quark content in the parents
        return (
            has_charm(track1_trueid)
            or has_charm(track2_trueid)
            or has_charm(trackb_trueid)
        )


# Factory to call the right matching class
def get_truth_matcher(matcher_type, source_type, sample):
    if matcher_type == "TwoBody":
        return TwoBodyMatching(sample=sample, source_type=source_type)
    elif matcher_type == "ThreeBody":
        return ThreeBodyMatching(sample=sample, source_type=source_type)
    else:
        raise ValueError("Invalid matcher type")
