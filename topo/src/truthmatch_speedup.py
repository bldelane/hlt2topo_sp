"""Container of all functionalities related to FromB and FromD matching"""
"""
To Track changes between tuples:
2023 tuples: TRUEID -> 2024 tuples: MCID
2023 tuples: MC_GD, MC_GD_GD (going up the generations); 2024 tuples: MC_MOTHER_ID, MC_GD_MOTHER_{int}_ID counting up to 15 generations
"""
from abc import ABC, abstractmethod
from particle.pdgid import has_bottom, has_charm
import pandas as pd
import awkward as ak
import numba
import numpy as np

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


class GhostFinder:
    """Ghost finder class: assign a label based on whether a ghost-like TM criteria are satisfied"""

    def check_ghosts(self, particles: list, sample):
        """Having defined a ghost a particle with ID == 0 and its parent ID == 0, this function checks if a particle is a ghost or not."""

        # book a container with n-track entries; for each track, inspect whether the ghost criteria are satisfied
        ghost_array = [
            (sample[f"{particle}_MCID"] == 0)
            & (sample[f"{particle}_MC_MOTHER_ID"] == 0)
            for particle in particles
        ]
        # if even one track is ghost-like, the candidate is a ghost
        ghost_tag = ak.any(ghost_array, axis=0)

        # sanity-check segment
        # --------------------
        non_ghost_tracks = [
            (sample[f"{particle}_MCID"] != 0)
            | (sample[f"{particle}_MC_MOTHER_ID"] != 0)
            for particle in particles
        ]  # at least one track not ghost-like
        non_ghost_tag = ak.all(
            non_ghost_tracks, axis=0
        )  # any test the case where you have non-zero track, but it neglects the other final-state tracks

        # if GhostTag is True, then both per-track MCID and per-track MC_MOTHER_ID must be 0
        if ak.any(ghost_tag & non_ghost_tag):
            raise ValueError(
                "A candidate is both ghost and non-ghost. This should not happen."
            )

        # global per-candidate ghost tag (NOTE: not per track!)
        return ghost_tag


class BaseMatching(ABC):
    """Abstract Base Class for truth-matching a (part-reco) B decay"""

    def __init__(self, source_type, sample, fs_tracks: list):
        # the source type here corresponds to the kind of matching we want to perform. Do we want to match to a B hadron or investigate charm contributions?
        if source_type not in ["fromB", "fromD"]:
            raise ValueError("Invalid source type provided.")

        self.source_type = source_type
        self._sample = sample
        self._pids = self.pdgids()[self.source_type]
        self.fs_tracks = fs_tracks

    @classmethod
    def pdgids(cls):
        return {
            "fromB": [
                511,
                521,
                531,
                541,
                5122,
                5132,
                5232,
                5212,
            ],  # B0, B+, Bs_0, Bc+, Lambdab0, Xib-, Xib0, Sigb0
            "fromD": [411, 421, 431, 4122, 4132],  # D+, D0, Ds+, Lambdac+, Xic0
        }

    @property
    def source_pdgids(self):
        return self.pdgids()[self.source_type]

    @property
    def pids(self):
        return self._pids

    @property
    def sample(self):
        # take in the dataset. If pandas DataFrame, convert to akward array object
        if isinstance(self._sample, pd.DataFrame):
            return ak.from_pandas(self._sample)
        else:
            return self._sample

    @abstractmethod
    def heavy_flavour_match(self, *args):
        """Establish whether a beauty/charm ancestor is common to all tracks"""
        pass

    @abstractmethod
    def exact_heavy_flavour_match(self, *args):
        pass

    @abstractmethod
    def has_bottom_parent(self, *args):
        pass

    def has_bottom_track(self, *args):
        pass

    def has_charm_parent(self, *args):
        pass

    def has_charm_track(self, *args):
        pass

    def is_from_source_pdgids(self, val):
        return abs(val) in self.source_pdgids

    def __str__(self) -> str:
        return f"Source type: {self.source_type}; "


class CompositeMatching(BaseMatching):
    """Truth-matcher accounting for a (part-reco) B -> 2-/3-track decay"""

    def check1d_shape(self) -> bool:
        """The ID variables should be 1D, not nested. This allows for numpy functionality"""
        are_all_1d = all(
            self.sample[f"{particle}_{ancestor}_{suffix}"].ndim == 1
            for particle in self.fs_tracks
            for ancestor in [
                "MC_MOTHER", "MC_GD_MOTHER_2", "MC_GD_MOTHER_3", "MC_GD_MOTHER_4", "MC_GD_MOTHER_5", "MC_GD_MOTHER_6", "MC_GD_MOTHER_7", "MC_GD_MOTHER_8","MC_GD_MOTHER_9", "MC_GD_MOTHER_10", "MC_GD_MOTHER_11", "MC_GD_MOTHER_12", "MC_GD_MOTHER_13", "MC_GD_MOTHER_14"
            ]
            for suffix in ["ID", "KEY"]
        )
        if not are_all_1d:
            raise ValueError("Not all arrays are 1D")

    def heavy_flavour_match(self) -> bool:
        """Execute beauty matching: one common heavy-flavour ancestor among the two tracks"""

        # check that the ID arrays are 1D
        self.check1d_shape()

        # Convert the PIDs to a NumPy array to unlock np.isin functionality
        numpy_pids = np.array(self.pids)

        # Now use NumPy's isin function to store an n-particle array where each encodes if at leats one per-particle ancestor is beauty-ful
        per_track_beauty_ancestor = [
            np.isin(
                np.abs(
                    np.array(
                        [
                            self.sample[f"{particle}_MC_MOTHER_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_2_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_3_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_4_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_5_ID"], 
                            self.sample[f"{particle}_MC_GD_MOTHER_6_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_7_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_8_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_9_ID"],  
                            self.sample[f"{particle}_MC_GD_MOTHER_10_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_11_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_12_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_13_ID"],  
                            self.sample[f"{particle}_MC_GD_MOTHER_14_ID"],                                                                                  
                        ]
                    )
                ),
                numpy_pids,
            ).any(
                axis=0
            )  # Check if any condition per particle is True across all MCID conditions
            for particle in self.fs_tracks
        ]  # this establishes if a beauty ancestor is present in the ancestry of each track, separately

        # Now check if the beauty ancestor is common to all final-state tracks
        return ak.all(ak.Array(per_track_beauty_ancestor), axis=0)

    def exact_heavy_flavour_match(self) -> bool:
        """Execute beauty matching: one common heavy-flavour ancestor among the tracks
        NOTE: this requires that the exact same candidate - tagged by its key - is present in the ancestry of all tracks
        """
        # Check that the ID arrays are 1D
        self.check1d_shape()

        # Convert the PIDs to a NumPy array to unlock np.isin functionality
        numpy_pids = np.array(self.pids)

        per_track_keys = {}

        for particle in self.fs_tracks:
            # simply collect per track the ancestor IDs
            ancestry = []
            keys = []
            for suffix in ("MC_MOTHER", "MC_GD_MOTHER_2", "MC_GD_MOTHER_3", "MC_GD_MOTHER_4", "MC_GD_MOTHER_5", "MC_GD_MOTHER_6", "MC_GD_MOTHER_7", "MC_GD_MOTHER_8","MC_GD_MOTHER_9", "MC_GD_MOTHER_10", "MC_GD_MOTHER_11", "MC_GD_MOTHER_12", "MC_GD_MOTHER_13", "MC_GD_MOTHER_14"):
                # apppend the ID
                ancestry.append(np.array(self.sample[f"{particle}_{suffix}_ID"]))
                # append the candidate key
                keys.append(np.array(self.sample[f"{particle}_{suffix}_KEY"]))

            ancestry = np.abs(np.array(ancestry))  # absolute value of the IDs
            per_track_ancestry = ancestry.T  # transpose to get per-track ancestry

            # book particle x keys
            per_track_ancestral_keys = np.array(
                keys
            ).T  # transpose to get per-track keys

            # now check if the ancestors are beauty; true if per-particle at least one ancestor is beauty
            beauty_ancestral_mask = np.isin(per_track_ancestry, numpy_pids)

            # now mask the keys: if it is beauty, keep it, otherwise set to the unique FS track identifier
            per_track_keys[particle] = np.where(
                beauty_ancestral_mask, per_track_ancestral_keys, particle
            )

        # per candidate, concatenate the keys
        flattened_keys = np.concatenate(list(per_track_keys.values()), axis=1)

        # FIXME: it would be great to parallelise this or at numba-accelerate it
        bool_same_keys = []
        for flattened in flattened_keys:
            unique_values, counts = np.unique(
                flattened[~np.isin(flattened, self.fs_tracks)], return_counts=True
            )  # count if there is any reperated numerical key value
            bool_same_keys.append(
                np.any(counts == len(self.fs_tracks))
            )  # one common key per track

        return bool_same_keys

    # FIXME
    def has_bottom_parent(self) -> bool:
        """Check if any of the final-state tracks has a bottom parent
        NOTE: one b-hadron parent is sufficient to return True
        """
        parents = [
            has_bottom(self.sample[f"{particle}_MC_MOTHER_ID"])
            for particle in self.fs_tracks
        ]
        return ak.any(parents, axis=0)

    # FIXME
    def has_bottom_track(self) -> bool:
        """Return True if any of the final-state tracks is a bottom hadron"""
        tracks = [
            has_bottom(self.sample[f"{particle}_MCID"]) for particle in self.fs_tracks
        ]
        return ak.any(tracks, axis=0)

    # FIXME
    def has_charm_parent(self) -> bool:
        """Check if any of the final-state tracks has a charm parent
        NOTE: one c-hadron parent is sufficient to return True
        """
        parents = [
            has_charm(self.sample[f"{particle}_MC_MOTHER_ID"])
            for particle in self.fs_tracks
        ]
        return ak.any(parents, axis=0)

    # FIXME
    def has_charm_track(self) -> bool:
        """Return True if any of the final-state tracks is a charm hadron"""
        tracks = [
            has_charm(self.sample[f"{particle}_MCID"]) for particle in self.fs_tracks
        ]
        return ak.any(tracks, axis=0)

    def detect_heavy_flavour(self) -> bool:
        """One heavy flavour ancestor is sufficient to return True
        NOTE: designed such at least one heavy flavour causes the candidate to be vetod in the minbias
        """
        # check that the ID arrays are 1D
        self.check1d_shape()

        # Convert the PIDs to a NumPy array to unlock np.isin functionality
        numpy_pids = np.array(self.pids)

        # Now use NumPy's isin function to store an n-particle array where each encodes if at leats one per-particle ancestor is beauty-ful
        per_track_beauty_ancestor = [
            np.isin(
                np.abs(
                    np.array(
                        [
                            self.sample[f"{particle}_MC_MOTHER_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_2_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_3_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_4_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_5_ID"], 
                            self.sample[f"{particle}_MC_GD_MOTHER_6_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_7_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_8_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_9_ID"],  
                            self.sample[f"{particle}_MC_GD_MOTHER_10_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_11_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_12_ID"],
                            self.sample[f"{particle}_MC_GD_MOTHER_13_ID"],  
                            self.sample[f"{particle}_MC_GD_MOTHER_14_ID"],   
                        ]
                    )
                ),
                numpy_pids,
            ).any(
                axis=0
            )  # Check if any condition per particle is True across all MCID conditions
            for particle in self.fs_tracks
        ]  # this establishes if a beauty ancestor is present in the ancestry of each track, separately

        # if there is a heavy flavour ancestor at all, return True
        return ak.any(ak.Array(per_track_beauty_ancestor), axis=0)


class TwoBodyMatching(CompositeMatching):
    def __init__(self, source_type, sample, fs_tracks: list):
        super().__init__(source_type, sample, fs_tracks)
        assert (
            len(self.fs_tracks) == 2
        ), "TwoBodyMatching only supports two-body decays"  # like to access final-state track names as class attributes; a good place for sanity test: only two allowed

    def __str__(self) -> str:
        return f"TwoBody truth-matcher class (source type: {self.source_type});\nallowed pids: {self.pids};\nfinal-state tracks: {self.fs_tracks}\n"


class ThreeBodyMatching(CompositeMatching):
    def __init__(self, source_type, sample, fs_tracks: list):
        super().__init__(source_type, sample, fs_tracks)
        assert (
            len(self.fs_tracks) == 3
        ), "ThreeBodyMatching only supports two-body decays"  # like to access final-state track names as class attributes; a good place for sanity test: only two allowed

    def __str__(self) -> str:
        return f"ThreeBody truth-matcher class (source type: {self.source_type});\nallowed pids: {self.pids};\nfinal-state tracks: {self.fs_tracks}\n"


# Factory to call the right matching class
def get_truth_matcher(key: str, source_type, sample: ak.Array, fs_tracks: list):
    """Fetch the correct truth matcher class based on the trigger type (key)

    Parameters
    ----------
    key: str
        The trigger key ['TwoBody', 'ThreeBody']
    sample: awkward array
        The input sample
    source_type:
        The source type, ie what is the parent particle type ['fromB', 'fromD']

    Returns
    -------
    matcher: BaseMatching
        The correct truth matcher class
    """
    match key:
        case "TwoBody":
            return TwoBodyMatching(
                sample=sample, source_type=source_type, fs_tracks=fs_tracks
            )
        case "ThreeBody":
            return ThreeBodyMatching(
                sample=sample, source_type=source_type, fs_tracks=fs_tracks
            )
        case _:
            raise ValueError("Invalid matcher type")
