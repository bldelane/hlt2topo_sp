"""Calculate the 'extra' features for the training"""

import pandas as pd
from typing import Any, Dict, Union, Tuple,List
from numpy.typing import ArrayLike
import awkward as ak
import hist
from .utils import yml_to_dict
from ..config.channels import channels
import numpy as np

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


def calc_extra_feats(df: pd.DataFrame,
               nbody:str) -> pd.DataFrame:

    if nbody == "TwoBody":
        df["min_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT"]].min(axis=1)
        df["sum_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT"]].sum(axis=1)
        assert(df.min_PT_final_state_tracks.all()<=df.sum_PT_final_state_tracks.all())

        df["min_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV"]].min(axis=1)
        df["max_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV"]].max(axis=1)
        assert(df.min_FS_IPCHI2_OWNPV.all()<=df.max_FS_IPCHI2_OWNPV.all())

    elif nbody == "ThreeBody":
        df["min_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV","TrackB_IPCHI2_OWNPV"]].min(axis=1)
        df["max_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV","TrackB_IPCHI2_OWNPV"]].max(axis=1)
        assert(df.min_FS_IPCHI2_OWNPV.all()<=df.max_FS_IPCHI2_OWNPV.all())

        df["min_PT_TRACK12"] = df[["Track1_PT","Track2_PT"]].min(axis=1)
        df["sum_PT_TRACK12"] = df[["Track1_PT","Track2_PT"]].sum(axis=1)
        assert(df.min_PT_TRACK12.all()<=df.sum_PT_TRACK12.all())

        df["min_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].min(axis=1)
        df["max_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].max(axis=1)
        df["sum_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].sum(axis=1)
        assert(df.min_PT_final_state_tracks.all()<=df.max_PT_final_state_tracks.all())

    return df

def transform_doca(df: pd.DataFrame,
                    nbody: str)-> pd.DataFrame:

    if nbody == "TwoBody":
        df["TwoBody_DOCAMAX"] *= 10.
    if nbody == "ThreeBody":
        df["TwoBody_DOCAMAX"] *= 10.
        df["ThreeBody_DOCAMAX"] *= 10.

    return df

def transform_gev_log(df: pd.DataFrame,
                      log_vars: List,
                      gev_vars: List) -> pd.DataFrame:

    for g in gev_vars:
        df[g] /= 1000.
    for l in log_vars:
        df.loc[df[l] <= 0, l] = 1e-5
        assert(df[l].all()>0)
        df[l] = np.log(df[l].to_numpy())

    return df