"""Factory to preprocess the features of the classifier.
Objects import from abstract base class `Transformer` defined in transform_base.py
"""

from sklearn.preprocessing import QuantileTransformer
from typing import Union, Any, List
import awkward as ak
from numpy.typing import ArrayLike
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# plt.style.use(["science"])

# make topo module visible
import pathlib, sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import Transformer

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


# uitlity functions
def extract_NOI(
    data: Union[ArrayLike, ak.Array],
) -> List[float]:
    """
    Extract the Numbers of Interest:
    mean and std of the distribution
    """
    return np.mean(data), np.std(data)


# transformer children classes
# ----------------------------
class QuantTransformer(Transformer):
    """Wrapper class for sklearn QuantileTransformer"""

    def transform(
        self,
    ) -> Union[pd.DataFrame, ArrayLike]:
        """Extract tunable scaler parameters from the dataset
        & transorm the dataset
        """
        transf = QuantileTransformer(n_quantiles=1000, output_distribution="normal")
        transf.fit(
            self.dataset[self.features]
        )  # probably redundant if fit_transform is used?

        self._dataset[self._features] = transf.fit_transform(
            self.dataset[self.features]
        )

        return self


class ClampTransformer(Transformer):
    """Tranformer implementing the clamping of the features"""

    n_sigma = 5

    def plot_NOI(
        self,
        n_sigma: int = n_sigma,
        plot_dir_suffix: str = "pre",
    ) -> None:
        """Plot the mean and std of the distribution & retention windows"""

        for v in self.features:
            mean, std = extract_NOI(self.dataset[v])

            fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(7, 2.5))
            _weights = np.ones_like(self.dataset[v]) / float(len(self.dataset[v]))

            ax0.hist(
                self.dataset[v],
                bins=30,
                histtype="stepfilled",
                color="lightgrey",
                alpha=1.0,
                label="Signal + Background",
                weights=_weights,
            )
            ax0.axvline(mean, color="black", ls=":", label="Global mean")
            ax0.axvline(
                np.median(self.dataset[v]),
                color="purple",
                ls="-.",
                label="Global median",
            )
            ax0.plot(
                [],
                [],
                label=r"$\sigma=$%0.1f" % (np.std(self.dataset[v])),
                color="white",
            )
            ax0.set_title(
                "LHCb Simulation",
            )
            ax0.legend()
            ax0.set_xlabel(rf"\texttt{{{v.replace('_', '-')}}}")
            ax0.set_ylabel("Candidates, normalised")

            bw = np.ones_like(self.dataset.query("class_label==0")[v]) / float(
                len(self.dataset.query("class_label==0")[v])
            )
            sw = np.ones_like(self.dataset.query("class_label==1")[v]) / float(
                len(self.dataset.query("class_label==1")[v])
            )
            ax1.hist(
                self.dataset.query("class_label==0")[v],
                histtype="stepfilled",
                color="tab:red",
                alpha=0.3,
                label="Minimum bias",
                weights=bw,
            )
            ax1.hist(
                self.dataset.query("class_label==1")[v],
                histtype="step",
                color="tab:blue",
                label="Inclusive beauty",
                weights=sw,
            )
            ax1.axvline(
                np.mean(self.dataset[v]),
                color="black",
                ls=":",
                label="Global mean",
            )
            ax1.plot(
                [],
                [],
                label=r"Global $\sigma=$ %0.1f" % (np.std(self.dataset[v])),
                color="white",
            )

            ax1.set_title("LHCb Simulation")
            ax1.legend()
            ax1.set_xlabel(rf"\texttt{{{v.replace('_', '-')}}}")
            ax1.set_ylabel("Candidates, normalised")

            _path = f"scratch/plots/{self.key}/transformed/{plot_dir_suffix}"
            pathlib.Path(f"{_path}").mkdir(parents=True, exist_ok=True)
            [plt.savefig(f"{_path}/{v}.{ext}") for ext in ["png"]]
            plt.close()

    @staticmethod
    def feat_constrain(
        feature: Union[ak.Array, ArrayLike],
        n_sigma: int = n_sigma,
    ) -> Union[ak.Array, ArrayLike]:
        """Per-feature simplified scaling:
        candidates outside the retentin window are clamped to the window edges
        """
        # compute distribution mean and std dev
        mean, std = extract_NOI(feature)

        # retain if within n_sigma of mean
        # --------------------------------
        # identify the retention winow
        retained = feature[
            (feature >= mean - n_sigma * std) & (feature <= mean + n_sigma * std)
        ]

        # assign the endpoints of the window to tails outside it - "clamping"
        feature[(feature < mean - n_sigma * std)] = np.min(retained)
        feature[(feature > mean + n_sigma * std)] = np.max(retained)

        return feature

    def transform(
        self,
    ) -> Union[pd.DataFrame, ak.Array]:
        """Clamp the dataset"""

        # sanity plot: mean, median and retention window pre-clamping
        self.plot_NOI()

        # apply the clamping
        self._dataset[self._features] = self._dataset[self._features].apply(
            lambda x: self.feat_constrain(x), axis=0
        )

        self.plot_NOI(plot_dir_suffix=f"post")

        return self


# interface
# ---------
class TransformerFactory:
    """
    Factory providing the desidered preprocessing method.
    The factory doesn't maintain the instances of the objects it creates [source: ArjanCodes]
    """

    def __init__(
        self,
        dataset: Union[ak.Array, ArrayLike],
        features: List[str],
        key=str,
    ) -> None:
        self._dataset = dataset
        self._features = features
        self._key = key

    @property
    def dataset(self) -> Union[ak.Array, ArrayLike]:
        return self._dataset

    @property
    def features(self) -> List[str]:
        return self._features

    @property
    def key(self) -> str:
        return self._key

    def get_transformer(self, format: str) -> Transformer:
        """Return the scaler object.

        Parameters
        ----------
        format: str
            The name of the scaler to return [`Quantile`, `Clamp`]

        dataset: Any
            The dataset containing the features to be scaled

        features: List[str]
            The list of features to be scaled

        Returns
        -------
        Transformer object
        """
        if format == "Quantile":
            return QuantTransformer(
                dataset=self.dataset,
                features=self.features,
                key=self.key,
            )
        elif format == "Clamp":
            return ClampTransformer(
                dataset=self.dataset,
                features=self.features,
                key=self.key,
            )
        else:
            raise ValueError(f"Tranformer {format} not recognised.")
