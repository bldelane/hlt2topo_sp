"""Assign class label, event label and clean the minbias from beauty events"""

import pandas as pd
import awkward as ak
from typing import Tuple, Any, Union, List
from collections import Counter
import uproot
import numpy as np
import time

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


def label_objects(df: pd.DataFrame, channel: str) -> pd.DataFrame:
    """Add class label and channel name

    Parameters
    ----------
    key: str
        Topo candidate identifier: ['TwoBody', 'ThreeBody']

    Returns
    -------
    None
        Modifies the dataframe in-place to assign class label [key: 'class_label']
        and per-channel name [key: 'channel']
    """
    df["channel"] = channel
    if channel == "minbias":
        df["class_label"] = 0
    else:
        df["class_label"] = 1

    return df


def calculate_unique_events(df: pd.DataFrame):
    """Add unique event information on a given input dataframe; for that run_number and event_number are needed in the dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        Input Dataframe for which the unique event information is needed

    Returns
    -------
    df: pandas.DataFrame
        Returns a DataFrame which now contains unique event information
    """
    # events should appear multiple times in the dataframe as there are multiple candidates
    # in an event. This way, the amount of unique events are filtered out.
    start = time.time()
    event_ids = ["runNumber", "eventNumber"]
    df["unique_event"] = list(zip(df.runNumber, df.eventNumber))
    df["unique_event"] = pd.factorize(df["unique_event"])[0]

    assert all(df.groupby(event_ids).unique_event.nunique() == 1)

    print("--- %s seconds ---" % (time.time() - start))
    print(
        "Unique instances of the runNumber_eventNumber ID: ", df.unique_event.nunique()
    )
    return df


def clean_minbias(df: pd.DataFrame, key: str) -> pd.DataFrame:
    """Clean the minbias sample from any beauty decay events for more purity in the training

    Parameters
    ----------
    dataframe: pd.DataFrame
        Input dataframe which requires cleaning of the minbias sample
    key: str
        Passed in the config.yaml as key; TwoBody and ThreeBody as possibility

    Returns
    -------
    dataframe: pd.DataFrame
        Returns DataFrame which now contains a clean minbias
    """
    # Check the amount of events and final composites in the minbias sample
    print(
        "Unique Events in Minbias:", df[df.channel == "minbias"].unique_event.nunique()
    )
    print("Total Composites in Minbias:", len(df[df.channel == "minbias"]))

    # Replace -1 with 0 otherwise the computer will not understand that -1 corresponds to False
    SameBs_minbias = df[f"{key}_HasBCandidate"]
    SameBs_minbias = (
        df[df.channel == "minbias"]
        .groupby("unique_event")[f"{key}_HasBCandidate"]
        .max()
    )
    SameBs_minbias = SameBs_minbias[SameBs_minbias].index
    df = df.set_index("unique_event").drop(SameBs_minbias).reset_index()
    print("Unique Events with FromB composites in MinBias:", len(SameBs_minbias))
    # This should be zero if the removal worked
    assert df[df.channel == "minbias"][f"{key}_HasBCandidate"].sum() == 0

    return df
