import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Specify path and read data
path = "scratch/ThreeBody/scaled/train.pkl"
df = pd.read_pickle(path)

feature_candidates = [
    "ThreeBody_ENDVERTEX_CHI2",
    "ThreeBody_DOCAMAX",
    "ThreeBody_OWNPV_CHI2",
    "ThreeBody_OWNPV_DOCAMAX",
    "ThreeBody_IPCHI2_OWNPV",
    "ThreeBody_FDCHI2_OWNPV",
    "ThreeBody_PT",
    "ThreeBody_nNeutral",
    "ThreeBody_nCharged",
    "ThreeBody_nChargedSlowPions",
    "TwoBody_ENDVERTEX_CHI2",
    "TwoBody_DOCAMAX",
    "TwoBody_OWNPV_CHI2",
    "TwoBody_OWNPV_DOCAMAX",
    "TwoBody_IPCHI2_OWNPV",
    "TwoBody_FDCHI2_OWNPV",
    "TwoBody_ORIVX_CHI2",
    "TwoBody_ORIVX_DOCAMAX",
    "TwoBody_FDCHI2_ORIVX",
    "TwoBody_PT",
    "TwoBody_nCharged",
    "TwoBody_nChargedSlowPions",
    "Track1_OWNPV_CHI2",
    "Track1_OWNPV_DOCAMAX",
    "Track1_IPCHI2_OWNPV",
    "Track1_ORIVX_CHI2",
    "Track1_ORIVX_DOCAMAX",
    "Track1_PT",
    "Track2_OWNPV_CHI2",
    "Track2_OWNPV_DOCAMAX",
    "Track2_IPCHI2_OWNPV",
    "Track2_ORIVX_CHI2",
    "Track2_ORIVX_DOCAMAX",
    "Track2_PT",
    "TrackB_OWNPV_CHI2",
    "TrackB_OWNPV_DOCAMAX",
    "TrackB_IPCHI2_OWNPV",
    "TrackB_ORIVX_CHI2",
    "TrackB_ORIVX_DOCAMAX",
    "TrackB_PT",
    "TwoBody_Mcorr",
    "TwoBody_TAU",
    "ThreeBody_Mcorr",
    "ThreeBody_TAU",
    "M12",
    "M2B",
    "min_FS_IPCHI2_OWNPV",
    "max_FS_IPCHI2_OWNPV",
    "min_PT_final_state_tracks",
    "sum_PT_final_state_tracks",
    "log_max_FS_IPCHI2_OWNPV",
    "log_TwoBody_FDCHI2_OWNPV",
    "TwoBody_ETA",
    "min_PT_TRACK12",
    "sum_PT_TRACK12",
    "ThreeBody_ETA",
    "log_ThreeBody_FDCHI2_OWNPV",
]

df = df[feature_candidates + ["class_label"]]


# Separate features and target
X = df.drop("class_label", axis=1)
y = df["class_label"]


# Split the data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# Define the model
model = GradientBoostingClassifier()

# Train the model
model.fit(X_train, y_train)

# Get feature importances
importances = model.feature_importances_

# Print feature importances
for feat, importance in zip(X.columns, importances):
    print("Feature: {0}, Importance: {1}".format(feat, importance))

# Make predictions and evaluate the model
y_pred = model.predict(X_test)
print("Accuracy: ", accuracy_score(y_test, y_pred))
