"""Compute observables of interest"""

import vector
from typing import Tuple, Any, Union
import numpy as np

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


# @nb.njit
def build_4vec(pX: float, pY: float, pZ: float, pE: float) -> vector.Lorentz:
    """Build a four-vector"""
    return vector.array({"x": pX, "y": pY, "z": pZ, "E": pE})


# @nb.njit
def build_3vec(
    pX: float,
    pY: float,
    pZ: float,
) -> vector.VectorNumpy3D:
    """build a three-vector"""
    return vector.array(
        {
            "x": pX,
            "y": pY,
            "z": pZ,
        }
    )


# @nb.njit
def calc_pointing_obs(
    pv: vector.obj,
    dv: vector.obj,
) -> Tuple[Any, ...]:
    """Calculate pointing observables"""
    pointing = dv - pv
    pointing_u = pointing.unit()  # unit vector
    assert pointing_u.mag.all() == 1.0, "Pointing vector not unit"

    theta = pointing_u.theta
    phi = pointing_u.phi

    return pointing, pointing_u, theta, phi


# @nb.njit
def calc_DIRA(vis_sys_3v: vector.VectorNumpy3D, pointing_u: vector.obj) -> float:
    """Calculate the DIRA"""
    return (vis_sys_3v @ pointing_u) / (vis_sys_3v.mag * pointing_u.mag)


# @nb.njit
def calc_mcorr(
    vis_sys_lv: Union[vector.VectorNumpy3D, vector.Lorentz],
    phi: float,
    theta: float,
    visM: float,
) -> float:
    """Compute the corrected mass"""
    rvs = vis_sys_lv.rotateZ(-phi).rotateY(-theta)
    miss_pt = rvs.pt
    mcorr = np.sqrt(visM**2 + miss_pt**2) + miss_pt

    return mcorr


# @nb.njit
def calc_ltime(
    vis_sys_lv: vector.Lorentz,
    pointing: vector.VectorNumpy3D,  # not unit
) -> float:
    """Calculate the lifetime"""
    # book 3-momentum
    vis_sys_3v = build_3vec(pX=vis_sys_lv.x, pY=vis_sys_lv.y, pZ=vis_sys_lv.z)

    # calculate the lifetime, factoring in c as per in LoKi
    ltime = vis_sys_lv.mass * (pointing @ vis_sys_3v) / vis_sys_3v.mag2
    c_light = 2.99792458e2  # NOTE: same cval as gaudipython but e2 instead of e8
    ltime /= c_light

    return ltime
