"""Plotting utilities"""

from typing import Callable, Union
import matplotlib.pyplot as plt
from numpy.typing import ArrayLike
import pathlib
import scienceplots
from typing import Any
import mplhep as hep
from matplotlib.colors import LogNorm
import boost_histogram as bh
from pathlib import Path
import numpy as np

plt.style.use(["science", "no-latex"])

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


def subplot_factory(n: int) -> Callable:
    """Generate a plotter function to book fig, axs with a given number of subplots"""

    def book_canvas():
        plt.tight_layout()
        if n == 1:
            fig, ax = plt.subplots()
        else:
            fig, ax = plt.subplots(
                nrows=n / (n // 3),
                ncols=n // 3,
            )
            return fig, ax

    return book_canvas


def twoclass_plot(
    data: ArrayLike,
    plotname: str,
    control: ArrayLike = None,
    data_label: str = "Data",
    control_label: str = "Control",
    plotdir: str = "scratch/plots/observables",
    xlabel: Union[None, str] = None,
    bins: int = 50,
    density: bool = True,
    histtype: str = "step",
    return_fig: bool = False,
    **kwargs,
) -> None:
    """Plot two classes of data/MC; typically devised for data vs control
    or signal vs bkg.

    Parameters
    ----------
    data: ArrayLike
        Manually computed variables

    control: ArrayLike
        Truth variables

    plotname: str
        Name of the plot

    plotdir: str
        Target directory for the saved plots

    density: bool
        Normalise the histograms

    bins: int
        Number of bins in the histograms

    histtype: str
        Histogram type

    return_fig: bool
        Return the figure object

    Returns
    -------
    None
        Plots the distributions of the variables
    """

    conf = {
        "density": density,
        "bins": bins,
    }

    # book a canvas & plot the distributions`
    fig, ax = plt.subplots()
    ax.set_title("LHCb Simulation")
    ax.set_ylabel("Candidates, normalised")
    if xlabel is None:
        ax.set_xlabel(rf"\texttt{{{plotname.replace('_', '-')}}}")
    else:  # use the custom label
        ax.set_xlabel(xlabel)
    ax.hist(
        data,
        label=data_label,
        color="tab:blue",
        histtype="step",
        **conf,
        **kwargs,
    )

    # if relevent, add a control
    if control is not None:
        plt.style.use(
            ["science", "high-contrast", "no-latex"]
        )  # for the control, add different style
        ax.hist(
            control,
            label=control_label,
            color="lightgrey",
            histtype="stepfilled",
            alpha=0.75,
            **conf,
            **kwargs,
        )

    ax.legend()

    # save
    pathlib.Path(f"{plotdir}").mkdir(parents=True, exist_ok=True)
    try:
        [plt.savefig(f"{plotdir}/{plotname}.{ext}") for ext in ("pdf", "png")]
    except:  # found that sometimes the labels mess things up, but it not essential to have the plots if the data is still scaled
        print(f"Error saving plot for {plotdir}/{plotname} -> skipping")
    plt.close()

    if return_fig is True:
        return fig, ax


# manual configuration of the observable plots
# --------------------------------------------
plt_config = {
    "min_PT_final_state_tracks": {"range": [0, 5], "xlabel": "$p_T^{min}$ [GeV$/c$]"},
    "sum_PT_final_state_tracks": {"range": [0, 15], "xlabel": "$\sum{p_T}$ [GeV$/c$]"},
    "min_FS_IPCHI2_OWNPV": {
        "range": [0, 15],
        "xlabel": "log(IP $\chi^2_{min}$) [Arbitrary Units]",
    },
    "max_FS_IPCHI2_OWNPV": {
        "range": [0, 15],
        "xlabel": "log(IP $\chi^2_{max}$) [Arbitrary Units]",
    },
    "TwoBody_DOCAMAX": {"range": [0, 5], "xlabel": "DOCA$_{max}$ [cm]"},
    "TwoBody_ENDVERTEX_CHI2": {
        "range": [0, 10],
        "xlabel": "$B$ $\chi^2_{vtx}$ [Arbitrary Units]",
    },
    "TwoBody_FDCHI2_OWNPV": {
        "range": [0, 11],
        "xlabel": "log($B$ FD $\chi^2$) [Arbitrary Units]",
    },
    "TwoBody_Mcorr": {"range": [1, 25], "xlabel": "$m_{corr}$ [GeV$/c^2$]"},
    "TwoBody_PT": {"range": [0, 15], "xlabel": "$p_T(B)$ [GeV$/c$]"},
}


# fig, ax factory functions
# -------------------------
def simple_ax(
    title: str | None = "LHCb Unofficial",  # FIXME: Syntax error for the pipe
    ylabel: str = "Candidates",
    normalised: bool = False,
    scale: str | None = None,
    logo: bool = True,
) -> tuple[Any, plt.Axes]:
    """Book simple ax

    Parameters
    ----------
    title: str | None
        Title of the plot (default: 'LHCb Unofficial')

    ylabel: str
        Y-axis label (default: 'Candidates')

    normalised: bool
        If true, normalise histograms to unity (default: False)

    Returns
    -------
    tuple[Any, Callable]
        Fig, ax plt.Axes objects
    """
    fig, ax = plt.subplots()

    ax.set_title(title, loc="right")
    ax.set_ylabel(ylabel)
    # logo
    ax.text(
        0.0,
        1.07,
        r"Preliminary",
        ha="left",
        va="top",
        transform=ax.transAxes,
        color="grey",
    )
    if scale is not None:
        ax.set_yscale(scale)

    return fig, ax


def simple_2ax(
    title: str | None = "LHCb Unofficial",
    ylabel: str = "Candidates",
    normalised: bool = False,
    scale: str | None = None,
    logo: bool = True,
) -> tuple[Any, plt.Axes, plt.Axes]:
    """Book simple ax

    Parameters
    ----------
    title: str | None
        Title of the plot (default: 'LHCb Unofficial')

    ylabel: str
        Y-axis label (default: 'Candidates')

    normalised: bool
        If true, normalise histograms to unity (default: False)

    Returns
    -------
    tuple[Any, Callable]
        Fig, ax plt.Axes objects
    """
    fig, (ax1, ax2) = plt.subplots(ncols=2, nrows=1, figsize=(8, 2.5))

    for ax in (ax1, ax2):
        ax.set_title(title, loc="right")
        # logo
        ax.text(
            0.0,
            1.07,
            r"Preliminary",
            ha="left",
            va="top",
            transform=ax.transAxes,
            color="grey",
        )

    if scale is not None:
        ax1.set_yscale(scale)
        ax1.set_ylabel(ylabel)

    return fig, ax1, ax2


def make_legend(
    ax: plt.Axes,
    on_plot: bool = True,
    ycoord: float = -0.6,
) -> None:
    """
    Place the legend below the plot, adjusting number of columns

    Parameters
    ----------
    ax: plt.Axes
        Axes object to place the legend on

    on_plot: bool
        If true, place the legend on the plot (default: True)

    ycoord: float
        Y-coordinate of the legend (default: -0.6)

    Returns
    -------
    None
        Places the legend on the axes
    """
    # count entries
    handles, labels = ax.get_legend_handles_labels()

    # decide the number of columns accordingly
    match len(labels):  # FIXME: getting syntax error here
        case 2:
            ncols = 2
        case other:
            ncols = 1

    # place the legend
    ax.legend(loc="best")
    if on_plot is False:
        ax.legend(
            bbox_to_anchor=(0.5, ycoord),
            loc="lower center",
            ncol=ncols,
            frameon=False,
        )


# save plots in multiple formats
def save_to(
    outdir: str,
    name: str,
) -> None:
    """Save the current figure to a path in multiple formats

    Generate directory path if unexeistent

    Parameters
    ----------
    outdir: str
        Directory path to save the figure
    name: str
        Name of the plot

    Returns
    -------
    None
        Saves the figure to the path in pdf and png formats
    """
    Path(outdir).mkdir(parents=True, exist_ok=True)
    [plt.savefig(f"{outdir}/{name}.{ext}") for ext in ["pdf", "png"]]


# plot data and pdfs
# ------------------
def fill_hist_w(
    data: ArrayLike, bins: int, range: tuple, weights: ArrayLike | None = None
) -> tuple[Any, ...]:
    """Fill histogram accounting weights

    Parameters
    ----------
    data: ArrayLike
        Data to be histogrammed
    bins: int
        Number of bins
    range: tuple
        Range of the histogram
    weights: ArrayLike | None
        Weights to be applied to the data (default: None)

    Returns
    -------
    nh: ArrayLike
        Bin contents
    xe: ArrayLike
        Bin edges
    xc: ArrayLike
        Bin centers
    nh_err: ArrayLike
        Bin errors
    """
    # bin contents, bin edges, bin centers
    nh, xe = np.histogram(data, bins=bins, range=range)
    xc = 0.5 * (xe[1:] + xe[:-1])

    # if no weights, Poisson errors
    nh_err = nh**0.5

    # if weights, careful treatment of bin contents and errors via boost-histogram
    # https://www.zeuthen.desy.de/~wischnew/amanda/discussion/wgterror/working.html
    if weights is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        whist.fill(data, weight=weights)
        cx = whist.axes[0].centers
        nh = whist.view().value
        nh_err = whist.view().variance ** 0.5

    return nh, xe, xc, nh_err


def plot_data(
    data: ArrayLike,
    ax: plt.Axes,
    range: tuple[float, float],
    bins: int = 50,
    weights: ArrayLike | None = None,
    label: str | None = None,
    norm: bool = False,
    color: str = "black",
    **kwargs: Any,
) -> None:
    """Plot the data, accounting for weights if provided

    Parameters
    ----------
    data: ArrayLike
        Data to be plotted

    ax: plt.Axes
        Axes to plot on

    bins: int
        Number of bins (default: 50)

    range: tuple[float, float]
        Range of the data

    weights: ArrayLike | None
        Weights for the data (default: None)

    label: str | None
        Legend label for the data (default: None)

    kwargs: Any
        Keyword arguments to be passed to the errorbar plot

    norm: bool
        If true, normalise the data to unit area (default: False)

    Returns
    -------
    None
        Plots the data on the axes
    """
    nh, xe, cx, err = fill_hist_w(data, bins, range, weights)

    # normalise to unity
    _normalisation = np.sum(nh)
    if norm:
        nh = nh / _normalisation
        err = err / _normalisation

    ax.errorbar(
        cx,
        nh,
        yerr=err,
        xerr=(xe[1] - xe[0]) / 2,
        label=label,
        fmt=".",
        markersize=3,
        color=color,
        **kwargs,
    )


def hist_err(
    data: ArrayLike,
    ax: plt.Axes,
    range: tuple[float, float],
    bins: int = 50,
    weights: ArrayLike | None = None,
    label: str | None = None,
    norm: bool = False,
    **kwargs,
) -> None:
    """Wrapper for mplhep histplot method

    Paramaters
    ----------
    nh: ArrayLike
        Histogram bin contents
    xe: ArrayLike
        Histogram bin edges
    ax: plt.Axes
        Axes to plot on
    label: str | None
        Legend label for the data (default: None)
    kwargs: Any
        Keyword arguments to be passed to the errorbar plot
    yerr: ArrayLike | None
        Error for the histogram bin contents (default: None)
    norm: bool
        If true, normalise the data to unit area (default: False)

    Returns
    -------
    None
        Plots the histogram on the axes with errorbars, if not None
    """
    nh, xe, xc, err = fill_hist_w(data, bins, range, weights)

    # normalise to unity
    _normalisation = np.sum(nh)
    if norm is True:
        nh = nh / _normalisation
        err = err / _normalisation

    hep.histplot(nh, xe, yerr=err, ax=ax, label=label, **kwargs)


def hist_step_fill(
    data: ArrayLike,
    range: tuple,
    ax: plt.Axes,
    bins: int = 50,
    weights: ArrayLike | None = None,
    label: str | None = None,
    norm: bool = False,
    **kwargs,
) -> None:
    """Histogram with shaded area

    Paramaters
    ----------
    x: ArrayLike
        Bin centers
    y: ArrayLike
        Bin population
    ax: plt.Axes
        Axes to plot on
    label: str | None
        Legend label for the data (default: None)
    kwargs: Any
        Keyword arguments to be passed to the errorbar plot
    yerr: ArrayLike | None
        Error for the histogram bin contents (default: None)
    norm: bool
        If true, normalise the data to unit area (default: False)

    Returns
    -------
    None
        Plots the histogram on the axes with errorbars, if not None
    """
    nh, xe, xc, err = fill_hist_w(data, bins, range, weights)

    # normalise to unity
    _normalisation = np.sum(nh)
    if norm is True:
        nh = nh / _normalisation
        err = err / _normalisation

    hep.histplot(nh, xe, ax=ax, **kwargs)
    lo_y = nh - err
    ax.bar(
        xc,
        bottom=lo_y,
        height=err * 2,
        alpha=0.33,
        width=xe[1] - xe[0],
        label=label,
        **kwargs,
    )


def eff_plot(
    x: ArrayLike,
    eff: Any,
    eff_err: Any,
    xerr: Any,
    label: str | None,
    ax: plt.Axes,
    fmt: str = ".",
    markersize: int = 5,
    draw_band: bool = True,
    **kwargs,
) -> None:
    """Efficiency plot with shaded error bands

    Parameters
    ----------
    x: ArrayLike
        Bin centers
    eff: Any
        Efficiency values
    eff_err: Any
        Efficiency errors
    label: str | None
        Legend label for the data (default: None)
    ax: plt.Axes
        Axes to plot on
    fmt: str
        Format of the plot (default: '.')
    markersize: int
        Size of the markers (default: 5)
    draw_band: bool
        If true, draw shaded error band (default: True)

    Returns
    -------
    None
        Plots the efficiency on the axes, with error band if draw_band is True
    """
    # quote in percentage
    eff = eff * 100.0
    eff_err = eff_err * 100.0

    if draw_band is True:
        _xerr = 0
        ax.fill_between(
            x, eff - eff_err, eff + eff_err, alpha=0.25, linewidth=0, **kwargs
        )
    else:
        _xerr = xerr

    ax.errorbar(
        x=x,
        y=eff,
        yerr=eff_err,
        xerr=_xerr,
        label=label,
        fmt=fmt,
        markersize=markersize,
    )
