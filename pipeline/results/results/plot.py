"""Plotting utility functions."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

import matplotlib.pyplot as plt
import scienceplots
import mplhep as hep
import pandas as pd
import numpy as np
from numpy.typing import ArrayLike
from typing import Any
from pathlib import Path
from results.channels import decay_labels

plt.style.use(
    [
        "science",
        "no-latex",
    ]
)


# fig, ax factory functions
# -------------------------
def simple_ax(
    title: str | None = "LHCb Unofficial",
    ylabel: str = "Candidates",
    normalised: bool = False,
    scale: str | None = None,
    logo: bool = True,
) -> tuple[Any, plt.Axes]:
    """Book simple ax

    Parameters
    ----------
    title: str | None
        Title of the plot (default: 'LHCb Unofficial')

    ylabel: str
        Y-axis label (default: 'Candidates')

    normalised: bool
        If true, normalise histograms to unity (default: False)

    Returns
    -------
    tuple[Any, Callable]
        Fig, ax plt.Axes objects
    """
    fig, ax = plt.subplots()

    ax.set_title(title, loc="right")
    ax.set_ylabel(ylabel)
    # logo
    ax.text(
        0.0,
        1.07,
        r"LHCb Simulation Preliminary",
        ha="left",
        va="top",
        transform=ax.transAxes,
        color="grey",
    )
    if scale is not None:
        ax.set_yscale(scale)

    return fig, ax


def simple_2ax(
    title: str | None = "LHCb Unofficial",
    ylabel: str = "Candidates",
    normalised: bool = False,
    scale: str | None = None,
    logo: bool = True,
) -> tuple[Any, plt.Axes, plt.Axes]:
    """Book simple ax

    Parameters
    ----------
    title: str | None
        Title of the plot (default: 'LHCb Unofficial')

    ylabel: str
        Y-axis label (default: 'Candidates')

    normalised: bool
        If true, normalise histograms to unity (default: False)

    Returns
    -------
    tuple[Any, Callable]
        Fig, ax plt.Axes objects
    """
    fig, (ax1, ax2) = plt.subplots(ncols=2, nrows=1, figsize=(8, 2.5))

    for ax in (ax1, ax2):
        ax.set_title(title, loc="right")
        # logo
        ax.text(
            0.0,
            1.07,
            r"LHCb Preliminary",
            ha="left",
            va="top",
            transform=ax.transAxes,
            color="grey",
        )

    if scale is not None:
        ax1.set_yscale(scale)
        ax1.set_ylabel(ylabel)

    return fig, ax1, ax2


def make_legend(
    ax: plt.Axes,
    on_plot: bool = True,
    ycoord: float = -0.6,
) -> None:
    """
    Place the legend below the plot, adjusting number of columns

    Parameters
    ----------
    ax: plt.Axes
        Axes object to place the legend on

    on_plot: bool
        If true, place the legend on the plot (default: True)

    ycoord: float
        Y-coordinate of the legend (default: -0.6)

    Returns
    -------
    None
        Places the legend on the axes
    """
    # count entries
    handles, labels = ax.get_legend_handles_labels()

    # decide the number of columns accordingly
    match len(labels):
        case 2:
            ncols = 2
        case other:
            ncols = 1

    # place the legend
    ax.legend(loc="best")
    if on_plot is False:
        ax.legend(
            bbox_to_anchor=(0.5, ycoord),
            loc="lower center",
            ncol=ncols,
            frameon=False,
        )


# save plots in multiple formats
def save_to(
    outdir: str,
    name: str,
) -> None:
    """Save the current figure to a path in multiple formats

    Generate directory path if unexeistent

    Parameters
    ----------
    outdir: str
        Directory path to save the figure
    name: str
        Name of the plot

    Returns
    -------
    None
        Saves the figure to the path in pdf and png formats
    """
    Path(outdir).mkdir(parents=True, exist_ok=True)
    [plt.savefig(f"{outdir}/{name}.{ext}") for ext in ["pdf", "png"]]


# plot data and pdfs
# ------------------
def fill_hist_w(
    data: ArrayLike, bins: int, range: tuple, weights: ArrayLike | None = None
) -> tuple[Any, ...]:
    """Fill histogram accounting weights

    Parameters
    ----------
    data: ArrayLike
        Data to be histogrammed
    bins: int
        Number of bins
    range: tuple
        Range of the histogram
    weights: ArrayLike | None
        Weights to be applied to the data (default: None)

    Returns
    -------
    nh: ArrayLike
        Bin contents
    xe: ArrayLike
        Bin edges
    xc: ArrayLike
        Bin centers
    nh_err: ArrayLike
        Bin errors
    """
    # bin contents, bin edges, bin centers
    nh, xe = np.histogram(data, bins=bins, range=range)
    xc = 0.5 * (xe[1:] + xe[:-1])

    # if no weights, Poisson errors
    nh_err = nh**0.5

    # if weights, careful treatment of bin contents and errors via boost-histogram
    # https://www.zeuthen.desy.de/~wischnew/amanda/discussion/wgterror/working.html
    if weights is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        whist.fill(data, weight=weights)
        cx = whist.axes[0].centers
        nh = whist.view().value
        nh_err = whist.view().variance ** 0.5

    return nh, xe, xc, nh_err


def plot_data(
    data: ArrayLike,
    ax: plt.Axes,
    range: tuple[float, float],
    bins: int = 50,
    weights: ArrayLike | None = None,
    label: str | None = None,
    norm: bool = False,
    color: str = "black",
    **kwargs: Any,
) -> None:
    """Plot the data, accounting for weights if provided

    Parameters
    ----------
    data: ArrayLike
        Data to be plotted

    ax: plt.Axes
        Axes to plot on

    bins: int
        Number of bins (default: 50)

    range: tuple[float, float]
        Range of the data

    weights: ArrayLike | None
        Weights for the data (default: None)

    label: str | None
        Legend label for the data (default: None)

    kwargs: Any
        Keyword arguments to be passed to the errorbar plot

    norm: bool
        If true, normalise the data to unit area (default: False)

    Returns
    -------
    None
        Plots the data on the axes
    """
    nh, xe, cx, err = fill_hist_w(data, bins, range, weights)

    # normalise to unity
    _normalisation = np.sum(nh)
    if norm:
        nh = nh / _normalisation
        err = err / _normalisation

    ax.errorbar(
        cx,
        nh,
        yerr=err,
        xerr=(xe[1] - xe[0]) / 2,
        label=label,
        fmt=".",
        markersize=3,
        color=color,
        **kwargs,
    )


def hist_err(
    data: ArrayLike,
    ax: plt.Axes,
    range: tuple[float, float],
    bins: int = 50,
    weights: ArrayLike | None = None,
    label: str | None = None,
    norm: bool = False,
    **kwargs,
) -> None:
    """Wrapper for mplhep histplot method

    Paramaters
    ----------
    nh: ArrayLike
        Histogram bin contents
    xe: ArrayLike
        Histogram bin edges
    ax: plt.Axes
        Axes to plot on
    label: str | None
        Legend label for the data (default: None)
    kwargs: Any
        Keyword arguments to be passed to the errorbar plot
    yerr: ArrayLike | None
        Error for the histogram bin contents (default: None)
    norm: bool
        If true, normalise the data to unit area (default: False)

    Returns
    -------
    None
        Plots the histogram on the axes with errorbars, if not None
    """
    nh, xe, xc, err = fill_hist_w(data, bins, range, weights)

    # normalise to unity
    _normalisation = np.sum(nh)
    if norm is True:
        nh = nh / _normalisation
        err = err / _normalisation

    hep.histplot(nh, xe, yerr=err, ax=ax, label=label, **kwargs)


def hist_step_fill(
    data: ArrayLike,
    range: tuple,
    ax: plt.Axes,
    bins: int = 50,
    weights: ArrayLike | None = None,
    label: str | None = None,
    norm: bool = False,
    **kwargs,
) -> None:
    """Histogram with shaded area

    Paramaters
    ----------
    x: ArrayLike
        Bin centers
    y: ArrayLike
        Bin population
    ax: plt.Axes
        Axes to plot on
    label: str | None
        Legend label for the data (default: None)
    kwargs: Any
        Keyword arguments to be passed to the errorbar plot
    yerr: ArrayLike | None
        Error for the histogram bin contents (default: None)
    norm: bool
        If true, normalise the data to unit area (default: False)

    Returns
    -------
    None
        Plots the histogram on the axes with errorbars, if not None
    """
    nh, xe, xc, err = fill_hist_w(data, bins, range, weights)

    # normalise to unity
    _normalisation = np.sum(nh)
    if norm is True:
        nh = nh / _normalisation
        err = err / _normalisation

    hep.histplot(nh, xe, ax=ax, **kwargs)
    lo_y = nh - err
    ax.bar(
        xc,
        bottom=lo_y,
        height=err * 2,
        alpha=0.33,
        width=xe[1] - xe[0],
        label=label,
        **kwargs,
    )


def viz_eff_1D(eff_dict: dict, path: str) -> None:
    """Visualise topo efficiencies, in 1D"""
    for stage in eff_dict.keys():
        for denom in eff_dict[stage].keys():
            fig, ax = simple_ax(title=f"{denom}")
            for trigger in eff_dict[stage][denom]:
                decay_modes = list(eff_dict[stage][denom][trigger].keys())
                effs = []
                errs = []
                xlabels = []
                for decay in decay_modes:
                    xlabels.append(decay_labels[decay]["label"])
                    effs.append(eff_dict[stage][denom][trigger][decay].n * 100.0)
                    errs.append(eff_dict[stage][denom][trigger][decay].s * 100.0)
                ax.errorbar(
                    x=decay_modes,
                    y=effs,
                    yerr=errs,
                    fmt=".",
                    markersize=5,
                    label=trigger,
                )
            ax.legend()
            ax.set_ylim(0, 110)
            ax.set_ylabel(r"Efficiency, $\varepsilon~[\%]$")
            ax.axhline(100, color="grey", linestyle="--", lw=0.5)
            ax.set_xticks(range(len(xlabels)))  # Set x-tick positions
            plt.xticks(
                ax.get_xticks(), labels=xlabels, rotation=90
            )  # Assign x-labels and rotate by 45 degrees
            plt_path = Path(f"{path}/{stage}")
            plt_path.mkdir(parents=True, exist_ok=True)
            plt.savefig(f"{plt_path}/{denom}.pdf")
            plt.close()
