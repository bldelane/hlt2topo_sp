"""Factory for the efficiency denominator categories."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

import pandas as pd
from numpy.typing import ArrayLike
from typing import Protocol, Tuple, Any
import fnmatch
import yaml

_config = "/home/nschulte/hlt2topo_sp/pipeline/config.yaml"

def yml_to_dict(
    _path: str,
) -> dict:
    """Read in a yaml file into a dict"""
    with open(_path, "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

class DataFrameBranchFinder:
    """Mixin class to provide method for finding dataframe columns based on a wildcard expression.

    Methods:
        find_branches_with_wildcard(df, wildcard): returns the dataframe columns that match the wildcard.
    """

    def find_branches_with_wildcard(self, df: pd.DataFrame, wildcard: str) -> list[str]:
        """Find dataframe columns that match the wildcard.

        Args:
            df: Pandas dataframe.
            wildcard: String to match column names with.

        Returns:
            List of dataframe columns that match the wildcard.
        """
        return [col for col in df.columns if fnmatch.fnmatch(col, wildcard)]


class DataFrameSelector(Protocol):
    """Define a protocol; if an `apply_selection` method is defined, then the class is a DataFrameSelector."""

    def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame] | None:
        """Abstract method to apply a selection to a dataframe."""
        pass


class NaiveSelector(DataFrameSelector):
    """NaiveSelector class implements DataFrameSelector protocol and returns the input dataframe as-is."""

    def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame]:
        """Apply naive selection: return the input dataframe as-is.

        Args:
            df: Pandas dataframe.

        Returns:
            Tuple of length of dataframe and the dataframe itself.
        """
        return len(df), df


# class PreFilter(DataFrameSelector):
#     """NaiveSelector class implements DataFrameSelector protocol and returns the input dataframe as-is."""

#     def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame]:
#         """Apply prefilter selection: return the input dataframe as-is.

#         Args:
#             df: Pandas dataframe.

#         Returns:
#             Tuple of length of dataframe and the dataframe itself.
#         """
#         config = yml_to_dict(_config)
#         _prefilter = config["prefilter_combination"][config["key"]]

#         print("enable prefiltering")
#         if _prefilter is not None:
#             print(" ----- Prefiltering dataset -----")
#             print(f"Booked prefilter: {_prefilter} ")
#             print("Candidates before prefiltering: ", len(df))
#             df = df.query(_prefilter)

#         return len(df), df
    

class RecoChildrenSelector(DataFrameBranchFinder, DataFrameSelector):
    """RecoChildrenSelector class finds 'Track*ETA' columns and ensures their values are between 2 and 5."""

    def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame]:
        """Apply selection that Track*ETA values are between 2 and 5.

        Args:
            df: Pandas dataframe.

        Returns:
            Tuple of length of dataframe and the dataframe after selection.
        """
        # Find all the 'Track*ETA' columns
        track_eta_columns = self.find_branches_with_wildcard(df, "Track*_ETA")

        # Use all() to ensure all the Track*ETA values are between 2 and 5
        condition = (df[track_eta_columns] > 2) & (df[track_eta_columns] < 5)
        condition = condition.all(
            axis=1
        )  # all Track*ETA values must satisfy the condition

        # Apply the condition to filter the dataframe
        filtered_df = df.loc[condition]

        # Check that the condition has been met
        assert (
            (
                (filtered_df[track_eta_columns] > 2)
                & (filtered_df[track_eta_columns] < 5)
            )
            .all()
            .all()
        ), "Some 'Track*ETA' values are not between 2 and 5"

        return len(filtered_df), filtered_df


class RecoChildrenParentCutSelector(RecoChildrenSelector):
    """RecoChildrenParentCutSelector class applies additional selection based on 'ThreeBody' or 'TwoBody' variables.

    This class inherits from RecoChildrenSelector and overrides the apply_selection method to apply additional
    selection based on 'ThreeBody_PT', 'ThreeBody_BPVLTIME', 'TwoBody_PT', and 'TwoBody_BPVLTIME'.
    """

    def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame]:
        """Apply additional selection based on 'ThreeBody' or 'TwoBody' variables.

        Args:
            df: Pandas dataframe.

        Returns:
            Tuple of length of dataframe and the dataframe after selection.
        """
        # Apply the original selection
        df_len, filtered_df = super().apply_selection(df)

        # Check if 'ThreeBody_PT' and 'ThreeBody_BPVLTIME' are in the dataframe
        if "ThreeBody_PT" in df.columns and "ThreeBody_BPVLTIME" in df.columns:
            # Apply the condition for ThreeBody_PT and ThreeBody_BPVLTIME
            filtered_df = filtered_df[
                (filtered_df["ThreeBody_PT"] > 2) #2000
                & (filtered_df["ThreeBody_BPVLTIME"] > 0.2e-3)
            ]
        else:
            # Apply the condition for TwoBody_PT and TwoBody_BPVLTIME
            print("Using 2body dataframe")
            filtered_df = filtered_df[
                (filtered_df["TwoBody_PT"] > 2) #2000
                & (filtered_df["TwoBody_BPVLTIME"] > 0.2e-3)
            ]

        # Return the number of remaining rows (events) and the filtered dataframe
        return len(filtered_df), filtered_df


class RecoChildrenAndChildPTSelector(RecoChildrenSelector):
    """RecoChildrenAndChildPTSelector class applies additional selection based on 'Track*PT' variables.

    This class inherits from RecoChildrenSelector and overrides the apply_selection method to apply additional
    selection based on 'Track*PT' which should be greater than 250.
    """

    def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame]:
        """Apply additional selection that all 'Track*PT' values are greater than 250.

        Args:
            df: Pandas dataframe.

        Returns:
            Tuple of length of dataframe and the dataframe after selection.
        """
        # Apply the original selection
        df_len, filtered_df = super().apply_selection(df)

        # Find all the 'Track*PT' columns
        track_pt_columns = self.find_branches_with_wildcard(filtered_df, "Track*_PT")

        # Create a condition where all 'Track*PT' values are greater than 250
        condition = filtered_df[track_pt_columns] > 250
        condition = condition.all(
            axis=1
        )  # all 'Track*PT' values must satisfy the condition

        # Apply the condition to filter the dataframe
        filtered_df = filtered_df.loc[condition]

        # Check that the condition has been met
        assert (
            (filtered_df[track_pt_columns] > 250).all().all()
        ), "Some 'Track*PT' values are not greater than 250"

        return len(filtered_df), filtered_df


class RecoBDecaySelector(DataFrameSelector):
    """RecoBDecaySelector class applies both RecoChildrenParentCutSelector and RecoChildrenAndChildPTSelector.

    This class combines the selection of RecoChildrenParentCutSelector and RecoChildrenAndChildPTSelector.
    """

    def __init__(self):
        """Initializes with dataframe and creates instances of parent and child selectors."""
        self.parent_selector = RecoChildrenParentCutSelector()
        self.child_selector = RecoChildrenAndChildPTSelector()

    def apply_selection(self, df: pd.DataFrame) -> Tuple[int, pd.DataFrame]:
        """Applies both RecoChildrenParentCutSelector and RecoChildrenAndChildPTSelector to the dataframe.

        Args:
            df: Pandas dataframe.

        Returns:
            Tuple of length of dataframe and the dataframe after selections.
        """
        # Apply the original selection from RecoChildrenParentCutSelector
        df_len, filtered_df = self.parent_selector.apply_selection(df)

        # Apply the selection from RecoChildrenAndChildPTSelector
        df_len, filtered_df = self.child_selector.apply_selection(filtered_df)

        return df_len, filtered_df


class DataFrameSelectorFactory:
    """Factory class for DataFrame selectors. Creates selectors based on a given mode."""

    def __init__(self):
        """Initialize with a dictionary of selector creators."""
        self._creators = {
            "Naive": NaiveSelector,
            #"PreFilter": PreFilter,
            "CanRecoChildren": RecoChildrenSelector,
            "CanRecoChildrenParentCut": RecoChildrenParentCutSelector,
            "CanRecoChildrenAndChildPT": RecoChildrenAndChildPTSelector,
            "RecoBDecay": RecoBDecaySelector,
        }

    def create_selector(self, mode: str) -> DataFrameSelector:
        """Create a selector based on the mode.

        Args:
            mode: String indicating the mode of the selector.
            df: DataFrame to which selector will be applied.

        Returns:
            An instance of a DataFrameSelector based on the mode.

        Raises:
            ValueError: If the mode does not match any selector.
        """
        creator = self._creators.get(mode)
        if not creator:
            raise ValueError(f"Invalid mode: {mode}")
        return creator()


# Usage example
# -------------
# # Assume df is your DataFrame
# df = pd.DataFrame()

# # Create an instance of DataFrameSelectorFactory
# factory = DataFrameSelectorFactory()

# FIXME: update the keys in the instructional snippet below
# # Create different types of selectors
# naive_selector = factory.create_selector("naive")
# reco_children_selector = factory.create_selector("reco_children")
# reco_children_parent_cut_selector = factory.create_selector("reco_children_parent_cut")
# reco_children_and_child_pt_selector = factory.create_selector("reco_children_and_child_pt")
# reco_b_decay_selector = factory.create_selector("reco_b_decay")

# # Apply selection on df
# _, naive_selected_df = naive_selector.apply_selection(df)
# _, reco_children_selected_df = reco_children_selector.apply_selection(df)
# _, reco_children_parent_cut_selected_df = reco_children_parent_cut_selector.apply_selection(df)
# _, reco_children_and_child_pt_selected_df = reco_children_and_child_pt_selector.apply_selection(df)
# _, reco_b_decay_selected_df = reco_b_decay_selector.apply_selection(df)
