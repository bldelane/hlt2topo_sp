"""Inference with the NN model & associated utils."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

import pandas as pd
import numpy as np
from .io import load_ntuple

# necessary to source the topo
import sys, os, pathlib

sys.path.insert(1, "/home/nschulte/hlt2topo_sp")
import topo
import torch


def execute_inference(
    model_spec: dict,
    model_path: str,
    probe: pd.DataFrame,
    key: str,  # 'TwoBody' or 'ThreeBody'
    config_path: str,
    _lambda: float = 2.0,
) -> pd.DataFrame:
    """Perform inference on the preprocessed minbias samples, using the trained model state."""
    # load the config file
    # --------------------
    config = topo.yml_to_dict(config_path)

    # NN config
    # ----------
    torch.manual_seed(config["seed"])  # reproducibility

    device = topo.get_device()
    features = topo.get_feats(config=config, key=key)

    print("Moving Model to Device...")
    model = topo.model_arch_optim(
        params_dict=model_spec,
        monotonic=config["monotonic"],
        robust=config["robust"],
        _nbody=key,
        _features=features,
        LIP=float(_lambda),
        config_path=config_path,
    ).to(device)
    print("Success\n")

    # load the trained model state dict
    # ---------------------------------
    if device == torch.device("cpu"):
        model.load_state_dict(
            torch.load(model_path, map_location=torch.device("cpu"))
        )  # Map Location needed if running on cpu
    else:
        model.load_state_dict(torch.load(model_path))
    model.eval()

    # execute inference
    # -----------------
    X = torch.tensor(probe[features].to_numpy().astype(np.float32)).to(device)

    # produce the two-class predictions
    probe["preds_per_cand"] = torch.sigmoid(model(X)).cpu().detach().numpy()

    return probe


def test_cleanup(df: pd.DataFrame, pre_cleanup_nevts: int) -> None:
    """Test that the cleanup was successful."""
    assert df["unique_event"].nunique() == df.shape[0], "Not all events are unique"
    assert len(df) < pre_cleanup_nevts, "No duplicates were dropped"


def retain_unique_evt(
    df: pd.DataFrame,
    mode: str = "random",  # max for conservative rate estimation - ignoring effect on efficiency per candidate
    seed: int = 0,  # added seed for reproducible random selection
) -> pd.DataFrame:
    "Retain only one candidate of interest, as identified by the mode, per event."
    pre_cleanup_nevts = len(df)

    print("Dropping duplicates...")

    match mode:
        case "max":
            # conservative approach: retain the highest-scoring candidate per event
            print("Enacting conservative approach: highest-scoring candidate per event")
            df = df.loc[
                df.groupby("unique_event")["preds_per_cand"].idxmax()
            ]  # sort all events by the highest response and retain, per-event, the highest-scoring candidate -> conservative approach
            test_cleanup(df, pre_cleanup_nevts)
            return df
        case "random":
            # random approach: retain a random candidate per event
            print("Enacting random approach: random candidate per event")
            df = (
                df.groupby("unique_event")
                .apply(lambda x: x.sample(n=1, random_state=seed))
                .reset_index(drop=True)
            )
            test_cleanup(df, pre_cleanup_nevts)
            return df
        case _:
            raise ValueError(f"Invalid mode: {mode}")


def inference_on_preprocessed(
    trigger_type: str,
    probe_data: pd.DataFrame,
    config_path: str,
    model_spec: dict,
    lipschitz_bound: float = 2.0,
    **kwargs,
) -> pd.DataFrame:
    """Execute load the dataset, execute NN inference, retain one candidate per event.

    Args:
        trigger_type (str): Trigger mode; allowed values: 'TwoBody', 'ThreeBody'
        mode (str): Decay mode
        models_dir (Path): Path to trained model
        data_dir (Path): Path to data

    """

    model_path = f"/home/nschulte/hlt2topo_sp/pipeline/{trigger_type}_models_master/lambda{lipschitz_bound}/train/trained_model.pt"

    return execute_inference(
        model_spec=model_spec,
        model_path=model_path,
        probe=probe_data,
        key=trigger_type,
        config_path=config_path,
        _lambda=lipschitz_bound,
    )
