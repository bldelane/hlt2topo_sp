import matplotlib.pyplot as plt
import numpy as np
import scienceplots
from .plot import simple_ax
from pathlib import Path
from hist import Hist
import hist
import pandas as pd
import tabulate
import pandas as pd
import math
from collections import namedtuple

plt.style.use(["science", "no-latex"])


class CombinedEvtEfficiencyCalculator:
    """Calculate the efficiency and binomial error for a given dataset, removing the candidate multiplicity."""

    def __init__(self, df_num: pd.DataFrame, df_denom: pd.DataFrame):
        """Initialize the efficiency calculator."""
        self.num_data = df_num["eventRun"].nunique()
        self.denom_data = df_denom["eventRun"].nunique()

        assert self.num_data <= self.denom_data, "Ill-defined efficiency ratio."
        assert self.num_data == len(
            df_num["eventRun"].unique()
        ), "Degenerate numerator."
        assert self.denom_data == len(
            df_denom["eventRun"].unique()
        ), "Degenerate denominator."

    def calculate(self):
        """Calculate the scalar efficiency and binomial error.

        Args:
            num_data: Number of UNIQUE events in the numerator.
            denom_data: Number of UNIQUE events in the denominator.

        Returns:
            Efficiency: Named tuple with fields 'value' and 'error'.

        Raises:
            ValueError: If denom_data is zero.
        """
        # Protect against division by zero
        if self.denom_data == 0:
            raise ValueError("denom_data cannot be zero")

        # Calculate efficiency
        efficiency = self.num_data / self.denom_data

        # Calculate binomial error
        error = math.sqrt((efficiency * (1 - efficiency)) / self.denom_data)

        # Return as named tuple
        Efficiency = namedtuple("Efficiency", ["n", "s"])
        return Efficiency(n=efficiency, s=error)

class HighestEfficiencySelector:
    """Select the best candidate per event based on a given criterion."""

    def __init__(self, twobody_df, threebody_df):
        self.twobody_eff = self.

    def filter(self, df, column):
        if self.criterion == "max":
            idx = df.groupby(["eventRun"])[column].idxmax()
        else:
            idx = df.groupby(["eventRun"])[column].idxmin()

        filtered_df = df.loc[idx].reset_index(drop=True)
        assert (
            len(filtered_df) == df["eventRun"].nunique()
        ), "Not all events are unique - filtering unsuccessful"

        return filtered_df