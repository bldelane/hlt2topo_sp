import matplotlib.pyplot as plt
import numpy as np
import scienceplots
from .plot import simple_ax
from pathlib import Path
from hist import Hist
import hist
import pandas as pd
import tabulate
import pandas as pd
import math
from collections import namedtuple

plt.style.use(["science", "no-latex"])


class UniqueEvtEfficiencyCalculator:
    """Calculate the efficiency and binomial error for a given dataset, removing the candidate multiplicity."""

    def __init__(self, df_num: pd.DataFrame, df_denom: pd.DataFrame):
        """Initialize the efficiency calculator."""
        self.num_data = df_num["eventRun"].nunique()
        self.denom_data = df_denom["eventRun"].nunique()

        assert self.num_data <= self.denom_data, "Ill-defined efficiency ratio."
        assert self.num_data == len(
            df_num["eventRun"].unique()
        ), "Degenerate numerator."
        assert self.denom_data == len(
            df_denom["eventRun"].unique()
        ), "Degenerate denominator."

    def calculate(self):
        """Calculate the scalar efficiency and binomial error.

        Args:
            num_data: Number of UNIQUE events in the numerator.
            denom_data: Number of UNIQUE events in the denominator.

        Returns:
            Efficiency: Named tuple with fields 'value' and 'error'.

        Raises:
            ValueError: If denom_data is zero.
        """
        # Protect against division by zero
        if self.denom_data == 0:
            raise ValueError("denom_data cannot be zero")

        # Calculate efficiency
        efficiency = self.num_data / self.denom_data

        # Calculate binomial error
        error = math.sqrt((efficiency * (1 - efficiency)) / self.denom_data)

        # Return as named tuple
        Efficiency = namedtuple("Efficiency", ["n", "s"])
        return Efficiency(n=efficiency, s=error)


def prefilter_eff_report(
    df: pd.DataFrame,
    selection: str,
    probe_branch: str = "eventRun",
    outfile: str | None = None,
    save: bool = True,
) -> None:
    """Computed the per-cut efficiencies for the given dataframe.

    Args:
        df: Dataframe to compute the efficiencies for.
        selection: Selection to compute the efficiencies for.
        probe_branch: branch against which the unique counter is adopted.
        outfile: TeX path to save the results to.

    Returns:
        None: Prints the efficiencies to the terminal.
    """
    # NOTE: count post-selection! ie if at least one candidate triggers, the event is counted but not multiple times
    tot_k = df.query(selection)[
        probe_branch
    ].nunique()  # apply selection and count such that any if at least one cand triggers, the event is counted but not multiple times
    tot_N = df[probe_branch].nunique()  # all event, neglect multiple cands
    tot_eff = tot_k / tot_N
    tot_eff_err = np.sqrt(1 / tot_N * tot_eff * (1 - tot_eff))

    conditions = selection.split("&")

    results = []

    for condition in conditions:
        condition = condition.strip()  # Remove leading/trailing whitespace
        efficiency = df.query(condition)[probe_branch].nunique() / tot_N
        error = np.sqrt(1 / tot_N * efficiency * (1 - efficiency))
        results.append((condition, efficiency, error))

    # add the total exclusive
    results.append(("Total", tot_eff, tot_eff_err))

    # Print as a table
    headers = ["Condition", "Efficiency", "Error"]
    print(tabulate.tabulate(results, headers=headers, tablefmt="pretty"))

    # # Save as a tex file
    if outfile is not None:
        with open(outfile, "w") as f:
            f.write(tabulate.tabulate(results, headers=headers, tablefmt="latex"))


class BestCandidateSelector:
    """Select the best candidate per event based on a given criterion."""

    def __init__(self, criterion):
        assert criterion in ["max", "min"], "Criterion should be either 'max' or 'min'"
        self.criterion = criterion

    def filter(self, df, column):
        if self.criterion == "max":
            idx = df.groupby(["eventRun"])[column].idxmax()
        else:
            idx = df.groupby(["eventRun"])[column].idxmin()

        filtered_df = df.loc[idx].reset_index(drop=True)
        assert (
            len(filtered_df) == df["eventRun"].nunique()
        ), "Not all events are unique - filtering unsuccessful"

        return filtered_df

class EfficiencyHistogram:
    def __init__(self, kind, edges, label):
        self.kind = kind
        self.edges = edges
        self.label = label

        # Define dictionaries to store numerators and denominators
        self.numerators = {}
        self.denominators = {}
        self.legend_labels = {}

    def fill(self, num_data, den_data, label):
        """
        Fill the histograms with provided data.
        """
        if label not in self.numerators:
            self.numerators[label] = Hist(
                hist.axis.Variable(self.edges, label=f"{label} (numerator)")
            )
            self.denominators[label] = Hist(
                hist.axis.Variable(self.edges, label=f"{label} (denominator)")
            )

            # Set default legend label to the distribution label
            self.legend_labels[label] = label

        self.numerators[label].fill(num_data)
        self.denominators[label].fill(den_data)

    def add_histogram(self, kind, num_data, den_data, label):
        """
        Add an additional histogram to the efficiency calculation.
        """
        # Strip "MeV" from the label for the legend
        legend_label = label.replace("MeV", "")
        self.legend_labels[label] = legend_label

        additional_hist = Hist(
            hist.axis.Variable(self.edges, label=f"{label} (numerator)")
        )
        additional_hist.fill(num_data)
        self.numerator += additional_hist

        additional_hist = Hist(
            hist.axis.Variable(self.edges, label=f"{label} (denominator)")
        )
        additional_hist.fill(den_data)
        self.denominator += additional_hist

    def efficiency(self, label):
        """
        Calculate efficiency histogram by taking the ratio of numerator and denominator.
        """
        N = self.numerators[label].view()
        D = self.denominators[label].view()

        # Initialize efficiency and error arrays with zeros
        efficiency = np.zeros_like(N)
        error = np.zeros_like(N)

        # Update where D is not zero
        mask = D > 0
        efficiency[mask] = N[mask] / D[mask]
        error[mask] = (
            1 / D[mask] * np.sqrt(N[mask] * (1 - N[mask] / D[mask]))
        )  # paterno

        # Set error to np.inf or another special value where D is zero
        error = np.nan_to_num(error, nan=100.0)  # 100.0 to signal degenerate

        # Fill new histogram and return
        eff_histogram = Hist(
            hist.axis.Variable(self.edges, label="Efficiency"),
            storage=hist.storage.Weight(),
        )

        # fill with eff and error**2
        eff_values = np.zeros_like(N, dtype=[("value", "f8"), ("variance", "f8")])
        eff_values["value"] = efficiency
        eff_values["variance"] = error**2
        eff_histogram.view()[:] = eff_values  # requires pairs

        return eff_histogram

    def eff_ratio(
        self, path: str, title: str, save: bool = True, channel: str | None = None
    ) -> None:
        """
        Visualize the efficiency histogram.
        """
        # Plotting
        fig, ax = simple_ax(title=title)

        for label in self.numerators.keys():
            ratio = self.efficiency(label)
            ax.errorbar(
                x=ratio.axes[0].centers,
                y=ratio.view().value * 100.00,
                yerr=ratio.view().variance ** 0.5 * 100.00,
                xerr=ratio.axes[0].widths / 2,
                fmt=".",
                markersize=3,
                alpha=0.6,
                label=self.legend_labels[label]  # Use the stripped legend label
            )

        ax.set_ylabel(r"$\varepsilon~[\%]]$")
        ax.set_xlabel(self.label)
        ax.set_ylim(bottom=0.0, top=110.0)
        ax.axhline(y=100.0, color="black", linestyle="--", linewidth=0.5)
        if channel is not None:
            ax.plot([], [], "", label=channel, color="white")
        ax.legend(loc="lower right", frameon=True)
        if save:
            Path(path).mkdir(parents=True, exist_ok=True)
            print(f"Saving to {path}")
            plt.savefig(f"{path}/{self.kind}_{title}.pdf")




class EfficiencyHistogramFactory:
    def __init__(self):
        # Pre-defined configurations for different histogram kinds
        self.configs = {
            "tau": {
                "edges": [0, 0.0010, 0.0020, 0.0030, 0.0040, 0.005],
                "label": r"$\tau$ [ns]",
            },
            "pt": {
                "edges": [
                    0,
                    1000,
                    2.0e3,
                    3.0e3,
                    4.0e3,
                    5.0e3,
                    6.0e3,
                    7.0e3,
                    8.0e3,
                    9.0e3,
                    10.0e3,
                    11.0e3,
                    12.0e3,
                    13.0e3,
                    14.0e3,
                    15.0e3,
                    16.0e3,
                    17.0e3,
                    18.0e3,
                    19.0e3,
                    20.0e3,
                ],
                "label": r"$p_T$ [MeV$/c$]",
            },
            "pt_gev": {
                "edges": [
                    0,
                    0.05,
                    0.1,
                    0.15,
                    0.2,
                    2.0,
                    2.5,
                    3.0,
                    3.5,
                    4.0,
                    4.5,
                    5.0,
                    10.0,
                ],
                "label": r"$p_T$ [GeV$/c$]",
            },
        }

    def create(self, kind):
        # Get the configuration for the specific kind
        config = self.configs.get(kind)

        # Create an EfficiencyHistogram object with the specific configuration
        return EfficiencyHistogram(kind, config["edges"], config["label"])


# # Usage
# factory = EfficiencyHistogramFactory()
# tau_histogram = factory.create("tau")


# def topo_hist_factory(
#     resp_23body_df: pd.DataFrame,
#     twobody_cut: float,
#     threebody_cut: float,
#     mode: str,  # exclusive, incluive or only one
#     variable: str,  # the variable to create the histogram for
# ) -> EfficiencyHistogram:
#     """Return the selected-events histogram for the given variable."""

#     # Create an instance of EfficiencyHistogramFactory
#     factory = EfficiencyHistogramFactory()

#     # check both response columns are present
#     assert "preds_2body" in resp_23body_df.columns, "Missing preds_2body column"
#     assert "preds_3body" in resp_23body_df.columns, "Missing preds_3body column"

#     # common
#     den = resp_23body_df.shape[0]

#     # how do we want to evaluate efficiencies?
#     match mode:
#         case "inclusive":  # need to work out the exclusive vs inlcusive etc cases
#             num = resp_23body_df.query(
#                 "preds_2body > @twobody_cut or preds_3body > @threebody_cut"
#             ).shape[0]
#             return factory.create(variable)
#         case "exclusive_2body":
#             num = resp_23body_df.query("preds_2body > @twobody_cut").shape[0]
#             return factory.create(variable)
#         case "exclusive_3body":
#             num = resp_23body_df.query("preds_3body > @threebody_cut").shape[0]
#             return factory.create(variable)
#         case "only_2body":
#             num = resp_23body_df.query(
#                 "preds_2body > @twobody_cut and preds_3body < @threebody_cut"
#             ).shape[0]
#             return factory.create(variable)
#         case _:
#             raise ValueError("missing mode, to be added")
