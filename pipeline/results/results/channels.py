decay_labels = {
    "3MuNu": {
        "label": r"$B^+ \to \mu^+ \mu^- \mu^+ \nu_{\mu}$",
        "multiplicity": 3,
        "process": "b23lnu", 
    },
    "D0Ds": {
        "label": r"$B^+ \to \overline{D}^0 D_s^+$",
        "multiplicity": 5, #KKpiKpi
        "process": "B2DD",
    },
    "D0KsPi": {
        "label": r"$B^+ \to \overline{D}^0 K_s^0 \pi^+$",
        "multiplicity": 5, #Kpipipipi
        "process": "B2DhX",
    },
    "DDKPi": {
        "label": r"$B^0 \to \overline{D}^0 D^0 K^+ \pi^-$", 
        "multiplicity": 8, #KpiKpipipiKpi
        "process": "B2DDX",
    },
    "DPiPiPi": {
        "label": r"$B^0 \to \overline{D}^0 \pi^+ \pi^- \pi^0$",
        "multiplicity": 5, #Kpipipipi
        "process": "B2DhX",
    },
    "DsMuNu": {
        "label": r"$B^+ \to D_s^+ \mu^+ \nu_{\mu}$",
        "multiplicity": 4, #KKpimu
        "process": "B2DhX",
    },
    "DsPi": {
        "label": r"$B^0 \to D_s^- \pi^+$",
        "multiplicity": 4, #pipiKpi
        "process": "B2Dh",
    },
    "DstD0": {
        "label": r"$B^0 \to D^{*+} \overline{D}^0$",
        "multiplicity": 5, #KpipiKpi
        "process": "B2DD",
    },
    "DstTauNu": {
        "label": r"$B^+ \to D^{*-} \tau^+ \nu_{\tau}$",
        "multiplicity": 7, #pipipipinuKpi
        "process": "B2DhX",
    },
    "JpsiPhiKs": {
        "label": r"$B^0 \to J/\psi K_s^0 \phi$",
        "multiplicity": 6, #mumuKKpipi
        "process": "?",
    },
    "K6Mu": {
        "label": r"$B^+ \to K^+ \mu^+ \mu^- \mu^+ \mu^- \mu^+ \mu^-$",
        "multiplicity": 7, #Kmumumumumumu
        "process": "B2ll",
    },
    "KPiPi": {
        "label": r"$B^+ \to K^+ \pi^- \pi^0$",
        "multiplicity": 3, #Kpipi
        "process": "?",
    },
    "KstGamma": {
        "label": r"$B^0 \to K^{*0} \gamma$",
        "multiplicity": 2, #Kpi
        "process": "?",
    },
    "Lc2625MuNu": {
        "label": r"$\Lambda_b^0 \to \Lambda_c(2625)^+ \mu^+ \nu_{\mu}$",
        "multiplicity": 6, #pipimupKpi
        "process": "Lb2LcX",
    },
    "LcPPi": {
        "label": r"$B^+ \to \overline{\Lambda}_c^- \pi^+ p$",
        "multiplicity": 5, #pKpippi
        "process": "?",
    },
    "PhiKst": {
        "label": r"$B^0 \to \phi K^{*0}$",  
        "multiplicity": 4, #KKKpi
        "process": "?",
    },
    "PiMuNu": {
        "label": r"$B^0 \to \pi^- \mu^+ \nu_{\mu}$",
        "multiplicity": 2, #pimu
        "process": "?",
    },
    "Bp_KpJpsi": {
        "label": r"$B^+ \to J/\psi K^+$",
        "multiplicity": 3, #mumuK
        "process": "?",
    },
    # muon 2- & 3-body b topo: include tau, mu, Vcb, Vub
    # --------------------------------------------------
    "Lb_LcMuNu": {
        "label": r"$\Lambda_b^0 \to \Lambda_c^+ \mu^- \overline{\nu}_{\mu}$",
        "multiplicity": 4, #pKpimu 
        "process": "?",
    },
    "Lb_PMuNu": {
        "label": r"$\Lambda_b^0 \to p \mu^- \overline{\nu}_{\mu}$",
        "multiplicity": 2, #pmu 
        "process": "?",
    },
    "Bc_JpsiTauNu": {
        "label": r"$B_c^+ \to J/\psi \tau^+ \nu_{\tau}$",
        "multiplicity": 3, #mumumu
        "process": "?",
    },
    "Bc_JpsiMuNu": {
        "label": r"$B_c^+ \to J/\psi \mu^+ \nu_{\mu}$",
        "multiplicity": 3, #mumumu 
        "process": "?",
    },
    "Bs_KMuNu": {
        "label": r"$B^0_s \to K^- \mu^+ \nu_{\mu}$",
        "multiplicity": 2, #Kmu 
        "process": "?",
    },
    "Bs_KTauNu": {
        "label": r"$B^0_s \to K^- \tau^+ \nu_{\tau}$",
        "multiplicity": 2,  #muK
        "process": "?",
    },
    "Bu_D0XMuNu": {
        "label": r"$B^- \to D^{\star 0} \mu^- \overline{\nu}_{\mu}$",
        "multiplicity": 4, #pimuKpi 
        "process": "?",
    },
    "Bu_D0TauNu": {
        "label": r"$B^- \to D^{0} \tau^- \overline{\nu}_{\tau}$",
        "multiplicity": 3, #Kpimu 
        "process": "?",
    },
    "Bu_Dst0TauNu": {
        "label": r"$B^+ \to \overline{D}^{\star 0} \tau^+ \nu_{\tau}$",
        "multiplicity": 6, #pipipipiKpi 
        "process": "?",
    },
    "Bu_PPTauNu": {
        "label": r"$B^- \to p \overline{p} \tau^- \overline{\nu}_{\tau}$",
        "multiplicity": 3, #mupp
        "process": "?",
    },
    "Bu_PPMuNu": {
        "label": r"$B^- \to p \overline{p} \mu^- \overline{\nu}_{\mu}$",
        "multiplicity": 3, #pipimu 
        "process": "?",
    },
    # new modes
    "Bd_Kstee": {
        "label": r"$B^0 \to K^{\star 0} e^+ e^-$",
        "multiplicity": 4, #Kpiee
        "process": "?",
    },
    "Bd_Kpi": {
        "label": r"$B^0 \to K^+ \pi^-$",
        "multiplicity": 2, #Kpi 
        "process": "?",
    },
    "Lb_Lambda1520Jpsi": {
        "label": r"$\Lambda_b^0 \to \Lambda(1520) J/\psi$",
        "multiplicity": 4, #pKmumu 
        "process": "?",
    },
    "Bu_Kmumu": {
        "label":  r"$B^+ \to K^+ \mu^+ \mu^- \gamma \gamma$",
        "multiplicity": 3, #Kmumu
        "process": "?",
    },
    "Bd_pipipi": {
        "label": r"$B^0 \to \pi^0 \pi^+ \pi^-$",
        "multiplicity": 2, #pipi pi0 to gamma gamma
        "process": "?",
    },
    "Bd_phiKst0": {
        "label": r"$B^0 \to \phi K^{\star 0}$",
        "multiplicity": 4, #KKKpi 
        "process": "?",
    },
    "Bd_ppbarmumu": {
        "label": r"$B^0 \to p \overline{p} \mu^+ \mu^-$",
        "multiplicity": 4, #ppmumu
        "process": "?",
    },
    "Bd_JpsiKS": {
        "label": r"$B^0 \to J/\psi K_s^0$",
        "multiplicity": 4, #pipimumu
        "process": "?",
    },
    "Bd_JpsiphiKs": {
        "label": r"$B^0 \to J/\psi \phi K_s^0$",
        "multiplicity": 6, #mumuKKpipi
        "process": "?",
    },
    "Bd_psi2SKst": {
        "label": r"$B^0 \to \psi K^{\star 0}$",
        "multiplicity": 4, #eeKpi
        "process": "?",
    },
    "Bd_Kst0rho0": {
        "label": r"$B^0 \to K^{\star 0} \rho^0$",
        "multiplicity": 4, #pipiKpi
        "process": "?",
    },
    "Bd_Lambdacmu": {
        "label": r"$B^0 \to \Lambda_c^+ \mu^-$",
        "multiplicity": 4, #pKpimu
        "process": "?",
    },
    "Bu_Ktautau": {
        "label": r"$B^+ \to K^+ \tau^+ \tau^-$",
        "multiplicity": 7, #pipipipipipiK
        "process": "?",
    },
    "Bu_KKK": {
        "label": r"$B^+ \to K^+ K^+K^-$",
        "multiplicity": 3, #KKK
        "process": "?",
    },
    "Bu_pipipi": {
        "label": r"$B^+ \to \pi^+ \pi^+ \pi^-$",
        "multiplicity": 3, #pipipi
        "process": "?",
    },
    "Bu_KsK": {
        "label": r"$B^+ \to K^+ K_s^0$",
        "multiplicity": 3, #pipiK
        "process": "?",
    },
    "Bu_KSpi": {
        "label": r"$B^+ \to \pi^+ K_s^0$",
        "multiplicity": 3, #pipipi
        "process": "?",
    },
    "Bs_Dspi": {
        "label": r"$B^0_s \to D_s^- \pi^+",
        "multiplicity": 4, #KKpipi 
        "process": "?",
    },
    "Lb_Lctaunu": {
        "label": r"$\Lambda_b^0 \to \Lambda_c^+ \tau^- \overline{\nu}_\tau$", 
        "multiplicity": 6,  #pKpipipipi
        "process": "?",
    },
    "B0_Dstmunu": {
        "label": r"$B^0 \to D^{\star -} \mu^+ \nu_\mu$",
        "multiplicity": 4, #pimuKpi
        "process": "?",
    },
    "B0_Kstmumu": {
        "label": r"$B^0 \to K^{\star 0} \mu^+ \mu^-$",
        "multiplicity": 4, #Kpimumu
        "process": "?",
    },
    "Bs_phiphi": {
        "label": r"$B^0_s \to \phi \phi$",
        "multiplicity": 4, #KKKK
        "process": "?",
    },
    "Bu_piKK": {
        "label": r"$B^+ \to \pi^+ K^+ K^-$",
        "multiplicity": 3, #KKpi
        "process": "?",
    },
    "Bu_D0Kst": {
        "label": r"$B^+ \to \overline{D}^0 K^{\star +}$",
        "multiplicity": 7, #Kpipipipipipi
        "process": "?",
    },
    "Bd_mumu": {
        "label": r"$B^0 \to \mu^+ \mu^-$",
        "multiplicity": 2, #mumu
        "process": "?",
    },
    "Bs_KK": {
        "label": r"$B^0_s \to K^+ K^-$",
        "multiplicity": 2, #KK
        "process": "?",
    },
    "Lb_pK": {
        "label": r"$\Lambda_b^0 \to p K^-$",
        "multiplicity": 2, #pK
        "process": "?",
    },
    "Lb_Kmu": {
        "label": r"$\Lambda_b^0 \to K^+ \mu^-$",
        "multiplicity": 2, #Kmu
        "process": "?",
    },
    # ------------------------------------
    "Bs_Jpsiphi": {
        "label": r"$B^0_s \to J/\psi \phi$",
        "multiplicity": 4, #mumuKK
        "process": "?",
    },
    "Lb_Lcpi": {
        "label": r"$\Lambda_b^0 \to \Lambda_c^+ \pi^-$",
        "multiplicity": 4, #pKpipi
        "process": "?",
    },
    "Omegac0_L0KS0": {
        "label": r"$\Omega_c^0 \to \Lambda^0_b K_s^0$",
        "multiplicity": 4, #pipippi
        "process": "?",
    },
    "Bs_KKgamma": {
        "label": r"$B^0_s \to K^+ K^- \gamma$",
        "multiplicity": 2, #KK
        "process": "?",
    },
    "Bd_Kpigamma": {
        "label": r"$B^0 \to K^+ \pi^- \gamma$",
        "multiplicity": 2, #Kpi
        "process": "?",
    },
    "Bu_JpsiPi": {
        "label": r"$B^+ \to J/\psi \pi^+$",
        "multiplicity": 3,  #mumupi
        "process": "?",
    },
    "Bu_JpsiK": {
        "label": r"$B^+ \to J/\psi K^+$",
        "multiplicity": 3, #mumuK
        "process": "?",
    },
    "B0_JpsiKst": {
        "label": r"$B^+ \to J/\psi K^{\star}$",
        "multiplicity": 3, #mumuKst
        "process": "?",
    },
}