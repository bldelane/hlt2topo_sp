"""Miscellaneous auxiliary functions."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

import pandas as pd
from numpy.typing import ArrayLike
import numpy as np
from .denom import DataFrameBranchFinder
from typing import Dict


def calc_eta(
    p: ArrayLike,
    pz: ArrayLike,
) -> ArrayLike:
    """Calculate eta.

    Args:
        p: Momentum.
        pz: Momentum in z direction.

    Returns:
        Eta.
    """
    return 0.5 * np.log((p + pz) / (p - pz))


def fs_eta(
    df: pd.DataFrame,
) -> pd.DataFrame:
    """Calculate eta for the final state particles.

    Args:
        df: Pandas dataframe.

    Returns:
        None.
    """
    tracks_pz = DataFrameBranchFinder().find_branches_with_wildcard(df, "Track*PZ")
    tracks_p = DataFrameBranchFinder().find_branches_with_wildcard(df, "Track*P")

    for track_pz, track_p in zip(tracks_pz, tracks_p):
        df[f"{track_pz.replace('_PZ', '_ETA')}"] = calc_eta(
            p=df[track_p], pz=df[track_pz]
        )

    return df


def match_dataframe_lengths(signals: Dict, key1: str, key2: str):
    """Match the lengths of two dataframes based on a common column."""
    len_key1 = len(signals[key1])
    len_key2 = len(signals[key2])

    # Match the lengths based on 'unique_event' column
    if len_key1 > len_key2:
        signals[key1] = signals[key1][
            signals[key1]["unique_event"].isin(signals[key2]["unique_event"])
        ]
    elif len_key2 > len_key1:
        signals[key2] = signals[key2][
            signals[key2]["unique_event"].isin(signals[key1]["unique_event"])
        ]

    # Reset the index
    for k in (key1, key2):
        signals[k] = signals[k].reset_index(drop=True)

    breakpoint()

    return signals


def make_topo_evts(signals: Dict, keys: list[str] = ["TwoBody", "ThreeBody"]):
    """Process the signals to produce the final DataFrame.
    a. Check that each DataFrame in signals contains 'preds_per_cand'
    b. Check that 'preds_per_cand' has no duplicate values in each DataFrame
    c. Check that all DataFrames in signals have the same length
    d. Create the final DataFrame by merging the two DataFramespredictions

    Args:
        signals (Dict): Dictionary of DataFrames.
        keys (list[str], optional): Keys of the DataFrames in signals. Defaults to ["TwoBody", "ThreeBody"].

    Raises:
        ValueError: If any of the checks fail.

    Returns:
        pd.DataFrame: Final DataFrame.
    """
    # Check that each DataFrame in signals contains 'preds_per_cand'
    for key in keys:
        if "preds_per_cand" not in signals[key].columns:
            raise ValueError(f"'preds_per_cand' not found in {key} DataFrame")

    # Check that 'preds_per_cand' has no duplicate values in each DataFrame
    for key in keys:
        if signals[key]["unique_event"].duplicated().any():
            raise ValueError(
                f"Duplicate values found in 'unique_event' of {key} DataFrame"
            )

    # Check that all DataFrames in signals have the same length
    if len(signals[keys[0]]) != len(signals[keys[1]]):
        raise ValueError("DataFrames in signals do not have the same length")

    # Create the final DataFrame
    _pred = pd.DataFrame(
        {
            # NN inference
            f"preds_{keys[0]}": signals[keys[0]]["preds_per_cand"],
            f"preds_{keys[1]}": signals[keys[1]]["preds_per_cand"],
            # observables for efficiencies
            f"{keys[0]}_PT": signals[keys[0]][f"{keys[0]}_PT"],
            f"{keys[1]}_PT": signals[keys[1]][f"{keys[1]}_PT"],
            f"{keys[0]}_ETA": signals[keys[0]][f"{keys[0]}_ETA"],
            f"{keys[1]}_ETA": signals[keys[1]][f"{keys[1]}_ETA"],
        }
    )

    return _pred
