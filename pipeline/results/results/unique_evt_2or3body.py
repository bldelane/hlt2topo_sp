"""Exploit the fact that we have inference to compile an inclusive 2- OR 3- unique-eevnt record"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import pathlib
import pandas as pd
import argparse
import torch
import numpy as np
import os
import json
import matplotlib.pyplot as plt
import seaborn as sns
import scienceplots

# must make the topo module visible
import sys
from numpy.typing import ArrayLike
from typing import Union, Tuple
from collections import OrderedDict
from functools import partial
from typing import Tuple
import os
from tqdm import tqdm

plt.style.use(
    [
        "science",
        "no-latex",
    ]
)


class UniqueDataframeSelector:
    def __init__(self, event_label: str) -> None:
        self.event_label = event_label

    def __call__(self, df: pd.DataFrame) -> pd.DataFrame:
        """Select the unique events from the dataframe"""
        df["eventRun"] = list(zip(df.runNumber, df.eventNumber))
        return df[df["eventRun"] == self.event_label]

    def __repr__(self) -> str:
        return f"UniqueDataframeSelector(event_label={self.event_label})"

    def __str__(self) -> str:
        return self.__repr__()

    def persist_common_events(
        self, df1: pd.DataFrame, df2: pd.DataFrame
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Given the identifier, filter out only common events from the two dataframes (maintaining candidate multiplicity)"""
        common_ids = set(df1[self.event_label]).intersection(set(df2[self.event_label]))

        # Filter both DataFrames
        filtered_df1 = df1[df1[self.event_label].isin(common_ids)]
        filtered_df2 = df2[df2[self.event_label].isin(common_ids)]

        assert (
            filtered_df1[self.event_label].nunique()
            == filtered_df2[self.event_label].nunique()
        ), "The number of unique candidates in the two dataframes is not the same."

        return filtered_df1, filtered_df2

    def select_unique_evt(
        self, df: pd.DataFrame, nn_response_var: str = "preds_per_cand"
    ) -> pd.DataFrame:
        """Select the unique events from the dataframe picking out the maximal respone-candidate"""
        # Group by 'eventRun' and then select the row with max 'preds_per_cand' in each group
        return df.loc[df.groupby(self.event_label)[nn_response_var].idxmax()]

    def persist_eval_dataframes(
        self, df1: pd.DataFrame, df2: pd.DataFrame
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Identify the common event labels, and persist only the maximal candidate for each event"""
        filtered_df1, filtered_df2 = self.persist_common_events(df1, df2)

        persisted_df1 = self.select_unique_evt(filtered_df1)
        persisted_df2 = self.select_unique_evt(filtered_df2)

        assert len(persisted_df1) == len(
            persisted_df2
        ), "The number of unique events in the two dataframes is not the same."
        assert (
            persisted_df1[self.event_label].nunique()
            == persisted_df2[self.event_label].nunique()
        ), "The number of unique events in the two dataframes is not the same."

        return persisted_df1, persisted_df2


def main(channels: Tuple[str, ...]) -> None:
    """Main function to persist the unique events from the inference dataframes"""
    for CHANNEL in tqdm(channels):
        # fetch the data: 2- or 3-body dataframes with the unique_event (runNumber, eventNumber) identifier and the respective responses
        df_2body = pd.read_pickle(
            f"/ceph/users/nschulte/scratch/TwoBody/probe/inference/{CHANNEL}"
        )
        df_3body = pd.read_pickle(
            f"/ceph/users/nschulte/scratch/ThreeBody/probe/inference/{CHANNEL}"
        )
        df_2body["eventRun"] = list(zip(df_2body.runNumber, df_2body.eventNumber))
        df_3body["eventRun"] = list(zip(df_3body.runNumber, df_3body.eventNumber))
        df_3body = df_3body.dropna(subset=['preds_per_cand'])
        df_2body = df_2body.dropna(subset=['preds_per_cand'])
        # select the unique events
        unique_selector = UniqueDataframeSelector("eventRun")
        (
            persisted_df_2body,
            persisted_df_3body,
        ) = unique_selector.persist_eval_dataframes(df_2body, df_3body)

        # path to output files
        outpath_2body = pathlib.Path(
            f"/ceph/users/nschulte/scratch/TwoBody/probe/inference/{CHANNEL}".replace(
                "inference", "per_event_inference"
            )
        )
        outpath_3body = pathlib.Path(
            f"/ceph/users/nschulte/scratch/ThreeBody/probe/inference/{CHANNEL}".replace(
                "inference", "per_event_inference"
            )
        )

        for _path in (outpath_2body, outpath_3body):
            _path.parent.mkdir(parents=True, exist_ok=True)

        # save the dataframes
        persisted_df_2body.to_pickle(outpath_2body)
        persisted_df_3body.to_pickle(outpath_3body)


# span all modes
CHANNELS = os.listdir(
    "/ceph/users/nschulte/scratch/ThreeBody/probe/inference/"
)

if __name__ == "__main__":
    print(CHANNELS)
    main(CHANNELS)
