"""Final report of performance metrics."""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

from results import load_ntuple, fs_eta, read_feats
from results.denom import DataFrameSelectorFactory
from results.eff import EfficiencyHistogramFactory as histfactory
from results.eff import (
    prefilter_eff_report,
    BestCandidateSelector,
    UniqueEvtEfficiencyCalculator,
)
from results.nn import inference_on_preprocessed
from results.channels import decay_labels
from results.plot import viz_eff_1D
from pathlib import Path
import pandas as pd
from functools import partial
import logging
from tqdm import tqdm
import json
import numpy as np
import warnings 
warnings.filterwarnings("ignore", message="A value is trying to be set on a copy of a slice from a DataFrame")
from pandas.errors import SettingWithCopyWarning
import yaml
import matplotlib.pyplot as plt
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
# config logging
logging.basicConfig(
    filename="results/effciency_results.log",
    filemode="w",
    level=logging.DEBUG,
    format="%(asctime)s - %(levelname)s - %(message)s",
)
# remove unwanted matplotlib warnings
logging.getLogger("matplotlib").setLevel(logging.WARNING)

CONFIG = "/home/nschulte/hlt2topo_sp/pipeline/config.yaml"


def yml_to_dict(
    _path: str,
) -> dict:
    """Read in a yaml file into a dict"""
    with open(_path, "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

# book a container for the per-mode unique efficiencyt calculations
efficiency_bank_all_prefilter = {}
efficiency_bank_all_noprefilter = {}
efficiency_bank_all_onlyprefilter = {}
efficiency_bank_all = {}
efficiency_bank_single = {}

def main(
    TRIGGER: str, MODE: str, NN_CUT: float, CONFIG: str = CONFIG, LAMBDA: str = "2.0"
) -> None:
    """Main function to produce the final report of performance metrics.

    Args:
        TRIGGER (str): Trigger mode, eg. ThreeBody
        MODE (str): Decay mode, eg. DDKPi
        CONFIG (str, optional): Path to config file. Defaults to "hlt2topo_sp/pipeline/config.yaml".
        NN_CUT (float, optional): NN score cut. Defaults to 0.95.

    Returns:
        None: Saves efficiency plots to hlt2topo_sp/pipeline/results/plots/efficiency/
    """
    # start by simply sourcing the truthmatched modes & computing the observables of interest.
    # This allows us to define the denominator-specific
    # effieciency definitions, ahead of any preselection and NN inference cut

    # -------------------------------------------------------------
    # load the truthmatched modes, compute eta, and apply prefilter
    # -------------------------------------------------------------
    storage_path = f"/ceph/users/nschulte/scratch/{TRIGGER}/probe/per_event_inference/{MODE}.pkl"
    
    # NOTE: pipeline modified to apply the unique event selection before any selection; this is to ensures consistency throughout
    cands = fs_eta(
        load_ntuple(
            file_path=storage_path,
            nbody_key=TRIGGER,
        ),
    )

    # study impact of prefilter, the invididual cuts
    # NOTE: this corresponds to the NAIVE mode: ie no prefilter
    _prefilter = read_feats(CONFIG, "prefilter_combination")[TRIGGER]

    logging.debug(f"Prefilter selection: {_prefilter}")
    logging.debug(f"Prefilter size [NAIVE mode]: {len(cands.query(_prefilter))}")

    # =====================================================================================================
    # NOTE: at this stage all dataframes have degenerate event multiplicities, ie. >=1 candidates per event
    # move onto computify efficiencies; if at least one candidate triggers, save the event - not multiple times
    # =====================================================================================================

    # proceed to study the impact of the topo selection relative to bespoke denominator selections
    for mode in tqdm(  # denom classes
        [
            "Naive",
            "CanRecoChildren",
            "CanRecoChildrenParentCut",
            "CanRecoChildrenAndChildPT",
            #"RecoBDecay",
        ],
        desc="Processing efficiency modes",
    ):
        logging.info(f"Processing efficiency-denom mode: {mode}")

        # -------------------------------------------------
        # now engage with booking the efficiency categories
        # -------------------------------------------------
        denom_factory = DataFrameSelectorFactory()
        naive_denom, denom_df = denom_factory.create_selector(mode).apply_selection(
            cands
        )  # NOTE: apply to unfiltered cands, not the prefiltered data

        logging.debug(
            f"Extrema of inference on signal: [{denom_df['preds_per_cand'].min()}, {denom_df['preds_per_cand'].max()}]"
        )
        logging.debug(
            f"Candidates passing 0.90: {len(denom_df.query('preds_per_cand > 0.90'))}"
        )
        logging.debug(
            f"Candidates passing 0.95: {len(denom_df.query('preds_per_cand > 0.95'))}"
        )
        logging.debug(
            f"Candidates passing 0.99: {len(denom_df.query('preds_per_cand > 0.99'))}"
        )

        # cut on response -> then pick up the "best" candidate per event so that at least one TOS candidate is sufficient to retain the event
        logging.info(f"Cutting on NN response: {NN_CUT}")
        topo_cands = denom_df.query("preds_per_cand > @NN_CUT")

        # efficiency wrt denominator class, pre-prefilter
        # -----------------------------------------------
        logging.info(f"Calculating topo efficiency wrt denominator {mode}.")
        topo_denom_eff_calculator = UniqueEvtEfficiencyCalculator(
            df_num=topo_cands,
            df_denom=denom_df,
        )
        logging.info("Topo efficiency wrt denominator under scrutiny...")
        logging.debug(f"Numerator length: {len(topo_cands)}")
        logging.debug(f"Denominator length: {len(denom_df)}")

        # log the prefilter efficiency, reserving slots as above
        if "topo_wrt_denom" not in efficiency_bank_single:
            efficiency_bank_single["topo_wrt_denom"] = {}
        if mode not in efficiency_bank_single["topo_wrt_denom"]:
            efficiency_bank_single["topo_wrt_denom"][mode] = {}
        if TRIGGER not in efficiency_bank_single["topo_wrt_denom"][mode]:
            efficiency_bank_single["topo_wrt_denom"][mode][TRIGGER] = {}
        if MODE not in efficiency_bank_single["topo_wrt_denom"][mode][TRIGGER]:
            efficiency_bank_single["topo_wrt_denom"][mode][TRIGGER][MODE] = {}

        # fill, per decay channel, the efficiency
        efficiency_bank_single["topo_wrt_denom"][mode][TRIGGER][
            MODE
        ] = topo_denom_eff_calculator.calculate()
        logging.info(
            f"Topo efficiency wrt denominator calculated successfully for mode: {mode} with denom mode{MODE}: {topo_denom_eff_calculator.calculate()}"
        )

        # -------------------------------------------------
        #          efficiency histograms plots
        # -------------------------------------------------
        # hf = histfactory()

        # # # efficiency relative to denominators -> NAIVE MODE: until we have access to True PT/TAU, we can only count candidates
        # pt_topo_hist = hf.create("pt")
        # tau_topo_hist = hf.create("tau")

        # # fill the histograms, one candidate per event
        # pt_topo_hist.fill(
        #     # numerator: topo TOS, maximal NN score per event
        #     # denominator: post-prefilter, maximal NN score per event
        #     # NOTE: here we cound candidates, NOT events -> just a proxy to view the impact of monotonicity
        #     num_data=(topo_cands)[f"{TRIGGER}_TRUEPT"],
        #     den_data=(denom_df)[
        #         f"{TRIGGER}_TRUEPT"
        #     ], 
        # )
        # # fill the histograms, one candidate per event
        # tau_topo_hist.fill(
        #     # numerator: topo TOS, maximal NN score per event
        #     # denominator: post-prefilter, maximal NN score per event
        #     # NOTE: here we cound candidates, NOT events -> just a proxy to view the impact of monotonicity
        #     num_data=(topo_cands)[f"{TRIGGER}_MC_LIFETIME"],
        #     den_data=(denom_df)[
        #         f"{TRIGGER}_MC_LIFETIME"
        #     ], 
        # )
        # # obtain efficiency and plot
        # pt_topo_hist.eff_ratio(
        #     path=f"plots/efficiency/topo/{TRIGGER}/{MODE}",
        #     title=mode,
        #     channel=decay_labels[MODE]["label"],
        # )

        # tau_topo_hist.eff_ratio(
        #     path=f"plots/efficiency/topo/{TRIGGER}/{MODE}",
        #     title=mode,
        #     channel=decay_labels[MODE]["label"],
        # )


if __name__ == "__main__":
    # ----------------------------------------------
    # develop the study wrt to the full topo trigger
    # ----------------------------------------------
    for dec in (
        "Bp_KpJpsi",
        "B0_Kstmumu",
        "B0_Dstmunu"
    ):

    # ----------------------------------------------
    # Now plot both efficiencies into one as well as the combined efficiency
    # ----------------------------------------------
        twobody_cands = fs_eta(
            load_ntuple(
                file_path=f"/ceph/users/nschulte/scratch/TwoBody/probe/per_event_inference/{dec}.pkl",
                nbody_key="TwoBody",
            ),
        )

        threebody_cands = fs_eta(
            load_ntuple(
                file_path=f"/ceph/users/nschulte/scratch/ThreeBody/probe/per_event_inference/{dec}.pkl",
                nbody_key="ThreeBody",
            ),
        )

        # ----------------------------------------------


        for mode in tqdm(  # denom classes
            [
                "Naive",
                "CanRecoChildren",
                "CanRecoChildrenParentCut",
                "CanRecoChildrenAndChildPT",
                #"RecoBDecay",
            ],
            desc="Processing efficiency modes",
        ):
            logging.info(f"Processing efficiency-denom mode: {mode}")
    
            # -------------------------------------------------
            # now engage with booking the efficiency categories
            # -------------------------------------------------

            denom_factory = DataFrameSelectorFactory()
            _, denom_df_twobody_noprefilter = denom_factory.create_selector(mode).apply_selection(
                twobody_cands
            )
            _, denom_df_threebody_noprefilter = denom_factory.create_selector(mode).apply_selection(
                threebody_cands
            ) 

            #Now I have two dfs. Merge them together based on the common identifier eventRun. If the same EventRun is in both, then keep the candidate with 
            #the highest NN score. If the EventRun is only in one, then keep that candidate.
    
            #Merge the two dfs first, then apply the selection for the combined efficiency
            merged_df_noprefilter = pd.merge(denom_df_twobody_noprefilter, denom_df_threebody_noprefilter, how='outer', on='eventRun',suffixes=('_2body', '_3body'))
            combined_topo_cands_noprefilter = merged_df_noprefilter.query("preds_per_cand_2body > 0.992 or preds_per_cand_3body > 0.998")
    
            topo_cands_twobody_noprefilter = merged_df_noprefilter.query("preds_per_cand_2body > 0.992")
            topo_cands_threebody_noprefilter = merged_df_noprefilter.query("preds_per_cand_3body > 0.998")

            #make sure that the length of the combined df is either as big or bigger than the biggest of the other two dfs
            assert len(combined_topo_cands_noprefilter) >= max(len(topo_cands_twobody_noprefilter), len(topo_cands_threebody_noprefilter))
    
            # Select the candidate with the highest NN_score for each 'eventRun'
            def select_candidate(row):
                if pd.notna(row['preds_per_cand_2body']) and pd.notna(row['preds_per_cand_3body']):
                    return row[['preds_per_cand_2body', 'preds_per_cand_3body']].max()
                elif pd.notna(row['preds_per_cand_2body']):
                    return row['preds_per_cand_2body']
                elif pd.notna(row['preds_per_cand_3body']):
                    return row['preds_per_cand_3body']
                else:
                    return None
    
            combined_topo_cands_noprefilter['preds_per_cand'] = combined_topo_cands_noprefilter.apply(select_candidate, axis=1)
    
            combined_denom_eff_calculator_noprefilter = UniqueEvtEfficiencyCalculator(
                df_num=combined_topo_cands_noprefilter,
                df_denom=merged_df_noprefilter,
            )
            twobody_denom_eff_calculator_noprefilter = UniqueEvtEfficiencyCalculator(
                df_num=topo_cands_twobody_noprefilter,
                df_denom=merged_df_noprefilter,
            )
            threebody_denom_eff_calculator_noprefilter = UniqueEvtEfficiencyCalculator(
                df_num=topo_cands_threebody_noprefilter,
                df_denom=merged_df_noprefilter,
            )
            logging.info("Combined Topo efficiency wrt denominator under scrutiny...")
    
    
            # log the prefilter efficiency, reserving slots as above
            for trig in ("TwoBody", "ThreeBody", "combined"):
                if "topo_wrt_denom_noprefilter" not in efficiency_bank_all_noprefilter:
                    efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"] = {}
                if mode not in efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"]:
                    efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode] = {}
                if trig not in efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode]:
                    efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode][trig] = {}
                if dec not in efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode][trig]:
                    efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode][trig][dec] = {}
    
            efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode]["combined"][
                dec
            ] = combined_denom_eff_calculator_noprefilter.calculate()
            efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode]["TwoBody"][
                dec
            ] = twobody_denom_eff_calculator_noprefilter.calculate()
            efficiency_bank_all_noprefilter["topo_wrt_denom_noprefilter"][mode]["ThreeBody"][
                dec
            ] = threebody_denom_eff_calculator_noprefilter.calculate()

            viz_eff_1D(
                efficiency_bank_all_noprefilter,
                path=f"plots/efficiency/1D/combined/noprefilter/",
            )

            hf = histfactory()

            # Create instances of EfficiencyHistogram for each distribution
            pt_topo_hist_noprefilter = hf.create("pt")

            # Fill histograms
            pt_topo_hist_noprefilter.fill(combined_topo_cands_noprefilter[f"TwoBody_TRUEPT_3body"], merged_df_noprefilter[f"TwoBody_TRUEPT_3body"], "combined")
            pt_topo_hist_noprefilter.fill(topo_cands_twobody_noprefilter[f"TwoBody_TRUEPT_3body"], merged_df_noprefilter[f"TwoBody_TRUEPT_3body"], "TwoBody")
            pt_topo_hist_noprefilter.fill(topo_cands_threebody_noprefilter[f"TwoBody_TRUEPT_3body"], merged_df_noprefilter[f"TwoBody_TRUEPT_3body"], "ThreeBody")

            # Visualize the efficiency plot
            pt_topo_hist_noprefilter.eff_ratio(
                path=f"plots/efficiency/topo/combined/noprefilter/{dec}",
                title=mode,
                #channel=decay_labels[dec]["label"],
            )
            #output_path = Path(f"/ceph/users/nschulte/scratch/combined")

            #merged_df.to_pickle(f"{output_path}/{dec}.pkl")
            
            # ----------------------------------------------------------
            # Now look at the prefiltered one
            # ----------------------------------------------------------

            # calculate the prefilter first

            config = yml_to_dict(CONFIG)
            _prefilter_twobody = config["scaled_prefilter_combination"]["TwoBody"]
            _prefilter_threebody = config["scaled_prefilter_combination"]["ThreeBody"]

            print("enable prefiltering")
            if _prefilter_twobody is not None:
                print(" ----- Prefiltering dataset -----")
                print(f"Booked prefilter: {_prefilter_twobody} ")
                print("Candidates before prefiltering: ", len(_prefilter_twobody))
                prefilter_twobody = twobody_cands.query(_prefilter_twobody)
                prefilter_threebody = threebody_cands.query(_prefilter_threebody)

            # _, prefilter_twobody = denom_factory.create_selector("PreFilter").apply_selection(twobody_cands)
            # _, prefilter_threebody = denom_factory.create_selector("PreFilter").apply_selection(threebody_cands)

            #Now calculate the denominator class wrt to the prefiltered samples
            _, denom_df_twobody_prefilter = denom_factory.create_selector(mode).apply_selection(
                prefilter_twobody
            )
            _, denom_df_threebody_prefilter = denom_factory.create_selector(mode).apply_selection(
                prefilter_threebody
            ) 

            #Merge the two dfs first, then apply the selection for the combined efficiency
            merged_df_prefilter = pd.merge(denom_df_twobody_prefilter, denom_df_threebody_prefilter, how='outer', on='eventRun',suffixes=('_2body', '_3body'))
            combined_topo_cands_prefilter = merged_df_prefilter.query("preds_per_cand_2body > 0.992 or preds_per_cand_3body > 0.998")
    
            #also look at the individual ones
            topo_cands_twobody_prefilter = merged_df_prefilter.query("preds_per_cand_2body > 0.992")
            topo_cands_threebody_prefilter = merged_df_prefilter.query("preds_per_cand_3body > 0.998")

            #make sure that the length of the combined df is either as big or bigger than the biggest of the other two dfs
            assert len(combined_topo_cands_prefilter) >= max(len(topo_cands_twobody_prefilter), len(topo_cands_threebody_prefilter))
    
            combined_topo_cands_prefilter['preds_per_cand'] = combined_topo_cands_prefilter.apply(select_candidate, axis=1)
    
            combined_denom_eff_calculator_prefilter = UniqueEvtEfficiencyCalculator(
                df_num=combined_topo_cands_prefilter,
                df_denom=merged_df_prefilter,
            )
            twobody_denom_eff_calculator_prefilter = UniqueEvtEfficiencyCalculator(
                df_num=topo_cands_twobody_prefilter,
                df_denom=merged_df_prefilter,
            )
            threebody_denom_eff_calculator_prefilter = UniqueEvtEfficiencyCalculator(
                df_num=topo_cands_threebody_prefilter,
                df_denom=merged_df_prefilter,
            )
            logging.info("Combined Topo efficiency wrt denominator under scrutiny...")
    
            # log the prefilter efficiency, reserving slots as above
            for trig in ("TwoBody", "ThreeBody", "combined"):
                if "topo_wrt_denom_prefilter" not in efficiency_bank_all_prefilter:
                    efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"] = {}
                if mode not in efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"]:
                    efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode] = {}
                if trig not in efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode]:
                    efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode][trig] = {}
                if dec not in efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode][trig]:
                    efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode][trig][dec] = {}
    
            efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode]["combined"][
                dec
            ] = combined_denom_eff_calculator_prefilter.calculate()
            efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode]["TwoBody"][
                dec
            ] = twobody_denom_eff_calculator_prefilter.calculate()
            efficiency_bank_all_prefilter["topo_wrt_denom_prefilter"][mode]["ThreeBody"][
                dec
            ] = threebody_denom_eff_calculator_prefilter.calculate()

            viz_eff_1D(
                efficiency_bank_all_prefilter,
                path=f"plots/efficiency/1D/combined/prefiltered/",
            )

            prefiltered_hist = histfactory()

            # Create instances of EfficiencyHistogram for each distribution
            pt_topo_hist_prefilter = prefiltered_hist.create("pt")

            # Fill histograms
            pt_topo_hist_prefilter.fill(combined_topo_cands_prefilter[f"TwoBody_TRUEPT_3body"], merged_df_prefilter[f"TwoBody_TRUEPT_3body"], "combined")
            pt_topo_hist_prefilter.fill(topo_cands_twobody_prefilter[f"TwoBody_TRUEPT_3body"], merged_df_prefilter[f"TwoBody_TRUEPT_3body"], "TwoBody")
            pt_topo_hist_prefilter.fill(topo_cands_threebody_prefilter[f"TwoBody_TRUEPT_3body"], merged_df_prefilter[f"TwoBody_TRUEPT_3body"], "ThreeBody")
            
            # Visualize the efficiency plot
            pt_topo_hist_prefilter.eff_ratio(
                path=f"plots/efficiency/topo/combined/prefilter/{dec}",
                title=mode,
                #channel=decay_labels[dec]["label"],
            )

            # -----------------------------------------------------------
            # now the complete efficiency
            # -----------------------------------------------------------

            combined_denom_eff_calculator_all = UniqueEvtEfficiencyCalculator(
                df_num=combined_topo_cands_prefilter,
                df_denom=merged_df_noprefilter,
            )
            twobody_denom_eff_calculator_all = UniqueEvtEfficiencyCalculator(
                df_num=topo_cands_twobody_prefilter,
                df_denom=merged_df_noprefilter,
            )
            threebody_denom_eff_calculator_all = UniqueEvtEfficiencyCalculator(
                df_num=topo_cands_threebody_prefilter,
                df_denom=merged_df_noprefilter,
            )
            logging.info("Combined Topo efficiency wrt denominator under scrutiny...")
    
            # log the prefilter efficiency, reserving slots as above
            for trig in ("TwoBody", "ThreeBody", "combined"):
                if "topo_wrt_denom_all" not in efficiency_bank_all:
                    efficiency_bank_all["topo_wrt_denom_all"] = {}
                if mode not in efficiency_bank_all["topo_wrt_denom_all"]:
                    efficiency_bank_all["topo_wrt_denom_all"][mode] = {}
                if trig not in efficiency_bank_all["topo_wrt_denom_all"][mode]:
                    efficiency_bank_all["topo_wrt_denom_all"][mode][trig] = {}
                if dec not in efficiency_bank_all["topo_wrt_denom_all"][mode][trig]:
                    efficiency_bank_all["topo_wrt_denom_all"][mode][trig][dec] = {}
    
            efficiency_bank_all["topo_wrt_denom_all"][mode]["combined"][
                dec
            ] = combined_denom_eff_calculator_all.calculate()
            efficiency_bank_all["topo_wrt_denom_all"][mode]["TwoBody"][
                dec
            ] = twobody_denom_eff_calculator_all.calculate()
            efficiency_bank_all["topo_wrt_denom_all"][mode]["ThreeBody"][
                dec
            ] = threebody_denom_eff_calculator_all.calculate()

            viz_eff_1D(
                efficiency_bank_all,
                path=f"plots/efficiency/1D/combined/all_together/",
            )

            prefiltered_hist = histfactory()

            # Create instances of EfficiencyHistogram for each distribution
            pt_topo_hist_all = prefiltered_hist.create("pt")

            # Fill histograms
            pt_topo_hist_all.fill(combined_topo_cands_prefilter[f"TwoBody_TRUEPT_3body"], merged_df_noprefilter[f"TwoBody_TRUEPT_3body"], "combined")
            pt_topo_hist_all.fill(topo_cands_twobody_prefilter[f"TwoBody_TRUEPT_3body"], merged_df_noprefilter[f"TwoBody_TRUEPT_3body"], "TwoBody")
            pt_topo_hist_all.fill(topo_cands_threebody_prefilter[f"TwoBody_TRUEPT_3body"], merged_df_noprefilter[f"TwoBody_TRUEPT_3body"], "ThreeBody")
            
            # Visualize the efficiency plot
            pt_topo_hist_all.eff_ratio(
                path=f"plots/efficiency/topo/combined/all_together/{dec}",
                title=mode,
                #channel=decay_labels[dec]["label"],
            )
