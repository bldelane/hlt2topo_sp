import json
import matplotlib.pyplot as plt
import scienceplots
import seaborn as sns
import pprint
from argparse import ArgumentParser
from results.plot import simple_ax
import numpy as np
import seaborn as sns
import pandas as pd

# set style
plt.style.use(["science", "no-latex"])

# violin plots for performance
# ----------------------------
# - all reco categories, with efficiency points
# - same as above, but colur-coded by WG
# - same as above, but only for standard candles
# - for a few standard candles: response vs efficiency

# terminal interface
# ------------------
parser = ArgumentParser()
parser.add_argument(
    "-t",
    "--trigger",
    help="Trigger to plot; choices: ['TwoBody', 'ThreeBody']",
    choices=["TwoBody", "ThreeBody"],
    required=True,
)
opts = parser.parse_args()

with open(
    "/work/submit/blaised/hlt2topo_sp/pipeline/results/TwoBody/res_dict/efficiency/results_1D.json"
) as f:
    data = json.load(f)

pprint.pprint(data)

# Create an empty DataFrame to hold all the data
df = pd.DataFrame(columns=["Efficiency", "Category"])

# Populate DataFrame with efficiency and category data
medians = []
for category, topo_data in data["topo_wrt_denom"].items():
    two_body_data = topo_data["TwoBody"]
    efficiencies = [eff[0] for eff in two_body_data.values()]

    temp_df = pd.DataFrame(
        {"Efficiency": efficiencies, "Category": [category] * len(efficiencies)}
    )

    df = pd.concat([df, temp_df])
    medians.append(np.median(efficiencies))

fig, ax = simple_ax(ylabel="", title=None)

# Use Seaborn's violinplot with the DataFrame
sns.violinplot(
    x="Efficiency",
    y="Category",
    data=df,
    orient="h",
    inner=None,
    color="#7fcdbb",
    linewidth=0,
    ax=ax,
)

# Overlay with error bars
for i, category in enumerate(df["Category"].unique()):
    efficiencies = df[df["Category"] == category]["Efficiency"]
    ax.errorbar(
        efficiencies,
        [i] * len(efficiencies),
        fmt=".",
        markersize=1.5,
        color="black",
    )

# Add median indicators
sns.scatterplot(
    x=medians,
    y=np.arange(len(medians)),
    marker=".",
    color="#99000d",
    zorder=5,
    ax=ax,
)

# Remove ticks that are not related to y-axis entries
ax.tick_params(axis="y", which="both", left=False, right=False)

# Add labels and title
ax.set_yticks(np.arange(len(data["topo_wrt_denom"])))
ax.set_yticklabels(data["topo_wrt_denom"].keys())
ax.set_xlabel(r"Topo (prefilter + NN) $\varepsilon~[\%]$")
ax.set_ylabel("")

# save
plt.savefig(
    "/work/submit/blaised/hlt2topo_sp/pipeline/results/plots/efficiency/1D/TwoBody/test.pdf"
)
