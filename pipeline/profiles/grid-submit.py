#!/usr/bin/env python3

import sys
import htcondor
from os import makedirs, environ
from os.path import join

from snakemake.utils import read_job_properties

jobscript = sys.argv[1]
job_properties = read_job_properties(jobscript)


jobDir = f'/ceph/users/{environ["USER"]}/condor_logs/'

makedirs(jobDir, exist_ok=True)

sub = htcondor.Submit({
    'executable': '/bin/bash',
    'arguments': jobscript,
    'max_retries': '5',
    'log': join(jobDir, '$(cluster).log'),
    'output': join(jobDir, '$(cluster).out'),
    'error': join(jobDir, '$(cluster).out'),
    'getenv': 'True',
    'request_cpus': str(job_properties['threads']),
})

# memory
if 'request_memory' in job_properties['resources']:
    request_memory = job_properties['resources']['request_memory']
else:
    request_memory = job_properties['resources'].get('mem_mb', None)

if request_memory is not None:
    sub['request_memory'] = str(request_memory)


# gpus
request_gpus = job_properties['resources'].get('request_gpus', None)

if request_gpus is not None:
    sub['request_gpus'] = str(request_gpus)


# disk
request_disk = job_properties['resources'].get('request_disk', None)

if request_disk is not None:
    sub['request_disk'] = str(request_disk)


# MaxRunHours
MaxRunHours = job_properties['resources'].get('MaxRunHours', None)

if MaxRunHours is not None:
    sub['+MaxRunHours'] = str(MaxRunHours)


schedd = htcondor.Schedd()
with schedd.transaction() as txn:
    clusterID = sub.queue(txn)

# print jobid for use in Snakemake
print(clusterID)

