{
    EOSPATH="/eos/lhcb/user/n/nschulte/public/2024_tuples"
    echo "# Command to generate this file:" > channels_cache.yml
    echo "# EOSPATH=\"$EOSPATH\"" >> channels_cache.yml
    echo "# find \$EOSPATH -type f | grep 'davinci_ntuple.root' | sed 's/^/- /' >> channels_cache.yml" >> channels_cache.yml
    find $EOSPATH -type f -print | grep 'davinci_ntuple.root' | sed 's/^/- /' >> channels_cache.yml
} > channels_cache.yml