"""Merge .pkl dataframe files. Authored by ChatGPT4.0"""

import pandas as pd
import argparse
import os


def check_row_count(input_files, merged_df):
    total_rows = 0
    for input_file in input_files:
        df = pd.read_pickle(input_file)
        total_rows += len(df)
    if total_rows != len(merged_df):
        raise ValueError(
            f"Row count mismatch: sum of rows in individual files = {total_rows}, rows in merged file = {len(merged_df)}"
        )


def merge_pkl_files(input_files, output_file):
    merged_data = None

    for input_file in input_files:
        if not os.path.exists(input_file):
            raise FileNotFoundError(f"Input file {input_file} not found")

        df = pd.read_pickle(input_file)

        if merged_data is None:
            merged_data = df
        else:
            merged_data = pd.concat([merged_data, df], ignore_index=True)

    check_row_count(input_files, merged_data)

    merged_data.to_pickle(output_file)


def main():
    parser = argparse.ArgumentParser(description="Merge .pkl files")
    parser.add_argument("-o", "--output", required=True, help="Output .pkl file")
    parser.add_argument(
        "-i", "--input", required=True, nargs="+", help="Input .pkl files to merge"
    )
    args = parser.parse_args()

    if not args.input:
        raise ValueError("No input files provided")

    merge_pkl_files(args.input, args.output)


if __name__ == "__main__":
    main()
