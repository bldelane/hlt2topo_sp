"""Matching and truthmatching stage in the pipeline"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import awkward as ak
import uproot
from typing import Callable, Any, Union, List
from tqdm import tqdm
import pathlib
import time
import os
from pathlib import Path
import numpy as np

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    yml_to_dict,
    get_truth_matcher,
    TWOBODY_BRANCHES,
    THREEBODY_BRANCHES,
)
import argparse


# define a batched truthmatching function, operating on a subset of events, and then aggregating over the subsets
def btm(
    file_path: str,
    config: Any,
    channel: str,
    outpath: str,
    branches: Union[List[str], None] = None,
    cut: Union[str, None] = None,
    max_entries: Union[int, None] = None,
    batch_size: Union[str, None, int] = "40 MB",
    probe: bool = False,
    **kwargs,
) -> None:
    """wrapper for uproot.iterate() to load ROOT files into a pandas DataFrame"""

    print(f"Considering channel: {channel}")
    print("-------------------------------")
    # Commenting out as its fixed in the tuples and we should keep things consistent between tuples and script
    # twobody_aliases = {
    #     "Track1_TrackCHI2NDOF": "Track1_ENDVERTEX_CHI2",
    #     "Track2_TrackCHI2NDOF": "Track2_ENDVERTEX_CHI2",
    # }
    # threebody_aliases = {
    #     "Track1_TrackCHI2NDOF": "Track1_ENDVERTEX_CHI2",
    #     "Track2_TrackCHI2NDOF": "Track2_ENDVERTEX_CHI2",
    #     "TrackB_TrackCHI2NDOF": "TrackB_ENDVERTEX_CHI2",
    # }

    # compute the batch size in entries
    events = uproot.open(
        f"{file_path}:{config['key']}/DecayTree", timeout=50_000, num_workers=256
    )
    bevs = events.num_entries_for(batch_size, branches, entry_stop=max_entries)
    tevs = events.num_entries
    nits = round(tevs / bevs + 0.5)

    # set the right aliases for the CHI2DOF
    if config["key"] == "TwoBody":
        events.arrays(aliases=twobody_aliases)
    elif config["key"] == "ThreeBody":
        events.arrays(aliases=threebody_aliases)

    # Store the original parent path and file name
    original_parent_path = os.path.dirname(outpath)
    original_file_name = os.path.basename(outpath)

    SANITY_NENTRIES = []

    # iterate over the batches
    for idx, batch in enumerate(
        tqdm(
            events.iterate(
                expressions=branches,
                cut=cut,
                entry_stop=max_entries,
                step_size=batch_size,
            ),
            total=nits,
            ascii=True,
            desc=f"batches loaded",
        )
    ):
        print(f"Successfully opened batch {idx}")

        # restructure the path to account for the batches; all files are called the same; introduce a sub-dir for each idx
        batch_outpath = f"{original_parent_path}/{idx}/{original_file_name}"
        pathlib.Path(batch_outpath).parent.mkdir(parents=True, exist_ok=True)
        file = uproot.recreate(
            batch_outpath.replace(original_file_name, "davinci_ntuple.root"),
            compression=uproot.ZLIB(4),
        )
        file[f"{config['key']}/DecayTree"] = batch
        file.close()
        print(
            f"Successfully saved batch {idx} to file: "
            + batch_outpath.replace(original_file_name, "davinci_ntuple.root"),
        )

        batch_events = uproot.open(
            f"{batch_outpath.replace(original_file_name, 'davinci_ntuple.root')}:{config['key']}/DecayTree"
        )
        SANITY_NENTRIES.append(batch_events.num_entries)

    # sanity check
    if (config["max_entries"] == -1) and (config["max_entries_minbias"] == -1):
        # NOTE: only if full stats - engage with closure test to verify we exploit all cands, within a 1% tolerance

        # inspect consistency of the number of events in the batches within 1% of the total number of events
        assert (
            np.abs(tevs - sum(SANITY_NENTRIES)) < 0.01 * tevs
        ), "Number of events in batches does not match number of events in original file within 1%"
        print(f"Original events: {tevs}; sum of batches: {sum(SANITY_NENTRIES)}")

    else:
        print(
            f"Original events: {tevs}; sum of batches: {sum(SANITY_NENTRIES)}; not running closure test"
        )


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--channel", help="Decay channel", required=True)
    parser.add_argument("-i", "--input", help="Inputf file path", required=True)
    parser.add_argument(
        "-o",
        "--output",
        help="Full path to output file (including filename)",
        required=True,
    )
    # TODO: Think if you really want to keep the ghosts (quite memory consuming)
    # parser.add_argument("-p", "--probe", help="Probe mode", action="store_true")
    opts = parser.parse_args()

    # load the config file
    config = yml_to_dict("./config.yaml")

    if config["key"] == "TwoBody":
        _BRANCHES = TWOBODY_BRANCHES
    elif config["key"] == "ThreeBody":
        _BRANCHES = THREEBODY_BRANCHES

    # se thee upper limit on the events read in
    max_entries = config["max_entries"]
    if opts.channel == "minbias":
        max_entries = config["max_entries_minbias"]

    start = time.time()
    # tmdf = truthmatched dataframe
    tmdf = btm(
        file_path=f"{opts.input}",  # changes to interface with subjobs
        config=config,
        channel=opts.channel,
        max_entries=max_entries,
        branches=_BRANCHES,
        outpath=opts.output,
    )
    # signal completion of truthmatching
    Path(f"{opts.output}").touch()
    print("--- %s seconds ---" % (time.time() - start))
