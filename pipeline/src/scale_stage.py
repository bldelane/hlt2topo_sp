"""
Implement the min/max and log scaling of variables of interest
and compute the KS score of the candidate training features.
"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import argparse
import pathlib
from typing import List, Dict, Callable

# must make the topo module visible
import sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    load_ntuple,
    yml_to_dict,
    ScalerFactory,
    TransformerFactory,
    twoclass_plot,
    plt_config,
    get_feats,
)


def plot_dists(
    var: str,
    scaled: bool,
    outdir: str,
    **kwargs,
) -> Callable:
    """
    Closure to plot the distributions of the variables, pre- and post-scaling.

    Parameters
    ----------
    var: str
        Name of the variable to plot

    scaled: bool
        Whether the var is pre- or post-scaling; informs the directory name

    Returns
    -------
    Callable
        A function that plots the distributions of the variables
    """
    dirname = "pre_scaling "
    if scaled is True:
        dirname = "post_scaling"

    return twoclass_plot(
        data=dataset.query("class_label==1")[var],
        data_label="Signal",
        control=dataset.query("class_label==0")[var],
        control_label="Minimum bias",
        plotdir=f"{outdir}/{dirname}",
        plotname=f"{var}",
        **kwargs,
    )


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file path", required=True)
    parser.add_argument("-o", "--output", help="Output file path", required=True)
    opts = parser.parse_args()

    # load the config file
    config = yml_to_dict("./config.yaml")

    # load json
    dataset = load_ntuple(
        file_path=opts.input,
        nbody_key=config["key"],
    )

    # having computed all training and efficiency variables, proceed to scale them & transform them
    # --------------------------------------------------------------------------------------------

    # book the to-be-scaled two-body variables
    _log_vars = ("min_FS_IPCHI2_OWNPV", "max_FS_IPCHI2_OWNPV", "TwoBody_FDCHI2_OWNPV", "TwoBody_ENDVERTEX_CHI2DOF")
    _gev_vars = (
        "sum_PT_final_state_tracks",
        "min_PT_final_state_tracks",
        "TwoBody_MCORR",
        "TwoBody_PT",
    )

    # NOTE: commented out section has been moved to observables_stage.py
    # # min/max
    # # -------
    # # IPCHI2
    # dataset["min_FS_IPCHI2_OWNPV"] = dataset[
    #     ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV"]
    # ].min(axis=1)
    # dataset["max_FS_IPCHI2_OWNPV"] = dataset[
    #     ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV"]
    # ].max(axis=1)
    # assert (
    #     dataset.min_FS_IPCHI2_OWNPV.all() <= dataset.max_FS_IPCHI2_OWNPV.all()
    # )  # sanity check

    # # PT
    # dataset["min_PT_final_state_tracks"] = dataset[["Track1_PT", "Track2_PT"]].min(
    #     axis=1
    # )
    # dataset["sum_PT_final_state_tracks"] = dataset[["Track1_PT", "Track2_PT"]].sum(
    #     axis=1
    # )
    # assert (
    #     dataset.min_PT_final_state_tracks.all()
    #     <= dataset.sum_PT_final_state_tracks.all()
    # )

    # must add the ThreeBody extension
    if config["key"] == "ThreeBody":
        # NOTE: commented out section has been moved to observables_stage.py
        # # overwrite the variables
        # # -----------------------
        # # IPCHI2
        # dataset["min_FS_IPCHI2_OWNPV"] = dataset[
        #     ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV", "TrackB_IPCHI2_OWNPV"]
        # ].min(axis=1)
        # dataset["max_FS_IPCHI2_OWNPV"] = dataset[
        #     ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV", "TrackB_IPCHI2_OWNPV"]
        # ].max(axis=1)
        # assert (
        #     dataset.min_FS_IPCHI2_OWNPV.all() <= dataset.max_FS_IPCHI2_OWNPV.all()
        # )  # sanity check

        # # PT
        # dataset["min_PT_TRACK12"] = dataset[["Track1_PT", "Track2_PT"]].min(axis=1)
        # dataset["sum_PT_TRACK12"] = dataset[["Track1_PT", "Track2_PT"]].sum(axis=1)
        # assert dataset.min_PT_TRACK12.all() <= dataset.sum_PT_TRACK12.all()

        # dataset["min_PT_final_state_tracks"] = dataset[
        #     ["Track1_PT", "Track2_PT", "TrackB_PT"]
        # ].min(axis=1)
        # dataset["sum_PT_final_state_tracks"] = dataset[
        #     ["Track1_PT", "Track2_PT", "TrackB_PT"]
        # ].sum(axis=1)
        # assert (
        #     dataset.min_PT_final_state_tracks.all()
        #     <= dataset.sum_PT_final_state_tracks.all()
        # )

        # overwrite the variable lists
        _gev_vars = (
            "sum_PT_final_state_tracks",
            "min_PT_final_state_tracks",
            "sum_PT_TRACK12",
            "min_PT_TRACK12",
            "TwoBody_PT",
            "ThreeBody_PT",
            "ThreeBody_MCORR",
            "TwoBody_MCORR",
        )

        _log_vars = (
            "min_FS_IPCHI2_OWNPV",
            "max_FS_IPCHI2_OWNPV",
            "TwoBody_FDCHI2_OWNPV",
            "ThreeBody_FDCHI2_OWNPV",
            "TwoBody_IPCHI2_OWNPV",
            "TwoBody_ENDVERTEX_CHI2DOF",
            "ThreeBody_ENDVERTEX_CHI2DOF",
        )
    # end of ThreeBody extension

    # plot the distributions prior to scaling
    # ---------------------------------------
    _outdir = str(pathlib.Path(opts.output).parent.absolute()).replace(
        "scratch", "scratch/plots"
    )
    for var in get_feats(config=config, key=config["key"]):
        if var in plt_config.keys():
            _xlabel = plt_config[var]["xlabel"]
        plot_dists(var, scaled=False, xlabel=_xlabel, outdir=_outdir)

    # book the scaler objects and fit
    # -------------------------------
    # logs
    logger = ScalerFactory().get_scaler("Log")
    for var in _log_vars:
        dataset[var] = logger.fit(dataset[var])

    # gev
    gever = ScalerFactory().get_scaler("GeV")
    for var in _gev_vars:
        dataset[var] = gever.fit(dataset[var])

    # misc
    dataset["TwoBody_DOCAMAX"] *= 10.0
    if config["key"] == "ThreeBody":
        dataset["ThreeBody_DOCAMAX"] *= 10.0

    # plots: signal and bkg after
    # -------------------------
    for var in get_feats(config=config, key=config["key"]):
        # if custom, use labels
        if var in plt_config.keys():
            _range = plt_config[var]["range"]
            _xlabel = plt_config[var]["xlabel"]

        plot_dists(
            var,
            scaled=True,
            range=_range,
            # xlabel=_xlabel,
            outdir=_outdir,
        )

    # write transformed data to file
    # ------------------------------
    pathlib.Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    dataset.to_pickle(opts.output)
