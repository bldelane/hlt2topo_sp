"""Train the model; take a functional approach to enable importability in optuna-supported optim"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import argparse
import pathlib
from typing import List, Dict
import torch
from torch.utils.data import TensorDataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn import functional as F
from torch.nn.functional import binary_cross_entropy_with_logits
from sklearn.metrics import roc_auc_score
import time
import numpy as np
import pandas as pd
from numpy.typing import ArrayLike
import logging
import json

# must make the topo module visible
import sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


def setup_logging(
    logfile: str, print_to_console: bool = True, level: str = "info"
) -> logging.Logger:
    """Encapsulate logging setup"""
    logger = logging.getLogger("Training")
    match level:
        case "debug":
            logger.setLevel(logging.DEBUG)
        case "info":
            logger.setLevel(logging.INFO)
    handler = logging.FileHandler(logfile, mode="w")
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # if queried, print the same log info to console
    if print_to_console is True:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(logging.Formatter("%(message)s"))
        logger.addHandler(console_handler)

    return logger


def preprocess_data(
    train_path: str,
    test_path: str,
    config: dict,
) -> tuple[torch.Tensor, ...]:
    train_sample = pd.read_pickle(train_path)
    test_sample = pd.read_pickle(test_path)

    torch.manual_seed(config["seed"])

    device = topo.get_device()
    features = topo.get_feats(config=config, key=config["key"])

    # compute class imbalance weights
    class_labels_tensor = (
        torch.tensor(train_sample["class_label"].values).long().flatten()
    )
    class_weights = (1.0 / torch.bincount(class_labels_tensor)).to(device)
    class_weights /= class_weights.sum()

    X_train = torch.tensor(train_sample[features].values.astype(np.float32)).to(device)
    y_train = torch.tensor(
        train_sample.class_label.values.astype(np.float32)[:, None]
    ).to(device)
    X_test = torch.tensor(test_sample[features].values.astype(np.float32)).to(device)
    y_test = torch.tensor(
        test_sample.class_label.values.astype(np.float32)[:, None]
    ).to(device)

    return X_train, y_train, X_test, y_test, class_weights


def focal_loss(
    output: torch.Tensor,
    target: torch.Tensor,
    alpha: float = 0.25,
    gamma: float = 2.0,
    reduction: str = "mean",
):
    """Focal loss for binary classification"""
    prob = torch.sigmoid(output)
    pt = prob * target + (1 - prob) * (1 - target)
    alpha_t = alpha * target + (1 - alpha) * (1 - target)

    loss = -alpha_t * (1 - pt) ** gamma * torch.log(pt + 1e-14)

    if reduction == "mean":
        loss = loss.mean()
    elif reduction == "sum":
        loss = loss.sum()

    return loss


def compute_loss(
    output: torch.Tensor | ArrayLike,
    target: torch.Tensor | ArrayLike,
    opts: argparse.Namespace,
    logger: logging.Logger,
    class_weights: torch.tensor | ArrayLike | None = None,
    **kwargs,
) -> torch.Tensor:
    """
    Interface to for loss selection; compute the loss for the given outputs and targets.
    """
    # if opts.hinge:
    #     return torch.relu(1e2 - target * output).mean()
    # elif opts.focal:
    #     return focal_loss(output, target, **kwargs)
    # else:
    #     if class_weights is not None:
    #         weights_for_batch = class_weights[target.long().flatten()]
    #         return binary_cross_entropy_with_logits(
    #             output, target, weight=weights_for_batch.unsqueeze(-1)
    #         )  # match the shape of the output
    #     else:
    #         logger.debug(
    #             "No class weights passed to compute_loss -> proceeding with BCEwLogits"
    #         )
    print("Using BCEwLogits")
    return binary_cross_entropy_with_logits(output, target)


def inference(
    model: "pytorch model",
    data: ArrayLike | torch.tensor,
    targets: ArrayLike | torch.tensor,
    opts: argparse.Namespace,
    class_weights: torch.tensor | ArrayLike | None = None,
    **kwargs,
) -> tuple[float, float, torch.tensor]:
    """
    Evaluate the model on the given data and return the loss and AUC.
    """
    with torch.no_grad():
        predictions = model(data)
        loss = compute_loss(
            output=predictions,
            target=targets,
            opts=opts,
            class_weights=None, #class_weights
            **kwargs,
        )
        auc = roc_auc_score(
            targets.cpu().detach().numpy(), predictions.cpu().detach().numpy()
        )
    return loss.cpu().detach().numpy(), auc, predictions.cpu().detach().numpy()


def evaluate_prediction(
    model_spec: dict,
    config: dict,
    features: list,
    data: ArrayLike,
    _lambda: float,
    config_path: str,
    model_path: str,
) -> ArrayLike:
    """
    Evaluate the model on the given data.
    """
    # meta config
    # -----------
    torch.manual_seed(config["seed"])  # reproducibility
    device = topo.get_device()

    # load the model structure
    OptimModel = topo.model_arch_optim(
        params_dict=model_spec,
        monotonic=config["monotonic"],
        robust=config["robust"],
        _nbody=config["key"],
        _features=features,
        LIP=_lambda,
        config_path=config_path,
    ).to(device)

    # load the trained model state dict
    # ---------------------------------
    if device == torch.device("cpu"):
        OptimModel.load_state_dict(
            torch.load(model_path, map_location=torch.device("cpu"))
        )  # Map Location needed if running on cpu
    else:
        OptimModel.load_state_dict(torch.load(model_path))

    # produce the two-class predictions
    # ---------------------------------
    OptimModel.eval()

    if config["key"] == "TwoBody":
        Y = torch.tensor(
            [
                0.502415,
                4.97842,
                4.54457,
                9.8016,
                4.94935,
                0.07149092,
                8.7687,
                10.5117,
                0.189736,
            ]
        ).to(device)
    else:
        Y = torch.tensor(
            [
                4.65653,
                5.93234,
                11.21230,
                1.42334,
                11.95423,
                2.43248,
                0.3123,
                1.4324,
                12.31234,
                -2.42354,
                8.4320,
                12.3123,
                2.09413,
                8.31247,
                5.31242,
                5.31290,
                7.03912,
            ]
        ).to(device)

    print("Test Vector Response on inference:")
    print(torch.sigmoid(OptimModel(Y)).cpu().detach().numpy())

    return (
        torch.sigmoid(
            OptimModel(torch.tensor(data.to_numpy().astype(np.float32)).to(device))
        )
        .cpu()
        .detach()
        .numpy()
    )  # NOTE: here we require the sigmoid, as the loss function is BCEwlogits


def execute_inference(
    model_spec: dict,
    model_path: str,
    probe: pd.DataFrame,
    key: str,  # 'TwoBody' or 'ThreeBody'
    config_path: str,
    _lambda: float = 2.0,
) -> pd.DataFrame:
    """Perform inference on the preprocessed minbias samples, using the trained model state."""
    # load the config file
    # --------------------
    config = topo.yml_to_dict(config_path)

    # NN config
    # ----------
    torch.manual_seed(config["seed"])  # reproducibility

    device = topo.get_device()
    features = topo.get_feats(config=config, key=key)

    print("Moving Model to Device...")
    model = topo.model_arch_optim(
        params_dict=model_spec,
        monotonic=config["monotonic"],
        robust=config["robust"],
        _nbody=key,
        _features=features,
        LIP=float(_lambda),
        config_path=config_path,
    ).to(device)
    print("Success\n")

    # load the trained model state dict
    # ---------------------------------
    if device == torch.device("cpu"):
        model.load_state_dict(
            torch.load(model_path, map_location=torch.device("cpu"))
        )  # Map Location needed if running on cpu
    else:
        model.load_state_dict(torch.load(model_path))
    model.eval()

    # execute inference
    # -----------------
    X = torch.tensor(probe[features].to_numpy().astype(np.float32)).to(device)

    # produce the two-class predictions
    probe["preds_per_cand"] = torch.sigmoid(model(X)).cpu().detach().numpy()

    return probe


def build_loader(
    feats: torch.tensor | ArrayLike,
    labels: torch.tensor | ArrayLike,
    config: dict,
) -> DataLoader:
    """
    Build a dataloader from the given features and labels.
    """
    dataset = TensorDataset(feats, labels)
    return DataLoader(dataset, batch_size=config["batch_size"], shuffle=True)


def train_and_evaluate(
    opts: argparse.Namespace,
    config: dict,
    logger: logging.Logger,
    model_spec: dict,
    X_train: torch.tensor | ArrayLike,
    y_train: torch.tensor | ArrayLike,
    X_test: torch.tensor | ArrayLike,
    y_test: torch.tensor | ArrayLike,
    class_weights: torch.tensor | ArrayLike | None = None,
) -> tuple:
    """Execute the training and evaluation loop."""

    # book device
    device = topo.get_device()

    # book the model, with some flexibility to import
    model = topo.model_arch_optim(
        params_dict=model_spec,
        monotonic=opts.monotonic,
        robust=opts.robust,
        _nbody=config["key"],
        _features=features,
        LIP=opts.Lambda,
        config_path="./config.yaml",
    ).to(device)

    # build the loaders
    train_loader = build_loader(X_train, y_train, config=config)
    test_loader = build_loader(X_test, y_test, config=config)

    # book optimizer
    optimizer = torch.optim.Adam(
        model.parameters(),
        lr=opts.lrate
    )
    scheduler = ReduceLROnPlateau(optimizer, "min", patience=5, factor=0.25)

    # containers for plots
    Train_loss_array = []
    ROC_Train_array = []
    Test_loss_array = []
    ROC_Test_array = []

    # global-scope pars
    loss_pars = {"opts": opts, "logger": logger}

    with open(f"{opts.outdir}/training_loop.log", "w") as logfile:
        logfile.seek(0)
        print("Start Training")
        start = time.time()

        for epoch in range(opts.epochs):
            epoch_train_loss = 0
            epoch_auc_train = 0
            total_train_batch = len(train_loader)

            model.train()
            for data, target in train_loader:
                data, target = data.to(device), target.to(device)
                optimizer.zero_grad()
                output = model(data)
                # log the loss
                loss_train = compute_loss(
                    output=output,
                    target=target,
                    class_weights=None,  #class_weights# found to yield better ROC AUC
                    **loss_pars,
                )
                # backprop
                loss_train.backward()
                optimizer.step()

                epoch_train_loss += loss_train.item()

                # compute the AUC
                auc_train = roc_auc_score(
                    target.cpu().detach().numpy(), output.cpu().detach().numpy()
                )
                epoch_auc_train += auc_train

            # epoch-wise averages
            avg_train_loss = epoch_train_loss / total_train_batch
            avg_train_auc = epoch_auc_train / total_train_batch

            # eval loop
            model.eval()
            epoch_eval_loss = 0
            epoch_auc_test = 0
            total_test_batch = len(test_loader)

            with torch.no_grad():
                for data_test, target_test in test_loader:
                    # load test data
                    data_test, target_test = data_test.to(device), target_test.to(
                        device
                    )

                    # FoMs
                    eval_loss, auc_test, test_prediction = inference(
                        model=model,
                        data=data_test,
                        targets=target_test,
                        class_weights=None, #class_weights
                        **loss_pars,
                    )
                    epoch_eval_loss += eval_loss
                    epoch_auc_test += auc_test

            avg_test_loss = epoch_eval_loss / total_test_batch
            avg_test_auc = epoch_auc_test / total_test_batch

            logger.info(
                f"Epoch {epoch} - Training AUC: {avg_train_auc:.4f}, Training Loss: {avg_train_loss:.4f}",
            )
            logger.info(
                f"Epoch {epoch} - Test AUC: {avg_test_auc:.4f}, Test Loss: {avg_test_loss:.4f}",
            )

            scheduler.step(avg_train_loss)

            Train_loss_array.append(avg_train_loss)
            ROC_Train_array.append(avg_train_auc)
            Test_loss_array.append(avg_test_loss)
            ROC_Test_array.append(avg_test_auc)

        end = time.time() - start
        logger.info(f"Total running time: {end:.2f} seconds")

    return (
        Train_loss_array,
        ROC_Train_array,
        Test_loss_array,
        ROC_Test_array,
        test_prediction,
        model,
    )


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-M",
        "--monotonic",
        action="store_true",
        help="engage monotonicity? [default: False]",
    )
    parser.add_argument(
        "-R",
        "--robust",
        action="store_true",
        help="engage robustness? [default: False]",
    )
    parser.add_argument(
        "-H",
        "--hinge",
        action="store_true",
        help="engage Hinge loss? [default: False]",
    )
    parser.add_argument(
        "-F",
        "--focal",
        action="store_true",
        help="engage Focal loss? [default: False]",
    )
    parser.add_argument(
        "-E",
        "--epochs",
        default=55,
        type=int,
        help="training epochs [int]",
    )
    parser.add_argument(
        "-l", "--Lambda", required=True, type=float, help="Lipschitz constant value"
    )
    parser.add_argument("-r", "--lrate", default=1e-2, help="learning rate")
    parser.add_argument("-o", "--outdir", help="Output directory path", required=True)
    parser.add_argument("-T", "--train", help="Path to train pkl file", required=True)
    parser.add_argument("-Y", "--test", help="Path to test pkl file", required=True)
    parser.add_argument("-v", "--log", help="Path to log file", required=True)
    parser.add_argument("-V", "--verbose", action="store_true")
    parser.add_argument(
        "--model_spec", help="Path to model complexity file", required=True
    )
    opts = parser.parse_args()

    # setup logging
    if opts.verbose:
        logger = setup_logging(opts.log, print_to_console=True, level="debug")
    else:
        logger = setup_logging(opts.log)

    # book outdir dir, if missing
    pathlib.Path(f"{opts.outdir}").mkdir(
        parents=True, exist_ok=True
    )

    # load the config file
    _config_path = "./config.yaml"
    config = topo.yml_to_dict(_config_path)

    # reproducibility
    torch.manual_seed(config["seed"])

    # load the model complexity dictionary
    with open(opts.model_spec, "r") as f:
        model_spec = json.load(f)

    device = topo.get_device()
    features = topo.get_feats(config=config, key=config["key"])

    # load train and test sample
    X_train, y_train, X_test, y_test, class_weights = preprocess_data(
        opts.train, opts.test, config
    )
    logger.info(f"Train sample size: {len(X_train)}; Test sample size: {len(X_test)}")

    # training loop
    (
        train_loss,
        train_roc,
        test_loss,
        test_roc,
        test_prediction,
        model,
    ) = train_and_evaluate(
        opts=opts,
        model_spec=model_spec,
        config=config,
        logger=logger,
        X_train=X_train,
        y_train=y_train,
        X_test=X_test,
        y_test=y_test,
        class_weights=None, #class_weights
    )

    # presist the test prediction to viz results
    test_preds = pd.read_pickle(opts.test)
    test_preds["preds_per_cand"] = torch.sigmoid(model(X_test)).cpu().detach().numpy()

    # other metrics
    performance_params = [
        train_loss,
        test_loss,
        train_roc,
        test_roc,
    ]

    # uncomment when debugging
    # if config["key"] == "TwoBody":
    #    Y = torch.tensor([0.502415, 4.97842, 4.54457, 9.8016, 4.94935, 0.07149092, 8.7687, 10.5117, 0.189736]).to(device)
    # elif config["key"] == "ThreeBody":
    #    Y = torch.tensor([4.65653, 5.93234, 11.21230, 1.42334, 11.95423, 2.43248, 0.3123, 1.4324, 12.31234, -2.42354, 8.4320, 12.3123, 2.09413, 8.31247, 5.31242, 5.31290, 7.03912]).to(device)

    # print("Test Vector Response in Training:")
    # print(torch.sigmoid(model(Y)).cpu().detach().numpy())

    # persist the results
    # -------------------
    print("Move Testframe to Pickle")
    test_preds.to_pickle(
        f"{opts.outdir}/{config['key']}_Train_Results.pkl"
    )

    # save model in pytorch format for inference
    torch.save(
        model.state_dict(),
        f"{opts.outdir}/trained_model.pt",
    )
    print(f"SUCCESS: trained model state written to {opts.outdir}/trained_model.pt")

    print("\nDumping Parameters to Numpy...")
    with open(
        f"{opts.outdir}/{config['key']}_performance_parameters.npy",
        "wb",
    ) as outputfile:
        np.save(outputfile, performance_params)
    print("SUCCESS --- execution complete")
