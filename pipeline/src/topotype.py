"""Dictionary containing the channels used in training the specific topo type."""

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

slb_vub_modes = ["3MuNu", "KMuNu", "KTauNu", "PMuNu", "PPMuNu", "PPTauNu", "PiMuNu"]

slb_vcb_modes = [
    "DsMuNu",
    "JpsiMuNu",
    "JpsiTauNu",
    "Lc2625MuNu",
    "D0TauNu",
]

# check that Vcb and Vub modes are disjoint
any(i in slb_vub_modes for i in slb_vcb_modes)

topotype_conf = {
    "inclusiveB": [
        "Bu_KKK",
        "Bd_Dstmunu",
        "Bd_JpsiKS",
        "Bd_JpsiphiKs",
        "Bd_Kpi",
        "Bd_Kst0rho0",
        "Bd_Kstee",
        "Bd_Kstmumu",
        "Bd_Lambdacmu",
        "Bd_phiKst0",
        "Bd_pipipi",
        "Bd_ppbarmumu",
        "Bd_psi2SKst",
        "Bs_Dspi",
        "Bs_phiphi",
        "Bu_D0Kst",
        "Bu_Kmumu",
        "Bu_KsK",
        "Bu_KSpi",
        "Bu_Ktautau",
        "Bu_piKK",
        "Bu_pipipi",
        "Lb_Lambda1520Jpsi",
        "Lb_Lctaunu",
        "minbias",
    ],
    "singlemuon": ["minbias"]
    + slb_vub_modes
    + slb_vcb_modes,  # NOTE: careful to include minbias here
}

# book the probe modes: these are exclusive MC samples used for efficiency appraisals
probes = [
    "minbias",
    "Bd_Kstmumu",
    "Bs_Jpsiphi"
    #    "minbias",  # use this to monitor the rate offline
]  # NOTE: should be disjoint from the training modes

# # ascertain lack of bias
# for m in probes:
#     assert (
#         m not in topotype_conf["singlemuon"]
#     ), "Bias alert: probe modes should be disjoint from singlemuon modes"
#     assert (
#         m not in topotype_conf["inclusiveB"]
#     ), "Bias alert: probe modes should be disjoint from inclusiveB modes"
