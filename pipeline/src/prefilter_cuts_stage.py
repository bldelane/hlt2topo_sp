"""Calculate observables of interest for training and performance evaluation"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import pathlib
from typing import List, Dict, Any
import argparse
import pandas as pd
from numpy.typing import ArrayLike
import matplotlib.pyplot as plt
from typing import Callable
import pathlib
import numpy as np
from pathlib import Path
import os

# import scienceplots

# plt.style.use(["science"])

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


def source_boi(
    config: Dict,
    key: str,
) -> List[str]:
    """
    Read in the config file and return an inclusive list of branches.
    This includes training features and branches used for performance evaluation.

    Parameters
    ----------
    config : Dict
        Pipeline config

    key: str
        Identifier ['TwoBody', 'ThreeBody']

    Returns
    -------
    List[str, ...]
        List of branches to be read in
    """
    boi = config["features"][config["key"]] + config["extra_boi"]

    # accommodate the fact that the 3-body contains additional branches
    if config["key"] == "ThreeBody":
        boi += [
            "ThreeBody_ENDVERTEX_X",
            "ThreeBody_ENDVERTEX_Y",
            "ThreeBody_ENDVERTEX_Z",
            "ThreeBody_OWNPV_X",
            "ThreeBody_OWNPV_Y",
            "ThreeBody_OWNPV_Z",
            "ThreeBody_PX",
            "ThreeBody_PY",
            "ThreeBody_PZ",
            "ThreeBody_PE",
            "ThreeBody_M",
            "TrackB_PX",
            "TrackB_PY",
            "TrackB_PZ",
            "TrackB_PE",
            "TrackB_M",
        ]

    return boi


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file path", required=True)
    parser.add_argument("-o", "--output", help="Output file path", required=True)
    parser.add_argument(
        "-n", "--no_prefilter", help="Do not prefilter", action="store_true"
    )
    opts = parser.parse_args()

    # load the config file
    config = topo.yml_to_dict("./config.yaml")

    # load json file
    dataset = topo.load_ntuple(
        file_path=opts.input,
        nbody_key=config["key"],
        branches=source_boi(config, config["key"]),
    )

    # if booked, prefilter the topo n-track combination
    _prefilter = config["prefilter_combination"][config["key"]]
    if (
        opts.no_prefilter is True
    ):  # in probe mode, we want to keep everything to understand the impact of prefilter+NN
        pass
    else:
        print("enable prefiltering")
        if _prefilter is not None:
            print(" ----- Prefiltering dataset -----")
            print(f"Booked prefilter: {_prefilter} ")
            dataset = dataset.query(_prefilter)
            print(" ----- Prefiltering complete -----")

    # write to file
    Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    dataset.to_pickle(opts.output)
    print(f" ----- Success: execution complete & {opts.output} written to file ----- ")
