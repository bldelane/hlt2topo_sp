import pandas as pd
import matplotlib.pyplot as plt
import pickle
import sys
import uproot
import pathlib
import argparse
import numpy as np
from pathlib import Path

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    yml_to_dict,
    clean_minbias,
    calculate_unique_events,
    load_root,
    label_objects,
)
# Step 1: Open the pickle file and load data
pickle_file_path = 'scratch/TwoBody/evtlbl/Bu_KKK.pkl'

with open(pickle_file_path, 'rb') as file:
    data = pickle.load(file)

# Step 2: Convert data to a Pandas DataFrame
df = pd.DataFrame(data)
config = yml_to_dict("./config.yaml")

# Step 3: Plot histograms of selected columns
features = []
for dict in config["features"][config["key"]]:
    for key in dict:
        features.append(key)

# Check if the specified columns exist in the DataFrame
valid_columns = [col for col in features if col in df.columns]

if valid_columns:
    df[valid_columns].hist(bins=10, alpha=0.7, figsize=(10, 6))
    plt.suptitle('Histograms of Selected Columns', y=1.02)
    plt.show()
else:
    print("One or more specified columns do not exist in the DataFrame.")
