"""Matching and truthmatching stage in the pipeline"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import awkward as ak
import uproot
from typing import Callable, Any, Union, List
from tqdm import tqdm
import pathlib
import time
import os
from pathlib import Path
import numpy as np
import pandas as pd

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    yml_to_dict,
    clean_minbias,
)
import argparse


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--channel", help="Decay channel", required=True)
    parser.add_argument("-i", "--input", help="Inputf file path", required=True)
    parser.add_argument(
        "-o",
        "--output",
        help="Full path to output file (including filename)",
        required=True,
    )
    opts = parser.parse_args()

    # load the config file
    config = yml_to_dict("./config.yaml")

    # load the file
    sample = pd.read_pickle(opts.input)

    # if minimum-bias, remove all beauty as identified by the truth-matching
    # ----------------------------------------------------------------------
    if opts.channel == "minbias":
        # remove beauty for trainig; for tuning, keep to monitor the purity
        print("Engaging with veto of beauty-like candidates removal")
        sample = clean_minbias(df=sample, key=config["key"])
        print(f"{len(sample)} candidates post-veto")
        assert (
            np.sum(sample[f"{config['key']}_HasBCandidate"]) == 0
        ), "There should be no beauty candidates left"
    else:
        # NOTE: effectively nothing to do on signal
        print(
            f"No removal of beauty-like candidates: {len(sample)} candidates persisted"
        )

    # writeout
    # --------
    sample.to_pickle(f"{opts.output}")
    print(f"Successfully saved to file {opts.output} with {len(sample)} candidates")
