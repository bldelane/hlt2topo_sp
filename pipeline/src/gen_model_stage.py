"""
Generate executables bash files spawned by the Lipshitz contant values under scrutiny, as specified 
in the config.yaml file
"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import argparse
import pathlib
from typing import List, Dict
import time
import numpy as np
import os

# must make the topo module visible
import sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


def gen_bash_exec(
    dir_path: str, stage: str, l: float, opts: argparse.Namespace
) -> None:
    """Generate bash executable files for each optimisation and training stage"""

    match stage:
        case "optim":
            # book the bash file
            sh_file = f"{dir_path}/run_optuna.sh"

            # book the command to run the relevant training script, with the relevant arguments
            base_sh = f"python src/optimise_architecture.py --Lambda {l} --train {opts.train} --test {opts.test}\
                 --log {opts.log}/nn_optim.log --output {dir_path}/model_spec.json"

            # NN-specific arguments - add them to the shell command string
            if opts.monotonic:
                base_sh += " --monotonic"
            if opts.robust:
                base_sh += " --robust"

            # write to executable, and make this runnable
            print(f"{base_sh}", file=open(sh_file, "w"))
            os.system(f"chmod +x {sh_file}")

        case "train":
            # book the bash file
            sh_file = f"{dir_path}/run_training.sh"

            # book the command to run the relevant training script, with the relevant arguments
            base_sh = f"python src/train_stage.py --Lambda {l} --outdir {dir_path} --train {opts.train} --test {opts.test}\
                --log {opts.log}/train.log --model_spec {dir_path}/model_spec.json"

            # NN-specific arguments - add them to the shell command string
            if opts.monotonic:
                base_sh += " --monotonic"
            if opts.robust:
                base_sh += " --robust"

            # write to executable, and make this runnable
            print(f"{base_sh}", file=open(sh_file, "w"))
            os.system(f"chmod +x {sh_file}")

        case _:
            raise ValueError(f"Unknown stage {stage}")


if __name__ == "__main__":
    # I/0
    parser = argparse.ArgumentParser(description="training+eval configuration")
    parser.add_argument(
        "-o",
        "--outdir",
        help="Output main exec directory, per training config",
        required=True,
    )
    parser.add_argument(
        "-M",
        "--monotonic",
        action="store_true",
        help="engage monotonicity? [default: False]",
    )
    parser.add_argument(
        "-R",
        "--robust",
        action="store_true",
        help="engage robustness? [default: False]",
    )
    parser.add_argument(
        "-T",
        "--train",
        required=True,
        help="Path to train dataset pkl file",
    )
    parser.add_argument(
        "-l",
        "--log",
        required=True,
        help="Directory path for logger",
    )
    parser.add_argument(
        "-x",
        "--test",
        required=True,
        help="Path to test dataset pkl file",
    )
    opts = parser.parse_args()

    # load the config file
    config = topo.yml_to_dict("./config.yaml")

    # load the Lipschitz constant which must be probed
    _lambdas = config["lambdas"]

    # for each Lipschitz constant, generate a directory and bash file defining the training
    for l in _lambdas:
        # custom path for each lambda
        dir_path = f"{opts.outdir}/lambda{l}/train"
        pathlib.Path(dir_path).mkdir(parents=True, exist_ok=True)

        # generate the specic bash files for each stage
        for stage in ("optim", "train"):
            gen_bash_exec(dir_path, stage, l, opts)
