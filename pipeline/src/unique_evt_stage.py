"""Calculate observables of interest for training and performance evaluation"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import pathlib
from typing import List, Dict, Any
import argparse
import pandas as pd
from numpy.typing import ArrayLike
import matplotlib.pyplot as plt
from typing import Callable
import pathlib
import numpy as np
from pathlib import Path
import os

# import scienceplots

# plt.style.use(["science"])

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


def source_boi(
    config: Dict,
    key: str,
) -> List[str]:
    """
    Read in the config file and return an inclusive list of branches.
    This includes training features and branches used for performance evaluation.

    Parameters
    ----------
    config : Dict
        Pipeline config

    key: str
        Identifier ['TwoBody', 'ThreeBody']

    Returns
    -------
    List[str, ...]
        List of branches to be read in
    """
    boi = config["features"][config["key"]] + config["extra_boi"]

    # accommodate the fact that the 3-body contains additional branches
    if config["key"] == "ThreeBody":
        boi += [
            "ThreeBody_ENDVERTEX_X",
            "ThreeBody_ENDVERTEX_Y",
            "ThreeBody_ENDVERTEX_Z",
            "ThreeBody_OWNPV_X",
            "ThreeBody_OWNPV_Y",
            "ThreeBody_OWNPV_Z",
            "ThreeBody_PX",
            "ThreeBody_PY",
            "ThreeBody_PZ",
            "ThreeBody_PE",
            "ThreeBody_M",
            "TrackB_PX",
            "TrackB_PY",
            "TrackB_PZ",
            "TrackB_PE",
            "TrackB_M",
        ]

    return boi


def retain_unique_evt(
    df: pd.DataFrame,
    mode: str = "random",  # max for conservative rate estimation - ignoring effect on efficiency per candidate
    seed: int = 0,  # added seed for reproducible random selection
) -> pd.DataFrame:
    "Retain only one candidate of interest, as identified by the mode, per event."
    pre_cleanup_nevts = len(df)

    print("Dropping duplicates...")

    match mode:
        case "max":
            # conservative approach: retain the highest-scoring candidate per event
            print("Enacting conservative approach: highest-scoring candidate per event")
            df = df.loc[
                df.groupby("unique_event")["preds_per_cand"].idxmax()
            ]  # sort all events by the highest response and retain, per-event, the highest-scoring candidate -> conservative approach
            test_cleanup(df, pre_cleanup_nevts)
            return df
        case "random":
            # random approach: retain a random candidate per event
            print("Enacting random approach: random candidate per event")
            df = (
                df.groupby("unique_event")
                .apply(lambda x: x.sample(n=1, random_state=seed))
                .reset_index(drop=True)
            )
            test_cleanup(df, pre_cleanup_nevts)
            return df
        case _:
            raise ValueError(f"Invalid mode: {mode}")


def test_cleanup(df: pd.DataFrame, pre_cleanup_nevts: int) -> None:
    """Test that the cleanup was successful."""
    assert df["unique_event"].nunique() == df.shape[0], "Not all events are unique"
    assert len(df) < pre_cleanup_nevts, "No duplicates were dropped"


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file path", required=True)
    parser.add_argument("-o", "--output", help="Output file path", required=True)
    parser.add_argument(
        "-n", "--no_prefilter", help="Do not prefilter", action="store_true"
    )
    opts = parser.parse_args()

    # load the config file
    config = topo.yml_to_dict("./config.yaml")

    # load json file
    dataset = topo.load_ntuple(
        file_path=opts.input,
        nbody_key=config["key"],
        branches=source_boi(config, config["key"]),
    )

    # select one unique event throughout for consistency -> see efficiency studies
    dataset = retain_unique_evt(
        dataset,
        mode="random",
    )

    # write to file
    Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    dataset.to_pickle(opts.output)
    print(f" ----- Success: execution complete & {opts.output} written to file ----- ")
