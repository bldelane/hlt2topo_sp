"""Calculate observables of interest for training and performance evaluation"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import pathlib
from typing import List, Dict, Any
import argparse
import pandas as pd
from numpy.typing import ArrayLike
import matplotlib.pyplot as plt
from typing import Callable
import pathlib
import numpy as np
from pathlib import Path
import os
import numpy as np
import uproot

# import scienceplots

# plt.style.use(["science"])

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


def source_boi(
    config: Dict,
    key: str,
) -> List[str]:
    """
    Read in the config file and return an inclusive list of branches.
    This includes training features and branches used for performance evaluation.

    Parameters
    ----------
    config : Dict
        Pipeline config

    key: str
        Identifier ['TwoBody', 'ThreeBody']

    Returns
    -------
    List[str, ...]
        List of branches to be read in
    """
    boi = config["features"][config["key"]] + config["extra_boi"]

    # accommodate the fact that the 3-body contains additional branches
    if config["key"] == "ThreeBody":
        boi += [
            "ThreeBody_ENDVERTEX_X",
            "ThreeBody_ENDVERTEX_Y",
            "ThreeBody_ENDVERTEX_Z",
            "ThreeBody_OWNPV_X",
            "ThreeBody_OWNPV_Y",
            "ThreeBody_OWNPV_Z",
            "ThreeBody_PX",
            "ThreeBody_PY",
            "ThreeBody_PZ",
            "ThreeBody_PE",
            "ThreeBody_M",
            "TrackB_PX",
            "TrackB_PY",
            "TrackB_PZ",
            "TrackB_PE",
            "TrackB_M",
        ]

    return boi


def calc_composite_obs(
    nbody_id: str,
    dataset: pd.DataFrame,
    config: Dict,
) -> None:
    """Compute the mass-like and lifetime observable for the topo candidate

    NOTE: this is entirely reliant on the visible system, which
    in practice amounts to manipulating the n-body candidate 4-vector

    FIXME: low cohesion and high coupling
    """
    # b-hadron PV
    pv = topo.build_3vec(
        pX=dataset[f"{nbody_id}_OWNPV_X"],
        pY=dataset[f"{nbody_id}_OWNPV_Y"],
        pZ=dataset[f"{nbody_id}_OWNPV_Z"],
    )

    # b-hadron decay vertex
    dv = topo.build_3vec(
        pX=dataset[f"{nbody_id}_ENDVERTEX_X"],
        pY=dataset[f"{nbody_id}_ENDVERTEX_Y"],
        pZ=dataset[f"{nbody_id}_ENDVERTEX_Z"],
    )

    # pointing verctor & related obs
    pointing = dv - pv
    # pointing_u = pointing.unit()  # unit vector
    # assert pointing_u.mag.all() == 1.0, "ValueError: pointing vector not unit vector"

    # theta = pointing_u.theta
    # phi = pointing_u.phi

    # # visible system (b-hadron reconstructed childer) 4-vec
    # vis_sys_lv = topo.build_4vec(
    #     pX=dataset[f"{nbody_id}_PX"],
    #     pY=dataset[f"{nbody_id}_PY"],
    #     pZ=dataset[f"{nbody_id}_PZ"],
    #     pE=dataset[f"{nbody_id}_PE"],
    # )

    # # spatial info only
    # vis_sys_3v = topo.build_3vec(pX=vis_sys_lv.x, pY=vis_sys_lv.y, pZ=vis_sys_lv.z)
    # eta = vis_sys_lv.eta

    # NOTE: compute eta using only the 3-vector info
    dataset[f"{nbody_id}_ETA_threevec"] = pointing.eta

    # # angle wrt to pointing: dot product / (product of magnitudes)
    # dira = topo.calc_DIRA(vis_sys_3v, pointing_u)

    # # corrected mass
    # rvs = vis_sys_lv.rotateZ(-phi).rotateY(-theta)
    # miss_pt = rvs.pt

    # ############################################################
    # # NOTE: removed because of new files with ThOr observables
    # # # mcorr
    # # dataset[f"{nbody_id}_Mcorr"] = topo.calc_mcorr(
    # #     vis_sys_lv, phi, theta, dataset[f"{nbody_id}_M"]
    # # )
    # ############################################################

    # # lifetime
    # dataset[f"{nbody_id}_TAU"] = topo.calc_ltime(vis_sys_lv, pointing)
    # dataset = dataset[dataset[f"{nbody_id}_TAU"] > 0]  # positive lifetime
    # assert dataset[f"{nbody_id}_TAU"].min() > 0, "ValueError: negative lifetime"

    # # validation plots
    # # ----------------
    # outdir = f"scratch/plots/{config['key']}/calc_obs/sanity/{pathlib.Path(f'{opts.input}').stem}"
    # # for singleton in ("Mcorr", "TAU"):
    # #     topo.twoclass_plot(
    # #         data=dataset[f"{nbody_id}_{singleton}"],
    # #         plotname=f"{nbody_id}_{singleton}",
    # #         plotdir=outdir,
    # #     )

    # topo.twoclass_plot(
    #     data=vis_sys_lv.mass,
    #     control=dataset[f"{nbody_id}_M"],
    #     plotname=f"{nbody_id}_M",
    #     plotdir=outdir,
    #     control_label="Truth",
    # )


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file path", required=True)
    parser.add_argument("-o", "--output", help="Output file path", required=True)
    parser.add_argument(
        "-n", "--no_prefilter", help="Do not prefilter", action="store_true"
    )
    opts = parser.parse_args()

    # load the config file
    config = topo.yml_to_dict("./config.yaml")

    # load json file
    dataset = topo.load_ntuple(
        file_path=opts.input,
        nbody_key=config["key"],
        library="pd",
        batch_size="20 MB"
        # branches=source_boi(config, config["key"]), # NOTE: removed because of new files with ThOr observables
    )

    #############################################################################################
    # # NOTE: removed because of new files with ThOr observables
    # -------------------------------------------------------------------------------------------
    # # calc observables
    # # ---------------
    # # baseline: 2-body
    common_kwargs = {"dataset": dataset, "config": config}
    calc_composite_obs(nbody_id="TwoBody", **common_kwargs)
    # # if 3-body, additionally calc the 3-body observables
    if config["key"] == "ThreeBody":
        calc_composite_obs(nbody_id="ThreeBody", **common_kwargs)

    # # === THIS NEEDS TO BE EXTENDED TO DALITZ & Q2 ===
    # # # check operation of adding lorentz vectors and getting the correct mass of the system
    # t1 = topo.build_4vec(
    #     dataset.Track1_PX, dataset.Track1_PY, dataset.Track1_PZ, dataset.Track1_PE
    # )
    # t2 = topo.build_4vec(
    #     dataset.Track2_PX, dataset.Track2_PY, dataset.Track2_PZ, dataset.Track2_PE
    # )
    # vism = (t1 + t2).mass

    # # ================================================
    # # attempt to conventional dalitz
    # if config["key"] == "ThreeBody":
    #     print("Computing Dalitz observables m12^2 m23^2")

    #     tB = topo.build_4vec(
    #         dataset.TrackB_PX, dataset.TrackB_PY, dataset.TrackB_PZ, dataset.TrackB_PE
    #     )
    #     vism12 = (t1 + t2).mass
    #     vism2B = (t2 + tB).mass

    #     # assign to dataset
    #     dataset["M12"] = vism12**2  # vis mass squared
    #     assert dataset.M12.min() > 0
    #     dataset["M2B"] = vism2B**2  # idem
    #     assert dataset.M2B.min() > 0
    # # ================================================
    ################################################################################################

    # snippet to compute the mins/max vars - copied over from `scale_stage.py`
    # min/max
    # -------
    # IPCHI2
    dataset["min_FS_IPCHI2_OWNPV"] = dataset[
        ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV"]
    ].min(axis=1)
    dataset["max_FS_IPCHI2_OWNPV"] = dataset[
        ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV"]
    ].max(axis=1)
    assert (
        dataset.min_FS_IPCHI2_OWNPV.all() <= dataset.max_FS_IPCHI2_OWNPV.all()
    )  # sanity check

    # PT
    dataset["min_PT_final_state_tracks"] = dataset[["Track1_PT", "Track2_PT"]].min(
        axis=1
    )
    dataset["sum_PT_final_state_tracks"] = dataset[["Track1_PT", "Track2_PT"]].sum(
        axis=1
    )
    assert (
        dataset.min_PT_final_state_tracks.all()
        <= dataset.sum_PT_final_state_tracks.all()
    )
    # P
    dataset["min_P_final_state_tracks"] = dataset[["Track1_P", "Track2_P"]].min(axis=1)
    dataset["sum_P_final_state_tracks"] = dataset[["Track1_P", "Track2_P"]].sum(axis=1)
    assert (
        dataset.min_P_final_state_tracks.all() <= dataset.sum_P_final_state_tracks.all()
    )
    # placeholder logged variables used in the prefilter selection
    dataset["log_max_FS_IPCHI2_OWNPV"] = np.log(dataset.max_FS_IPCHI2_OWNPV)
    dataset["log_TwoBody_FDCHI2_OWNPV"] = np.log(dataset.TwoBody_FDCHI2_OWNPV)

    ############################################################
    # NOTE: removed because of new files with ThOr observables
    # # compute eta, to enact prefilter selection
    # dataset["TwoBody_ETA"] = 0.5 * np.log(
    #     (dataset.TwoBody_P + dataset.TwoBody_PZ)
    #     / (dataset.TwoBody_P - dataset.TwoBody_PZ)
    # )
    ############################################################

    # compute radial FD
    dataset["TwoBody_radial_FD"] = np.sqrt(
        (dataset.TwoBody_ENDVERTEX_X - dataset.TwoBody_OWNPV_X) ** 2
        + (dataset.TwoBody_ENDVERTEX_Y - dataset.TwoBody_OWNPV_Y) ** 2
    )

    # set the aliases for trackchi2ndof
    # comment out as its fixed and should be consistent between tuples and script
    # for tr in ("Track1", "Track2"):
    #     dataset[f"{tr}_TrackCHI2NDOF"] = dataset[f"{tr}_ENDVERTEX_CHI2"]

    # must add the ThreeBody extension
    if config["key"] == "ThreeBody":
        # overwrite the variables
        # -----------------------
        # IPCHI2
        dataset["min_FS_IPCHI2_OWNPV"] = dataset[
            ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV", "TrackB_IPCHI2_OWNPV"]
        ].min(axis=1)
        dataset["max_FS_IPCHI2_OWNPV"] = dataset[
            ["Track1_IPCHI2_OWNPV", "Track2_IPCHI2_OWNPV", "TrackB_IPCHI2_OWNPV"]
        ].max(axis=1)
        assert (
            dataset.min_FS_IPCHI2_OWNPV.all() <= dataset.max_FS_IPCHI2_OWNPV.all()
        )  # sanity check

        # PT
        dataset["min_PT_TRACK12"] = dataset[["Track1_PT", "Track2_PT"]].min(axis=1)
        dataset["max_PT_TRACK12"] = dataset[["Track1_PT", "Track2_PT"]].max(axis=1)
        dataset["sum_PT_TRACK12"] = dataset[["Track1_PT", "Track2_PT"]].sum(axis=1)
        assert dataset.min_PT_TRACK12.all() <= dataset.sum_PT_TRACK12.all()
        # P
        dataset["min_P_TRACK12"] = dataset[["Track1_P", "Track2_P"]].min(axis=1)
        dataset["max_P_TRACK12"] = dataset[["Track1_P", "Track2_P"]].max(axis=1)
        dataset["sum_P_TRACK12"] = dataset[["Track1_P", "Track2_P"]].sum(axis=1)
        assert dataset.min_P_TRACK12.all() <= dataset.sum_P_TRACK12.all()

        dataset["min_PT_final_state_tracks"] = dataset[
            ["Track1_PT", "Track2_PT", "TrackB_PT"]
        ].min(axis=1)
        dataset["max_PT_final_state_tracks"] = dataset[
            ["Track1_PT", "Track2_PT", "TrackB_PT"]
        ].max(axis=1)
        dataset["sum_PT_final_state_tracks"] = dataset[
            ["Track1_PT", "Track2_PT", "TrackB_PT"]
        ].sum(axis=1)
        assert (
            dataset.min_PT_final_state_tracks.all()
            <= dataset.sum_PT_final_state_tracks.all()
        )

        # P
        dataset["min_P_final_state_tracks"] = dataset[
            ["Track1_P", "Track2_P", "TrackB_P"]
        ].min(axis=1)
        dataset["max_P_final_state_tracks"] = dataset[
            ["Track1_P", "Track2_P", "TrackB_P"]
        ].max(axis=1)
        dataset["sum_P_final_state_tracks"] = dataset[
            ["Track1_P", "Track2_P", "TrackB_P"]
        ].sum(axis=1)
        assert (
            dataset.min_P_final_state_tracks.all()
            <= dataset.sum_P_final_state_tracks.all()
        )

        ############################################################
        # NOTE: removed because of new files with ThOr observables
        # # compute eta, to enact prefilter selection
        # dataset["ThreeBody_ETA"] = 0.5 * np.log(
        #     (dataset.ThreeBody_P + dataset.ThreeBody_PZ)
        #     / (dataset.ThreeBody_P - dataset.ThreeBody_PZ)
        # )
        ############################################################

        dataset["log_ThreeBody_FDCHI2_OWNPV"] = np.log(dataset.TwoBody_FDCHI2_OWNPV)

        # compute radial FD
        dataset["ThreeBody_radial_FD"] = np.sqrt(
            (dataset.ThreeBody_ENDVERTEX_X - dataset.ThreeBody_OWNPV_X) ** 2
            + (dataset.ThreeBody_ENDVERTEX_Y - dataset.ThreeBody_OWNPV_Y) ** 2
            + (dataset.ThreeBody_ENDVERTEX_Z - dataset.ThreeBody_OWNPV_Z) ** 2
        )

        # # set the aliases for trackchi2ndof
        # for tr in ("Track1", "Track2", "TrackB"):
        #     dataset[f"{tr}_TrackCHI2NDOF"] = dataset[f"{tr}_ENDVERTEX_CHI2"]

    # if booked, prefilter the topo n-track combination
    _prefilter = config["prefilter_combination"][config["key"]]
    if (
        opts.no_prefilter is True
    ):  # in probe mode, we want to keep everything to understand the impact of prefilter+NN
        pass
    else:
        print("enable prefiltering")
        if _prefilter is not None:
            print(" ----- Prefiltering dataset -----")
            print(f"Booked prefilter: {_prefilter} ")
            print("Candidates before prefiltering: ", len(dataset))
            dataset = dataset.query(_prefilter)
            print("Candidates after prefiltering: ", len(dataset))
            print(" ----- Prefiltering complete ----- ")

    # make plots to verify the prefiltering
    _feats = []
    for dict in config["features"][config["key"]]:
        for key in dict:
            _feats.append(key)

    # get the name of the channel
    infile_name = os.path.basename(opts.input)  # Get the base name of the file
    channel = os.path.splitext(infile_name)[0]

    # make the plots
    channel_plot_dir = f"scratch/plots/{config['key']}/calc_obs/prefilter/{channel}"
    Path(channel_plot_dir).mkdir(parents=True, exist_ok=True)
    for f in _feats:
        print(f)

        fig, ax = topo.simple_ax()
        topo.hist_err(
            dataset[f],
            ax=ax,
            bins=100,
            range=(dataset[f].min(), dataset[f].max()),
        )
        ax.set_xlabel(f)
        ax.set_ylabel("Candidates")
        ax.set_yscale("log")
        [fig.savefig(f"{channel_plot_dir}/{f}.{ext}") for ext in ["pdf", "png"]]

    # write to file
    Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    # file = uproot.recreate(
    #     opts.output,
    #     compression=uproot.ZLIB(4),
    # )
    # file[f"{config['key']}/DecayTree"] = dataset
    dataset.to_pickle(opts.output)
    print(f" ----- Success: execution complete & {opts.output} written to file ----- ")
