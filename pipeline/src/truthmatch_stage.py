"""Matching and truthmatching stage in the pipeline"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import awkward as ak
import uproot
from typing import Callable, Any, Union, List
from tqdm import tqdm
import pathlib
import time
import os
from pathlib import Path
from tabulate import tabulate

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    yml_to_dict,
    GhostFinder,
    get_truth_matcher,
    TWOBODY_BRANCHES,
    THREEBODY_BRANCHES,
)
import argparse

if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--channel", help="Decay channel", required=True)
    parser.add_argument("-i", "--input", help="Inputf file path", required=True)
    parser.add_argument(
        "-o",
        "--output",
        help="Full path to output file (including filename)",
        required=True,
    )
    opts = parser.parse_args()

    # PREPROCESSING
    # -------------
    # load the config file
    config = yml_to_dict("./config.yaml")

    # handle config differences between Two- and Three-body triggers
    match config["key"]:
        case "TwoBody":
            _BRANCHES = TWOBODY_BRANCHES
            particles = ["Track1", "Track2"]
        case "ThreeBody":
            _BRANCHES = THREEBODY_BRANCHES
            particles = ["Track1", "Track2", "TrackB"]
        case _:
            raise ValueError(f"Unknown key {config['key']}")

    # se thee upper limit on the events read in
    max_entries = config["max_entries"]
    if opts.channel == "minbias":
        max_entries = config["max_entries_minbias"]

    # take stock of execution time
    # ----------------------------
    start = time.time()

    # read in the data
    # ----------------
    events = uproot.open(f"{opts.input}:{config['key']}/DecayTree", timeout=10_000)
    dataset = events.arrays(
        expressions=_BRANCHES,
        cut=None,
        library="ak",
        entry_stop=max_entries,
    )
    print(f"Success: loaded {len(dataset)} candidates")

    # book report metrics
    # -------------------
    report_metrics = {}

    # First things firs: GHOST REJECTION
    # ----------------------------------
    ghost_finder = GhostFinder()
    dataset["GhostTag"] = ghost_finder.check_ghosts(particles=particles, sample=dataset)

    # add to report metrics
    report_metrics["ghost_rate"] = (
        f"{ak.sum(dataset['GhostTag']) / len(dataset) * 100.:.4f}%"
        + " (%s ghosts of %s total; %s non-ghosts)"
        % (
            ak.sum(dataset["GhostTag"]),
            len(dataset),
            len(dataset[~dataset["GhostTag"]]),
        )
    )

    # persist only non-ghosts; no positive ghost tag should be present
    dataset = dataset[~dataset["GhostTag"]]
    assert (
        ak.sum(dataset["GhostTag"]) == 0
    ), "Ghost removal failed; non-zero ghost tag detected"

    # Second things second: B/D-TRUTHMATCHING
    # ---------------------------------------
    global_tm_pars = {
        "key": config["key"],
        "sample": dataset,
        "fs_tracks": particles,
    }

    # fetch the BEAUTY truthmatcher
    # -----------------------------
    btm = get_truth_matcher(source_type="fromB", **global_tm_pars)
    print(f"\nBooked {btm}")

    # establish the fromB tag: if any beauty ancestor if found in the decay chain, common to the FS tracks, the tag is set to True
    dataset[f"{config['key']}_FromB"] = btm.heavy_flavour_match()
    # now one level further: the beauty ancestor must be the exact same b-hadron (tagged by its key) for all final state particles
    dataset[f"{config['key']}_FromSameB"] = btm.exact_heavy_flavour_match()

    # fetch the CHARM truthmatcher
    # ----------------------------
    ctm = get_truth_matcher(source_type="fromD", **global_tm_pars)
    print(f"\nBooked {ctm}")
    # establish the fromD tag: if any charm ancestor if found in the decay chain, common to the FS tracks, the tag is set to True
    dataset[f"{config['key']}_FromD"] = ctm.heavy_flavour_match()

    # report log
    # ----------
    report_metrics[
        "common-beauty-ancestor rate (post-ghost-veto)"
    ] = "%.10f%% (%s pass out of %s total)" % (
        ak.sum(dataset[f"{config['key']}_FromB"]) / len(dataset) * 100.0,
        ak.sum(dataset[f"{config['key']}_FromB"]),
        len(dataset),
    )
    report_metrics[
        "exact-beauty-signal rate (post-ghost-veto)"
    ] = "%.10f%% (%s pass out of %s total)" % (
        ak.sum(dataset[f"{config['key']}_FromSameB"]) / len(dataset) * 100.0,
        ak.sum(dataset[f"{config['key']}_FromSameB"]),
        len(dataset),
    )

    # NOTE: minbias veto of any beauty or charm
    if opts.channel == "minbias":
        dataset[
            f"{config['key']}_HasBCandidate"
        ] = (
            btm.detect_heavy_flavour()
        )  # NOTE: if any beauty at all from TM, return True for vetoing purposes
        assert ak.sum(dataset[f"{config['key']}_HasBCandidate"]) <= len(
            dataset
        ), "HasBCandidate True-tagged should be a subset of the full dataset"

        # log in report
        report_metrics[
            "any-beauty-signal rate (minbias only) (post-ghost-veto)"
        ] = "%.4f%% (%s beauty-matched [to veto]out of %s total)" % (
            ak.sum(dataset[f"{config['key']}_HasBCandidate"]) / len(dataset) * 100.0,
            ak.sum(dataset[f"{config['key']}_HasBCandidate"]),
            len(dataset),
        )

    # sanity checks
    # -------------
    assert ak.sum(dataset[f"{config['key']}_FromSameB"]) <= ak.sum(
        dataset[f"{config['key']}_FromB"]
    ), "FromSameB True-tagged should be a subset of FromB True-tagged (not only a beauty common ancestor, but the exact same beauty candidate)"
    assert ak.sum(dataset[f"{config['key']}_FromB"]) <= len(
        dataset
    ), "FromB True-tagged should be a subset of the full dataset"
    assert ak.sum(dataset[f"{config['key']}_FromD"]) <= len(
        dataset
    ), "FromD True-tagged should be a subset of the full dataset"
    assert ak.sum(dataset[f"{config['key']}_FromSameB"]) <= len(
        dataset
    ), "FromSameB True-tagged should be a subset of the full dataset"

    # REPORT
    # ------
    print("REPORT:")
    print(tabulate([report_metrics], headers="keys", tablefmt="grid"))

    # TRUTHMATCH: persist only the TM'd candidates
    # --------------------------------------------
    if opts.channel != "minbias":
        dataset = dataset[dataset[f"{config['key']}_FromSameB"] > 0]
        assert ak.sum(dataset[f"{config['key']}_FromSameB"]) == len(
            dataset
        ), "Exact-B matching persistence failed"
    elif opts.channel == "minbias":
        pass  # NOTE: we persist the HasBCandidate tag for minbias, and remove the corresponding *events* later in the pipeline
        assert (
            ak.sum(dataset[f"{config['key']}_HasBCandidate"]) > 0
        ), "Non-zero beauty expected at this stage; beauty veto should take place later in the pipeline"
    else:
        raise ValueError(f"Unknown channel {opts.channel}")

    # writeout
    # --------
    # book the root file
    pathlib.Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    file = uproot.recreate(opts.output, compression=uproot.ZLIB(4))

    # write & close
    print(f"Writing tm dataset to file {opts.output}")
    file[f"{config['key']}/DecayTree"] = dataset
    file.close()
    print(f"Successfully saved to file {opts.output} with {len(dataset)} candidates")

    print("---Completed in %s seconds ---" % (time.time() - start))
