"""Exporting the model to JSON in the Pipeline stage"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import pathlib
import argparse
import json
import torch
import os
import pathlib

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo

if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m", "--model", help="Path to the pytorch model", required=True
    )
    parser.add_argument(
        "-c", "--constrainpath", help="Path to the variable constrains", required=True
    )
    parser.add_argument(
        "-l", "--Lambda", required=True, type=str, help="Lipschitz constant value"
    )
    parser.add_argument(
        "-o", "--output", help="Path to the output json file", required=True
    )
    parser.add_argument(
        "--model_spec",
        help="Path to model complexity file",
        required=False,
        default=None,
    )
    opts = parser.parse_args()

    # load the config file
    _config_path = "./config.yaml"
    config = topo.yml_to_dict(_config_path)

    # NN config
    torch.manual_seed(config["seed"])
    lip_constant = float(opts.Lambda.replace("lambda", ""))
    nn_config = {
        "robust": config["robust"],
        "monotonic": config["monotonic"],
        "_nbody": config["key"],
        "_features": topo.get_feats(config=config, key=config["key"]),
        "LIP": lip_constant,
        "config_path": _config_path,
    }

    # load the pytorch model and parse it as a state_dict
    # also norm the weights in the function
    state_dict = topo.load_model(
        modelpath=opts.model, nn_config=nn_config, params_dict=opts.model_spec, export=True
    )

    # Adjust the labels according to stack requirements
    state_dict = topo.assign_sigmanet_label(state_dict=state_dict)

    # Pass the variable contrains to the dictionary
    state_dict = topo.assign_constrains(
        constrain_path=opts.constrainpath, state_dict=state_dict
    )

    # Dump the model to json
    pathlib.Path(f"{os.path.dirname(opts.output)}").mkdir(parents=True, exist_ok=True)

    with open(f"{opts.output}", "w") as f:  # to format the json file: strg+shift+I
        json.dump(state_dict, f)