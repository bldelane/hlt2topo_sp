"""Train/Test viz of NN training"""

__author__ = "Blaise Delaney & Nicole Schulte"
__email__ = "blaise.delaney@cern.ch"


from numpy.typing import ArrayLike
import numpy as np
from typing import Union, Tuple
import hist
import matplotlib.pyplot as plt
import pandas as pd
import mplhep as hep
from argparse import ArgumentParser
import sklearn.metrics
import os

# import scienceplots

# plt.style.use(["science"])

# must make the topo module visible
import sys
import pathlib

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


parser = ArgumentParser(description="training configuration")
parser.add_argument(
    "-R",
    "--results",
    required=True,
    help="Path to the results file",
)
parser.add_argument(
    "-p",
    "--plot_dir",
    required=True,
    help="Path to the results file",
)
opts = parser.parse_args()


# def plot_eff_prompt_charm(df, _label=1, outputpath=None):

#     sig = df[df.label == _label]
#     prompt_charm = sig.query(
#         f"{opts.nbody}_FromSameB==False and {opts.nbody}_FromSameD==1"  # not a beauty grandmother
#     )

#     # denominators
#     _tot = len(sig)
#     _tot_prompt_charm = len(prompt_charm)

#     # containers
#     sig_effs = []
#     sig_errs = []
#     pch_effs_c = []
#     pch_errs_c = []

#     NNcuts = np.array([0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99])
#     for nncut in NNcuts:
#         # total sig effs
#         _pass = len(sig[sig.preds_per_cand > nncut])
#         eff = _pass / _tot
#         err = std_eff(_tot=_tot, _pass=_pass)
#         sig_effs.append(eff)
#         sig_errs.append(err)

#         # prompt charm effs
#         try:
#             pch_pass = len(prompt_charm[prompt_charm.preds_per_cand > nncut])
#             pch_eff = pch_pass / _tot_prompt_charm
#             pch_err = std_eff(_tot=_tot_prompt_charm, _pass=pch_pass)
#             pch_effs_c.append(pch_eff)
#             pch_errs_c.append(pch_err)
#         except:
#             pch_effs_c.append(0)
#             pch_errs_c.append(0)

#     fig, ax = plt.subplots()

#     ax.errorbar(
#         x=NNcuts,
#         y=sig_effs,
#         # xerr = (NNcuts[:-1] + NNcuts[1:])/2.,
#         yerr=sig_errs,
#         elinewidth=1.5,
#         capsize=3,
#         markeredgewidth=1.5,
#         color="tab:blue",
#         fmt=".",
#         markersize=8,
#         label="Inclusive Beauty",
#     )

#     ax.errorbar(
#         x=NNcuts,
#         y=pch_effs_c,
#         yerr=pch_errs_c,
#         elinewidth=1.5,
#         capsize=0,
#         markeredgewidth=0,
#         color="tab:blue",
#         fmt=".",
#         markersize=0.0,
#     )

#     ax.bar(
#         x=NNcuts,
#         height=pch_effs_c,
#         # width = (NNcuts[:-1] + NNcuts[1:])/2.,
#         color="tab:red",
#         alpha=0.75,
#         label="Prompt Charm",
#     )

#     ax.axhline(1.0, color="darkorange", ls="--", lw=1)
#     ax.set_xlabel("NN response cut [Arbitrary Units]")
#     ax.set_ylabel("Efficiency Ratio")
#     ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
#     ax.set_title(r"$\lambda = %s$" % (_lambda), loc="right", fontsize=25)
#     ax.legend(fontsize=23)

#     ax.set_ylim(bottom=0, top=1.05)
#     plt.savefig(f"{outputpath}_prompt_charm.png")
#     plt.savefig(f"{outputpath}_prompt_charm.pdf")


def plot_roc(truth: ArrayLike, preds: ArrayLike, outputpath=None) -> None:
    """Plot the ROC curve for a given set of predictions and truth labels"""
    roc_auc = sklearn.metrics.roc_auc_score(truth, preds)
    fpr, tpr, _ = sklearn.metrics.roc_curve(truth, preds)
    fig, ax = plt.subplots()
    ax.plot(fpr, tpr, label=f"ROC AUC: {roc_auc:.3f}")
    ax.plot([0, 1], [0, 1], color="grey", linestyle="--", label="Random choice")
    ax.set_xlabel("False positive rate")
    ax.set_ylabel("True positive rate")
    ax.set_xlim(right=1)
    ax.set_ylim(bottom=0)
    ax.set_xticks(np.arange(0, 1, 0.05), minor=True)
    ax.set_yticks(np.arange(0, 1, 0.05), minor=True)
    ax.grid(linestyle="--")
    ax.legend(loc="lower right")
    if outputpath:
        [plt.savefig(f"{outputpath}.{ext}") for ext in ("pdf", "png")]
    plt.close()


def plot_loss(
    train_loss: ArrayLike, test_loss: ArrayLike, outputpath: str = None
) -> None:
    """Plot the loss curves"""
    fig, ax = plt.subplots()
    ax.plot(Train_loss_array, color="dodgerblue", label="Training")
    ax.plot(Test_loss_array, color="deeppink", label="Testing")
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Loss")
    ax.legend(loc="best")
    if outputpath:
        #!mkdir outputpath
        [plt.savefig(outputpath + f"loss.{ext}") for ext in ("pdf", "png")]
    plt.close()


def plot_response(predictions: ArrayLike, outputpath: Union[None, str] = None) -> None:
    """Plot response of the NN"""
    fig, ax = plt.subplots()
    plt.hist(
        predictions[predictions.class_label == 1].preds_per_cand,
        alpha=0.5,
        bins=100,
        log=True,
        label="Signal Predictions",
        density=True,
    )
    plt.hist(
        predictions[predictions.class_label == 0].preds_per_cand,
        alpha=0.5,
        bins=100,
        log=True,
        label="Background Predictions",
        density=True,
    )
    ax.set_xlabel("NN response [Arbitrary Units]")
    ax.set_ylabel("Candidates, normalised")
    plt.legend(loc="best")
    if outputpath:
        [plt.savefig(outputpath + f".{ext}") for ext in ("pdf", "png")]
    plt.close()

def plot_absolute_response(predictions: ArrayLike, outputpath: Union[None, str] = None) -> None:
    """Plot response of the NN"""
    fig, ax = plt.subplots()
    plt.hist(
        predictions[predictions.class_label == 1].preds_per_cand,
        alpha=0.5,
        bins=100,
        log=True,
        label="Signal Predictions",
    )
    plt.hist(
        predictions[predictions.class_label == 0].preds_per_cand,
        alpha=0.5,
        bins=100,
        log=True,
        label="Background Predictions",
    )
    ax.set_xlabel("NN response [Arbitrary Units]")
    ax.set_ylabel("Candidates, normalised")
    plt.legend(loc="best")
    if outputpath:
        [plt.savefig(outputpath + f".{ext}") for ext in ("pdf", "png")]
    plt.close()

if __name__ == "__main__":
    # load the config file
    _config_path = "./config.yaml"
    config = topo.yml_to_dict(_config_path)

    test_sample = pd.read_pickle(f"{opts.results}")
    print(f"Sucessfully imported test dataframe @ {opts.results}")

    _lambda = os.path.normpath(f"{opts.results}").split(os.sep)[-3]
    _lambda = _lambda.replace("lambda", "")

    main_path = opts.plot_dir
    pathlib.Path(main_path).mkdir(parents=True, exist_ok=True)
    pathlib.Path(f"{main_path}/plots").mkdir(parents=True, exist_ok=True)
    Parameterfile = open(
        f"{main_path}/{config['key']}_performance_parameters.npy", "rb"
    )
    print("Unpacking Numpy Arrays")
    params = np.load(Parameterfile, allow_pickle=True)
    print("Unpacking complete")
    Parameterfile.close()
    print("Successfully loaded Parameterfile")

    Train_loss_array = params[0]
    Test_loss_array = params[1]
    ROC_Train_array = params[2]
    ROC_Test_array = params[3]

    print("plotting loss")
    plot_loss(Train_loss_array, Test_loss_array, outputpath=f"{main_path}/plots/")
    print("plotting response")
    plot_response(test_sample, outputpath=f"{main_path}/plots/response")

    # per topo candidate
    plot_roc(
        test_sample.class_label,
        test_sample.preds_per_cand,
        outputpath=f"{main_path}/plots/roc_per_combination",
    )
    plot_absolute_response(test_sample, outputpath=f"{main_path}/plots/absolute_response")
    # per event
    print("per event test set")
    plot_roc(
        test_sample.groupby("unique_event").class_label.max(),
        test_sample.groupby("unique_event").preds_per_cand.max(),
        outputpath=f"{main_path}/plots/roc_per_event",
    )
