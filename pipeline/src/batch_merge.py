"""Merge .pkl dataframe files. Authored by ChatGPT4.0"""

import pandas as pd
import argparse
import os
import glob
import sys
import pathlib
import uproot
import awkward as ak

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo
from topo import load_root, yml_to_dict

# HACK: load the config file
config = yml_to_dict("./config.yaml")


def fetch_matching_files(complete_path):
    # Derive the base directory from the complete_path
    base_dir = os.path.dirname(complete_path)

    # Construct the pattern to match desired files
    pattern = os.path.join(base_dir, "*", "davinci_ntuple.root")

    # Use glob to find all the files matching the pattern
    matching_files = glob.glob(pattern)

    return matching_files


def check_row_count(input_files, merged_df):
    total_rows = 0
    for input_file in input_files:
        evts = uproot.open(f"{input_file}:{config['key']}/DecayTree")
        df = ak.to_dataframe(evts.arrays()).reset_index(
            drop=True
        )  # reset indexing as a precaution
        total_rows += len(df)
    if total_rows != len(merged_df):
        raise ValueError(
            f"Row count mismatch: sum of rows in individual files = {total_rows}, rows in merged file = {len(merged_df)}"
        )


def merge_pkl_files(input_files, output_file):
    batch_files = []

    for input_file in input_files:
        if not os.path.exists(input_file):
            raise FileNotFoundError(f"Input file {input_file} not found")

        evts = uproot.open(f"{input_file}:{config['key']}/DecayTree")
        batch_files.append(
            ak.to_dataframe(evts.arrays()).reset_index(
                drop=True
            )  # reset indexing as a precaution
        )

    # merge
    merged_data = pd.concat(batch_files, ignore_index=True)
    # sanity check
    check_row_count(input_files, merged_data)

    # book outpath
    outdir = os.path.dirname(output_file)
    pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)

    # write to root file, booking the same directory as input root files
    file = uproot.recreate(
        f"{outdir}/davinci_ntuple.root",
        compression=uproot.ZLIB(4),
    )

    merged_data = merged_data.iloc[:-3]
    breakpoint()
    file[f"{config['key']}/DecayTree"] = merged_data

    file.close()


def main():
    parser = argparse.ArgumentParser(description="Merge .pkl files")
    parser.add_argument("-o", "--output", required=True, help="Output .pkl file")
    parser.add_argument(
        "-i", "--input", required=True, nargs="+", help="Input .root files to merge"
    )
    args = parser.parse_args()

    if not args.input:
        raise ValueError("No dummy input files provided")

    # fetch the truthmatch-level files spawned by the truthmatching
    merge_pkl_files(args.input, args.output)


if __name__ == "__main__":
    main()
