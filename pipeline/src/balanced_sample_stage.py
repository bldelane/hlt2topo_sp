"""Create the balanced sample"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import pandas as pd
import os
import glob
import argparse
import pathlib

# must make the topo module visible
import sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    yml_to_dict,
    determine_overall_population,
    determine_minimal_population,
    drop_less_populated,
    create_balanced_train_sample,
    clean_from_training_events,
    create_balanced_test_sample,
    calculate_unique_events,
    determine_minimal_population_unbalanced,
)


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input", nargs="+", default=[], help="Input file path", required=True
    )
    opts = parser.parse_args()

    config = yml_to_dict("./config.yaml")

    files = opts.input
    all_data = pd.concat(pd.read_pickle(file) for file in files)

    all_data = calculate_unique_events(all_data)

    population_dict = determine_overall_population(df=all_data)

    # ==============================================================================
    # ORIGINAL CODE - nominal scheme generating balanced sample
    # ------------------------------------------------------------------------------
    samples_to_drop_list, parameters_balanced_sample = determine_minimal_population(
        population_dict=population_dict,
        lower_bound=config["lower_bound"],
        puffer=config["puffer"],
    )
    all_data = drop_less_populated(drop_list=samples_to_drop_list, df=all_data)
    # ==============================================================================

    # # ==============================================================================
    # # HACK: remove balancing to understand the increase in stats
    # # ------------------------------------------------------------------------------
    # parameters_balanced_sample = determine_minimal_population_unbalanced(
    #         population_dict=population_dict,
    # )
    # # ==============================================================================

    train_sample = create_balanced_train_sample(
        df=all_data, parameters=parameters_balanced_sample, key=config["key"]
    )

    remaining_data = clean_from_training_events(
        complete_df=all_data, train_df=train_sample
    )

    test_sample = create_balanced_test_sample(
        df=remaining_data, parameters=parameters_balanced_sample, key=config["key"]
    )
