"""Create the balanced sample"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import pandas as pd
import os
import glob
import argparse
import pathlib

# must make the topo module visible
import sys
from sklearn.model_selection import train_test_split

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
from topo import (
    yml_to_dict,
    determine_overall_population,
    determine_minimal_population,
    drop_less_populated,
    create_balanced_train_sample,
    clean_from_training_events,
    create_balanced_test_sample,
    calculate_unique_events,
    determine_minimal_population_unbalanced,
)


def split_dataframe(df, ratio=0.8):
    unique_events = df["unique_event"].unique()
    train_events, test_events = train_test_split(unique_events, train_size=ratio)

    train_df = df[df["unique_event"].isin(train_events)]
    test_df = df[df["unique_event"].isin(test_events)]

    return train_df, test_df


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input", nargs="+", default=[], help="Input file path", required=True
    )
    parser.add_argument(
        "-trp",
        "--train_outpath",
        help="Storage path for the training files",
        required=True,
    )
    parser.add_argument(
        "-tsp",
        "--test_outpath",
        help="Storage path for the test files",
        required=True,
    )
    opts = parser.parse_args()

    config = yml_to_dict("./config.yaml")

    files = opts.input

    train_container = []
    test_container = []

    for file in files:
        data = pd.read_pickle(file)
        data = calculate_unique_events(data)

        train_df, test_df = split_dataframe(data)
        train_container.append(train_df)
        test_container.append(test_df)

        # Checking no instance of `unique_event` shared by train_df and test_df
        assert (
            len(set(train_df["unique_event"]) & set(test_df["unique_event"])) == 0
        ), "Test and Train dataframes share unique_event instances."

    train_data = pd.concat(train_container)
    test_data = pd.concat(test_container)

    # Checking the 'class_labels' in train_data and test_data
    for label, data in [("train", train_data), ("test", test_data)]:
        counts = data["class_label"].value_counts()
        print(f"For {label} data: ")
        print(f"Number of instances with class label 0: {counts.get(0, 'None')}")
        print(f"Number of instances with class label 1: {counts.get(1, 'None')}")

    # Saving the train and test dataframes
    for outpath, data in [
        (opts.train_outpath, train_data),
        (opts.test_outpath, test_data),
    ]:
        _outpath = pathlib.Path(outpath)
        _outpath.parent.mkdir(parents=True, exist_ok=True)
        data.to_pickle(
            f"{_outpath}"
        )  # NOTE: we pass the full file path in the pipeline; no need to specify the file name/extension
