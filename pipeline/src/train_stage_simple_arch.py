# """Train the n-body topo BDT/NN
# For now, this is just for simple architecture training. If you want to use this, you need to change the gen_train_execs function in gen_model_stage.py to call this script instead of the train_stage.py script. Also you need to change the eval_stage to call the get_model function instead of the Optim one. This way, you just need to comment out the optuna stage in the pipeline and you can train the model with the given architecture. Not sure yet if the export works
# """

# __author__ = ["Blaise Delaney", "Nicole Schulte"]
# __emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

# from builtins import breakpoint
# from cgi import test
# import os, yaml
# from argparse import ArgumentParser
# from pathlib import Path
# import numpy as np
# import pandas as pd
# import time
# import pickle
# import torch
# from torch.utils.data import TensorDataset, DataLoader
# from torch.optim.lr_scheduler import ReduceLROnPlateau
# from torch.nn import functional as F
# from torch.nn.functional import binary_cross_entropy_with_logits
# from torch.nn import HingeEmbeddingLoss
# from sklearn.metrics import roc_auc_score
# from monotonenorm import SigmaNet, GroupSort, direct_norm
# # must make the topo module visible
# import sys
# import pathlib
# from numpy.typing import ArrayLike


# sys.path.insert(1, "/home/nschulte/purity_development/hlt2topo_sp")
# sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
# import topo

# _config_path = "./config.yaml"
# config = topo.yml_to_dict(_config_path)

# # decorator to check input path exists
# def check_argpath(func):
#     def wrapper(feats_path, **kwargs):
#         try:
#             path = Path(feats_path)
#         except IOError:
#             print("Incorrect input path")
#         features = func(feats_path)
#         return features

#     return wrapper

# # config model
# def get_model(
#     robust: bool,
#     monotonic: bool,
#     _nbody: "keyword: [TwoBody|ThreeBody]",
#     _features: "training feature list",
#     LIP: "Lipschitz const value",
# ) -> "torch.model":
#     def lipschitz_norm(module, is_norm=False, _kind="one"):
#         if not robust:
#             print("Running with uncostrained NN")
#             return module  # condition to call this only if robust
#         else:
#             print("Booked Lip NN")
#             print(is_norm, _kind)
#             return direct_norm(
#                 module,  # the layer to constrain
#                 always_norm=is_norm,
#                 kind=_kind,  # |W|_1 constraint type
#                 max_norm=float(LIP) ** (1 / 4),  # norm of the layer (LIP ** (1/nlayers))
#             )

#     model = torch.nn.Sequential(
#         lipschitz_norm(torch.nn.Linear(len(_features), 32)), 
#         GroupSort(32 // 2),
#         lipschitz_norm(torch.nn.Linear(32, 64)), 
#         GroupSort(64 // 2),
#         lipschitz_norm(torch.nn.Linear(64, 32)), 
#         GroupSort(32 // 2),
#         lipschitz_norm(torch.nn.Linear(32, 1)), 
#     )

#     if robust:
#         print("NOTE: running with monotonicity/robustness in place")
#         print(f"Lambda: {LIP}")
#         if monotonic:
#             _monotone_constraints = topo.load_monotone_constrs(
#                 path=_config_path, key=_nbody
#             )
#         else:
#             _monotone_constraints = list(np.zeros(len(_features)))
#             for i in range(len(_features)):
#                 print(_features[i], _monotone_constraints[i])
#         if not monotonic:
#             _monotone_constraints = np.zeros(len(_features))
#         assert len(_monotone_constraints) == len(_features)
#         model = SigmaNet(model, sigma=LIP, monotone_constraints=_monotone_constraints)
#     print(model)
#     return model

# def evaluate_prediction_simple(
#     config: dict,
#     features: list,
#     data: ArrayLike,
#     _lambda: float,
#     model_path: str,
# ) -> ArrayLike:
#     """
#     Evaluate the model on the given data.
#     """
#     # meta config
#     # -----------
#     torch.manual_seed(config["seed"])  # reproducibility
#     device = topo.get_device()

#     # load the model structure
#     OptimModel = get_model(
#         monotonic=config["monotonic"],
#         robust=config["robust"],
#         _nbody=config["key"],
#         _features=features,
#         LIP=_lambda,
#     ).to(device)

#     # load the trained model state dict
#     # ---------------------------------
#     if device == torch.device("cpu"):
#         OptimModel.load_state_dict(
#             torch.load(model_path, map_location=torch.device("cpu"))
#         )  # Map Location needed if running on cpu
#     else:
#         OptimModel.load_state_dict(torch.load(model_path))

#     # produce the two-class predictions
#     # ---------------------------------
#     OptimModel.eval()

#     if config["key"] == "TwoBody":
#         Y = torch.tensor(
#             [
#                 0.502415,
#                 4.97842,
#                 4.54457,
#                 9.8016,
#                 4.94935,
#                 0.07149092,
#                 8.7687,
#                 10.5117,
#                 0.189736,
#             ]
#         ).to(device)
#     else:
#         Y = torch.tensor(
#             [
#                 4.65653,
#                 5.93234,
#                 11.21230,
#                 1.42334,
#                 11.95423,
#                 2.43248,
#                 0.3123,
#                 1.4324,
#                 12.31234,
#                 -2.42354,
#                 8.4320,
#                 12.3123,
#                 2.09413,
#                 8.31247,
#                 5.31242,
#                 5.31290,
#                 7.03912,
#             ]
#         ).to(device)

#     print("Test Vector Response on inference:")
#     print(torch.sigmoid(OptimModel(Y)).cpu().detach().numpy())

#     return (
#         torch.sigmoid(
#             OptimModel(torch.tensor(data.to_numpy().astype(np.float32)).to(device))
#         )
#         .cpu()
#         .detach()
#         .numpy()
#     )  # NOTE: here we require the sigmoid, as the loss function is BCEwlogits


# if __name__ == "__main__":
#     parser = ArgumentParser(description="training configuration")
#     parser.add_argument(
#         "-l", "--Lambda", required=True, type=float, help="Lipschitz constant value"
#     )
#     parser.add_argument(
#         "-o",
#         "--outdir",
#         required=True,
#         help="directory of sh files for train/eval [default: exec]",
#     )
#     parser.add_argument(
#         "-M",
#         "--monotonic",
#         action="store_true",
#         help="engage monotonicity? [default: False]",
#     )
#     parser.add_argument(
#         "-R",
#         "--robust",
#         action="store_true",
#         help="engage robustness? [default: False]",
#     )
#     parser.add_argument(
#         "-E", "--epochs", default=120, type=int, help="training epochs [int]"
#     )
#     parser.add_argument("-r", "--lrate", default=1e-2, help="learning rate")
#     parser.add_argument("-T", "--train", help="Path to train pkl file", required=True)
#     parser.add_argument("-Y", "--test", help="Path to test pkl file", required=True)
#     parser.add_argument("-v", "--log", help="Path to log file", required=True)
#     parser.add_argument(
#         "--model_spec", help="Path to model complexity file", required=True
#     )
#     opts = parser.parse_args()

#     # load the config file
#     _config_path = "./config.yaml"
#     config = topo.yml_to_dict(_config_path)
#     torch.manual_seed(config["seed"])
#     features = topo.get_feats(config=config, key=config["key"])

#     # if available, run on GPU
#     if torch.cuda.is_available():
#         device = torch.device("cuda")
#         print("Running on cuda")
#     else:
#         device = torch.device("cpu")
#         print("Running on cpu")

#     # ------------------------------------------------------
#     #                   global variables

#     BATCH_SIZE = config["batch_size"]
#     LR = opts.lrate
#     # ------------------------------------------------------

#     train_sample = pd.read_pickle(opts.train)
#     test_sample = pd.read_pickle(opts.test)

#     print("Making Data to Tensors...")
#     X_train = torch.tensor(train_sample[features].values.astype(np.float32))
#     y_train = torch.tensor(train_sample.class_label.values.astype(np.float32))[:, None]
#     X_test = torch.tensor(test_sample[features].values.astype(np.float32))
#     y_test = torch.tensor(test_sample.class_label.values.astype(np.float32))[:, None]
#     print("SUCESS\n")

#     print("Moving Data to Device...")
#     X_train = X_train.to(device)
#     y_train = y_train.to(device)
#     X_test = X_test.to(device)
#     y_test = y_test.to(device)
#     print("SUCESS\n")

#     print("Moving Model to Device...")
#     model = get_model(
#         monotonic=opts.monotonic,
#         robust=opts.robust,
#         _nbody=config["key"],
#         _features=features,
#         LIP=opts.Lambda,
#     ).to(device)
#     print("SUCCESS\n")

#     train_data = TensorDataset(X_train, y_train)
#     train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True)
#     optimizer = torch.optim.Adam(model.parameters(), lr=LR)
#     scheduler = ReduceLROnPlateau(optimizer, "min", patience=10)

#     Train_loss_array = []
#     Test_loss_array = []
#     ROC_Train_array = []
#     ROC_Test_array = []
#     ACC_Test = []
#     ACC_train = []

#     # generate the directory where I can dump the pkld trained model and any training performance plot
#     Path(f"{opts.outdir}/plots").mkdir(parents=True, exist_ok=True)

#     with open(f"{opts.outdir}/training_loop.log", "w") as logfile:
#         logfile.seek(0)

#         print("Start Training")
#         start = time.time()
#         for i in range(opts.epochs):
#             epoch_train_loss = 0
#             epoch_auc_train = 0
#             total_batch = len(train_loader)
#             print(
#                 f"Epoch Number {i} ---------------------------------------------------",
#                 file=logfile,
#             )
#             model.train()
#             for data, target in train_loader:
#                 if device == "cuda":
#                     torch.cuda.synchronize()
#                 optimizer.zero_grad()
#                 output = model(data)
#                 if device == "cuda":
#                     torch.cuda.synchronize()
#                 loss_train = binary_cross_entropy_with_logits(output, target)
#                 loss_train.backward()
#                 epoch_train_loss += loss_train
#                 auc_train = roc_auc_score(
#                     target.cpu().detach().numpy(), output.cpu().detach().numpy()
#                 )
#                 epoch_auc_train += auc_train
#                 epoch_time = time.time() - start
#                 curr_lr = optimizer.param_groups[0]["lr"]
#                 optimizer.step()
#                 print(
#                     f"{i}:  AUC Score:{auc_train}   Loss: {loss_train}, time needed {epoch_time}, LR: {curr_lr}",
#                     file=logfile,
#                 )
#             scheduler.step(loss_train)
#             Train_loss_array.append(
#                 epoch_train_loss.cpu().detach().numpy() / total_batch
#             )
#             ROC_Train_array.append(epoch_auc_train / total_batch)
#             acc_train = np.mean(
#                 (output.cpu().detach().numpy() > 0.5) == target.cpu().detach().numpy()
#             )
#             print(f"Epoche {i} Mean AUC: {epoch_auc_train/ total_batch}")
#             ACC_train.append(acc_train)

#             model.eval()
#             epoch_eval_loss = 0
#             with torch.no_grad():
#                 prediction = model(X_test)
#                 truth = y_test
#                 loss_eval = binary_cross_entropy_with_logits(prediction, truth)
#                 epoch_eval_loss += loss_eval
#                 auc_test = roc_auc_score(
#                     truth.cpu().detach().numpy(), prediction.cpu().detach().numpy()
#                 )
#                 acc_test = np.mean(
#                     (prediction.cpu().detach().numpy() > 0.5)
#                     == truth.cpu().detach().numpy()
#                 )
#             Test_loss_array.append(epoch_eval_loss.cpu().detach().numpy())
#             ROC_Test_array.append(auc_test)
#             ACC_Test.append(acc_test)

#     end = time.time() - start
#     print(f"Total running time: {end}")

#     scaled_predictions = torch.sigmoid(prediction)

#     predictions = np.zeros(len(test_sample))
#     predictions = scaled_predictions.cpu().detach().numpy()
#     test_preds = test_sample
#     test_preds["preds_per_cand"] = predictions

#     # ===== persist to next stage =====
#     performance_params = [
#         Train_loss_array,
#         Test_loss_array,
#         ROC_Train_array,
#         ROC_Test_array,
#         ACC_Test,
#         ACC_train,
#     ]

#     # persist the results
#     # -------------------
#     print("Move Testframe to Pickle")
#     test_preds.to_pickle(
#         f"{opts.outdir}/{config['key']}_Train_Results.pkl"
#     )

#     # save model in pytorch format for inference
#     torch.save(
#         model.state_dict(),
#         f"{opts.outdir}/trained_model.pt",
#     )
#     print(f"SUCCESS: trained model state written to {opts.outdir}/trained_model.pt")

#     print("\nDumping Parameters to Numpy...")
#     with open(
#         f"{opts.outdir}/{config['key']}_performance_parameters.npy",
#         "wb",
#     ) as outputfile:
#         np.save(outputfile, performance_params)
#     print("SUCCESS --- execution complete")