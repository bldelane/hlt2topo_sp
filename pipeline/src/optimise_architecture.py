"""Optuna-based network complexity optimisation"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney at cern.ch"

import sys
import pathlib
import argparse
import numpy as np
import pandas as pd
import torch
from torch.utils.data import TensorDataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn.functional import binary_cross_entropy_with_logits
from sklearn.metrics import roc_auc_score
import optuna
from optuna.pruners import MedianPruner
from optuna.visualization import plot_optimization_history, plot_parallel_coordinate
from typing import Any, List
from train_stage import (
    build_loader,
    inference,
    compute_loss,
    preprocess_data,
    setup_logging,
)
import logging
from functools import partial
import json

# Importing custom modules
sys.path.insert(1, "hlt2topo_sp")
sys.path.insert(1, str(pathlib.Path(__file__).resolve().parents[2]))
import topo

try:
    from monotonenorm import SigmaNet, GroupSort, direct_norm
except ImportError:
    raise ImportError("Ensure the monotonenorm package is installed!")

__authors__ = ["Blaise Delaney"]
__emails__ = ["blaise.delaney@cern.ch"]


def create_model(
    layers: List[int],
    robust: bool,
    monotonic: bool,
    _nbody: str,
    _features: List[str],
    LIP: float,
    config_path: str,
) -> torch.nn.Module:
    """Model with floating layer- and node-level complexity"""

    def lipschitz_norm(module, is_norm=False, _kind="one"):
        if not robust:
            print("Running with uncostrained NN")
            return module  # condition to call this only if robust
        else:
            print("Booked Lip NN")
            print(is_norm, _kind)
            return direct_norm(
                module,  # the layer to constrain
                always_norm=is_norm,
                kind=_kind,  # |W|_1 constraint type
                max_norm=LIP
                ** (1 / (len(layers) + 1)),  # norm of the layer (LIP ** (1/nlayers))
            )

    model_layers = []
    in_features = len(_features)

    for n_units in layers:
        model_layers.append(lipschitz_norm(torch.nn.Linear(in_features, n_units)))
        model_layers.append(GroupSort(n_units // 2))
        in_features = n_units

    # Final layer
    model_layers.append(lipschitz_norm(torch.nn.Linear(in_features, 1)))

    model = torch.nn.Sequential(*model_layers)

    if robust:
        print("NOTE: running with monotonicity/robustness in place")
        print(f"Lambda: {LIP}")
        if monotonic:
            _monotone_constraints = topo.load_monotone_constrs(
                path=config_path,
                key=_nbody,
            )
        if not monotonic:
            _monotone_constraints = list(np.zeros(len(_features)))
        for i in range(len(_features)):
            print(_features[i], _monotone_constraints[i])
        assert len(_monotone_constraints) == len(_features)
        model = SigmaNet(model, sigma=LIP, monotone_constraints=_monotone_constraints)

    print(model)

    return model


def objective(
    trial: optuna.trial.Trial,
    opts: argparse.Namespace,
    _config_path: str,
    logger: logging.Logger,
):
    # Defining the range for number of layers and nodes per layer
    n_layers = trial.suggest_int("n_layers", 1, 7)
    layers = []

    for i in range(n_layers):
        n_units = trial.suggest_categorical(
            f"global_n_units_layer", [8, 16, 32, 64, 128]
        )
        layers.append(n_units)



    # Create model using the extracted hyperparameters
    device = topo.get_device()
    features = topo.get_feats(config=topo.yml_to_dict(_config_path), key=config["key"])
    model = create_model(
        layers=layers,
        robust=opts.robust,
        monotonic=opts.monotonic,
        _nbody=config["key"],
        _features=features,
        LIP=opts.Lambda,
        config_path=_config_path,
    ).to(device)

    # book optimizer, floating the weight decay
    optimizer = torch.optim.Adam(
        model.parameters(), lr=opts.lrate
    )
    scheduler = ReduceLROnPlateau(optimizer, "min", patience=5, factor=0.25)

    # reproducibility
    torch.manual_seed(config["seed"])

    # other requirements
    device = topo.get_device()

    # prepare the data: get train and test sets, and class-imbalance weights
    X_train, y_train, X_test, y_test, class_weights = preprocess_data(
        opts.train,
        opts.test,
        config,
    )

    # build the loaders
    train_loader = build_loader(X_train, y_train, config=config)
    test_loader = build_loader(X_test, y_test, config=config)

    # global-scope pars
    loss_pars = {"opts": opts, "logger": logger}

    for epoch in range(opts.epochs):
        epoch_train_loss = 0
        epoch_auc_train = 0
        total_train_batch = len(train_loader)

        model.train()
        for data, target in train_loader:
            data, target = data.to(device), target.to(device)
            optimizer.zero_grad()
            output = model(data)
            # log the loss
            loss_train = compute_loss(
                output=output,
                target=target,
                class_weights=None, #class_weights
                **loss_pars,
            )
            # backprop
            loss_train.backward()
            optimizer.step()

            epoch_train_loss += loss_train.item()

            # compute the AUC
            auc_train = roc_auc_score(
                target.cpu().detach().numpy(), output.cpu().detach().numpy()
            )
            epoch_auc_train += auc_train

        # epoch-wise averages
        avg_train_loss = epoch_train_loss / total_train_batch
        avg_train_auc = epoch_auc_train / total_train_batch

        # eval loop
        model.eval()
        epoch_eval_loss = 0
        epoch_auc_test = 0
        total_test_batch = len(test_loader)

        with torch.no_grad():
            for data_test, target_test in test_loader:
                # load test data
                data_test, target_test = data_test.to(device), target_test.to(device)

                # FoMs
                eval_loss, auc_test, test_prediction = inference(
                    model=model,
                    data=data_test,
                    targets=target_test,
                    class_weights=None, #class_weights,
                    **loss_pars,
                )
                epoch_eval_loss += eval_loss
                epoch_auc_test += auc_test

        avg_test_loss = epoch_eval_loss / total_test_batch
        avg_test_auc = epoch_auc_test / total_test_batch

        logger.info(
            f"Epoch {epoch} - Training AUC: {avg_train_auc:.4f}, Training Loss: {avg_train_loss:.4f}",
        )
        logger.info(
            f"Epoch {epoch} - Test AUC: {avg_test_auc:.4f}, Test Loss: {avg_test_loss:.4f}",
        )

        scheduler.step(avg_train_loss)

    trial.report(avg_test_auc, epoch)
    # Handle pruning based on the intermediate value.
    if trial.should_prune():
        raise optuna.TrialPruned()

    return avg_test_auc


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-M",
        "--monotonic",
        action="store_true",
        help="engage monotonicity? [default: False]",
    )
    parser.add_argument(
        "-R",
        "--robust",
        action="store_true",
        help="engage robustness? [default: False]",
    )
    parser.add_argument(
        "-H",
        "--hinge",
        action="store_true",
        help="engage Hinge loss? [default: False]",
    )
    parser.add_argument(
        "-F",
        "--focal",
        action="store_true",
        help="engage Focal loss? [default: False]",
    )
    parser.add_argument(
        "-E",
        "--epochs",
        default=25,
        type=int,
        help="training epochs [int]",
    )
    parser.add_argument(
        "-l", "--Lambda", required=True, type=float, help="Lipschitz constant value"
    )
    parser.add_argument(
        "-Z",
        "--hack",
        action="store_true",
        help="HACK: just touch model.pt to work around latency issues",
    )
    parser.add_argument("-r", "--lrate", default=1e-2, help="learning rate")
    parser.add_argument(
        "-o", "--output", help="Best-fit parameters log path", required=True
    )
    parser.add_argument("-T", "--train", help="Path to train pkl file", required=True)
    parser.add_argument("-Y", "--test", help="Path to test pkl file", required=True)
    parser.add_argument("-v", "--log", help="Path to log file", required=True)
    parser.add_argument("-V", "--verbose", action="store_true")
    opts = parser.parse_args()

    # load the config file
    _config_path = "./config.yaml"
    config = topo.yml_to_dict(_config_path)

    # features and device
    device = topo.get_device()
    features = topo.get_feats(config=config, key=config["key"])

    # setup logging
    if opts.verbose:
        logger = setup_logging(opts.log, print_to_console=True, level="debug")
    else:
        logger = setup_logging(opts.log)

    # pruner
    pruner = optuna.pruners.HyperbandPruner()

    study = optuna.create_study(
        direction="maximize",
        pruner=pruner,
    )  # Assuming you want to minimize the objective
    study.optimize(
        partial(
            objective,
            opts=opts,
            logger=logger,
            _config_path=_config_path,
        ),
        n_trials=50,
    )

    logger.info(f"Best parameters: {study.best_params}")
    logger.info(f"Best validation ROC AUC: {study.best_value}")

    with open(opts.output, "w") as f:
        json.dump(study.best_params, f)

    logger.info(f"Best parameters have been saved to {opts.output}")
