"""Compute training variables and, if required, preprocess"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import argparse
import pathlib
from typing import List, Dict
from tabulate import tabulate
import numpy as np
import scipy
import os

# must make the topo module visible
import sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input file path", required=True)
    parser.add_argument("-o", "--output", help="Output file path", required=True)
    parser.add_argument(
        "-N",
        "--noreport",
        help="If passed, skip anything that is not the transform",
        action="store_true",
    )
    opts = parser.parse_args()

    # load the config file
    config = topo.yml_to_dict("./config.yaml")

    # load dataframe from file
    dataset = topo.load_ntuple(file_path=opts.input, nbody_key=config["key"])

    # execute the scaling
    # -------------------
    # book the training variables
    _features = topo.get_feats(config=config, key=config["key"])

    # book the scaler, transform, and return the preprocessed data
    scaler = topo.TransformerFactory(
        dataset=dataset, features=_features, key=config["key"]
    ).get_transformer(
        format="Clamp"
    )  # NOTE: use this factory to book the kind of transformer

    transformed = scaler.transform().dataset
    pathlib.Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    # visualize the transformed features
    # ----------------------------------
    for v in _features:
        # viz sig and minbias post-transform
        topo.twoclass_plot(
            plotdir=str(pathlib.Path(opts.output).parent.absolute()).replace(
                "scratch", "scratch/plots"
            ),
            plotname=f"{v}",
            control=dataset.query("class_label==0")[v],
            control_label="Minimum bias, processed",
            data=transformed.query("class_label==1")[v],
            data_label="Inclusive beauty, processed",
            bins=100,
        )

    # write to file
    # -------------
    transformed.to_pickle(opts.output)

    # whence training, interested in KS report and extrema for online
    if not opts.noreport:
        # produce a report of feature importance: tally the KS test values
        # ----------------------------------------------------------------
        KS_container = []
        for v in topo.get_feats(config=config, key=config["key"]):
            ks = scipy.stats.ks_2samp(
                dataset.query("class_label==0")[v], dataset.query("class_label==1")[v]
            )[0]
            KS_container.append([v, float(ks)])

        KS_container = np.array(KS_container)

        # sort in descending order features to maximise KS score
        KS_container = KS_container[np.argsort(KS_container[:, 1])[::-1]]

        print("KS report - fancy format:")
        print(
            tabulate(
                KS_container, headers=["Variable", "KS score"], tablefmt="fancy_grid"
            )
        )

        print("\nKS report - TeX format:")
        print(
            tabulate(KS_container, headers=["Variable", "KS score"], tablefmt="latex")
        )

        # write to file extrema
        # ---------------------
        bdy_file = open(f"{os.path.splitext(opts.output)[0]}_extrema.log", "w")
        print(
            f"Post-processing extra report for sample {opts.input}\n",
            file=bdy_file,
        )
        for v in _features:
            print(
                f"""
            {v} min: {transformed[v].min()}
            {v} max: {transformed[v].max()}
            """,
                file=bdy_file,
            )
