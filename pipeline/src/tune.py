"""Tune the cut values on the respective two- and three-body responses, individually."""

__author__ = ["Blaise Delaney"]
__emails__ = ["blaise.delaney at cern.ch"]

import pathlib
import pandas as pd
import argparse
import torch
import numpy as np
import os
from typing import Any
import math
from typing import Tuple
from collections import namedtuple
from pathlib import Path

# must make the topo module visible
import sys

sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo

import matplotlib.pyplot as plt

# Define a namedtuple type called "EfficiencyResult"
EfficiencyResult = namedtuple("EfficiencyResult", ["n", "s"])


def compute_efficiency_and_error(n, denom):
    if denom == 0:
        return EfficiencyResult(
            None, None
        )  # return a namedtuple with None values if denom is 0

    efficiency = n / denom
    error = math.sqrt((efficiency * (1 - efficiency)) / denom)

    return EfficiencyResult(efficiency, error)


def eff_factory(
    twobody_probe_df: pd.DataFrame,
    threebody_probe_df: pd.DataFrame,
    mode: str,  # exclusive, incluive or only one
    twobody_cut: float,
    threebody_cut: float,
) -> Tuple[float, float]:  # eff, err
    """Compute the efficiency and uncertainty for the given mode."""
    match mode:
        case "combined":  # need to work out the exclusive vs inlcusive etc cases
            denominator = len(twobody_probe_df) + len(
                threebody_probe_df
            )  # NOTE: not 100% sure this is correct
            numerator = len(
                twobody_probe_df.query(f"preds_per_cand>{twobody_cut}")
            ) + len(threebody_probe_df.query(f"preds_per_cand>{threebody_cut}"))
            return compute_efficiency_and_error(numerator, denominator)
        case "_":
            raise ValueError("missing mode, to be added")


def list_files_remove_extension(path):
    # List all files in the directory
    files = os.listdir(path)

    # Remove extensions
    files_without_extension = [os.path.splitext(file)[0] for file in files]

    return files_without_extension


def execute_inference(
    model_path: str,
    probe: pd.DataFrame,
    key: str,  # 'TwoBody' or 'ThreeBody'
    _config_path: str = "./config.yaml",
    _lambda: float = 2.0,
) -> pd.DataFrame:
    """Perform inference on the preprocessed minbias samples, using the trained model state."""
    # load the config file
    # --------------------
    config = topo.yml_to_dict(_config_path)

    # NN config
    # ----------
    torch.manual_seed(config["seed"])  # reproducibility

    device = topo.get_device()
    features = topo.get_feats(config=config, key=key)

    print("Moving Model to Device...")
    model = topo.get_model(
        monotonic=config["monotonic"],
        robust=config["robust"],
        _nbody=key,
        _features=features,
        LIP=float(_lambda),  # HACK to remove the `lambda` prefix from wildcard`
        config_path=_config_path,
    ).to(device)
    print("Success\n")

    # load the trained model state dict
    # ---------------------------------
    if device == torch.device("cpu"):
        model.load_state_dict(
            torch.load(model_path, map_location=torch.device("cpu"))
        )  # Map Location needed if running on cpu
    else:
        model.load_state_dict(torch.load(model_path))
    model.eval()

    # execute inference
    # -----------------
    X = torch.tensor(probe[features].to_numpy().astype(np.float32)).to(device)

    # produce the two-class predictions
    probe["preds_per_cand"] = torch.sigmoid(model(X)).cpu().detach().numpy()

    return probe


def test_cleanup(df: pd.DataFrame, pre_cleanup_nevts: int) -> None:
    "Test that the cleanup was successful."
    assert df["unique_event"].nunique() == df.shape[0], "Not all events are unique"
    assert len(df) < pre_cleanup_nevts, "No duplicates were dropped"


def retain_unique_evt(
    df: pd.DataFrame,
    mode: str = "max",  # max for conservative rate estimation - ignoring effect on efficiency per candidate
) -> pd.DataFrame:
    "Retain only one candidate of interest, as identified by the mode, per event."
    pre_cleanup_nevts = len(df)

    assert df["class_label"].eq(0).all(), "Not all values in 'class_label' are 0"
    print("Dropping duplicates...")

    match mode:
        case "max":
            # conservative approach: retain the highest-scoring candidate per event
            print("Enacting conservative approach: highest-scoring candidate per event")
            df = df.loc[
                df.groupby("unique_event")["preds_per_cand"].idxmax()
            ]  # sort all events by the highest response and retain, per-event, the highest-scoring candidate -> conservative approach
            test_cleanup(df, pre_cleanup_nevts)
            return df
        case _:
            raise ValueError(f"Invalid mode: {mode}")


MAXEVTS = -1

if __name__ == "__main__":
    # read in the preprocessed minbias samples
    # NB: verify consistency of orthogonal cuts with the online prefilter
    twobody_minbias = pd.read_pickle(
        "scratch/TwoBody/probe/preprocessed/minbias/minbias.pkl"
    )[:MAXEVTS]
    threebody_minbias = pd.read_pickle(
        "scratch/ThreeBody/probe/preprocessed/minbias/minbias.pkl"
    )[:MAXEVTS]

    # step 1: perform inference on the minbias samples
    # this is necessary to assign a response to each candidate
    # --------------------------------------------------------
    # source the trained model paths
    _twobody_model_path = "TwoBody_models/lambda2.0/train/trained_model.pt"
    _threebody_model_path = "ThreeBody_models/lambda2.0/train/trained_model.pt"

    # perform inference on the minbias samples
    twobody_minbias = execute_inference(
        model_path=_twobody_model_path,
        probe=twobody_minbias,
        key="TwoBody",
        _config_path="./config.yaml",
        _lambda=2.0,
    )
    threebody_minbias = execute_inference(
        model_path=_threebody_model_path,
        probe=threebody_minbias,
        key="ThreeBody",
        _config_path="./config.yaml",
        _lambda=2.0,
    )

    # step 2: per-event, retain only the highest-scoring candidate to be conservative
    # ------------------------------------------------------------------------------
    # ensure that the minbiases are zero-labelled, and drop all duplicates of the unique event ID (~only one ignore that each event yields multiple candidates, for rate eval)
    twobody_minbias = retain_unique_evt(twobody_minbias, mode="max")
    threebody_minbias = retain_unique_evt(threebody_minbias, mode="max")

    # now, we want to tally the events in the OR of the two- and three-body triggers
    # to that end, constrain the number of events to the minimum of the two
    n = min(len(twobody_minbias), len(threebody_minbias))
    twobody_minbias = twobody_minbias.head(n)
    threebody_minbias = threebody_minbias.head(n)

    # step 3: formalise the suppression factor in the event rate
    # ----------------------------------------------------------
    INPUT_RATE = 1e6  # Hz
    OUTPUT_RATE = 10e3  # Hz - nominal
    event_suppression_factor = OUTPUT_RATE / INPUT_RATE
    print(f"Event suppression factor: {event_suppression_factor}\n")

    # now, we want to tally the events in the OR of the two- and three-body triggers
    input_events_total = len(twobody_minbias) + len(threebody_minbias)
    assert (
        input_events_total == 2 * n
    ), "Input events are not equal to 2 * n"  # sanity check

    # assemble dataframe with respective responses -> 1to1 mapping of unique events, tallied conservatively based on the highest-scoring candidate
    topo_responses = pd.DataFrame()
    topo_responses["twobody"] = twobody_minbias["preds_per_cand"].reset_index(drop=True)
    topo_responses["threebody"] = threebody_minbias["preds_per_cand"].reset_index(
        drop=True
    )

    # list the signal modes of interest
    # ---------------------------------
    _threebody_probe_path = (
        "scratch/ThreeBody/probe/preprocessed"
    )
    _twobody_probe_path = (
        "/scratch/TwoBody/probe/preprocessed"
    )

    # get a handle on the signal modes
    probe_sig_modes = [
        mode
        for mode in list_files_remove_extension(_threebody_probe_path)
        if (
            mode != "minbias" and os.path.isdir(f"{_threebody_probe_path}/{mode}")
        )  # not minbias and available to both 2body and 3body
    ]

    # loop through the respective responses
    fix, (ax1, ax2) = plt.subplots(ncols=2, nrows=1, figsize=(12, 6))
    ax1.set_xlabel("2body NN Response [A.U.]")
    ax1.set_ylabel("3body NN Response [A.U.]")
    ax1.set_title("LHCb Simulation @ 10 kHz Output Rate")
    ax1.set_xlim(0.9, 1.0)
    ax1.set_ylim(0.9, 1.0)

    # Placeholder for lines & annotations
    lines = []
    annotations = []

    Path("plots/tuning").mkdir(parents=True, exist_ok=True)
    for i in np.arange(0.99, 1.0, 1e-4):
        for j in np.arange(0.99, 1.0, 1e-4):
            _yield = len(
                topo_responses.query(f"twobody > {i} or threebody > {j}")
            )  # combined rate
            print(f"Two-body response: {i}")
            print(f"Three-body response: {j}")
            print(
                f"Total events: {_yield}, with target: {input_events_total * event_suppression_factor}"
            )
            if np.abs(
                _yield - (input_events_total * event_suppression_factor)
            ) < math.floor(
                0.1 * (input_events_total * event_suppression_factor)
            ):  # tolerance of 10% (rounded up) of the nominal output rate
                print(f"Two-body response: {i}")
                print(f"Three-body response: {j}")
                print(
                    f"Total events: {_yield}, with target: {input_events_total * event_suppression_factor}"
                )
                breakpoint()
                ax1.plot(
                    i,
                    j,
                    marker="+",
                    color="tab:blue",
                    markersize=7.5,
                    markeredgewidth=2,
                    label="10 kHz output rate",
                )

                # Remove previous lines and annotations
                for line in lines:
                    line.remove()
                for annotation in annotations:
                    annotation.remove()
                lines = []
                annotations = []

                # Draw new lines and store them
                (line1,) = ax1.plot([i, i], [0.9, j], color="grey", linestyle="dashed")
                (line2,) = ax1.plot([0.9, i], [j, j], color="grey", linestyle="dashed")
                lines.extend([line1, line2])

                # Add new annotation and store it
                annotation = ax1.text(
                    i + 0.002,
                    j + 0.002,
                    f"({i:.3f}, {j:.3f})",
                    ha="left",
                    va="bottom",
                )
                annotations.append(annotation)

                # end of plot

                # now, onto per-candidate eff
                # for twobody and three-body, execute inference
                # ---------------------------------------------
                ax2.clear()
                for sigmode in probe_sig_modes:
                    # twobody inference
                    probe_df = pd.read_pickle(
                        f"{_threebody_probe_path}/{sigmode}/{sigmode}.pkl".replace(
                            "ThreeBody", "TwoBody"
                        )
                    )
                    twobody_inference_sig = execute_inference(
                        model_path=_twobody_model_path,
                        probe=probe_df,
                        key="TwoBody",
                        _config_path="./config.yaml",
                        _lambda=2.0,
                    )

                    # threebody inference
                    probe_df = pd.read_pickle(
                        f"{_threebody_probe_path}/{sigmode}/{sigmode}.pkl"
                    )
                    threebody_inference_sig = execute_inference(
                        model_path=_threebody_model_path,
                        probe=probe_df,
                        key="ThreeBody",
                        _config_path="./config.yaml",
                        _lambda=2.0,
                    )

                    # compute the efficiencies, per mode
                    eff_result = eff_factory(
                        twobody_probe_df=twobody_inference_sig,
                        threebody_probe_df=threebody_inference_sig,
                        mode="combined",
                        twobody_cut=i,
                        threebody_cut=j,
                    )
                    print(
                        f"Signal mode: {sigmode}, eff: {eff_result.n}, err: {eff_result.s}"
                    )
                    ax2.errorbar(
                        x=sigmode,
                        y=eff_result.n * 100.0,
                        yerr=eff_result.s * 100.0,
                        color="tab:blue",
                        marker="o",
                        markersize=3.5,
                        markeredgewidth=2,
                    )
                ax2.set_ylim(bottom=60.0, top=85.0)
                plt.xticks(rotation=45)
                ax2.set_ylabel("Inclusive efficiency [%]")
                [plt.savefig(f"plots/tuning/{i}_{j}_10kHz.{ext}") for ext in ["png"]]
