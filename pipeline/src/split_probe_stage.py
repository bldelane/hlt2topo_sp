"""Assign class label, event label and clean the minbias from beauty events"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import uproot
import pathlib
import argparse
import numpy as np
from pathlib import Path

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo
from topo import (
    yml_to_dict,
    clean_minbias,
    calculate_unique_events,
    load_root,
    label_objects,
)


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--channel", help="Decay channel", required=True)
    parser.add_argument("-i", "--input", help="Input file path", required=True)
    parser.add_argument("-o", "--output", help="Output file path", required=True)
    parser.add_argument(
        "-p",
        "--probe",
        help="Full path to probe file (including filename)",
        required=True,
    )
    opts = parser.parse_args()

    # load the config file
    config = topo.yml_to_dict("./config.yaml")

    # load json file
    sample = topo.load_ntuple(
        file_path=opts.input,
        nbody_key=config["key"],
    )
    print(f"Received sample of length {len(sample)}")

    #the unique event needs to be assigned AFTER merging all subjobs together since it is simply counting up
    sample = calculate_unique_events(sample)
    # --------------------------------------------------------------------
    # make the 80/20 split for (train/eval) and probe
    # --------------------------------------------------------------------      

    # Find unique events
    unique_events = sample['unique_event'].unique()

    # Calculate the number of events for each split
    num_total_events = len(unique_events)
    num_events_65_percent = int(0.65 * num_total_events)
    print(f"65 percent of all number of events: {num_events_65_percent} out of {num_total_events}")

    # Randomly select 80% of events
    selected_events = np.random.choice(unique_events, num_events_65_percent, replace=False)

    # Create two DataFrames based on the selected events
    df_65_percent = sample[sample['unique_event'].isin(selected_events)].copy()
    df_35_percent = sample[~sample['unique_event'].isin(selected_events)].copy()

    print(f"65 percent sample has now {len(df_65_percent)} candidates")
    print(f"35 percent sample has now {len(df_35_percent)} candidates")

    assert len(df_35_percent) < len(df_65_percent) < len(sample)
    df_65_percent.to_pickle(opts.output)
    df_35_percent.to_pickle(opts.probe)
