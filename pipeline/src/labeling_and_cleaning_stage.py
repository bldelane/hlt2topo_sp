"""Assign class label, event label and clean the minbias from beauty events"""

__author__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import sys
import uproot
import pathlib
import argparse
import numpy as np
from pathlib import Path

# must make the topo module visible
sys.path.insert(1, f"{pathlib.Path(__file__).resolve().parents[2]}")
import topo
from topo import (
    yml_to_dict,
    clean_minbias,
    calculate_unique_events,
    load_root,
    label_objects,
)


if __name__ == "__main__":
    # I/0 for channel
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--channel", help="Decay channel", required=True)
    parser.add_argument(
        "-i",
        "--input",
        help="Full path to input file (including filename)",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Full path to output file (including filename)",
        required=True,
    )
    parser.add_argument(
        "-B",
        "--keep_beauty",
        help="Probe mode: do not remove beauty-like candidates",
        action="store_true",
    )
    opts = parser.parse_args()

    # load the config file
    config = yml_to_dict("./config.yaml")

    # load the root file
    sample = load_root(
        file=f"{opts.input}:{config['key']}/DecayTree", library="pd", name=opts.channel
    )

    sample = label_objects(sample, opts.channel)
    # removing from minbias-specific loop to be able to assign to signal events as well, not only minbias
    print(f"{len(sample)} composites before cleaning")

    # --------------------------------------------------------------------
    # NOTE: moved to dedicated executable in the training pipeline
    # if opts.channel == "minbias":
    #     # remove beauty for trainig; for tuning, keep to monitor the purity
    #     if opts.keep_beauty is False:
    #         print("Engaging with removal of beauty-like candidates removal")
    #         sample = clean_minbias(df=sample, key=config["key"])
    #         # del sample["unique_event"] # NOTE: not sure why this is but removed for tuning hacky study
    #     # FIXME: Answer from nicole: This is removed as the unique event get's reassigned after removing beauty to keep a consistent numbering scheme
    #     print(f"{len(sample)} composites after cleaning")
    # --------------------------------------------------------------------

    # --------------------------------------------------------------------
    # if singlemuon trigger, require at least one ismuon positive decision
    # --------------------------------------------------------------------
    if config["topotype"] == "singlemuon":
        print("Booked singlemuon trigger")

        if config["key"] == "TwoBody":
            sample = sample.query("Track1_isMuon + Track2_isMuon > 0")
            _bs = [
                "Track1_isMuon",
                "Track2_isMuon",
            ]
        if config["key"] == "ThreeBody":
            sample = sample.query("Track1_isMuon + Track2_isMuon + TrackB_isMuon > 0")
            _bs = ["Track1_isMuon", "Track2_isMuon", "TrackB_isMuon"]

        # sanity check
        assert (
            np.array(sample[_bs].sum(axis=1)).all() > 0
        ), "ValueError: the singlemuon must have at least one positive isMuon decision"

    # features = []
    # for dict in config["features"][config["key"]]:
    #     for key in dict:
    #         features.append(key)

    # channel_plot_dir = f"scratch/plots/{config['key']}/truthmatch/FromB/{opts.channel}"
    # Path(channel_plot_dir).mkdir(parents=True, exist_ok=True)
    # for f in features:
    #     fig, ax = topo.simple_ax()
    #     topo.hist_err(
    #         sample[f],
    #         ax=ax,
    #         bins=100,
    #         range=(sample[f].min(), sample[f].max()),
    #     )
    #     ax.set_xlabel(f)
    #     ax.set_ylabel("Candidates")
    #     ax.set_yscale("log")
    #     [fig.savefig(f"{channel_plot_dir}/{f}.{ext}") for ext in ["pdf", "png"]]
    # write to file
    Path(opts.output).parent.mkdir(parents=True, exist_ok=True)
    # file = uproot.recreate(
    #     opts.output,
    #     compression=uproot.ZLIB(4),
    # )
    # file[f"{config['key']}/DecayTree"] = dataset
    sample.to_pickle(opts.output)
