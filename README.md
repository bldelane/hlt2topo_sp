# Hlt2Topo_SP

Development of the HLT2 inclusive-beauty and single-muon topological triggers, and selective persistence (SP).

Maintainers: Blaise Delaney, Nicole Schulte

Email: blaise.delaney@cern.ch, nicole.schulte@cern.ch

## Pipeline

The workflow is regulated by a pipeline makes use of [snakemake](https://snakemake.readthedocs.io/en/stable/). The idea is to implement, sequentially, the following steps:

