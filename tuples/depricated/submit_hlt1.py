"""Ganga options to run HLT1 on the (x)digi files in Moore.

This must run in a dedicated Moore build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga submit_hlt1.py --channel <channel ID> --polarity <MagUp/MagDown> [--sjobs <n (int)>] [--test] [--dryrun]
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

import argparse
import sys
import pathlib
from gen_bkk_paths import main as bkk_paths
from typing import List, Union

parser = argparse.ArgumentParser(description="Decay identifier")
parser.add_argument("-t", "--test", help="Run on local backend", action="store_true")
parser.add_argument("-sj", "--sjobs", help="Number of subjobs", type=int, default=5)
parser.add_argument("-c", "--channel", help="Decay channel alias", required=True)
parser.add_argument("-p", "--polarity", help="MagUp or MagDown", required=True)
parser.add_argument(
    "-d", "--dryrun", help="If True, do NOT submit()", action="store_true"
)
opts = parser.parse_args()


def retrieve_bkk_paths(
    channel: str,
    polarity: str,
    processed_bkk: dict = bkk_paths(),
) -> Union[str, List[str]]:
    """Compile list of bkk paths for the job dataset

    Parameters
    ----------
    channel: str
        Decay channel identifier

    processed_bkk: dict = bkk_paths()
        Dict of processed bkk paths

    Returns
    -------
    Union[str, List[str]]
        List of per-channel bkk paths
    """
    return processed_bkk[channel][polarity] 


# prerequisite
gridProxy.renew()

# job label
_name = f"{opts.channel}_to_hlt1_filtered_dst"

# configure jobs app
j = Job()
j.name = _name
myApp = GaudiExec()
myApp.directory = f"{pathlib.Path().resolve().parents[2]}/MooreDevTwo"  # relative path; run in /ganga/ folder
print(f"{pathlib.Path().resolve().parents[2]}/MooreDevTwo")
j.application = myApp
j.application.platform = "x86_64_v2-centos7-gcc12+detdesc-opt"
j.application.options = ["HLT1_options.py"]

# I/O
bkk_path = retrieve_bkk_paths(opts.channel, opts.polarity)
j.inputdata = BKQuery(bkk_path).getDataset()
j.outputfiles = [DiracFile("*.dst"), LocalFile("*.tck.json")]

# submit config
if opts.test:
    _name += "_testjob"
    j.backend = Local()
else:
    j.backend = Dirac()
    j#.backend.settings['BannedSites'] = ['SARA-USER','LCG.SARA.nl','grid.sara.nl', 'grid.surfsara.nl']
    j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])'
    if opts.sjobs > 1:
        j.splitter = SplitByFiles(
            filesPerJob=opts.sjobs, ignoremissing=True, bulksubmit=False
        )
    else:
        j.splitter = SplitByFiles(filesPerJob=1, ignoremissing=True, bulksubmit=False)

if not opts.dryrun:
    # if dryrun, build only and do NOT submit
    j.submit()
