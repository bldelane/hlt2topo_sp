import subprocess
from termcolor import colored


#ugly like this, but I launched the others manually, so it will have to do
#samples = ['DPiPiPi', 'DsMuNu', 'DsPi','DstD0','DstTauNu','JpsiPhiKs','K6Mu','KPiPi','KstGamma','Lc2625MuNu','LcPPi','PhiKst','PiMuNu','JPsiK_mumu','Lb_LcMuNu','Lb_PMuNu','Bc_JpsiTauNu','Bc_JpsiMuNu','Bs_KMuNu','Bs_KTauNu','Bu_D0XMuNu','Bu_D0TauNu','Bu_Dst0TauNu', 'Bu_PPTauNu','Bu_PPMuNu']  # Add your list of sample names here
samples = ['Bs_Jpsiphi','Lb_Lcpi','Omegac0_L0KS0','Bs_KKgamma','Bd_Kpigamma','Bu_JpsiPi','Bu_JpsiK']
polarities = ['MagUp', 'MagDown']  # List of polarities

for sample in samples:
    for polarity in polarities:
        command = f"ganga submit_hlt1.py -c {sample} -p {polarity}"
        try:
            subprocess.run(command, shell=True, check=True)
            print(colored(f"Launched job for {sample} with polarity {polarity}", 'magenta'))
        except subprocess.CalledProcessError:
            print(colored(f"Job launch failed for {sample} with polarity {polarity}", 'magenta'))