__author__ = "Jamie Gooding, Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

import argparse
import json
import os
import uproot

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--template", default="chained_hlt1_hlt2_template.py", required=True)
parser.add_argument("-sj", "--subjobs", type=int, default=20, required=True)
parser.add_argument("-c", "--channel", type=str, required=True)
parser.add_argument("-e", "--events",  type=int, default=1000, required=False)

parser.add_argument("-cp", "--ceph_path", type=str, required=True)
parser.add_argument("-ep", "--eos_path", type=str, required=False)
parser.add_argument("-op", "--output_path", type=str, default= "HLT_options/", required=False)

opts = parser.parse_args()

template = opts.template
filename = template.replace('template', 'options')

with open(f'/net/nfshome2/home/nschulte/stack/hlt2topo_sp/tuples/samples/{opts.channel}.json', 'r') as bkk_file:
        data = json.load(bkk_file)

filename = filename.replace('chained', f'{opts.channel}')

print(f"Filename: {filename}")

datafile_count = 0
polarity = 'magUp'
for n in range(opts.subjobs):
    outdir = f"{opts.output_path}{opts.channel}/"
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    outpath = f'{outdir}{filename}'.replace('.py', f'_{n}.py')
    with open(outpath, 'w') as outfile:
        loaded_events = 0
        print(f"Writing {outpath}")
        with open(opts.template) as infile:
            for inline in infile:
                if 'NUM_EVENTS' in inline:
                    print(f'Requested {opts.events} events')
                    outfile.write(inline.replace('NUM_EVENTS', str(opts.events)))
                elif 'INFILES' in inline:
                    outstring = inline.replace('INFILES', '[').replace('\n', '')
                    temp_load_events = 0
                    tempstring = ""
                    datafiles = data[polarity]['datafiles']
                    while loaded_events < opts.events and datafile_count < len(datafiles):
                        data_path = datafiles[datafile_count]
                        print(f"Reading from '{data_path}'")
                        tempstring += f'"{data_path}",'
                        with uproot.open(f'{data_path}:Event', num_workers=8) as ntuple:
                            temp_load_events = len(ntuple['_Event.'].array())
                        print(f'{temp_load_events} found')
                        datafile_count += 1
                        if datafile_count >= len(datafiles):
                            break
                        else:
                            loaded_events += temp_load_events
                    if datafile_count >= len(datafiles) and polarity == 'magUp':
                        loaded_events = 0
                        datafile_count = 0
                        print("Switching to magDown")
                        polarity = 'magDown'
                        tempstring = ""
                        while loaded_events < opts.events and datafile_count < len(datafiles):
                            data_path = datafiles[datafile_count]
                            print(f"Reading from '{data_path}'")
                            tempstring += f'"{data_path}",'
                            with uproot.open(f'{data_path}:Event', num_workers=8) as ntuple:
                                loaded_events += len(ntuple['_Event.'].array())

                            print(f'{loaded_events} found')
                            datafile_count += 1
                        outstring += tempstring
                    elif datafile_count >= len(datafiles) and polarity == 'magDown':
                        print("OUT OF DATA")
                    else:
                        outstring += tempstring
                    outstring += ']\n'
                    outfile.write(outstring)
                elif 'DDDBTAG' in inline:
                    dddb_tag= data[polarity]['dddb_tag']
                    outfile.write(inline.replace('DDDBTAG', dddb_tag))
                elif 'CONDDBTAG' in inline:
                    conddb_tag = data[polarity]['conddb_tag']
                    outfile.write(inline.replace('CONDDBTAG', conddb_tag))
                elif 'OUTDST' in inline:
                    #TODO: Check if this works for eos paths
                    outfile.write(inline.replace('OUTDST', f'"{opts.ceph_path}{opts.channel}_hlt1_hlt2_filtered_{n}' + '.{stream}.dst"'))
                elif 'OUTTCK' in inline:
                    outfile.write(inline.replace('OUTTCK', f'"{opts.ceph_path}{opts.channel}_hlt1_hlt2_filtered_{n}.tck.json"'))
                elif 'NUM' in inline and '.root' in inline:
                    outfile.write(inline.replace('NUM', str(n)))
                else:
                    outfile.write(inline)
    print()
