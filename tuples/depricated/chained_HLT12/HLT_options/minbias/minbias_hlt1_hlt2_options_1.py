###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = "Jamie Gooding, Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from typing import List, Union

from Moore.options import options
from Moore.config import (
    moore_control_flow, allen_control_flow, register_line_builder
)
from Moore.lines import Hlt2Line

from RecoConf.hlt1_allen import (
    allen_gaudi_config as allen_sequence,
    call_allen_decision_logger,
)

from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction, upfront_reconstruction
from RecoConf.protoparticles import make_charged_protoparticles


from PyConf import configurable

from RecoConf.decoders import default_ft_decoding_version

from Hlt2Conf.lines.topological_b import (
    make_unfiltered_topo_twobody,
    make_unfiltered_topo_threebody,
    line_prefilters,
)

#from Hlt2Conf.lines import topological_b

all_lines = {}


@register_line_builder(all_lines)
@configurable
def hlt1filtered_twobody_line(
    name: str = "Hlt2TwoBodyTopo_Hlt1Filtered",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Two-body topo line with Hlt1*Line MVA filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered two-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_twobody()  # no mva selection
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,
        hlt1_filter_code=["Hlt1.*Decision"],  # filter on HLT1 decisions
    )


@register_line_builder(all_lines)
@configurable
def hlt1filtered_threebody_line(
    name: str = "Hlt2ThreeBodyTopo_Hlt1Filtered",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Three-body topo line with Hlt1*Line filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered three-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_threebody()  # no mva selection
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,
        hlt1_filter_code=["Hlt1.*Decision"],  # filter on HLT1 decisions
    )


def make_lines() -> List[Hlt2Line]:
    """Make the lines."""
    return [hlt1filtered_twobody_line(), hlt1filtered_threebody_line()]



# FT decoder version to use
ft_decoding_version=6
default_ft_decoding_version.global_bind(value=ft_decoding_version)

# set the options
options.evt_max = -1
options.input_files = ["root://x509up_u5240@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151659/0000/00151659_00000013_1.xdigi","root://x509up_u5240@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151659/0000/00151659_00000017_1.xdigi",]
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
options.simulation = True
options.input_type = 'ROOT'
options.input_raw_format = 0.3

# We have long names for algorithms and would like to see them in full glory
options.msg_svc_format = "% F%56W%S%7W%R%T %0W%M"

options.output_file = "/ceph/users/nschulte/tuplesminbias_hlt1_hlt2_filtered_1.{stream}.dst"
options.output_type = "ROOT"
options.output_manifest_file = "/ceph/users/nschulte/tuplesminbias_hlt1_hlt2_filtered_1.tck.json"

with allen_sequence.bind(sequence="hlt1_pp_veloSP"),\
     reconstruction.bind(from_file=False):

    config = configure_input(options)

    allen_node = allen_control_flow(
        options, write_all_input_leaves=False
    )
    hlt1_node = CompositeNode(
        "MooreAllenWithLogger",
        combine_logic=NodeLogic.LAZY_AND,
        children=[allen_node, call_allen_decision_logger()],
        force_order=True,
    )

    hlt2_streams = make_lines()
    # Create default streams definition if make_streams returned a list
    if not isinstance(hlt2_streams, dict):
        hlt2_streams = dict(full=hlt2_streams)

    # Combine all lines and output in a global control flow.
    hlt2_node = moore_control_flow(
        options, hlt2_streams, "hlt2"
    )

    top_cf_node = CompositeNode(
        "HLT_1andHLT2",
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[hlt1_node, hlt2_node],
        force_order=False,
    )

    config.update(
        configure(
            options,
            top_cf_node,
            public_tools=[stateProvider_with_simplified_geom()],
        )
    )
