from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
"LFN:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagUp-nu7.6/hlt1_filtered/11102005/input_1.dst"
], clear=True)
