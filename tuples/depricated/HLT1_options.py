###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Running hlt1 is required to persist the hlt1 trigger decisions.

Based on:
- https://gitlab.cern.ch/amathad/bachelors-project-uzh-2022/-/blob/master/Hlt1_Hlt2_DV_options/Hlt1/run_myhlt1_MagDown.py
- Help provided by N. Skidmore: https://gitlab.cern.ch/-/snippets/2259

Instructions:
$ Moore/run gaudirun.py <path/to/HLT1_options.py>
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from Moore import options
from Moore.config import allen_control_flow
from RecoConf.hlt1_allen import call_allen_decision_logger
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt1_allen import allen_gaudi_config as allen_sequence
from AllenCore.generator import make_transposed_raw_banks
from RecoConf.hlt1_tracking import default_ft_decoding_version
from RecoConf.decoders import default_ft_decoding_version
from Configurables import RootCnvSvc

# # job options - comment out when Ganga-submitting
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = 'dddb-20210617'
options.conddb_tag = 'sim-20210617-vc-md100'
options.input_type = "ROOT"
options.input_raw_format = 0.5
# https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/different_samples.html?highlight=default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
options.evt_max = -1
# generate a dst file
options.output_file = "HLT1filteredFile.dst"
options.output_type = "ROOT"

# needed to submit the HLT2 jobs
options.output_manifest_file = "HLT1filteredFile.tck.json"


# run the application - with many thanks to N. Skidmmore for the help
# see https://gitlab.cern.ch/-/snippets/2259
# -------------------------------------------------------------------
# Run Allen for MC without Retina clusters ("hlt1_pp_veloSP") and log HLT1 decisions
default_ft_decoding_version.global_bind(value=6)
with allen_sequence.bind(sequence="hlt1_pp_veloSP"):
    config = configure_input(options)
    allen_node = allen_control_flow(options)
    top_cf_node = CompositeNode(
        "MooreAllenWithLogger",
        combine_logic=NodeLogic.LAZY_AND,
        children=[allen_node, call_allen_decision_logger()],
        force_order=False,
    )
    config.update(configure(options, top_cf_node))

