import uproot4 as uproot
import pandas as pd
import awkward as ak
import numpy as np

def flatten_array(array):
    # If the array is a structured array (array or vector), flatten it
    if array.dtype.fields:
        flattened = array.reshape(-1)
        return flattened
    else:
        return array

def parse_root_file(file_path):
    # Open the ROOT file
    root_file = uproot.open(file_path)

    # Get the TTree from the file
    tree = root_file["TwoBody/DecayTree"]  # Replace "tree_name" with the actual name of your TTree

  # Initialize an empty dictionary to hold the data
    data = {}

    # Iterate over the TTree and parse it into a dictionary
    for branch in tree.iterate(library="np"):
        for branch_name, array in branch.items():
            # Flatten the array or vector structure
            flattened_array = flatten_array(array)

            # Add the flattened array to the data dictionary
            if branch_name in data:
                # If the branch already exists, append the array to it
                data[branch_name].extend(flattened_array)
            else:
                # If the branch doesn't exist, create a new entry in the dictionary
                data[branch_name] = flattened_array.tolist()

    # Create a DataFrame from the data dictionary
    data_frame = pd.DataFrame(data)

    return data_frame

# Example usage
file_path = "/net/nfshome2/home/nschulte/stack/Bd_Kpi.root"
parsed_data = parse_root_file(file_path)

truth = [211.0, 211.0]
dataset = ak.Array(parsed_data[f"TwoBody_DAUGHTERS"].to_list())

print(dataset)
dataset = dataset[
    ak.count(dataset,axis=1) == len(truth)
]
print(dataset)
mask = ak.sum(abs(dataset), axis=1) == ak.sum(np.abs(truth))
print(mask)
for i in range(len(truth)):  # NOTE: the only loop here is over the track slots (O(10)), as opposed to per-candidate (>~O(10^4))
    # if i == 0:
    #     continue
    dataset = dataset[
        abs(dataset[..., i]) == abs(truth[i])
        ]
    

print(parsed_data["TwoBody_DAUGHTERS"])
print(len(dataset))

print("Track1_TRUEID and Track2_TRUEID both 0:", len(parsed_data[(parsed_data["Track1_TRUEID"] == 0) & (parsed_data["Track2_TRUEID"] == 0)]))
print("Track1_TRUEID 0: ", len(parsed_data[parsed_data["Track1_TRUEID"] == 0]))
print("Track2_TRUEID 0: ", len(parsed_data[parsed_data["Track2_TRUEID"] == 0]))

