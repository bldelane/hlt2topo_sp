"""Ganga options to launch the DaVinci Tupling script on the output of the HLT2 filtered candidates

This must run in a dedicated Moore build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga submit_davinci.py --hlt2_jid <HLT2 job ID> --event_type <event_type number> [--test] [--dryrun]
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
import argparse
import sys
import pathlib
from typing import List, Union

parser = argparse.ArgumentParser(description="Decay identifier")
parser.add_argument("-t", "--test", help="Run on local backend", action="store_true")
parser.add_argument("-i", "--hlt2_jid", help="Job ID whose hlt2-filtered .dst files must be taken as input",required=True)
parser.add_argument("-e", "--event_type", help="What MC sample is being processed", type=int, required=True)
parser.add_argument("-p", "--polarity", help="MagUp or MagDown", type=str, required=True)
parser.add_argument("-d", "--dryrun", help="If True, do NOT submit()", action="store_true")
opts = parser.parse_args()

gridProxy.renew()
test = False

if test:
    name = "testjob"

_name = f"{opts.hlt2_jid}_{opts.event_type}_tupling"

# configure jobs app
j = Job()
j.name = _name
myApp = GaudiExec()
myApp.directory = f"{pathlib.Path().resolve().parents[1]}/DaVinciDev"  
print(f"{pathlib.Path().resolve().parents[1]}/DaVinciDev")

j.application = myApp
j.application.platform = "x86_64_v3-el9-gcc13+detdesc-opt+g"


j.application.options = [LocalFile(f"funtuple_options_{opts.polarity}.py")]

# define the Dataset
ds = LHCbDataset()

for _lfn in jobs(opts.hlt2_jid).backend.getOutputDataLFNs():
  ds.append(DiracFile(lfn=_lfn))

lfns = []
submission_list = [] # empty list to hold unique elements from the list
duplicate_list = [] # empty list to hold the duplicate elements from the list
for diracfile in ds:
    lfns.append(diracfile.lfn)

# Remove any duplicates from the submission list to avoid splitting errors
for i in lfns:
    if i not in submission_list:
        submission_list.append(i)
    else:
        duplicate_list.append(i)

# As the LFNs are not remote paths, you need to add LFN: to avoid more splitting issues 
new_list = []
for i in submission_list: 
    new_list.append("LFN:"+i)

# Make the list a lhcbdataset
submission_data = LHCbDataset(new_list)


tck_path = "LFN:/lhcb/user/n/nschulte/"

j.inputdata = submission_data
j.outputfiles = [DiracFile(f"davinci_ntuple.root")]
j.inputfiles = [f"{tck_path}hlt2_tck.tck.json", f"options_{opts.polarity}.yaml"]

if test:
    j.backend = Local()
else:
    j.backend = Dirac()
    j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])'
    j.splitter = SplitByFiles(filesPerJob=1, ignoremissing=True, bulksubmit=False)

j.submit()
