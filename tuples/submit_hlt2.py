"""Ganga options to assemble the HLT2 beauty candidates using HLT1-filtered simulated samples.

This must run in a dedicated Moore build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga submit_hlt2.py --event_type <event_type_number> -- polarity <polarity> [--sjobs <n (int)>] [--test]
"""
import argparse
import pathlib
from typing import List, Union
import pickle
import os

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

parser = argparse.ArgumentParser(description="Decay identifier")
parser.add_argument("-t", "--test", help="Run on local backend", action="store_true")
parser.add_argument("-sj", "--sjobs", help="Number of subjobs", type=int, default=5)
parser.add_argument("-e", "--event_type", help="Event type number whose hlt1-filtered .dst files must be taken as input", required=True)
parser.add_argument("-p", "--polarity", help="MagUp or MagDown", type=str, required=True)
opts = parser.parse_args()

# prerequisite
gridProxy.renew()

# job label
_name = f"{opts.event_type}_to_HLT2cands_dst"

# configure jobs app
j = Job()
myApp = GaudiExec()
myApp.directory = f"{pathlib.Path().resolve().parents[1]}/MooreDev"  # relative path; run in /ganga/ folder
j.application = myApp
j.application.platform = "x86_64_v3-el9-gcc13+detdesc-opt+g"

# generate the dataset inheriting from hlt1-filtered .dst
input_job = opts.event_type
_name += f"__{(input_job)}__"  # bookeeping for sanity checks; dundr to annotate origin job
j.name = _name


# I/O

j.application.readInputData(f"lfn_paths/{opts.polarity}/{opts.event_type}.py")
j.application.options = [f"HLT2_options_{opts.polarity}.py"]
j.outputfiles = [DiracFile("*.dst"), LocalFile("*.json")]

# submit config
if opts.test:
    _name += "_testjob"
    j.backend = Local()
else:
    j.backend = Dirac()
    j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])'
    if opts.sjobs > 1:
        j.splitter = SplitByFiles(
            filesPerJob=opts.sjobs, ignoremissing=True, bulksubmit=False
        )
    else:
        j.splitter = SplitByFiles(filesPerJob=1, ignoremissing=True, bulksubmit=False)

j.submit()
