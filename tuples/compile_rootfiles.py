"""Per-job, retrieve the ROOT file and move them EOS

This must run in a dedicated Moore build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga compile_rootfiles.py --davinci_jid <List of DaVinci jids> --channel <channel name> --eos_path <path to eos directory> 
"""
__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import os, sys, glob
from typing import Union, List, Dict
import subprocess
import pprint
from pathlib import Path
import argparse

parser = argparse.ArgumentParser(description="Arguementparser for getting all the root files and hadding them together")
parser.add_argument("-i", "--davinci_jid", help="Job IDs whose davinci .root files must be taken as input", nargs='+',required=True)
parser.add_argument("-c", "--channel", help="What MC sample is being processed", type=str, required=True)
parser.add_argument("-e", "--eos_path", help="Where on eos should the tuples be stored", type=str, required=False, default="/eos/lhcb/user/n/nschulte/public/2024_tuples/")
opts = parser.parse_args()

# prerequisite
gridProxy.renew()

# First get all the subjob root files and put them on eos
for job in opts.davinci_jid:
    Path("tmp").mkdir(parents=True, exist_ok=True)
    lfns = jobs(job).backend.getOutputDataAccessURLs()
    for lfn in range(len(lfns)):
        Path(f"{opts.eos_path}/{opts.channel}/{job}/{lfn}").mkdir(parents=True, exist_ok=True)
        cmd = "lb-dirac dirac-dms-get-file %s -D %s%s/%s/%s/" % (
            lfns[lfn],
            opts.eos_path,
            opts.channel,
            job,
            lfn,
        )
        print("Executing %s" % cmd)
        os.system(cmd)
        print("Done \n")


