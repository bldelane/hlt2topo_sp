"""Container for the paths obtained from the Dirac paths
for the Upgrade MC samples
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from configparser import NoOptionError
from typing import Dict
import pprint

# container
bkk_paths = {
    # inclusive 2- & 3-body b topo
    # ----------------------------
    "3MuNu": {
        "MagUp": "evt+std://MC/Upgrade/12513020/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12513020/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "D0Ds": {
        "MagUp": "evt+std://MC/Upgrade/12195047/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12195047/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "D0KsPi": {
        "MagUp": "evt+std://MC/Upgrade/12165181/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "DDKPi": {
        "MagUp": "evt+std://MC/Upgrade/11198098/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "DPiPiPi": {
        "MagUp": "evt+std://MC/Upgrade/12265002/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "DsMuNu": {
        "MagUp": "evt+std://MC/Upgrade/13774000/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13774000/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "DsPi": {
        "MagUp": "",
        "MagDown": "evt+std://MC/Upgrade/13164044/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/DIGI",
    },
    "DstD0": {
        "MagUp": "evt+std://MC/Upgrade/12195032/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12195032/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "DstTauNu": {
        "MagUp": "evt+std://MC/Upgrade/11160001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11160001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "JpsiPhiKs": {
        "MagUp": "evt+std://MC/Upgrade/11146114/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11146114/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "K6Mu": {
        "MagUp": "evt+std://MC/Upgrade/12117015/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12117015/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "KPiPi": {
        "MagUp": "evt+std://MC/Upgrade/12103025/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103025/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "KstGamma": {
        "MagUp": "evt+std://MC/Upgrade/11102202/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11102202/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lc2625MuNu": {
        "MagUp": "evt+std://MC/Upgrade/15576011/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15576011/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "LcPPi": {
        "MagUp": "evt+std://MC/Upgrade/12165094/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12165094/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "PhiKst": {
        "MagUp": "evt+std://MC/Upgrade/11104020/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11104020/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "PiMuNu": {
        "MagUp": "evt+std://MC/Upgrade/11512011/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11512011/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "JPsiK_mumu": {
        "MagUp": "evt+std://MC/Upgrade/12143001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12143001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    # muon 2- & 3-body b topo: include tau, mu, Vcb, Vub
    # --------------------------------------------------
    "Lb_LcMuNu": {
        "MagUp": "evt+std://MC/Upgrade/15874041/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15874041/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_PMuNu": {
        "MagUp": "evt+std://MC/Upgrade/15512014/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15512014/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bc_JpsiTauNu": {
        "MagUp": "evt+std://MC/Upgrade/14643048/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/14643048/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
    },
    "Bc_JpsiMuNu": {
        "MagUp": "evt+std://MC/Upgrade/14543010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/14543010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
    },
    "Bs_KMuNu": {
        "MagUp": "evt+std://MC/Upgrade/13512010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13512010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bs_KTauNu": {
        "MagUp": "evt+std://MC/Upgrade/13512030/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13512030/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_D0XMuNu": {
        "MagUp": "evt+std://MC/Upgrade/12873441/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12873441/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_D0TauNu": {
        "MagUp": "evt+std://MC/Upgrade/12573001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12573001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_Dst0TauNu": {
        "MagUp": "evt+std://MC/Upgrade/12562410/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12562410/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_PPTauNu": {
        "MagUp": "evt+std://MC/Upgrade/12513061/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12513061/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_PPMuNu": {
        "MagUp": "evt+std://MC/Upgrade/12513051/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12513051/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    # new modes
    "Bd_Kstee": {
        "MagUp": "evt+std://MC/Upgrade/11124001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11124001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_Kpi": {
        "MagUp": "evt+std://MC/Upgrade/11102001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11102001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_Lambda1520Jpsi": {
        "MagUp": "evt+std://MC/Upgrade/15144040/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15144040/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_Kmumu": {
        "MagUp": "evt+std://MC/Upgrade/12113002/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12113002/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_pipipi": {
        "MagUp": "evt+std://MC/Upgrade/11102405/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11102405/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_phiKst0": {
        "MagUp": "evt+std://MC/Upgrade/11104020/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11104020/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_ppbarmumu": {
        "MagUp": "evt+std://MC/Upgrade/11114018/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11114018/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_JpsiKS": {
        "MagUp": "evt+std://MC/Upgrade/11144103/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11144103/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_JpsiphiKs": {
        "MagUp": "evt+std://MC/Upgrade/11146114/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11146114/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_psi2SKst": {
        "MagUp": "evt+std://MC/Upgrade/11154011/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11154011/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_Kst0rho0": {
        "MagUp": "evt+std://MC/Upgrade/11104041/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11104041/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_Lambdacmu": {
        "MagUp": "evt+std://MC/Upgrade/11274030/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11274030/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_Ktautau": {
        "MagUp": "evt+std://MC/Upgrade/12101010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12101010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_KKK": {
        "MagUp": "evt+std://MC/Upgrade/12103017/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103017/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_pipipi": {
        "MagUp": "evt+std://MC/Upgrade/12103007/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103007/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_KsK": {
        "MagUp": "evt+std://MC/Upgrade/12103121/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103121/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_KSpi": {
        "MagUp": "evt+std://MC/Upgrade/12103101/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103101/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bs_Dspi": {
        "MagUp": "evt+std://MC/Upgrade/13264021/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13264021/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_Lctaunu": {
        "MagUp": "evt+std://MC/Upgrade/15863030/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15863030/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_Dstmunu": {
        "MagUp": "evt+std://MC/Upgrade/11574094/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11574094/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_Kstmumu": {
        "MagUp": "evt+std://MC/Upgrade/11114002/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11114002/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bs_phiphi": {
        "MagUp": "evt+std://MC/Upgrade/13104012/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13104012/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_piKK": {
        "MagUp": "evt+std://MC/Upgrade/12103035/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103035/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_D0Kst": {
        "MagUp": "evt+std://MC/Upgrade/12167181/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "Bd_mumu": {
        "MagUp": "evt+std://MC/Upgrade/11112001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11112001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bs_KK": {
        "MagUp": "evt+std://MC/Upgrade/13102004/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13102004/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_pK": {
        "MagUp": "evt+std://MC/Upgrade/15102001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15102001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_Kmu": {
        "MagUp": "evt+std://MC/Upgrade/15112001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15112001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    # ------------------------------------
    "Bs_Jpsiphi": {
        "MagUp": "evt+std://MC/Upgrade/13144011/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13144011/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_Lcpi": {
        "MagUp": "evt+std://MC/Upgrade/15364010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15364010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Omegac0_L0KS0": {
        "MagUp": "evt+std://MC/Upgrade/26104188/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/26104188/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bs_KKgamma": {
        "MagUp": "evt+std://MC/Upgrade/13102232/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13102232/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bd_Kpigamma": {
        "MagUp": "evt+std://MC/Upgrade/11102262/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11102262/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_JpsiPi": {
        "MagUp": "evt+std://MC/Upgrade/12143010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12143010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_JpsiK": {
        "MagUp": "evt+std://MC/Upgrade/12143001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12143001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    # minimum bias (and inclusive beauty)
    # -----------------------------------
    "minbias": {
        "MagUp": "evt+std://MC/Upgrade/30000000/Beam7000GeV-Upgrade-MagUp-Nu7.6-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/30000000/Beam7000GeV-Upgrade-MagDown-Nu7.6-Pythia8/Sim10aU1/XDIGI",
    },

}

if __name__ == "__main__":
    # print container
    print("BKK path container:")
    pprint.pprint(bkk_paths)

