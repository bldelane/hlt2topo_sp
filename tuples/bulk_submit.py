import subprocess
#11102005,11102401, 11104202,11123000,11144103,11196009,11584030,12143001,12197003,12513002,13104012,13112001,13144011,13264021,13774000,15104132,15874041,30000000,11102202,11104124,11114002,11124001,11164063,11574020,12103009,12165042,12313001,12562000,13104190,13122001,13160001,13512010,15104115,15512011,16103432
samples = [30000000]
polarities = ['MagUp', 'MagDown']  # List of polarities

for sample in samples:
    print(f"PROCESSING {sample} NOW")
    for polarity in polarities:
        print(f"PROCESSING {polarity} NOW")
        try:
            command = f"ganga submit_hlt2.py --polarity {polarity} --event_type {sample} --sj 1"
            subprocess.run(command, shell=True, check=True)
            print((f"processed sample {sample} with polarity {polarity}"))
        except subprocess.CalledProcessError:
            print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            print(f"Job launch failed for sample {sample} with polarity {polarity}")
            print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print(f"{sample} NOW COMPLETED")
    print(f"-------------------------------------------------------------------------------------------------------------------------------------------")
