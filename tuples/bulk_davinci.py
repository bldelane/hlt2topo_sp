import subprocess
import numpy as np

MagUp = {
# "11102005" : [0],
# "11102401" : [2],
# "11104202" : [128],
# "11123000" : [8],
# "11144103" : [10],
# "11196009" : [12],
# "11584030" : [14],
# "12143001" : [16],
# "12197003" : [72],
# "12513002" : [74],
# "13104012" : [76],
# "13112001" : [78],
# "13144011" : [80],
# "13264021" : [82],
# "13774000" : [84],
# "15104132" : [86],
# "15874041" : [88],
"30000000" : [150], #has yet to finish
# "11102202" : [94],
# "11104124" : [96],
# "11114002" : [98],
# "11124001" : [100],
# "11164063" : [102],
# "11574020" : [104],
# "12103009" : [106],
# "12165042" : [108],
# "12313001" : [110],
# "12562000" : [112],
# "13104190" : [114],
# "13122001" : [116],
# "13160001" : [118],
# "13512010" : [120],
# "15104115" : [122],
# "15512011" : [124],
# "16103432" : [126],
}

MagDown = {
# "11102005" : [1],
# "11102401" : [3],
# "11104202" : [129],
# "11123000" : [9],
# "11144103" : [11],
# "11196009" : [13],
# "11584030" : [15],
# "12197003" : [73],
# "13104012" : [77],
# "13112001" : [79],
# "13144011" : [81],
# "13264021" : [83],
# "13774000" : [85],
# "15104132" : [87],
# "15874041" : [89],
"30000000" : [151], #has yet to finish
# "11102202" : [95],
# "11104124" : [97],
# "11114002" : [99],
# "11124001" : [101],
# "11164063" : [103],
# "11574020" : [105],
# "12103009" : [107],
# "12165042" : [109],
# "12313001" : [110],
# "12562000" : [113],
# "13104190" : [115],
# "13122001" : [117],
# "15104115" : [123],
# "16103432" : [127],
}
for channel, ids in MagUp.items():
    for job_id in ids:
        command = f"ganga submit_davinci.py --event_type {channel} --hlt2_jid {job_id} --polarity MagUp"
        try:
            subprocess.run(command, shell=True, check=True)
            print(f"Launched job for DaVinci Job with HLT2 Ganga ID {job_id} with polarity MagUp and eventtype {channel}")
        except subprocess.CalledProcessError:
            print(f"Job launch failed for DaVinci Job with HLT2 Ganga ID {job_id} with polarity MagUp and eventtype {channel}")

for channel, ids in MagDown.items():
    for job_id in ids:
        command = f"ganga submit_davinci.py --event_type {channel} --hlt2_jid {job_id} --polarity MagDown"
        try:
            subprocess.run(command, shell=True, check=True)
            print(f"Launched job for DaVinci Job with HLT2 Ganga ID {job_id} with polarity MagDown and eventtype {channel}")
        except subprocess.CalledProcessError:
            print(f"Job launch failed for DaVinci Job with HLT2 Ganga ID {job_id} with polarity MagDown and eventtype {channel}")
