"""Per-job, retrieve the ROOT file, hadd and move to EOS

This must run in a dedicated Moore build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga hadd_rootfiles.py --channel <channel name> --eos_path <path to eos directory> 
$ ganga compile_rootfiles.py --channel <channel name> -i <HLT2 Job IDs> && python hadd_rootfiles.py --channel <channel name>
"""
__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import os, sys, glob
from typing import Union, List, Dict
import subprocess
import pprint
from pathlib import Path
import argparse

parser = argparse.ArgumentParser(description="Arguementparser for getting all the root files and hadding them together")
parser.add_argument("-c", "--channel", help="What MC sample is being processed", type=str, required=True)
parser.add_argument("-e", "--eos_path", help="Where on eos should the tuples be stored", type=str, required=False, default="/eos/lhcb/user/n/nschulte/public/2024_tuples/")
opts = parser.parse_args()

# Now hadd them together 
hadd_cmd = "hadd %s%s/%s.root $(find %s%s/ -name 'davinci_ntuple.root')" % (
    opts.eos_path,
    opts.channel,
    opts.channel,
    opts.eos_path,
    opts.channel,
)
print("Executing %s" % hadd_cmd)
os.system(hadd_cmd)
print("Done \n")
