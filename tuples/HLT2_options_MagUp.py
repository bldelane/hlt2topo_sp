###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Script to run the Hlt1 filtered dsts and produce DSTs with
the two- and three-body candidates.

This follows the follwing tutorial and scripts:
https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/running_over_mc.html
Bind taken from this snippet from Ross:
https://gitlab.cern.ch/-/snippets/2694

To keep these settings in line with HLTEfficiencyChecker
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from Moore import options, run_moore
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from Hlt2Conf.lines.topological_b import (
    make_unfiltered_topo_twobody,
    make_unfiltered_topo_threebody,
    line_prefilters,
)
from RecoConf.global_tools import stateProvider_with_simplified_geom,trackMasterExtrapolator_with_simplified_geom
from RecoConf.ttrack_selections_reco import make_ttrack_reco
from RecoConf.reconstruction_objects import upfront_reconstruction
from PyConf import configurable
from typing import List, Union
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.calorimeter_reconstruction import make_digits
from RecoConf.hlt2_global_reco import (
    reconstruction as hlt2_reconstruction,
    make_light_reco_pr_kf_without_UT,
)


make_digits.global_bind(calo_raw_bank=True)

all_lines = {}


@register_line_builder(all_lines)
@configurable
def hlt1filtered_twobody_line(
    name: str = "Hlt2TwoBodyTopo_Hlt1Filtered",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Two-body topo line with Hlt1*Line MVA filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered two-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_twobody(twobody_muon_topo=False)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,  
        #hlt1_filter_code=["Hlt1.*Decision"],  # filter on HLT1 decisions
    )


@register_line_builder(all_lines)
@configurable
def hlt1filtered_threebody_line(
    name: str = "Hlt2ThreeBodyTopo_Hlt1Filtered",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Three-body topo line with Hlt1*Line filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered three-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_threebody(treebody_muon_topo=False)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco, 
        #hlt1_filter_code=["Hlt1.*Decision"],  # filter on HLT1 decisions
    )


@register_line_builder(all_lines)
@configurable
def hlt1filtered_muon_twobody_line(
    name: str = "Hlt2TwoBodyMuonTopo_Hlt1Filtered",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Two-body topo line with Hlt1*Line MVA filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered two-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_twobody(twobody_muon_topo=True)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,  
    )


@register_line_builder(all_lines)
@configurable
def hlt1filtered_muon_threebody_line(
    name: str = "Hlt2ThreeBodyMuonTopo_Hlt1Filtered",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Three-body topo line with Hlt1*Line filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered three-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_threebody(treebody_muon_topo=True)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco, 
    )

def make_lines() -> List[Hlt2Line]:
    """Make the lines."""
    return [hlt1filtered_twobody_line(), hlt1filtered_threebody_line(), hlt1filtered_muon_twobody_line(),hlt1filtered_muon_threebody_line()]
    
# config
options.data_type = "Upgrade"
options.simulation = True
options.input_type = "ROOT"

options.conddb_tag = "sim-20231017-vc-mu100" # Or mu for "Mag Up"
options.dddb_tag = "dddb-20231017"
options.input_raw_format = 0.5
options.evt_max = -1

# options output
options.output_file = "HLT2candidates_HLT1filtered.dst"
options.output_type = "ROOT"
options.scheduler_legacy_mode = False
options.output_manifest_file = "HLT2_HLT1filteredTCK.tck.json"

with reconstruction.bind(from_file=False), make_light_reco_pr_kf_without_UT.bind(
    skipRich=False, skipCalo=False, skipMuon=False
), make_ttrack_reco.bind(skipCalo=False), hlt2_reconstruction.bind(
    make_reconstruction=make_light_reco_pr_kf_without_UT #keep binds in line with the EfficiencyChecker & Bandwidth devision binds
):
    config = run_moore(
        options,
        make_lines,
        public_tools=[
            stateProvider_with_simplified_geom(), trackMasterExtrapolator_with_simplified_geom()
        ] 
    )
