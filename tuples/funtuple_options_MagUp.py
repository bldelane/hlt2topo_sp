###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
To run:
./run gaudirun.py ../hlt2topo_sp/tuples/funtuple_options.py
"""

__author__ = "Nicole Skidmore, Nicole Schulte"

import Functors as F
import FunTuple.functorcollections as FC
from FunTuple import FunctorCollection, FunTuple_Particles as Funtuple
from PyConf.reading import get_particles, get_rec_summary, get_odin, get_pvs, get_pvs_v1
from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from DaVinciMCTools import MCTruthAndBkgCat
from PyConf.Algorithms import PrintDecayTree
from DaVinciMCTools import MCReconstructible
from PyConf.reading import get_mc_track_info


###############################################

def nbody_alg(decay:str, options: Options):

    # beauty hadron variables
    v2_pvs = get_pvs()
    B_variables = FunctorCollection({
    "ID": F.PARTICLE_ID,
    "PT": F.PT,
    "ETA": F.ETA,
    "P": F.P,
    "SUMPT": F.SUM(F.PT),
    "M": F.MASS,
    "BPVDIRA": F.BPVDIRA(v2_pvs),
    "ENDVERTEX_CHI2DOF": F.CHI2DOF @ F.ENDVERTEX,
    "CHI2DOF": F.CHI2DOF,
    "BPVCHI2DOF": F.CHI2DOF @ F.BPV(v2_pvs),
    "CHI2": F.CHI2,
    "ENDVERTEX_CHI2": F.CHI2 @ F.ENDVERTEX,
    "BPVIPCHI2": F.BPVIPCHI2(v2_pvs),
    "BPVIP": F.BPVIP(v2_pvs),
    "IPCHI2_OWNPV": F.MINIPCHI2(v2_pvs),
    "MINIP": F.MINIP(v2_pvs),
    "FDCHI2_OWNPV": F.BPVFDCHI2(v2_pvs),
    "BPVLTIME": F.BPVLTIME(v2_pvs),
    "BPVFD": F.BPVFD(v2_pvs),
    "PX": F.PX,
    "PY": F.PY,
    "PZ": F.PZ,
    "PE": F.ENERGY,
    "OWNPV_X": F.BPVX(v2_pvs),
    "OWNPV_Y": F.BPVY(v2_pvs),
    "OWNPV_Z": F.BPVZ(v2_pvs),
    "ENDVERTEX_X": F.END_VX,
    "ENDVERTEX_Y": F.END_VY,
    "ENDVERTEX_Z": F.END_VZ,
    "MCORR": F.BPVCORRM(v2_pvs), 
    "SDOCAMAX": F.MAXSDOCA,
    "DOCAMAX": F.MAXDOCA,
    "MAXDOCACHI2": F.MAXDOCACHI2,
    "MAXSDOCACHI2": F.MAXSDOCACHI2,
    "GhostProb": F.GHOSTPROB,
    })

    #Final state variables
    fs_variables = FunctorCollection({
    "ID": F.PARTICLE_ID,
    "PT": F.PT,
    "ETA": F.ETA,
    "P": F.P,
    "M": F.MASS,
    "CHI2DOF": F.CHI2DOF,
    "TR_CHI2": F.CHI2,
    "IPCHI2_OWNPV": F.MINIPCHI2(v2_pvs),
    "FDCHI2_OWNPV": F.BPVIPCHI2(v2_pvs),
    "PX": F.PX,
    "PY": F.PY,
    "PZ": F.PZ,
    "PE": F.ENERGY,
    "PIDK": F.PID_K,
    "PIDp": F.PID_P,
    "PIDe": F.PID_E,
    "PIDmu": F.PID_MU,
    "isMuon": F.ISMUON,
    "TRACK_GhostProb": F.GHOSTPROB,
    "ProbNN_Ghost": F.PROBNN_GHOST,
    "ProbNNp": F.PROBNN_P,
    "TrackNDof": F.VALUE_OR(-1) @ F.NDOF @ F.TRACK,
    "TrackCHI2DOF": F.CHI2DOF @ F.TRACK,
    "TrackCHI2": F.CHI2 @ F.TRACK,
    })

    mode_info = {
    "TwoBody": {"fields": {'TwoBody' : '[B0 -> pi+ pi-]CC',
        'Track1' : '[B0 -> ^pi+ pi-]CC',
	  	'Track2' : '[B0 -> pi+ ^pi-]CC',},
	    "line": "Hlt2TwoBodyTopo_Hlt1Filtered",
	    "Hlt2_decisions": ['Hlt2TwoBodyTopo_Hlt1FilteredDecision'],
        "variables": {"TwoBody": B_variables, "Track1": fs_variables, "Track2": fs_variables}
             },
    "ThreeBody": {"fields": {'ThreeBody' : '[B_s0 -> (B0 -> pi+ pi-) [pi+]CC]CC',
        'TwoBody' : '[B_s0 -> ^(B0 -> pi+ pi-) [pi+]CC]CC',
        'Track1' : '[B_s0 -> (B0 -> ^pi+ pi-) [pi+]CC]CC',
	  	'Track2' : '[B_s0 -> (B0 -> pi+ ^pi-) [pi+]CC]CC',
        'TrackB' : '[B_s0 -> (B0 -> pi+ pi-) ^[pi+]CC]CC',},
	    "line": "Hlt2ThreeBodyTopo_Hlt1Filtered",
	    "Hlt2_decisions": ['Hlt2ThreeBodyTopo_Hlt1FilteredDecision'],
        "variables": {"ThreeBody": B_variables, "TwoBody": B_variables, "Track1": fs_variables,
        "Track2": fs_variables, "TrackB": fs_variables}
             },
    "SingleMuon_TwoBody": {"fields": {'TwoBody' : '[B0 -> pi+ pi-]CC',
        'Track1' : '[B0 -> ^pi+ pi-]CC',
	  	'Track2' : '[B0 -> pi+ ^pi-]CC',},
	    "line": "Hlt2TwoBodyMuonTopo_Hlt1Filtered",
	    "Hlt2_decisions": ['Hlt2TwoBodyMuonTopo_Hlt1FilteredDecision'],
        "variables": {"TwoBody": B_variables, "Track1": fs_variables, "Track2": fs_variables}
             },
    "SingleMuon_ThreeBody": {"fields": {'ThreeBody' : '[B_s0 -> (B0 -> pi+ pi-) [pi+]CC]CC',
        'TwoBody' : '[B_s0 -> ^(B0 -> pi+ pi-) [pi+]CC]CC',
        'Track1' : '[B_s0 -> (B0 -> ^pi+ pi-) [pi+]CC]CC',
	  	'Track2' : '[B_s0 -> (B0 -> pi+ ^pi-) [pi+]CC]CC',
        'TrackB' : '[B_s0 -> (B0 -> pi+ pi-) ^[pi+]CC]CC',},
	    "line": "Hlt2ThreeBodyMuonTopo_Hlt1Filtered",
	    "Hlt2_decisions": ['Hlt2ThreeBodyMuonTopo_Hlt1FilteredDecision'],
        "variables": {"ThreeBody": B_variables, "TwoBody": B_variables, "Track1": fs_variables,
        "Track2": fs_variables, "TrackB": fs_variables}
             },
              }


    fields = mode_info[decay]["fields"]
    line = mode_info[decay]["line"]
    Hlt2_decisions = mode_info[decay]["Hlt2_decisions"]
    my_filter = create_lines_filter(f"Hlt2Line_Filter_{decay}",
                                    lines=[f"{line}"])
    data = get_particles(f"/Event/HLT2/{line}/Particles")
    variables = mode_info[decay]["variables"]

    mctruth = MCTruthAndBkgCat(input_particles=data, name=f"MCTruthAndBkgCat_{decay}")
    reconstructible_alg = MCReconstructible(get_mc_track_info())

    mchierarchy_info = FC.MCHierarchy(mctruth_alg=mctruth)
    mcpromtdecay = FC.MCPromptDecay(mctruth_alg=mctruth)
    mcvertexinfo = FC.MCVertexInfo(mctruth_alg=mctruth)

    # Do the truthmatching up to 15 generations, like Abhijit suggested (Code provided by him https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions/-/blob/0ac016864dba21c90e00f1a60fb9bc1a7d5ad88d/SL_run3_isolation_Dzl/run_Dzl_dv.py#L161-171)
    truthmatching_vars = FunctorCollection()
    MCMOTHER_ID  = lambda n: F.VALUE_OR(0)  @ mctruth(F.MC_MOTHER(n, F.PARTICLE_ID))
    MCMOTHER_KEY = lambda n: F.VALUE_OR(-1) @ mctruth(F.MC_MOTHER(n, F.OBJECT_KEY ))
    truthmatching_vars["MCID"] = F.VALUE_OR(0) @ mctruth(F.PARTICLE_ID)
    truthmatching_vars["MCKEY"] = F.VALUE_OR(0) @ mctruth(F.OBJECT_KEY)
    truthmatching_vars["MC_MOTHER_ID"] = MCMOTHER_ID(1)
    truthmatching_vars["MC_MOTHER_KEY"] = MCMOTHER_KEY(1)
    for i in range(2,15):
      prefix = "MC_GD_MOTHER_{}".format(i)
      truthmatching_vars[f"{prefix}_ID"]  = MCMOTHER_ID(i)
      truthmatching_vars[f"{prefix}_KEY"] = MCMOTHER_KEY(i)

    trueid_bkgcat_info = {
        "TRUEPT":
        mctruth(F.PT),
        "TRUEPX":
        mctruth(F.PX),
        "TRUEPY":
        mctruth(F.PY),
        "TRUEPZ":
        mctruth(F.PZ),
        "TRUEENERGY":
        mctruth(F.ENERGY),
        "MC_RECONSTRUCTIBLE": 
        mctruth(reconstructible_alg.Reconstructible),
        "MC_LIFETIME": 
        mctruth(F.MC_LIFETIME),
        "TRUEP":
        mctruth(F.P),
        "TRUEFOURMOMENTUM":
        mctruth(F.FOURMOMENTUM),
        "BKGCAT":
        F.BKGCAT(Relations=mctruth.BkgCatTable),
        "TrackType":
        F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKTYPE @ F.TRACK,
    }
    for field in variables.keys():
        variables[field] += FunctorCollection(trueid_bkgcat_info)
        variables[field] += FunctorCollection(mchierarchy_info)
        variables[field] += FunctorCollection(mcpromtdecay)
        variables[field] += FunctorCollection(mcvertexinfo)
        variables[field] += FunctorCollection(truthmatching_vars)
    
    odin = get_odin()
    rec_summary = get_rec_summary()
    #define event level variables
    evt_variables  = FunctorCollection({
        "runNumber": F.RUNNUMBER(odin),
        "eventNumber": F.EVENTNUMBER(odin),
        "nPVs": F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nPVs"),
        "nTracks": F.VALUE_OR(-1) @ F.RECSUMMARY_INFO(rec_summary, "nTracks"),
    })

    evt_variables+=FC.SelectionInfo(selection_type="Hlt2", trigger_lines=Hlt2_decisions)

    #define FunTuple instance
    my_tuple = Funtuple(
        name=decay,
        tuple_name="DecayTree",
        fields=fields,
        variables=variables,
        event_variables=evt_variables,
        inputs=data)

    user_algorithms = [my_filter, my_tuple]

    return {f"{decay}_Algs": user_algorithms}

def print_decay_tree(options: Options):
    algorithms = {}
    for decay in ["TwoBody", "ThreeBody","SingleMuon_TwoBody","SingleMuon_ThreeBody"]:
        user_algorithms = nbody_alg(decay, options)    
        algorithms.update(user_algorithms)

    return make_config(options, algorithms)

import sys
if sys.argv[0].endswith("gaudirun.py"):
    import yaml
    from DaVinci import Options
    with open("options_MagUp.yaml", "r") as f:
        options = Options(**yaml.safe_load(f))
    with options.apply_binds():  # required as gaudirun.py does not apply binds!
        print_decay_tree(options)
